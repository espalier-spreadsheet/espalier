# Coding guidelines

Matt kindly requests that all code on the branches officially maintained by the Object Spreadsheets team (currently `master`) adhere to the following guidelines.  Either code should meet the guidelines before being pushed or any concerns should be raised in a pull request.  The guidelines cover the most common issues Matt has seen so far and do not take the place of common sense.  They are open to negotiation and will be revised with experience.

(For now, all of the guidelines are from me and I'm taking responsibility for the decision to impose them.  Of course, others may request guidelines as well, in which case this file will be revised accordingly. ~ Matt)

## No TypeScript errors

No TypeScript errors should appear when you run `tests/dev-tools/check-code` (or equivalent) as described in the next section or when you run `meteor`.  These cases relate to the all-in-one project defined by `tsconfig.json` and to the Meteor unibuilds; see the "TypeScript" section of [DEVELOPMENT.md](DEVELOPMENT.md) for more information.

## Mechanics

Our TypeScript code generally follows several of the tslint rules; see [tslint.json](tslint.json) for details and rationale.  To save time in the edit-build-run cycle, the Meteor build _does not_ check the lint rules.  Run `tests/dev-tools/check-code` (or press Ctrl-Shift-B in Visual Studio Code) to check your code before pushing it or sending it for review; no output means no problems were found.  In Visual Studio Code, the [tslint extension](https://marketplace.visualstudio.com/items?itemName=eg2.tslint) shows you lint warnings for the currently open files as you type, and it can fix some types of warnings automatically if you place the cursor on the affected code and click the lightbulb icon.

If your code merits an exception to our rationale for one of the lint rules, suppress the warning(s) with [tslint special comments](https://palantir.github.io/tslint/usage/rule-flags/), specifying the individual rule or rules to suppress.  Please include a comment briefly stating the reason for the exception.

In general, avoid glaring deviations from the prevailing formatting conventions.  (This primarily applies to spacing; we don't follow specific rules for line breaking other than trying to make the code easy to understand.)  The linter catches many deviations, but not all.  A few specific points:

* Put a space between the closing `}` of an `if` block and the `else` keyword.  (This is the only spacing rule that has come up so far that, oddly, [isn't supported by tslint](https://github.com/palantir/tslint/issues/746).  tslint does check the space after the `else` keyword, so you would be likely to notice the inconsistency.)
* A comment should have a space after the opening `//` or `/*` symbol.  If the text is longer than a few words and starts with a letter that isn't part of the name of something, that letter should be uppercase.  When code is commented out by prepending `//`, adding a space along with the `//` is optional.  (Matt's preference has been not to add a space, but Visual Studio Code's "Toggle Line Comment" command adds one, and it doesn't seem important.)

## Prioritize understandability

Our end goals for code style are understandability and maintainability; all other properties (conciseness, minimization of duplication, etc.) should be pursued only inasmuch as they aid understandability and maintability.

When a small piece of code is used in only one or two places, put it in a separate function or class only if that makes the caller(s) easier to understand.  One factor is whether you can give the helper a name good enough to remind everyone (not just yourself!) of the essential aspects of its behavior.

## Documentation

We don't attempt to document the entire codebase to the point that someone new to it could pick it up easily; that would be an unreasonable cost for a research project.  The most efficient way to find out how something works is often to ask the original author.  However, as a backstop, the code should be clear enough that a diligent reader could figure out the intended behavior and how the code achieves it (modulo any documented limitations), relying on good naming, navigation among definitions and references, occasional clarifying comments, and the documentation of the third-party libraries we use.  It's sometimes a judgment call whether information is worth including or the reader can be assumed to figure it out.

If you do something tricky, explain how it works, and mention any more obvious approaches that you tried that failed and why they failed (if you know).  This information will help us find the best approach to keep the code working if we have to modify it in the future.

Information that helps readers understand how the current code works belongs in comments in the code, not solely in commit messages.  On the other hand, the reasons why previous designs were abandoned may be stated in either place.

## Manage complexity

Often it isn't worth writing code that behaves sensibly for 100% of the possible calls to the interfaces you define.  Document the major limitations of your code so that others know what is and is not expected to work and can endeavor to keep the former working.  Avoid introducing an interface that is gratuitously general compared to the cases you currently need and are implementing.  Finally, be sure you have consensus that functionality is really desired before introducing significant complexity to implement it.

## Type annotations

Make a reasonable attempt to statically type your code.  Feel free to use `fixmeAny` for type annotations where it's not straightforward to write an appropriate type or you're using an external library element for which we don't have a type declaration; we can search for `fixmeAny` later to find potential places to improve the typing.  Use `any` and suppress the resulting lint warning only if it seems clear that there is no change that would avoid the gap in static typing that we would ever realistically want to make (this can be a judgment call), and state the rationale briefly in a comment as for any other suppression.  When the same rationale applies to many occurrences of `any`, define an alias `type any_xx = any;` in an appropriate library file and state the rationale there.  To maintain readability, `xx` should be a short abbreviation; readers can always "jump to definition" for more information.
