# Development notes

This file covers some things you may need to know in order to work on Object Spreadsheets.  Many topics are described in greater detail in the files that implement them.

Matt and Shachar have additional information on many of these topics that has not been written up.  Feel free to ask us before you spend a significant amount of time researching something yourself.

## Coding guidelines

See [CODING_GUIDELINES.md](CODING_GUIDELINES.md).

## Distributing modified versions of dependencies

Object Spreadsheets depends on modified versions of several other software packages that come from their own "upstream" git repositories.  We need a way to keep track of which revision of each dependency should be used with a given revision of the main codebase and distribute the dependencies to users.  As of 2016-11-30, we're using [Braid](http://cristibalan.github.io/braid/), which bundles the dependencies in the main tree, so if you just want to run Object Spreadsheets or view or modify our versions of the dependencies, you don't have to do anything special.  If you want to add or remove dependencies or switch to a different upstream version, you'll need to use Braid; see its documentation.  The upstream repository locations and revisions are stored in the `.braids` file.

Currently, Braid operations that change the working tree automatically commit the changes and require that you commit any previous changes first.  This is [a limitation of the current implementation](https://github.com/cristibalan/braid/issues/36).  Please squash the automatic commits and write a meaningful commit message as you would for any other change.

Before 2016-11-10, we used git submodules, which were a pain for many reasons, including that we had to set up a repository to host our modified version of each dependency.  These repositories remain available under https://bitbucket.org/espalier-spreadsheet/ so that people can check out revisions of Object Spreadsheets from before the migration (you'll have to manually update the submodule URLs, sorry).

## Meteor

Object Spreadsheets is an application in the [Meteor](https://www.meteor.com/) web framework.  We chose Meteor because it worked well for [Sunny](https://github.com/aleksandarmilicevic/sunny.js), a related previous project in our research group, and indeed we've found that it gives us a lot of help with the boring parts of a data-heavy web application.

See the [Meteor API documentation](https://docs.meteor.com/) and additional topics in the [Meteor guide](https://guide.meteor.com/).

Meteor automatically scans the entire project directory for different kinds of files as described in the [Application Structure article](https://guide.meteor.com/structure.html#special-directories), with the exception of a few directories, the documented one being `tests`.  So we use the `tests` directory for anything that Meteor doesn't need to touch and that we want to make sure doesn't trip Meteor up.  It's a misnomer. :/

## IDEs

We primarily use and support [Visual Studio Code](https://code.visualstudio.com/).  Visual Studio Code shows TypeScript errors for the currently open files as you type; lint warnings are shown in the same way if you install the [tslint extension](https://marketplace.visualstudio.com/items?itemName=eg2.tslint).  The build command (Ctrl-Shift-B) is set up to run both the TypeScript compiler and the linter on the whole project and display errors and warnings in the Problems pane.

## Source code layout

We mostly observe the following conventions:

* The code for one area of functionality is split among files `DIR/foo.ts`, `DIR/client/foo.ts`, and/or `DIR/server/foo.ts` depending on whether it is used on the client, the server, or both.  (Files that would be empty are omitted.)
* `src/lib` is for code that isn't inherently specific to Object Spreadsheets.  Note that the special `lib` name also affects the [load order of files](https://guide.meteor.com/structure.html#load-order).  If we need to decouple the two, we can use a name other than `lib` for this convention.

Some important files:

* `src/tablespace.ts`, `src/client/tablespace.ts`, `src/server/tablespace.ts`: Definition of a "tablespace", i.e., the set of Meteor collections that makes up the representation of a single named spreadsheet hosted on the server.  Global aliases for the rest of the code to access the collections of the "current" tablespace.  Data management and transaction management functionality.
* `src/{schema,data}{,-ui}.ts`: Definitions related to the data model.
* `src/client/data.ts`, `src/server/data.ts`: Some simple write operations for sheet data (the interface is different on client and server).
* `src/server/model.ts`: Data-model class that implements all schema write operations and the remaining data write operations.
* `src/client/sheet.ts`, `src/client/action-bar.ts`: The bulk of the code for the spreadsheet UI.
* `src/language`: Support for the formula and procedure language, including the interpreter.
* `private/dump`: Dump files for example applications that are automatically loaded when you visit the sheet of the corresponding name for the first time.  (Not all of these are listed on the server's home page.)
* `src/apps`: Hard-coded application UIs and stored procedures for example applications.

## TypeScript

All of our code is in [TypeScript](http://www.typescriptlang.org/), a language based on JavaScript with optional static typing.  Typing catches a significant fraction of our silly mistakes up front and enables IDEs to cross-reference the code for navigation and (in theory) refactoring.  The code was converted from (untyped) CoffeeScript in February 2016.  As of October 2016, most type annotations have been filled in, but some gaps remain.  Developers should be aware of the risk that "find all references" or refactoring misses something, especially in certain parts of the codebase.

All of our code is in a TypeScript namespace (a.k.a. internal module) `Objsheets`.  Items marked `export` can be used in any file by unqualified names; items not marked `export` can only be used in the same file.  This is the same organization that the TypeScript compiler uses.  Both TypeScript and Meteor are heavily encouraging the use of ECMAScript 6 modules (called external modules by TypeScript), but such a structure would require us to qualify every reference to an item defined in a different file, which seems to Matt to be a big hassle for no practical benefit at the current scale of the codebase.  Unfortunately, since our files are not external modules, they cannot import external modules (e.g., libraries) directly; see `src/lib/server/modules.ts` for the workaround.

Our TypeScript code is compiled using the Meteor build plugin in our modified version of the `hansoft:typescript` Meteor package, which is in the `packages/hansoft_typescript` directory.  This package also determines the versions of tsc and tslint used by all of our other development tools; when Meteor builds the package, it downloads tsc and tslint into `packages/hansoft_typescript/.npm/plugin/compileTypescript/node_modules`, and the other tools reference them from there.  This means you need to run `meteor` once before the affected tools will work.  (And if you manually delete `packages/hansoft_typescript/.npm/plugin/compileTypescript/node_modules`, you need to also delete `.meteor/local/isopacks/hansoft_typescript` before Meteor will redownload tsc and tslint.  This setup is arguably fragile, but it seems most convenient in practice.)

The `hansoft:typescript` build plugin compiles all source files for a given Meteor unibuild (client or server) in a single scope, just like `tsc -p .` would with a `tsconfig.json` file that includes the same source files.  The `barbatus:typescript` plugin is more sophisticated and will likely be officially endorsed by Meteor in the future, but it does not have an option to compile all source files in a single scope (it promotes the use of external modules, for which scope is not an issue), so for now we prefer to pay the cost of maintaining our version of `hansoft:typescript` rather than the cost of clutter in our code.

We have a `tsconfig.json` file that defines a project containing all of our source files, both client and server, and our compiler options; the Meteor build plugin reads the compiler options but ignores the source files.  For most of our work, the all-in-one project is more convenient than dealing with separate client and server projects.  However, if a shared file references something that is only declared in (for example) the client unibuild (hopefully from inside an `if (Meteor.isClient)` block), then Meteor will report a TypeScript error when it does the server unibuild, but tools that use the all-in-one project will not report an error.  Our current policy is to find workarounds to keep the Meteor unibuilds at zero errors.

Our code is checked against type declarations for external libraries.  For a given library, if a `@types` package is available and we do not need to modify it, we simply add the package to `objsheets-npm-deps`; in this case, the type declaration files must be declared individually in `packages/objsheets-npm-deps/package.js`, otherwise Meteor does not seem to pass them to the TypeScript build plugin.  If we cannot use an unmodified `@types` package, then we import the type declarations from their original source to a subdirectory of `packages/objsheets-npm-deps/typings` using Braid and modify them as necessary.  The `packages` directory is excluded in `tsconfig.json` to avoid errors on test files, but TypeScript will still load `index.d.ts` in each `typings` subdirectory, so we need to ensure that all necessary type declaration files are referenced by `index.d.ts`.

## React

As of 2018-01-05, the [React](https://github.com/facebook/react) templating engine is available in our codebase and we're starting to use it instead of Meteor's [Blaze](http://blazejs.org/) for some templates, particularly for custom layouts.  Its main advantages over Blaze (so far) are:
- React performs significantly better according to some basic tests.
- The code is better structured: templates (called components) can be written as real classes, and the [JSX](https://www.typescriptlang.org/docs/handbook/jsx.html) rendering code is fully type-checked and has more familiar semantics to TypeScript programmers than Blaze's Spacebars.  (Matt likes having the rendering code alongside the related helper and event handling code, though he knows others have argued for separation of HTML templates and code.)

The main disadvantages are that extra work is needed to:
- Integrate with Meteor reactive data (see [`withTracker`](https://github.com/meteor/react-packages/tree/devel/packages/react-meteor-data)).
- Embed [React components in Blaze templates](https://github.com/meteor/react-packages/tree/devel/packages/react-template-helper) and vice versa ([example](https://www.meteor.com/tutorials/react/adding-user-accounts)).

We're still learning how React is going to work for us.

Note that [React's default error handling behavior](https://reactjs.org/docs/error-boundaries.html#new-behavior-for-uncaught-errors) is stricter than Blaze's: when a component (including a `withTracker` data function) throws an unhandled error, React removes the root component from the DOM.  To recover, you'll need to refresh the page.  (Modifying code used by the client automatically refreshes the page, but if you fix the root cause of the error by modifying server-only code, you still have to refresh the page manually!)

## Dependencies

Object Spreadsheets uses modified versions of the [Handsontable](https://github.com/handsontable/handsontable) spreadsheet widget and the [Mavo](http://mavo.io/) HTML templating engine.  The source trees are in `tests/{handsontable,mavo}` (where Meteor doesn't look at them) and are managed with Braid as described above.  After you modify the source files, run `tests/dev-tools/update-{handsontable,mavo}` (respectively) to invoke the subproject's build system and copy the output files to the expected place in the main Meteor application.  These scripts require that you [install Yarn](https://yarnpkg.com/en/docs/install); we use Yarn to manage the npm dependencies of Handsontable and Mavo because it's much easier to achieve deterministic results with Yarn than with npm.

Meteor encourages the use of application-level npm packages (in the application's `node_modules` directory) but doesn't provide a way to automate their installation (https://github.com/meteor/meteor/issues/6848) and Matt isn't willing to add it as a manual step, so we minimize the use of application-level npm packages.  Unmodified npm packages that are used by normal application code go in the `objsheets-npm-deps` wrapper Meteor package, where the Meteor build system will keep them in sync automatically, and have to be manually re-exported and imported by the application.  Meteor requires all applications to have certain application-level npm packages (currently, `babel-runtime`) and several attempted workarounds were unsuccessful, so we check those packages in and update them using Yarn.  We even check in the <code>.meteor-portable-<em>N</em>.json</code> files that Meteor creates to ensure that they don't get left behind after switching branches, which seems like it might cause problems; this decision is subject to change.

We also have Meteor package dependencies, but they don't require any special attention from developers (kudos to Meteor).

## Deployment

The settings file `multiuser.settings` can be used to configure the server so that each visitor gets their own instances of the example applications.
