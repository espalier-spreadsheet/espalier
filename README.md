# Espalier

Espalier is an enhanced spreadsheet tool with support for storing and manipulating structured data.  End-user developers can use it to work directly with a data set or to build a web application that offers constrained view and update access to a larger population of users.

This is a research prototype and we do not recommend relying on it for anything important at this point.  If you put significant work into a spreadsheet, we highly recommend you back it up using the `tests/dev-tools/mkdump` tool (we may provide an easier-to-use mechanism in the future).

[Project web site](https://sdg.csail.mit.edu/projects/espalier/)

Espalier is in the process of being renamed from Object Spreadsheets, so you'll still see the Object Spreadsheets name in some places.

## Running

### System requirements

Espalier is intended to build and run on all Meteor supported platforms (Linux, Mac OS X, and Windows), although we don't test it regularly on platforms other than Linux.  Many of our auxiliary tools assume a unix-like environment (on Windows, the MSYS2 environment that comes with Git for Windows may be good enough) and have additional dependencies.

You'll need a browser to access the server.  We try to support recent versions of Firefox and Google Chrome; your mileage with other browsers may vary.

### Linux and Mac OS X

1. Install Git from your OS distribution or https://git-scm.com/downloads.
2. In the terminal:

        curl https://install.meteor.com/ | sh
        git clone https://bitbucket.org/espalier-spreadsheet/espalier
        cd espalier
        meteor

3. Open http://localhost:3000/ in your browser.

### Windows

1. [Install Git](https://git-scm.com/downloads) and choose "Use Git from the Windows Command Prompt".
2. [Install Meteor](https://www.meteor.com/install).
3. Using the Windows command prompt (cmd):

        git clone https://bitbucket.org/espalier-spreadsheet/espalier
        cd espalier
        meteor

4. Open http://localhost:3000/ in your browser.

(This is not the only way to do it, but it's one that we've tested.)

### Version compatibility

We make no guarantee of compatibility across revisions of Espalier.  Before you attempt to switch revisions, we highly recommend backing up any valuable spreadsheets with `tests/dev-tools/mkdump` as mentioned above.  We try to maintain backward compatibility on the `master` branch when it isn't too much extra work, so in many cases, you'll be able to check out a newer revision of `master` and it will work with your existing database, automatically making any necessary format upgrades.  Other revision switches are more likely to result in problems.  If you run into problems that you think might be related to incompatible data in your database, stop the server and delete the `.meteor/local/db` directory that holds the database.  (`meteor reset` does this and also resets the Meteor build system, which normally shouldn't be necessary.)

## Developing

See [DEVELOPMENT.md](DEVELOPMENT.md).
