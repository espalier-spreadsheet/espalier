Package.describe({
	name: "hansoft:typescript",
	summary: "TypeScript is a staticaly typed superset of JavaScript",
	git: "https://github.com/Hansoft/meteor-typescript.git",
	version: "1.1.1-objsheets"
});

Package.onUse(function(api) {
	api.use("isobuild:compiler-plugin@1.0.0");
});

Package.registerBuildPlugin({
	name: "compileTypescript",
	use: ["meteor"],
	sources: [
		"plugin/compile-typescript.js"
	],
	npmDependencies: {
		"typescript": "3.5.3",
		"tslint": "5.11.0"
	}
});
