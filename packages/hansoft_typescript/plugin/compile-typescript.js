"use strict";
// Edit compile-typescript.ts and run `tsc -p .` manually in the
// `packages/hansoft_typescript` directory to compile it.  The --watch option to
// tsc may be useful during development.
var pathModule = Npm.require("path");
// See src/lib/client/modules.ts .
var ExternalModules;
(function (ExternalModules) {
    (function () { })(); // Force namespace to be created at runtime
})(ExternalModules || (ExternalModules = {}));
ExternalModules.ts = Npm.require("typescript");
var typescript = ExternalModules.ts;
//////
// I don't understand the TypeScript compiler APIs well enough to have any
// confidence this is the best approach, but it does seem to work.
//
// It's unfortunate to do caching and other stuff by hand that the TypeScript
// language service is designed to do for us, but this implementation is
// measurably faster than one based on the language service (~9.4s compared to
// ~9.8s in my test for a Meteor incremental build after a trivial change
// affecting both unibuilds), so I'll keep it. ~ Matt 2018-01-06
var sourceMapReferenceLineRegExp = new RegExp("//# sourceMappingURL=.*$", "m");
function fullFilePath(f) {
    return (f.getPackageName() == null ? "" : "packages/" + f.getPackageName().replace(":", "_") + "/") + f.getPathInPackage();
}
var TypeScriptCompiler = /** @class */ (function () {
    function TypeScriptCompiler() {
        this.targetCache = new Map();
        // XXX: Memory leak: We never remove source files that were removed from the
        // app.  We'd have to keep track of when we've seen all architectures and the
        // files are no longer present in any of them.
        this.sourceFileCache = new Map();
    }
    TypeScriptCompiler.prototype.processFilesForTarget = function (files) {
        var _this = this;
        var wantedFiles = files.filter(function (f) {
            if (f.getPackageName() == "hansoft:typescript")
                return false;
            var fullPath = fullFilePath(f);
            if (/^packages\/objsheets-npm-deps\/typings\//.test(fullPath) && !/\.d\.ts$/.test(fullPath))
                return false;
            return true;
        });
        if (wantedFiles.length == 0)
            return; // XXX log?
        var arch = wantedFiles[0].getArch();
        var tsconfigFile /*nullable*/ = wantedFiles.filter(function (f) { return fullFilePath(f) == "tsconfig.json"; })[0];
        var sourceFiles = wantedFiles.filter(function (f) { return fullFilePath(f) != "tsconfig.json"; });
        var appSourceFiles = sourceFiles.filter(function (f) { return f.getPackageName() == null; });
        if (appSourceFiles.length == 0)
            return; // XXX log?
        var sourceFilesMap = new Map(sourceFiles.map(function (f) { return [fullFilePath(f), f]; }));
        var inputHashes = wantedFiles.map(function (f) { return ({ path: fullFilePath(f), hash: f.getSourceHash() }); });
        var result;
        var entry = this.targetCache.get(arch);
        if (entry != null && JSON.stringify(inputHashes) == JSON.stringify(entry.inputHashes)) {
            result = entry.result;
        }
        else {
            // Need to recompile
            result = {
                source: "",
                sourceMap: "",
                errors: [],
                nonfatalErrors: []
            };
            var errorsToConvert = void 0;
            var haveFatalErrors_1;
            var program = void 0;
            compileBlock: {
                var rcfResult = typescript.readConfigFile("tsconfig.json", 
                // No support for tsconfig.json includes at this time.
                function (path) { return (path == "tsconfig.json") ? tsconfigFile.getContentsAsString() : undefined; });
                var configObject = rcfResult.config;
                if (!configObject) {
                    errorsToConvert = [rcfResult.error];
                    haveFatalErrors_1 = true;
                    break compileBlock;
                }
                // typescript.parseJsonConfigFileContent determines all the source files in
                // the project (scanning the entire base directory if nothing is specified
                // in tsconfig.json) and raises an error if there are no files.  We don't
                // want to use this functionality (we get the list of files from Meteor), so
                // specify a single dummy filename to disable the automatic scan and pass
                // the check (TypeScript doesn't check whether the file actually exists).
                configObject.files = ["dummy.ts"];
                var configParseResult = typescript.parseJsonConfigFileContent(configObject, {}, "");
                if (configParseResult.errors.length > 0) {
                    errorsToConvert = configParseResult.errors;
                    haveFatalErrors_1 = true;
                    break compileBlock;
                }
                var options = configParseResult.options;
                // Emit-specific options.
                options.noEmit = false;
                options.out = "out.js";
                options.sourceMap = true;
                options.removeComments = true;
                var defaultHost_1 = typescript.createCompilerHost(options);
                var defaultLibLocation_1 = defaultHost_1.getDefaultLibLocation();
                var fileIsDefaultLib_1 = function (fileName) { return pathModule.dirname(fileName) == defaultLibLocation_1; };
                var cleanFileName_1 = function (fileName) { return fileName.replace(/^\/dummy\//, ""); };
                var host = {
                    // Support --traceResolution.
                    trace: function (msg) {
                        console.log(msg);
                    },
                    getSourceFile: function (fileName, languageVersion, onError) {
                        var hash, makeSourceFile;
                        if (fileIsDefaultLib_1(fileName)) {
                            hash = ""; // No change detection :/
                            makeSourceFile = function () { return defaultHost_1.getSourceFile(fileName, languageVersion, onError); };
                        }
                        else {
                            fileName = cleanFileName_1(fileName);
                            var inputFile_1 = sourceFilesMap.get(fileName);
                            if (inputFile_1 == null) {
                                return undefined;
                            }
                            hash = inputFile_1.getSourceHash();
                            makeSourceFile = function () { return typescript.createSourceFile(fileName, inputFile_1.getContentsAsString(), languageVersion); };
                        }
                        var csf = _this.sourceFileCache.get(fileName);
                        if (csf == null || csf.hash != hash) {
                            csf = { hash: hash, parsedSourceFiles: new Map() };
                            _this.sourceFileCache.set(fileName, csf);
                        }
                        var sf = csf.parsedSourceFiles.get(languageVersion);
                        if (sf == null) {
                            sf = makeSourceFile();
                            csf.parsedSourceFiles.set(languageVersion, sf);
                        }
                        return sf;
                    },
                    fileExists: function (fileName) {
                        if (fileIsDefaultLib_1(fileName)) {
                            return defaultHost_1.fileExists(fileName);
                        }
                        else {
                            fileName = cleanFileName_1(fileName);
                            return sourceFilesMap.get(fileName) != null;
                        }
                    },
                    writeFile: function (fileName, data, writeByteOrderMark, onError, sourceFiles) {
                        if (fileName == "out.js")
                            result.source = data;
                        else
                            result.sourceMap = data;
                    },
                    getDefaultLibFileName: defaultHost_1.getDefaultLibFileName,
                    getCurrentDirectory: function () { return "/dummy"; },
                    useCaseSensitiveFileNames: function () { return true; },
                    getCanonicalFileName: function (name) { return name; },
                    getNewLine: defaultHost_1.getNewLine
                };
                program = typescript.createProgram(sourceFiles.map(function (f) { return fullFilePath(f); }), options, host, entry == null ? undefined : entry.program);
                var emitResult = program.emit();
                errorsToConvert = typescript.getPreEmitDiagnostics(program).concat(emitResult.diagnostics);
                // Syntactic diagnostics are included in errorsToConvert and they are
                // held in memory, so there should be essentially no cost to requesting
                // them again.
                haveFatalErrors_1 = emitResult.emitSkipped || program.getSyntacticDiagnostics().length > 0;
                if (!haveFatalErrors_1) {
                    // Remove source map reference line from generated js file.
                    // Meteor sets up source map through HTTP response header instead.
                    // FIXME: Should be an option for TypeScript compiler.
                    result.source = result.source.replace(sourceMapReferenceLineRegExp, "");
                    // FIXME: Embed sources directly in the source map, as there is no way to make Meteor serve them as files.
                    var sourceMapObject_1 = JSON.parse(result.sourceMap);
                    //sourceMapObject.file = input.pathForSourceMap;
                    sourceMapObject_1.sourcesContent = [];
                    sourceMapObject_1.sources.forEach(function (sourcePath) {
                        var sourceContent = sourceFilesMap.get(sourcePath).getContentsAsString();
                        sourceMapObject_1.sourcesContent.push(sourceContent);
                    });
                    result.sourceMap = JSON.stringify(sourceMapObject_1);
                }
                // Consider running the linter here if there were no errors?
                // That will lengthen the edit-compile-run cycle for everyone,
                // in contrast to "check-code", which can just be run once after
                // the code is finished.
            }
            errorsToConvert.forEach(function (diagnostic) {
                var sourcePath = null, line = null, character = null;
                if (diagnostic.file) {
                    sourcePath = diagnostic.file.fileName;
                    var lineAndCharacter = diagnostic.file.getLineAndCharacterOfPosition(diagnostic.start);
                    line = lineAndCharacter.line + 1;
                    character = lineAndCharacter.character + 1;
                }
                var diagnosticCategory = typescript.DiagnosticCategory[diagnostic.category];
                var message = typescript.flattenDiagnosticMessageText(diagnostic.messageText, "\n");
                var error = {
                    message: diagnosticCategory + " TS" + diagnostic.code + ": " + message,
                    sourcePath: sourcePath,
                    line: line,
                    column: character
                };
                if (haveFatalErrors_1)
                    result.errors.push(error);
                else
                    result.nonfatalErrors.push(error);
            });
            entry = { inputHashes: inputHashes, program: program, result: result };
            this.targetCache.set(arch, entry);
        }
        result.errors.forEach(function (error) {
            sourceFilesMap.get(error.sourcePath).error(error);
        });
        // Re-display the errors on a cache hit.  This could be distracting, but it's
        // less bad than letting the user think the errors are gone.  (A user who
        // remembers whether they changed a TypeScript file might be able to avoid
        // getting confused, but this way if you see two successive "Meteor server
        // restarted" or "Client modified -- refreshing" messages without intervening
        // errors, you can pretty much conclude there are no errors.  I tried a bunch
        // of cases and didn't find any that violated this rule. ~ Matt 2016-09-19)
        result.nonfatalErrors.forEach(function (error) {
            console.log(error.sourcePath + " (" + error.line + ", " + error.column + "): " + error.message);
        });
        if (result.source) {
            appSourceFiles[0].addJavaScript({
                path: fullFilePath(appSourceFiles[0]).replace(/\.tsx?$/, ".js"),
                data: result.source,
                sourceMap: result.sourceMap
            });
        }
    };
    return TypeScriptCompiler;
}());
Plugin.registerCompiler({
    extensions: ["ts", "tsx"],
    filenames: ["tsconfig.json"]
}, function () { return new TypeScriptCompiler(); });
