// Edit compile-typescript.ts and run `tsc -p .` manually in the
// `packages/hansoft_typescript` directory to compile it.  The --watch option to
// tsc may be useful during development.

// tslint:disable-next-line:no-any
type fixmeAny = any;

declare let Npm: fixmeAny;
declare let Plugin: fixmeAny;
declare let console: fixmeAny;

let pathModule = Npm.require("path");

// See src/lib/client/modules.ts .
namespace ExternalModules {
  (() => {})();  // Force namespace to be created at runtime
}
ExternalModules.ts = Npm.require("typescript");
declare module "dummy" {
  import * as ts1 from "typescript";
  global {
    // tslint:disable-next-line:no-shadowed-variable
    namespace ExternalModules {
      export import ts = ts1;
    }
  }
}
import typescript = ExternalModules.ts;

//////

// Meteor build plugin API type declarations
// https://docs.meteor.com/api/packagejs.html#build-plugin-api

interface MeteorBuildPluginInputFile {
  getPackageName(): string;
  getPathInPackage(): string;
  getContentsAsString(): string;
  getSourceHash(): string;
  getArch(): string;
  error(error: MeteorBuildPluginError): void;
}

interface MeteorCompilerInputFile extends MeteorBuildPluginInputFile {
  addJavaScript(f: {
    path: string,
    data: string,
    sourceMap: string
  }): void;
}

type MeteorBuildPluginError = {
  message: string,
  sourcePath?: string,
  line: number,
  column: number
};

//////

// I don't understand the TypeScript compiler APIs well enough to have any
// confidence this is the best approach, but it does seem to work.
//
// It's unfortunate to do caching and other stuff by hand that the TypeScript
// language service is designed to do for us, but this implementation is
// measurably faster than one based on the language service (~9.4s compared to
// ~9.8s in my test for a Meteor incremental build after a trivial change
// affecting both unibuilds), so I'll keep it. ~ Matt 2018-01-06

let sourceMapReferenceLineRegExp = new RegExp("//# sourceMappingURL=.*$", "m");

type TargetResult = {
  source: string,
  sourceMap: string,
  errors: MeteorBuildPluginError[],
  nonfatalErrors: MeteorBuildPluginError[]
};

type FileHashInfo = {
  path: string;
  hash: string;
};
type CachedSourceFile = {
  hash: string,
  parsedSourceFiles: Map<typescript.ScriptTarget, typescript.SourceFile>
};
type TargetCacheEntry = {
  // Including tsconfig.json!!  Order might matter.
  inputHashes: FileHashInfo[],
  program: typescript.Program,
  result: TargetResult
};

function fullFilePath(f: MeteorCompilerInputFile) {
  return (f.getPackageName() == null ? "" : "packages/" + f.getPackageName().replace(":", "_") + "/") + f.getPathInPackage();
}

class TypeScriptCompiler {
  targetCache = new Map<string /* arch */, TargetCacheEntry>();
  // XXX: Memory leak: We never remove source files that were removed from the
  // app.  We'd have to keep track of when we've seen all architectures and the
  // files are no longer present in any of them.
  sourceFileCache = new Map<string /* path in package */, CachedSourceFile>();
  processFilesForTarget(files: MeteorCompilerInputFile[]) {
    let wantedFiles = files.filter((f) => {
      if (f.getPackageName() == "hansoft:typescript") return false;
      let fullPath = fullFilePath(f);
      if (/^packages\/objsheets-npm-deps\/typings\//.test(fullPath) && !/\.d\.ts$/.test(fullPath)) return false;
      return true;
    });
    if (wantedFiles.length == 0) return;  // XXX log?
    let arch = wantedFiles[0].getArch();

    let tsconfigFile /*nullable*/ = wantedFiles.filter((f) => fullFilePath(f) == "tsconfig.json")[0];
    let sourceFiles = wantedFiles.filter((f) => fullFilePath(f) != "tsconfig.json");
    let appSourceFiles = sourceFiles.filter((f) => f.getPackageName() == null);
    if (appSourceFiles.length == 0) return;  // XXX log?
    let sourceFilesMap = new Map(sourceFiles.map<[string, MeteorCompilerInputFile]>((f) => [fullFilePath(f), f]));

    let inputHashes = wantedFiles.map<FileHashInfo>((f) => ({ path: fullFilePath(f), hash: f.getSourceHash() }));
    let result: TargetResult;
    let entry = this.targetCache.get(arch);
    if (entry != null && JSON.stringify(inputHashes) == JSON.stringify(entry.inputHashes)) {
      result = entry.result;
    } else {
      // Need to recompile

      result = {
        source: "",
        sourceMap: "",
        errors: [],
        nonfatalErrors: [],  // custom field
      };
      let errorsToConvert;
      let haveFatalErrors: boolean;
      let program;

      compileBlock: {
        let rcfResult = typescript.readConfigFile("tsconfig.json",
          // No support for tsconfig.json includes at this time.
          (path) => (path == "tsconfig.json") ? tsconfigFile.getContentsAsString() : undefined);
        let configObject = rcfResult.config;
        if (!configObject) {
          errorsToConvert = [rcfResult.error];
          haveFatalErrors = true;
          break compileBlock;
        }

        // typescript.parseJsonConfigFileContent determines all the source files in
        // the project (scanning the entire base directory if nothing is specified
        // in tsconfig.json) and raises an error if there are no files.  We don't
        // want to use this functionality (we get the list of files from Meteor), so
        // specify a single dummy filename to disable the automatic scan and pass
        // the check (TypeScript doesn't check whether the file actually exists).
        configObject.files = ["dummy.ts"];
        let configParseResult = typescript.parseJsonConfigFileContent(
          configObject, <typescript.ParseConfigHost>{}, "");
        if (configParseResult.errors.length > 0) {
          errorsToConvert = configParseResult.errors;
          haveFatalErrors = true;
          break compileBlock;
        }

        let options = configParseResult.options;
        // Emit-specific options.
        options.noEmit = false;
        options.out = "out.js";
        options.sourceMap = true;
        options.removeComments = true;

        let defaultHost = typescript.createCompilerHost(options);
        let defaultLibLocation = defaultHost.getDefaultLibLocation();
        let fileIsDefaultLib = (fileName: string) => pathModule.dirname(fileName) == defaultLibLocation;
        let cleanFileName = (fileName: string) => fileName.replace(/^\/dummy\//, "");

        let host = <typescript.CompilerHost>{
          // Support --traceResolution.
          trace: (msg: string) => {
            console.log(msg);
          },
          getSourceFile: (fileName, languageVersion, onError) => {
            let hash, makeSourceFile;
            if (fileIsDefaultLib(fileName)) {
              hash = "";  // No change detection :/
              makeSourceFile = () => defaultHost.getSourceFile(fileName, languageVersion, onError);
            } else {
              fileName = cleanFileName(fileName);
              let inputFile = sourceFilesMap.get(fileName);
              if (inputFile == null) {
                return undefined;
              }
              hash = inputFile.getSourceHash();
              makeSourceFile = () => typescript.createSourceFile(fileName, inputFile.getContentsAsString(), languageVersion);
            }
            let csf = this.sourceFileCache.get(fileName);
            if (csf == null || csf.hash != hash) {
              csf = { hash: hash, parsedSourceFiles: new Map() };
              this.sourceFileCache.set(fileName, csf);
            }
            let sf = csf.parsedSourceFiles.get(languageVersion);
            if (sf == null) {
              sf = makeSourceFile();
              csf.parsedSourceFiles.set(languageVersion, sf);
            }
            return sf;
          },
          fileExists: (fileName) => {
            if (fileIsDefaultLib(fileName)) {
              return defaultHost.fileExists(fileName);
            } else {
              fileName = cleanFileName(fileName);
              return sourceFilesMap.get(fileName) != null;
            }
          },
          writeFile: (fileName, data, writeByteOrderMark, onError, sourceFiles) => {
            if (fileName == "out.js")
              result.source = data;
            else
              result.sourceMap = data;
          },
          getDefaultLibFileName: defaultHost.getDefaultLibFileName,
          getCurrentDirectory: () => "/dummy",
          useCaseSensitiveFileNames: () => true,
          getCanonicalFileName: (name) => name,
          getNewLine: defaultHost.getNewLine
        };

        program = typescript.createProgram(sourceFiles.map((f) => fullFilePath(f)), options, host, entry == null ? undefined : entry.program);
        let emitResult = program.emit();

        errorsToConvert = typescript.getPreEmitDiagnostics(program).concat(emitResult.diagnostics);
        // Syntactic diagnostics are included in errorsToConvert and they are
        // held in memory, so there should be essentially no cost to requesting
        // them again.
        haveFatalErrors = emitResult.emitSkipped || program.getSyntacticDiagnostics().length > 0;

        if (!haveFatalErrors) {
          // Remove source map reference line from generated js file.
          // Meteor sets up source map through HTTP response header instead.
          // FIXME: Should be an option for TypeScript compiler.
          result.source = result.source.replace(sourceMapReferenceLineRegExp, "");

          // FIXME: Embed sources directly in the source map, as there is no way to make Meteor serve them as files.
          let sourceMapObject = JSON.parse(result.sourceMap);
          //sourceMapObject.file = input.pathForSourceMap;
          sourceMapObject.sourcesContent = [];
          sourceMapObject.sources.forEach(function (sourcePath: string) {
            let sourceContent = sourceFilesMap.get(sourcePath).getContentsAsString();
            sourceMapObject.sourcesContent.push(sourceContent);
          });
          result.sourceMap = JSON.stringify(sourceMapObject);
        }

        // Consider running the linter here if there were no errors?
        // That will lengthen the edit-compile-run cycle for everyone,
        // in contrast to "check-code", which can just be run once after
        // the code is finished.
      }

      errorsToConvert.forEach(function (diagnostic) {
        let sourcePath = null, line = null, character = null;
        if (diagnostic.file) {
          sourcePath = diagnostic.file.fileName;
          let lineAndCharacter = diagnostic.file.getLineAndCharacterOfPosition(diagnostic.start);
          line = lineAndCharacter.line + 1;
          character = lineAndCharacter.character + 1;
        }
        let diagnosticCategory = typescript.DiagnosticCategory[diagnostic.category];
        let message = typescript.flattenDiagnosticMessageText(diagnostic.messageText, "\n");

        let error = {
          message: diagnosticCategory + " TS" + diagnostic.code + ": " + message,
          sourcePath: sourcePath,
          line: line,
          column: character
        };
        if (haveFatalErrors)
          result.errors.push(error);
        else
          result.nonfatalErrors.push(error);
      });

      entry = {inputHashes, program, result};
      this.targetCache.set(arch, entry);
    }

    result.errors.forEach(function (error) {
      sourceFilesMap.get(error.sourcePath).error(error);
    });

    // Re-display the errors on a cache hit.  This could be distracting, but it's
    // less bad than letting the user think the errors are gone.  (A user who
    // remembers whether they changed a TypeScript file might be able to avoid
    // getting confused, but this way if you see two successive "Meteor server
    // restarted" or "Client modified -- refreshing" messages without intervening
    // errors, you can pretty much conclude there are no errors.  I tried a bunch
    // of cases and didn't find any that violated this rule. ~ Matt 2016-09-19)
    result.nonfatalErrors.forEach(function (error) {
      console.log(error.sourcePath + " (" + error.line + ", " + error.column + "): " + error.message);
    });

    if (result.source) {
      appSourceFiles[0].addJavaScript({
        path: fullFilePath(appSourceFiles[0]).replace(/\.tsx?$/, ".js"),
        data: result.source,
        sourceMap: result.sourceMap,
      });
    }
  }
}

Plugin.registerCompiler({
  extensions: ["ts", "tsx"],
  filenames: ["tsconfig.json"]
}, () => new TypeScriptCompiler());
