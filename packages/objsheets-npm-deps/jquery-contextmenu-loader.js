// jquery-contextmenu wants to `require('jquery')`, and the straightforward way
// to make that work is to install jQuery as an app-level NPM package, but we're
// minimizing the use of app-level NPM packages until Meteor automates their
// installation.  So instead, register a fake module that exports the copy of
// jQuery already loaded by the `jquery` Atmosphere package.
meteorInstall({"node_modules":{"jquery":function(require,exports,module) {
  module.exports = $;
}}});

require('jquery-contextmenu');
