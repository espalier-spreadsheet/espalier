// This Meteor package holds npm packages used by Object Spreadsheets so we can
// take advantage of the Meteor build system's ability to automatically install
// the npm packages.  See the "Dependencies" section of DEVELOPMENT.md.

Package.describe({
  name: 'objsheets-npm-deps',
  version: '0.0.1',
  summary: 'npm dependencies packaged for Object Spreadsheets',
});

Package.onUse(function(api) {
  api.use('hansoft:typescript@1.1.1-objsheets');  // for typings

  api.use('ecmascript');  // for the export in spin.js/spin.js

  api.export('ExternalModules');
  api.mainModule('external-modules-init.js');

  // The datejs npm package does not include the concatenated file.
  // Fortunately, the Meteor build process is capable of gathering up all the
  // modules!
  api.use('modules');
  api.addFiles('datejs-loader.js', null, {lazy: false});

  // These were previously loaded on all pages by src/client/sheet.html.
  // Maintaining that behavior for now.  If we want them only on the sheet, we
  // can add them as assets and load them from the Spreadsheet template, like
  // 005q does for semantic.min.css. ~ Matt 2016-09-16
  api.export('CodeMirror');
  api.addFiles('.npm/package/node_modules/codemirror/lib/codemirror.css', 'client');
  api.addFiles('codemirror-loader.js', 'client', {lazy: false});
  // Meteor doesn't seem to pass files in node_modules to the TypeScript build
  // plugin unless we declare them.  Rather than spending time investigating,
  // just declare them.
  api.addFiles([
    '.npm/package/node_modules/@types/codemirror/index.d.ts',
  ], 'client');

  // I tried unbundling semantic-ui (used by the 005q application UI).  There's
  // an npm package semantic-ui-css, but 005q is using version 0.12.1 and
  // doesn't seem to work with the latest version 2.2.4 (the UI was messed up),
  // and the semantic-ui-css package doesn't have a version 0.12.1 available.
  // So give up. ~ Matt 2016-09-16

  // Note: We don't use the `react` Meteor package because it has peer
  // dependencies on a bunch of app-level NPM packages we're not interested in,
  // and in the current app-level NPM package situation, we'd have to alias all
  // of them from the objsheets-npm-deps package.

  // Expose `React` rather than `ExternalModules.React` because we'll be using
  // it a lot and that's what TypeScript's JSX support expects by default.  Do
  // the same with `ReactDOM` for consistency.
  api.export('React');
  api.export('ReactDOM');
  // react-meteor-data package wants React to be loaded on the server.  Why??
  // If we really didn't want to load React on the server, maybe we could move
  // the react-meteor-data dependency to this package and make it client-only.
  api.addFiles('react-loader.js', null, {lazy: false});
  api.addFiles([
    '.npm/package/node_modules/@types/react/index.d.ts',
    '.npm/package/node_modules/@types/react/global.d.ts',
    '.npm/package/node_modules/@types/prop-types/index.d.ts',
    '.npm/package/node_modules/csstype/index.d.ts',
  ], 'client');

  api.use('jquery');
  api.addFiles('.npm/package/node_modules/jquery-contextmenu/dist/jquery.contextMenu.css', 'client');
  api.addFiles('jquery-contextmenu-loader.js', 'client', {lazy: false});

  api.export('WebFont');
  api.addFiles('webfontloader-loader.js', 'client', {lazy: false});
  api.addFiles('.npm/package/node_modules/@types/webfontloader/index.d.ts', 'client');

  api.addFiles('spinjs-loader.js', 'client', {lazy: false});
  api.addFiles('spin.js/spin.css', 'client');

  api.addFiles([
    '.npm/package/node_modules/@types/node/index.d.ts',
    '.npm/package/node_modules/@types/node/inspector.d.ts',
  ]);
  // XXX "node-fibers" should be named "fibers". ~ Matt 2018-08-07
  api.addFiles('.npm/package/node_modules/@types/node-fibers/index.d.ts', 'server');

  // Has to be loaded on the server because meteor.d.ts is loaded on both sides
  // and references BaseJQueryEventObject.
  api.addFiles('.npm/package/node_modules/@types/jquery/index.d.ts');

  api.addFiles('.npm/package/node_modules/@types/connect/index.d.ts');
});

Npm.depends({
  "codemirror": "5.42.0",
  "@types/codemirror": "0.0.69",
  "datejs": "1.0.0-rc3",
  "jquery-contextmenu": "2.7.1",
  "react": "16.6.3",
  "@types/react": "16.4.18",
  "react-dom": "16.6.3",
  "@types/react-dom": "16.0.10",
  "webfontloader": "1.6.28",
  "@types/webfontloader": "1.6.29",

  "@types/node": "8.10.38",  // global, require, Error.stackTraceLimit; Meteor 1.8.0.1 is on Node 8.11.4
  "@types/node-fibers": "0.0.28",

  "@types/jquery": "1.10.34",

  "@types/connect": "3.4.32",  // dependency of @types/meteor; webapp 1.7.2 is on connect 3.6.5
});
