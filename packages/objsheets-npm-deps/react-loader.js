React = require('react');
ReactDOM = require('react-dom');

// Make react-meteor-data and react-template-helper packages happy.
var reactPackageJson = require('react/package.json');
var reactDomPackageJson = require('react-dom/package.json');

meteorInstall({"node_modules": {
  "react": {
    "package.json": function(require, exports, module) {
      module.exports = reactPackageJson;
    },
    "index.js": function(require, exports, module) {
      module.exports = React;
    }
  },
  "react-dom": {
    "package.json": function(require, exports, module) {
      module.exports = reactDomPackageJson;
    },
    "index.js": function(require, exports, module) {
      module.exports = ReactDOM;
    }
  }
}});
