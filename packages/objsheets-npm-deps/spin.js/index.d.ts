// Because package.json doesn't get passed to the TypeScript build plugin for it
// to look at the `types` field, we do this instead. ~ Matt 2018-08-10
export * from "./spin";
