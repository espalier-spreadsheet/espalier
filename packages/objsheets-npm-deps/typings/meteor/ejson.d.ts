declare module "meteor/ejson" {
    interface EJSONableCustomType {
        clone?(): EJSONableCustomType;
        equals?(other: Object): boolean;
        toJSONValue(): JSONable;
        typeName(): string;

        // Dummy properties that a class can optionally declare to make itself fail to
        // implement EJSONableCustomType if one of its type parameters is not
        // EJSONable, since TypeScript isn't strict enough checking the "this" type of
        // toJSONValue.  See EJSONKeyedMap for an example.  The properties should be
        // declared in an interface merged with the class, since declaring them in the
        // class will cause the symbols to be evaluated at runtime, which will fail
        // because they don't exist.
        [EJSON.symbols.ejsonableTypeParam1]?: EJSONable;
        [EJSON.symbols.ejsonableTypeParam2]?: EJSONable;
    }
    // Rewrite to allow primitives and arrays at the top and be consistent in our
    // treatment of top-level and nested objects (even if that means more false
    // positives!). ~ Matt 2016-02-29

    // Direct recursion (type EJSONable = ... | Array<EJSONable> | ...) is not
    // allowed.  But because typing is structural, the use of EJSONableArray is
    // equivalent in effect.
    //
    // The [E]JSONableDict interfaces are factored out to make error messages
    // containing the expansions of the [E]JSONable aliases less horrifying.
    //
    // Note that an object literal type with all [E]JSONable properties is
    // assignable to [E]JSONableDict
    // (https://github.com/Microsoft/TypeScript/pull/7029).

    interface EJSONableArray extends Array<EJSONable> {}
    interface EJSONableDict {
        // Let types with optional properties be assignable to EJSONableDict. ~ Matt 2018-06-30
        //
        // Caution: EJSON.equals is sensitive to "undefined"-valued properties, but
        // EJSON.stringify throws them away! ~ Matt 2018-08-01
        [key: string]: undefined | EJSONable;
    }
    type EJSONable = null | number | string | boolean | EJSONableArray | EJSONableDict | Date | Uint8Array | EJSONableCustomType;

    interface JSONableArray extends Array<JSONable> {}
    interface JSONableDict {
        [key: string]: undefined | JSONable;
    }
    type JSONable = null | number | string | boolean | JSONableArray | JSONableDict;

    type EJSON = EJSONable;

    module EJSON {
        type Binary = Uint8Array | number[];

        // Each EJSON custom type `Foo` is expected to define a type for its JSON
        // representation, by convention named `FooJSON`, and use that type for the
        // parameter to the factory and the return value of `Foo.toJSONValue`. This
        // way, fields can be renamed correctly across the factory and
        // `Foo.toJSONValue`.
        //
        // If `Foo` has type parameters, the factory should set the type arguments of
        // the created object to `any_ta`, the alias of `any` defined for this
        // scenario; that's more truthful than letting them default to the upper
        // bounds of the parameters
        // (https://github.com/Microsoft/TypeScript/blob/master/doc/spec.md#4.15.2).
        type any_ta = any;
        function addType<J extends JSONable>(name: string,
            factory: (val: J) => EJSONableCustomType & {toJSONValue(): J}): void;

        function clone < T extends undefined | EJSONable > (val: T): T;

        function equals(a: undefined | EJSON, b: undefined | EJSON, options?: {
            keyOrderSensitive?: boolean;
        }): boolean;

        // Rationale for return type: The caller will expect the return value to be a
        // particular EJSON type depending on the data passed.  There doesn't seem to
        // be value in forcing the caller to explicitly downcast the return value, on
        // top of putting it somewhere that imposes a type.  (If the caller assigns it
        // to a variable without declaring the type, they'll get dinged for an
        // implicit any.)  Same idea as JSON.parse.
        function fromJSONValue(val: JSONable): any;

        function isBinary(x: Object): boolean;
        function newBinary(size: number): Binary;

        // See fromJSONValue re return type.
        function parse(str: string): any;

        function stringify(val: EJSON, options?: {
            indent?: boolean | number | string;
            canonical?: boolean;
        }): string;

        // Rationale for return type: The caller normally shouldn't know about the
        // JSON representation.
        function toJSONValue(val: EJSON): JSONable;

        namespace symbols {
            const ejsonableTypeParam1: unique symbol;
            const ejsonableTypeParam2: unique symbol;
        }
    }
}
