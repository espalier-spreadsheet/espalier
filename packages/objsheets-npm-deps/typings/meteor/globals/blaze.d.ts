declare module Blaze {
    var View: ViewStatic;

    interface ViewStatic {
        new (name?: string, renderFunction?: Function): View;
    }

    interface View {
        name: string;
        parentView: View;
        isCreated: boolean;
        isRendered: boolean;
        isDestroyed: boolean;
        renderCount: number;
        autorun(runFunc: (computation: Tracker.Computation) => void): Tracker.Computation;
        onViewCreated(func: Function): void;
        onViewReady(func: Function): void;
        onViewDestroyed(func: Function): void;
        firstNode(): Node;
        lastNode(): Node;
        template: Template;
        templateInstance(): TemplateInstance;
    }
    var currentView: View;

    function isTemplate(value: any): boolean;

    interface HelpersMap {
        [key: string]: Function;
    }

    var Template: TemplateStatic;

    interface TemplateStatic {
        new (viewName?: string, renderFunction?: Function): Template;

        registerHelper(name: string, func: Function): void;
        // Helpers can write `Template.instance(this)` (where `this` is the data
        // context) to get the template instance with the correct type.  The extra
        // argument is ignored by Blaze at runtime.
        instance<TInstance extends TemplateInstance>(data?: {[symbols.instanceType]: TInstance}): TInstance;
        currentData(): any;
        // numLevels is optional:
        // http://blazejs.org/api/templates.html#Template-parentData
        // ~ Matt 2016-09-19
        parentData(numLevels?: number): any;
    }

    interface Template1<TInstance extends TemplateInstance> {
        viewName: string;
        renderFunction: Function;
        constructView(): View;
        // I don't think this actually exists. ~ Matt 2016-10-09
        //head: Template;
        find(selector: string): HTMLElement;
        findAll(selector: string): HTMLElement[];
        $: any;
        onCreated(cb: (this: TInstance) => void): void;
        onRendered(cb: (this: TInstance) => void): void;
        onDestroyed(cb: (this: TInstance) => void): void;
        created: (this: TInstance) => void;
        rendered: (this: TInstance) => void;
        destroyed: (this: TInstance) => void;
        // For objsheets, we should always specify TData.  To upstream this, we
        // would probably need something for backward compatibility.
        //
        // As of 2017-09-21, the only documentation of ThisType appears to be its
        // pull request:
        // https://github.com/Microsoft/TypeScript/pull/14141
        //
        // ThisType only works as part of an intersection type.  Search for
        // `getThisTypeFromContextualType` in TypeScript `src/compiler/checker.ts`.
        //
        // Add instanceType marker for use by our Template.instance typing.
        helpers<TData>(helpersMap: HelpersMap & ThisType<TData & {[symbols.instanceType]: TInstance}>): void;
        events<TData>(eventsMap: Meteor.EventMap<TData, TInstance>): void;
    }

    type Template = Template1<fixmeAny>;

    var TemplateInstance: TemplateInstanceStatic;

    interface TemplateInstanceStatic {
        new (view: View): TemplateInstance;
    }

    interface TemplateInstance {
        $(selector: string): any;
        autorun(runFunc: (computation: Tracker.Computation) => void): Tracker.Computation;
        data: fixmeAny;
        find(selector: string): HTMLElement;
        findAll(selector: string): HTMLElement[];
        firstNode: Object;
        lastNode: Object;
        subscribe(name: string, ...args: any[]): Meteor.SubscriptionHandle;
        subscriptionsReady(): boolean;
        view: View;  // CHECK ~ Matt 2017-02-16
    }

    function Each(argFunc: Function, contentFunc: Function, elseFunc?: Function): View;

    function Unless(conditionFunc: Function, contentFunc: Function, elseFunc?: Function): View;

    function If(conditionFunc: Function, contentFunc: Function, elseFunc?: Function): View;

    function Let(bindings: Function, contentFunc: Function): View;

    function With(data: Object | Function, contentFunc: Function): View;

    function getData(elementOrView?: HTMLElement | View): Object;

    function getView(element?: HTMLElement): View;

    function remove(renderedView: View): void;

    function render(templateOrView: Template | View, parentNode: Node, nextNode?: Node, parentView?: View): View;

    function renderWithData(templateOrView: Template | View, data: Object | Function, parentNode: Node, nextNode?: Node, parentView?: View): View;

    function toHTML(templateOrView: Template | View): string;

    function toHTMLWithData(templateOrView: Template | View, data: Object | Function): string;

    namespace symbols {
        // This symbol doesn't exist at runtime, which is fine because it's not used
        // at runtime.
        const instanceType: unique symbol;
    }
}
