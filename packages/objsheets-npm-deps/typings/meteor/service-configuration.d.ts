declare module "meteor/service-configuration" {
    type Configuration = {
        // Hack for Espalier.  Upstreamability TBD. ~ Matt 2018-08-07
        _id: string;
        appId: string;
        secret: string;
    };
    var ServiceConfiguration: {
        configurations: Mongo.Collection<Configuration>;
    }
}
