
<template name="Todo">
  {{> MavoToDoStylesheets}}

  <main mv-storage="objsheets:{{sheet}}/SubsetView/%5B%22{{subset}}%22%5D" class="mv-autoedit">
    <meta property="totalCount" style="display: none;"/>
    <meta property="activeCount" style="display: none;"/>
    <meta property="subset" style="display: none;"/>
    <div mv-if="totalCount" style="color: black; margin-bottom: 0.5em;">
      {{! Hack hack.  Guess we could bind these to a collection of SubsetView
          references if we had a way to join them appropriately.
          ~ Matt 2017-02-27 }}
      <a href="all" class="[if(subset = 'all', 'currentSubset', '')]">All</a> |
      <a href="active" class="[if(subset = 'active', 'currentSubset', '')]">Active</a> |
      <a href="completed" class="[if(subset = 'completed', 'currentSubset', '')]">Completed</a>
    </div>
    <div mv-if="totalCount" style="color: black;">[activeCount] item<span mv-if="activeCount != 1">s</span> left</div>
    <ul>
      {{! If we use mv-if instead of "display: none;", then Mavo goes crazy
          after we save an edit that hides an item. }}
      <li property="TaskView" mv-multiple style="[if(hide, 'display: none;', '')]">
        {{! For some reason, when this is a plain "meta property", Mavo removes
            the "hide" property from tasks where it is false and we fail to
            write that change to the server.  TODO: investigate. }}
        <input type="checkbox" property="hide" style="display: none;"/>
        <label>
          <input type="checkbox" property="completed">
          <span property="title"></span>
        </label>
      </li>
    </ul>
    <div mv-if="totalCount" style="color: black; margin-top: 0.5em;"><input type="checkbox" property="allCompleted"> Mark all completed (save to apply)</div>
    <meta property="anyCompleted" style="display: none;"/>
    <div mv-if="anyCompleted"><button class="clear-completed" mv-call="clearCompleted">Clear Completed</button></div>
  </main>
</template>
