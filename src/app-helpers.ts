namespace Objsheets {

  // "Simple" forms of values for easier use in Mavo and not getting confused
  // when converting singletons back to families.  We may find a better
  // solution at some point.
  export type SimpleOSValue = boolean | number | string |
    /* simplified Date and CellId */ string;
  export function passSimpleOSValue(type: OSType, value: OSValue): SimpleOSValue {
    return typeIsReference(type) ? JSON.stringify(value) :
           // XXX Our simple date representation loses precision, and since we
           // diff, the precision will be lost on the server only if the
           // low-resolution value is changed, which is inconsistent.  And any
           // caller of readObj that actually manipulates the date would
           // probably prefer a more machine-friendly representation, but the
           // formula language currently has such limited support for dates that
           // such a caller is more likely to put the unix time in a SpecialType.NUMBER
           // type instead.  If the latter ceases to be the case, we can add
           // back a parameter to choose one of two representations, but
           // hopefully by then we can just get Mavo to support the
           // machine-friendly representation. ~ Matt 2017-04-07
           (type == SpecialType.DATE) ? stringifyDate(unpackDate(value)) :
           <SimpleOSValue>value;
  }
  export function acceptSimpleOSValue(type: OSType, value: SimpleOSValue): OSValue {
    return typeIsReference(type) ? <CellId>JSON.parse(<string>value) :
           (type == SpecialType.DATE) ? packDate(parseDate(<string>value)) :
           value;
  }

  // Canonical object dump format returned by readObj.
  //
  // So, this is useful as documentation, but some callers just want to access
  // dotted properties.  Guess they can convert to fixmeAny.
  export interface ObjectDump extends EJSONableDict {
    // Reference to the object.
    _ref: SimpleOSValue;
    // Display string of the object.  (This is typically redundant with the
    // object's reference display field, but this way you don't have to think
    // about which field or what to do if that field isn't a string!)
    _display: string;
    // If "foo" is a field of reference type, you also get "foo_display" that
    // contains the display strings.  Similar for the key field.
    [field: string]:
      null  // field of a "new" object for Mavo's benefit (see readObj)
      | FamilyDump  // normal field and its display
      | SimpleOSValue;  // key field and its display, "_ref", or "_display"
  }
  export type FamilyDump = ValueFamilyDump | ObjectFamilyDump;
  export type ValueFamilyDump = SimpleOSValue[];
  export type ObjectFamilyDump = ObjectDump[];

  // Object dump coming back from Mavo, which may lack _ref and _display and may
  // have singleton fields (and nested objects?).  A little duplication, but
  // trying to factor it out makes the declaration harder to read.
  //
  // Hack to work around
  // https://github.com/Microsoft/TypeScript/issues/17277 .
  interface MavoObjectDumpFields {
    [field: string]: SimpleOSValue | SimpleOSValue[] | MavoObjectDump | MavoObjectDump[];
  }
  export type MavoObjectDump = {
    _ref?: SimpleOSValue;
    _display?: string;
  } & MavoObjectDumpFields;

  export interface ObjectDiff extends EJSONableDict {
    [field: string]: FamilyDiff;
  }
  export type FamilyDiff = FamilyDiffItem[];
  export type FamilyDiffItem = {
    key: null | OSValue;  // null-ish for "new"
    add?: boolean;
    edit?: ObjectDiff;
    remove?: boolean;
  };

  export interface ObjectDiffResponse extends EJSONableDict {
    [field: string]: FamilyDiffResponse;
  }
  export type FamilyDiffResponse = FamilyDiffItemResponse[];
  export type FamilyDiffItemResponse = {
    key?: OSValue;  // set only for "new"
    edit?: ObjectDiffResponse;
  };

  export namespace MeteorMethods {
    export let writeDiff = new MeteorMethod3<TablespaceId, QCellId, ObjectDiff, null | ObjectDiffResponse>("writeDiff");
  }

}
