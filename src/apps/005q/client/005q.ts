namespace Objsheets {

  Router.route("/:sheet/apps/005q", function(this: fixmeAny) {
    this.render("DoubleOhFiveQueue", {data: this.params});
  });
  Router.route("/:sheet/apps/005q/staff", function(this: fixmeAny) {
    this.render("DoubleOhFiveQueueStaffLogin", {data: this.params});
  });
  Router.route("/:sheet/apps/005q/staff/:cellId", function(this: fixmeAny) {
    this.render("DoubleOhFiveQueueStaff", {data: this.params});
  });
  Template["DoubleOhFiveQueue"].rendered = Mavo.init;
  Template["DoubleOhFiveQueueStaffLogin"].rendered = Mavo.init;

  Template["DoubleOhFiveQueueStaff"].helpers({
    encodeURI: encodeURI
  });

  Template["DoubleOhFiveQueueStaff"].rendered = Mavo.init;

}
