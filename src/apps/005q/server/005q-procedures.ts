namespace Objsheets {

  Meteor.startup(() => {  // load order for ServerAppHelpers
    ServerAppHelpers.procedures("005q", {
      enqueue: {
        params: [["name", SpecialType.TEXT], ["issue", SpecialType.TEXT], ["location", SpecialType.TEXT]],
        body: `
          let q = new $Call
          q.time := now()
          q.name := name
          q.location := location
          q.issue := {s : $Skill | s.name = issue}
        `
      },
      pick: {
        params: [["call", "Call"], ["user", "Staff"]],
        body: `
          call.assign := user
        `
      },
      forfeit: {
        params: [["call", "Call"], ["user", "Staff"]],
        body: `
          call.assign := {}
          call.forfeit := user
        `
      },
      done: {
        params: [["call", "Call"]],
        body: `
          delete call
        `
      }
    });
  });

}
