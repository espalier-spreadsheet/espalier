namespace Objsheets {

  Router.route("/:sheet/apps/conf", function(this: fixmeAny) {
    this.render("ConfLogin", {data: this.params});
  });

  Router.route("/:sheet/apps/conf/user/:cellId", function(this: fixmeAny) {
    this.render("ConfProfile", {data: this.params});
  });

  Router.route("/:sheet/apps/conf/user/:cellId/papers", function(this: fixmeAny) {
    this.render("ConfPapers", {data: this.params});
  });

  Router.route("/:sheet/apps/conf/paper/:cellId", function(this: fixmeAny) {
    this.render("ConfPaper", {data: this.params});
  });

  for (let key of ["ConfLogin", "ConfProfile", "ConfPapers", "ConfPaper"]) {
    Template[key].rendered = Mavo.init;
    Template[key].helpers({
      encodeURI: encodeURI
    });
  }

}
