namespace Objsheets {

  Meteor.startup(() => {  // load order for ServerAppHelpers
    ServerAppHelpers.procedures("conf", {
      viewUpdateSpec: {
        // SelfProfileView
        "ClientUserView:SelfProfileView:email": {editableIf: "true"},
        "ClientUserView:SelfProfileView:name": {editableIf: "true"},
        "ClientUserView:SelfProfileView:affiliation": {editableIf: "true"},

        // UserAdminView
        "ClientUserView:UserAdminView:level": {editableIf: "true"},

        // PaperView

        "ClientUserView:PaperView": {
          editableIf: "true",
          newTrigger: `
            let result = new $Paper
            result.isNew := true
            result.author := ctxt
          `
          // removeTrigger is undefined: you can't remove.
        },

        "ClientUserView:PaperView:coauthors": {editableIf: "paper.isNew"},

        // Note: We'd like to default the paper PC conflicts to the user PC
        // conflicts, but that can only be done properly on the client because the
        // client will always send a diff starting from an empty PaperView, so it
        // would be impossible to remove any of the default conflicts.
        "ClientUserView:PaperView:pcConflicts": {editableIf: /*"paper.isNew"*/ "true"},
        "ClientUserView:PaperView:pcConflicts_text": {
          editableIf: "true",
          addTrigger: "to set paper.pcConflicts add {u : $pcMembers | u.name = value}",
          removeTrigger: "from set paper.pcConflicts remove {u : $pcMembers | u.name = value}"
        },

        // Editing latest{Title,Abstract,Contents} triggers creation of a new Version.
        // This version will have isNew set; it will be cleared on cleanup.
        // Note: Trigger code depends on the trait that when a new version is created
        // (with time = now) it immediately becomes the latestVersion.

        "ClientUserView:PaperView:latestTitle": {
          // TODO: Once editability is automatically exposed to Mavo, we'll want
          // this to be editable if "ClientUserView.ctxt = paper.author".
          // Figure out how to avoid duplicating this logic from VersionView.
          editableIf: "true",
          // Note: A beforeEditTrigger would make this a lot nicer.
          addTrigger: `
            if (paper.latestVersion.isNew != true) {  # "clever" hack so this is true when there are no versions yet
              new VersionView
            }
            to set paper.latestVersion.title add value
          `,
          removeTrigger: `
            if (paper.latestVersion.isNew != true) {
              new VersionView
            }
            from set paper.latestVersion.title remove value
          `
        },

        "ClientUserView:PaperView:latestAbstract": {
          editableIf: "true",
          addTrigger: `
            if (paper.latestVersion.isNew != true) {
              new VersionView
            }
            to set paper.latestVersion.abstract add value
          `,
          removeTrigger: `
            if (paper.latestVersion.isNew != true) {
              new VersionView
            }
            from set paper.latestVersion.abstract remove value
          `
        },

        "ClientUserView:PaperView:latestContents": {
          editableIf: "true",
          addTrigger: `
            if (paper.latestVersion.isNew != true) {
              new VersionView
            }
            to set paper.latestVersion.contents add value
          `,
          removeTrigger: `
            if (paper.latestVersion.isNew != true) {
              new VersionView
            }
            from set paper.latestVersion.contents remove value
          `
        },

        // VersionView
        // XXX: Do we want to stop an author from adding multiple versions during
        // the initial paper submission?
        "ClientUserView:PaperView:VersionView": {
          editableIf: "ClientUserView.ctxt = paper.author",
          newTrigger: `
            let previousVersion = paper.latestVersion
            let result = new paper.Version
            result.isNew := true
            result.time := now()
            # Filling in the following is OK because VersionView only receives
            # "new" from other triggers, which expect this behavior, not from
            # diffs.
            result.title := previousVersion.title
            result.abstract := previousVersion.abstract
            result.contents := previousVersion.contents
          `
          // removeTrigger is undefined: you can't remove.
        },
        "ClientUserView:PaperView:VersionView:title": {editableIf: "version.isNew"},
        "ClientUserView:PaperView:VersionView:abstract": {editableIf: "version.isNew"},
        "ClientUserView:PaperView:VersionView:contents": {editableIf: "version.isNew"},

        // ReviewView
        "ClientUserView:PaperView:ReviewView": {
          // There are various other cases in which a user sees a PaperView, so
          // check this condition to submit a review.
          editableIf: "paper in ClientUserView.papersAsPc",
          newTrigger: `
            let result = new paper.Review
            result.isNew := true
            result.reviewer := ClientUserView.ctxt
            result.time := now()
          `
          // removeTrigger is undefined: you can't remove.
        },
        "ClientUserView:PaperView:ReviewView:contents": {editableIf: "review.isNew"},
        "ClientUserView:PaperView:ReviewView:score_novelty": {editableIf: "review.isNew"},
        "ClientUserView:PaperView:ReviewView:score_presentation": {editableIf: "review.isNew"},
        "ClientUserView:PaperView:ReviewView:score_technical": {editableIf: "review.isNew"},
        "ClientUserView:PaperView:ReviewView:score_confidence": {editableIf: "review.isNew"},

        // CommentView
        "ClientUserView:PaperView:CommentView": {
          // There are various other cases in which a user sees a PaperView, so
          // check this condition to submit a review.
          editableIf: "paper in ClientUserView.papersAsPc",
          newTrigger: `
            let result = new paper.Comment
            result.isNew := true
            result.user := ClientUserView.ctxt
            result.time := now()
          `
          // removeTrigger is undefined: you can't remove.
        },
        "ClientUserView:PaperView:CommentView:contents": {editableIf: "comment.isNew"},

        // For now, assume that the chair assigns reviewers via PaperView.
        "ClientUserView:PaperView:assignedReviewers": {
          editableIf: `ClientUserView.ctxt.level = "chair"`,
          // Formula: if(paper in ClientUserView.papersAsPc, paper.assignedReviewers, {})
          // We could teach the reverse engineering code to handle that, but first confirm we are keeping this formula!
          addTrigger: `to set paper.assignedReviewers add value`,
          removeTrigger: `from set paper.assignedReviewers remove value`
        }
      },

      // Cleanup

      cleanup: {
        params: [],
        body: `
          $Paper.isNew := false
          $Paper.Version.isNew := false
          $Paper.Review.isNew := false
          $Paper.Comment.isNew := false
          check $valid
        `
      },
    });
  });

}
