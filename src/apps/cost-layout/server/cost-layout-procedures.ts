// Not procedures at all: misnomer. :(

namespace Objsheets.Layout.Examples {

  // Has to be deferred because parse* functions depend on the current tablespace.
  function ProjectMember<_>() {
    // Can we have nameof?  Try the workaround from
    // https://github.com/Microsoft/TypeScript/issues/1579 ?
    const
      rDept = R("rDept"),
       rDeptHeader = R("rDeptHeader"),
       rPeriods = R("rPeriods"),
        // The original example shows only a single Department:PeriodInfo and
        // starts at rPeriod x root.  Right now, we don't have support for
        // showing a subtree of the data set, so show the whole thing and add a
        // department name header for clarity.
        rPeriod = R("rPeriod"),
         rPeriodHeader = R("rPeriodHeader"),
         rColumnHeaders1 = R("rColumnHeaders1"),
         rColumnHeaders2 = R("rColumnHeaders2"),
         rProjects = R("rProjects"),
          rProject = R("rProject"),
           rProjectDiv = R("rProjectDiv"),
            rEntry = R("rEntry"),
         rPeriodTotal = R("rPeriodTotal"),
      // Whee, merged scopes!  We no longer need to introduce explicit
      // intermediate fixed splits to use as scopes for row splits.
      cProject = C("cProject"),
      cPeriod = C("cPeriod"),
      // Division row split starts here.
      cDiv = C("cDiv"),
      // Entry row split starts here.
      cRole = C("cRole"),
      cName = C("cName"),
      cEntryEffort = C("cEntryEffort"),
      // cDivEffort has non-leaf allocations across the entry row split.
      cDivEffort = C("cDivEffort"),
      cActivities = C("cActivities");
      // Entry and division row splits end here.

    return ExampleLayout<_>({
      _id: rootColumnId,
      splits: PA([
        VariableSplit(rRoot, PE(cProject, cActivities), rDept, "Department"),
        FixedSplit(rDept, PE(cProject, cActivities), [rDeptHeader, rPeriods]),
        VariableSplit(rPeriods, PE(cProject, cActivities), rPeriod, "Department:PeriodInfo"),
        FixedSplit(rPeriod, PE(cProject, cActivities),
          [rPeriodHeader, rColumnHeaders1, rColumnHeaders2, rProjects, rPeriodTotal]),
        VariableSplit(rProjects, PE(cProject, cActivities), rProject, "Department:PeriodInfo:ProjectInfo"),
        VariableSplit(rProject, PE(cDiv, cActivities), rProjectDiv,
          "Department:PeriodInfo:ProjectInfo:ProjDivSupportInfo"),
        VariableSplit(rProjectDiv, PE(cRole, cActivities), rEntry,
          "Department:PeriodInfo:ProjectInfo:ProjDivSupportInfo:SupportEntry"),
      ], [
        FixedSplit(cRoot, rRoot,
          [cProject, cPeriod, cDiv, cRole, cName, cEntryEffort, cDivEffort, cActivities]),
      ]),
      allocations: [
        FieldAlloc(rDeptHeader, PE(cProject, cActivities), "Department:name"),
        FieldAlloc(rPeriodHeader, cProject, "Department:PeriodInfo:period"),
        LabelAlloc(PE(rColumnHeaders1, rColumnHeaders2), cProject, "Support Item"),
        LabelAlloc(PE(rColumnHeaders1, rColumnHeaders2), cPeriod, "Support Period"),
        LabelAlloc(PE(rColumnHeaders1, rColumnHeaders2), cDiv, "Support Unit"),
        LabelAlloc(PE(rColumnHeaders1, rColumnHeaders2), cRole, "Role"),
        LabelAlloc(PE(rColumnHeaders1, rColumnHeaders2), cName, "Name"),
        LabelAlloc(rColumnHeaders1, PE(cEntryEffort, cDivEffort), "Support Effort (man-month)"),
        LabelAlloc(rColumnHeaders2, cEntryEffort, "by Role"),
        LabelAlloc(rColumnHeaders2, cDivEffort, "by Div."),
        LabelAlloc(PE(rColumnHeaders1, rColumnHeaders2), cActivities, "Activities"),
        // Note: all field values get converted to text.
        FieldAlloc(rProject, cProject, "Department:PeriodInfo:ProjectInfo:project"),
        FieldAlloc(rProject, cPeriod, "Department:PeriodInfo:ProjectInfo:period"),
        FieldAlloc(rProjectDiv, cDiv, "Department:PeriodInfo:ProjectInfo:ProjDivSupportInfo:division"),
        FieldAlloc(rEntry, cRole, "Department:PeriodInfo:ProjectInfo:ProjDivSupportInfo:SupportEntry:role"),
        FieldAlloc(rEntry, cName, "Department:PeriodInfo:ProjectInfo:ProjDivSupportInfo:SupportEntry:employee"),
        FieldAlloc(rEntry, cEntryEffort, "Department:PeriodInfo:ProjectInfo:ProjDivSupportInfo:SupportEntry:effort"),
        FieldAlloc(rProjectDiv, cDivEffort, "Department:PeriodInfo:ProjectInfo:ProjDivSupportInfo:effort"),
        FieldAlloc(rEntry, cActivities, "Department:PeriodInfo:ProjectInfo:ProjDivSupportInfo:SupportEntry:activities"),
        LabelAlloc(rPeriodTotal, PE(cProject, cName), "Total Support Effort (man-month)"),
        FieldAlloc(rPeriodTotal, PE(cEntryEffort, cDivEffort), "Department:PeriodInfo:effort")
      ]
    });
  }

  function FinancialAssistant<_>() {
    // See http://localhost:3000/cost-layout/views/financial-assistant .

    const
      rDeptHeader = R("rDeptHeader"),
      // The original example shows a single department, excluding rDeptHeader,
      // which we add for clarity.  See explanation for ProjectMember.
      rColumnHeaders1 = R("rColumnHeaders1"),
      rColumnHeaders2 = R("rColumnHeaders2"),
      rProjects = R("rProjects"),
       rProject = R("rProject"),
        rProjectPeriod = R("rProjectPeriod"),
      cProjectName = C("cProjectName"),
      // Skip "requested customer": unclear on what it is and it isn't important
      // as part of the example of 2D layout.
      // Period row split starts here.
      cPeriod = C("cPeriod"),
      cResourceCharges = C("cResourceCharge"),
       cDivResourceCharge = C("cDivResourceCharge"),
      // Period row split ends here.
      cProjectTotal = C("cProjectTotal");

    return ExampleLayout<_>({
      _id: parseObjectTypeRef("FinancialAssistantView"),
      splits: PA([
        FixedSplit(rRoot, PE(cProjectName, cProjectTotal),
          [rDeptHeader, rColumnHeaders1, rColumnHeaders2, rProjects]),
        VariableSplit(rProjects, PE(cProjectName, cProjectTotal), rProject, "FinancialAssistantView:ProjectView"),
        VariableSplit(rProject, PE(cPeriod, cResourceCharges), rProjectPeriod,
          "FinancialAssistantView:ProjectView:PeriodView"),
      ], [
        FixedSplit(cRoot, rRoot,
          [cProjectName, cPeriod, cResourceCharges, cProjectTotal]),
        VariableSplit(cResourceCharges, PE(rColumnHeaders2, rProjects), cDivResourceCharge,
          "FinancialAssistantView:DeptDivisionInfo"),
      ]),
      allocations: [
        FieldAlloc(rDeptHeader, PE(cProjectName, cProjectTotal), "FinancialAssistantView:department"),
        FieldAlloc(rProjectPeriod, cDivResourceCharge,
          "FinancialAssistantView:ProjectView:PeriodView:ProjDivSupportInfo2:effort"),
        LabelAlloc(PE(rColumnHeaders1, rColumnHeaders2), cProjectName, "Project Name"),
        LabelAlloc(PE(rColumnHeaders1, rColumnHeaders2), cPeriod, "Period"),
        LabelAlloc(rColumnHeaders1, cResourceCharges, "Resource Charge"),
        LabelAlloc(PE(rColumnHeaders1, rColumnHeaders2), cProjectTotal, "Sub-total"),
        FieldAlloc(rColumnHeaders2, cDivResourceCharge, "FinancialAssistantView:DeptDivisionInfo:division"),
        FieldAlloc(rProject, cProjectName, "FinancialAssistantView:ProjectView:project"),
        FieldAlloc(rProjectPeriod, cPeriod, "FinancialAssistantView:ProjectView:PeriodView:period"),
        FieldAlloc(rProject, cProjectTotal, "FinancialAssistantView:ProjectView:effort"),
      ],
      crossBindings: [
        [PA(rProject, cDivResourceCharge), {
          type: parseObjectTypeRef("FinancialAssistantView:ProjectView:ProjDivCross"),
          ownerAxis: Axis.ROW,
          crossKeyIsKey: true
        }],
        [PA(rProjectPeriod, cDivResourceCharge), {
          type: parseObjectTypeRef("FinancialAssistantView:ProjectView:PeriodView:ProjDivSupportInfo2"),
          ownerAxis: Axis.ROW,
          crossKeyIsKey: true
        }],
      ]
    });
  }

  Meteor.startup(() => {  // load order for ServerAppHelpers
    let procedures: ServerAppHelpers.AppPreProcedures = {
      layouts: () => [
        ProjectMember(),
        FinancialAssistant(),
      ]
    };

    ServerAppHelpers.procedures("cost-layout", procedures);
    ServerAppHelpers.procedures("cost-layout-large", procedures);
  });

}
