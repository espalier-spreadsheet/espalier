// Load order for Axis and Extreme
/// <reference path="../../../layout/core.ts" />
namespace Objsheets.Layout.Examples {

  function Crosses<_>() {
    const rMonthHeader = R("rMonthHeader"), rDayHeader = R("rDayHeader"), rBody = R("rBody");
    const rState = R("rState"), rCity = R("rCity");
    const cStateHeader = C("cStateHeader"), cCityHeader = C("cCityHeader"), cBody = C("cBody");
    const cMonth = C("cMonth"), cDay = C("cDay");

    return ExampleLayout<_>({
      _id: rootColumnId,
      splits: PA([
        FixedSplit(rRoot, cRoot, [rMonthHeader, rDayHeader, rBody]),
        VariableSplit(rBody, PE(cStateHeader, cBody), rState, "State"),
        VariableSplit(rState, PE(cCityHeader, cBody), rCity, "State:City"),
      ], [
        FixedSplit(cRoot, rRoot, [cStateHeader, cCityHeader, cBody]),
        VariableSplit(cBody, PE(rMonthHeader, rBody), cMonth, "Month"),
        VariableSplit(cMonth, PE(rDayHeader, rBody), cDay, "Month:Day"),
      ]),
      allocations: [
        FieldAlloc(rState, cStateHeader, "State:name"),
        FieldAlloc(rCity, cCityHeader, "State:City:name"),
        FieldAlloc(rMonthHeader, cMonth, "Month:name"),
        FieldAlloc(rDayHeader, cDay, "Month:Day:number"),
        FieldAlloc(rCity, cDay, "State:City:CityMonth:CityDay:temperatureF"),
      ],
      crossBindings: [
        [PA(rState, cMonth), {
          type: parseObjectTypeRef("State:StateMonth"),
          ownerAxis: Axis.ROW,
          crossKeyIsKey: false
        }],
        [PA(rState, cDay), {
          type: parseObjectTypeRef("State:StateMonth:StateDay"),
          ownerAxis: Axis.ROW,
          crossKeyIsKey: false
        }],
        [PA(rCity, cMonth), {
          type: parseObjectTypeRef("State:City:CityMonth"),
          ownerAxis: Axis.ROW,
          crossKeyIsKey: false
        }],
        [PA(rCity, cDay), {
          type: parseObjectTypeRef("State:City:CityMonth:CityDay"),
          ownerAxis: Axis.ROW,
          crossKeyIsKey: false
        }],
      ]
    });
  }

  Meteor.startup(() => {  // load order for ServerAppHelpers
    ServerAppHelpers.procedures("crosses", {
      layouts: () => [
        Crosses()
      ]
    });
  });

}
