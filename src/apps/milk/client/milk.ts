namespace Objsheets {

  Router.route("/:sheet/apps/maid", function(this: fixmeAny) {
    this.render("MilkMaid", {data: this.params});
  });

  Template["MilkMaid"].rendered = Mavo.init;

}
