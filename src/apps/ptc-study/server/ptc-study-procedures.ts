namespace Objsheets {

  Meteor.startup(() => {  // load order for ServerAppHelpers
    ServerAppHelpers.procedures("ptc-study", {
      parentScheduleMeeting: {
        params: [["block", "FamilyPage:EnrollmentBlock:AvailableSlotBlock"]],
        body: `
          check false
        `
      },
      parentCancelMeeting: {
        params: [["block", "FamilyPage:EnrollmentBlock:ExistingMeetingBlock"]],
        body: `
          check false
        `
      }
    });
  });

}
