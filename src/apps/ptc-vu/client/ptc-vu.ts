namespace Objsheets {

  Router.route("/:sheet/apps/ptc-vu", function(this: fixmeAny) {
    this.redirect(`/${this.params.sheet}/apps/ptc-vu/parent`);
  });
  Router.route("/:sheet/apps/ptc-vu/parent", function(this: fixmeAny) {
    this.render("PTC_Parent_login", {data: this.params});
  });
  Template["PTC_Parent_login"].rendered = Mavo.init;

  Router.route("/:sheet/apps/ptc-vu/parent/:cellId", function(this: fixmeAny) {
    this.render("PTC_Parent", {data: this.params});
  });
  Template["PTC_Parent"].rendered = Mavo.init;
  Template["PTC_Parent"].helpers({
    encodeURI: encodeURI
  });

  Router.route("/:sheet/apps/ptc-vu/teacher", function(this: fixmeAny) {
    this.render("PTC_Teacher_login", {data: this.params});
  });
  Template["PTC_Teacher_login"].rendered = Mavo.init;

  Router.route("/:sheet/apps/ptc-vu/teacher/:cellId", function(this: fixmeAny) {
    this.render("PTC_Teacher", {data: this.params});
  });
  Template["PTC_Teacher"].rendered = Mavo.init;
  Template["PTC_Teacher"].helpers({
    encodeURI: encodeURI
  });

  Router.route("/:sheet/apps/ptc-vu/calendar", function(this: fixmeAny) {
    this.render("PTC_Calendar", {data: this.params});
  });
  Template["PTC_Calendar"].rendered = Mavo.init;

}
