namespace Objsheets {

/*
Test case (from the server shell):

Objsheets.ServerTablespace.runIn("ptc-vu", () => {
  new Objsheets.FamilyHandle({
    columnId: Objsheets.parseColumnRef("ParentView:[student]:[enrollment]:scheduledSlot")[0],
    parentCellId:
      Objsheets.parseValue(Objsheets.parseColumnRef("ParentView:[student]:[enrollment]")[0], "Ronald Weasley in 6.005")
  }).add_Server(
    Objsheets.parseValue(Objsheets.parseColumnRef("Person:Teacher:Slot")[0], "Filius Flitwick @ 2014-12-16 13:00")
  );
})
*/

  Meteor.startup(() => {  // load order for ServerAppHelpers
    let procedures: ServerAppHelpers.AppPreProcedures = {
      viewUpdateSpec: {
        "Class:Section:Enrollment:scheduledSlot": {
          // Formula: {s : $Person.Slot | Enrollment in s.scheduledEnrollment}
          //
          // This is the canonical formula for an inverse relation; the special
          // syntax we contemplated is {$Person.Slot by scheduledEnrollment} (the
          // current Enrollment is implicit as the context).  Below are the
          // corresponding general form of the triggers.  For a relation that is
          // singular on both sides, we could write "=" in place of "in", but then
          // it's less clear that the correct way for the remove trigger to make
          // s.scheduledEnrollment unequal to Enrollment is to set it to {}.
          editableIf: "true"
        },
        "ParentView:StudentView:EnrollmentView:scheduledSlot": {
          // Formula: enrollment.scheduledSlot
          //
          // This is just a passthrough.
          editableIf: "true"
        },
        "TeacherView:SlotView": {
          // Formula: clientUser.Slot
          //
          // This is just a passthrough to an object family using new instead of add.
          editableIf: "true"
        },
        "TeacherView:SlotView:time": {
          // Formula: slot.time
          //
          // Passthrough, except we don't allow the time of a slot to be changed if
          // it has a meeting scheduled.  If there's no meeting, then changing the
          // time is harmless except for a possible race with a parent who viewed
          // the old time and is scheduling a meeting.  We can claim this is a
          // semantic edit conflict we currently don't handle, or we can use the
          // isNew pattern to disallow changing times of existing slots at all.
          editableIf: "scheduledEnrollment = {}"
        }
      },

      cleanup: {
        params: [],
        body: `
          check $valid
        `
      }
    };

    ServerAppHelpers.procedures("ptc-vu", procedures);
    ServerAppHelpers.procedures("ptc-vu-small", procedures);
  });

}
