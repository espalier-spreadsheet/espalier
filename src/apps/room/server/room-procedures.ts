namespace Objsheets.Layout.Examples {

  function MockupLayout<_>() {
    const
      rRoomHeaders = R("rRoomHeaders"),
      rRooms = R("rRooms"),
        rRoom = R("rRoom"),
          rOccupant = R("rOccupant"),
      rRoleHeaders = R("rRoleHeaders"),
      rRoles = R("rRoles"),
        rRole = R("rRole"),
      rExtra1 = R("rExtra1"), rExtra2 = R("rExtra2"), rExtra3 = R("rExtra3"),
      c1 = C("c1"), c2 = C("c2"), c3 = C("c3"), c4 = C("c4"), c5 = C("c5"), c6 = C("c6"), c7 = C("c7");

    return ExampleLayout<_>({
      _id: rootColumnId,
      splits: PA([
        FixedSplit(rRoot, cRoot,
          [rRoomHeaders, rRooms, rRoleHeaders, rRoles, rExtra1, rExtra2, rExtra3]),
        VariableSplit(rRooms, PE(c1, c5), rRoom, "Room"),
        VariableSplit(rRoom, PE(c3, c5), rOccupant, "Room:Occupant"),
        VariableSplit(rRoles, PE(c1, c2), rRole, "Role"),
      ], [
        FixedSplit(cRoot, rRoot, [c1, c2, c3, c4, c5, c6, c7]),
      ]),
      allocations: [
        LabelAlloc(rRoomHeaders, c1, "Room"),
        LabelAlloc(rRoomHeaders, c2, "Sq foot"),
        LabelAlloc(rRoomHeaders, c3, "Occupant"),
        LabelAlloc(rRoomHeaders, c4, "Role"),
        LabelAlloc(rRoomHeaders, c5, "Alloc"),
        FieldAlloc(rRoom, c1, "Room:name"),
        FieldAlloc(rRoom, c2, "Room:sqFoot"),
        FieldAlloc(rOccupant, c3, "Room:Occupant:name"),
        FieldAlloc(rOccupant, c4, "Room:Occupant:role"),
        FieldAlloc(rOccupant, c5, "Room:Occupant:alloc"),
        LabelAlloc(rRoleHeaders, c1, "Role"),
        LabelAlloc(rRoleHeaders, c2, "Alloc"),
        FieldAlloc(rRole, c1, "Role:title"),
        FieldAlloc(rRole, c2, "Role:allocSpace"),
      ]
    });
  }

/*
I wanted to include an example procedure for the room application in a talk, so
I wrote it here to test it.  There's no application UI, but the procedure can be
called manually from the browser's developer console:

role = Objsheets.parseValue(Objsheets.parseTypeStr("Role"), "Grad. student");
Objsheets.ClientAppHelpers.call("assign", {name: ["Harry"], role: [role]});

~ Matt 2016-10-04
*/

  Meteor.startup(() => {  // load order for ServerAppHelpers
    ServerAppHelpers.procedures("room", {
      assign: {
        params: [["name", SpecialType.TEXT], ["role", "Role"]],
        body: `
          let spaceNeeded = role.allocSpace
          let room = oneOf({r : $Room | r.free >= spaceNeeded})
          let occupant = new room.Occupant
          occupant.name := name
          occupant.role := role
        `
      },
      layouts: () => [MockupLayout()]
    });
  });

}
