namespace Objsheets {

  Router.route("/:sheet/apps/todo", function(this: fixmeAny) {
    this.redirect(`/${this.params.sheet}/apps/todo/all`);
  });
  Router.route("/:sheet/apps/todo/:subset", function(this: fixmeAny) {
    this.render("Todo", {data: this.params});
  });

  Template["Todo"].rendered = Mavo.init;

}
