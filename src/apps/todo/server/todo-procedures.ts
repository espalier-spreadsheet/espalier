namespace Objsheets {

  Meteor.startup(() => {  // load order for ServerAppHelpers
    ServerAppHelpers.procedures("todo", {
      viewUpdateSpec: {
        "SubsetView:allCompleted": {
          editableIf: "true",
          // We are cheating a little.  We really want a "set" trigger.
          removeTrigger: "",
          addTrigger: "$Task.completed := value"
        },

        "SubsetView:TaskView": {editableIf: "true"},
        "SubsetView:TaskView:title": {editableIf: "true"},
        "SubsetView:TaskView:completed": {editableIf: "true"}
      },

      clearCompleted: {
        params: [],
        body: `
          delete {t : $Task | t.completed}
        `
      }
    });
  });

}
