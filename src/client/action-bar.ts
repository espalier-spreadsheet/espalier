// Note, this is equivalent to:
// namespace Objsheets { *export* namespace ActionBar { ... } }
namespace Objsheets.ActionBar {

  export interface ActionBarTemplateInstance extends Blaze.TemplateInstance {
    // For callers to poke at.
    fullTextToShow: ReactiveVar<null | string>;
    isLoading: ReactiveVar<boolean>;
    changeColumnArgs: ReactiveVar<ChangeColumnData[]>;

    // Internal.
    //
    // Some of these might make more sense on the ChangeColumnTemplateInstance,
    // but I wanted to get rid of the global variables in the simplest way
    // possible without spending time redesigning this code. ~ Matt 2018-01-30
    actionBarMode: ReactiveVar<ActionBarMode>;
    currentExpansionMode: ReactiveVar<null | ExpansionMode>;
    origFormulaStr: ReactiveVar<fixmeNullable<string>>;
    newFormulaStr: ReactiveVar<fixmeNullable<string>>;
    newFormulaInfo: ReactiveVar<fixmeNullable<FormulaInfo>>;
    tracingView: fixmeNullable<TracingView>;
    typeMenuCommonItems: ReactiveVar<HtmlSelectItem[]>;
    spinner: ExternalModules.Spinner;
    prevOperationInProgress: boolean;
  }

  enum ActionBarMode {
    STRUCTURE = "structure",
    GLOBAL_MACROS = "global-macros"
  }
  enum ExpansionMode {
    FORMULA_DEBUGGER,
    EDIT_CONFIG
  }
  function toggleExpansionMode(template: ActionBarTemplateInstance, mode: ExpansionMode) {
    template.currentExpansionMode.set(isExpansionModeOpen(template, mode) ? null : mode);
  }
  function isExpansionModeOpen(template: ActionBarTemplateInstance, mode: ExpansionMode) {
    return template.currentExpansionMode.get() == mode;
  }

  export const actionBarTemplateName = "actionBar";
  let actionBarTemplate: Blaze.Template1<ActionBarTemplateInstance> = Template[actionBarTemplateName];
  actionBarTemplate.created = function() {
    this.fullTextToShow = new ReactiveVar(null);
    this.isLoading = new ReactiveVar(true);
    this.changeColumnArgs = new ReactiveVar([], EJSON.equals);

    this.actionBarMode = new ReactiveVar(ActionBarMode.STRUCTURE);
    this.currentExpansionMode = new ReactiveVar(null);
    this.origFormulaStr = new ReactiveVar(null);
    this.newFormulaStr = new ReactiveVar(null);
    this.newFormulaInfo = new ReactiveVar(null);
    this.tracingView = null;
    this.typeMenuCommonItems = new ReactiveVar([]);
    this.spinner = new ExternalModules.Spinner({scale: 0.5, width: 3});
    this.prevOperationInProgress = false;

    // Scanning for all possible reference types is slow enough to make the selection
    // feel laggy, so cache the menu and reuse it.
    ClientAppHelpers.onOpen(() => {
      this.autorun(() => {
        // Note: It's possible to create cycles in the "key + parent" relation on
        // object types.  This is a pointless thing to do but does not break our
        // tool; it's as if all of those reference types were merely empty.  So
        // don't try to prevent it for now.
        let refItems: HtmlOption[] = [];
        function scan(colId: ColumnId) {
          let c = getExistingColumn(colId);
          if (colId !== rootColumnId && c.isObject) {
            refItems.push(new HtmlOption(colId, stringifyType(colId)));
          }
          for (let childId of c.children) {
            scan(childId);
          }
        }
        scan(rootColumnId);

        let items: HtmlSelectItem[] = [];
        items.push(new HtmlOptgroup("Basic types", MAIN_PRIMITIVE_TYPES.map((t) => new HtmlOption(t, t))));
        items.push(new HtmlOptgroup("Reference to:", refItems));
        this.typeMenuCommonItems.set(items);
      });
    });
  };
  actionBarTemplate.rendered = function() {
    ClientAppHelpers.onOpen(() => {
      this.autorun(() => {
        let operationInProgress = $$client.numOperationsInProgress() > 0;
        if (operationInProgress != this.prevOperationInProgress) {
          if (operationInProgress) {
            this.spinner.spin(this.find("#spinnerHost"));
          } else {
            this.spinner.stop();
          }
          this.prevOperationInProgress = operationInProgress;
        }
      });
    });
  };

  actionBarTemplate.helpers({
    loading: function() { return Template.instance(this).isLoading.get(); },
    fullTextToShow: function() { return Template.instance(this).fullTextToShow.get(); },
    isGlobalMacrosOpen: function() {
      return Template.instance(this).actionBarMode.get() == ActionBarMode.GLOBAL_MACROS;
    },
    changeColumnArgs: function() { return Template.instance(this).changeColumnArgs.get(); },
    canUndo: function() {
      return !hasUnsavedData(Template.instance(this)) && $$.canUndo();
    },
    canRedo: function() {
      return !hasUnsavedData(Template.instance(this)) && $$.canRedo();
    },
    showCollapseLink: function() {
      let expansionMode = Template.instance(this).currentExpansionMode.get();
      switch (expansionMode) {
      case ExpansionMode.FORMULA_DEBUGGER:
      case ExpansionMode.EDIT_CONFIG:
        let cca = arrayGet(Template.instance(this).changeColumnArgs.get(), 0);
        if (cca == null || cca.onObjectHeader) return true;
        let col = getExistingColumn(cca.columnId);
        return (col.formula == null || expansionMode == ExpansionMode.FORMULA_DEBUGGER && col.viewObjectType != null);
      case null:
        return false;
      }
    },
    hasUnsavedData: function() {
      return hasUnsavedData(Template.instance(this));
    },
    actionBarModeMenu: function() {
      let actionBar = Template.instance(this);
      let items = [new HtmlOption(ActionBarMode.STRUCTURE, "Structure"),
        new HtmlOption(ActionBarMode.GLOBAL_MACROS, "Global macros")];
      return new HtmlSelectData(items, actionBar.actionBarMode.get());
    },
  });

  actionBarTemplate.events<{}>({
    "change #actionBarMode": function(event, template) {
      let newMode = getValueOfSelectedOption(template, "#actionBarMode");
      template.actionBarMode.set(<ActionBarMode>newMode);
    },
    "click #undo": function(event, template) {
      MeteorMethods.undo.call($$.id, $$client.wrapCallback(standardServerCallback));
    },
    "click #redo": function(event, template) {
      MeteorMethods.redo.call($$.id, $$client.wrapCallback(standardServerCallback));
    },
    "click #cleanup": function(event, template) {
      MeteorMethods.executeCannedTransaction.call($$.id, TRIGGER_CLEANUP, {}, null,
        $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
          if (error == null) {
            alert("Successful cleanup.");
          }
        })));
    },
    "click #collapse-link": function(event, template) {
      template.currentExpansionMode.set(null);
    }
  });

  //
  // Template changeColumn
  //
  interface ChangeColumnData {
    _id: string;
    columnId: NonRootColumnId;
    onObjectHeader: null | boolean;
  }
  interface ChangeColumnTemplateInstance extends Blaze.TemplateInstance {
    actionBar: ActionBarTemplateInstance;
    codeMirror: fixmeNullable<CodeMirror.Editor>;
    codeMirrorDoc: fixmeNullable<CodeMirror.Doc>;
    formulaBarComputations: fixmeNullable<Tracker.Computation[]>;
    formulaBandsView: fixmeNullable<Blaze.View>;
  }

  let NESTED_UNDERLINING_PX_PER_LEVEL = 4;
  let NESTED_UNDERLINING_MAX_DEPTH = 5;

  function strForFormula(col: Column, formula: Formula) {
    return EJSON.equals(formula, DUMMY_FORMULA) ? "" :
      stringifyFormula(fixmeAssertNotNull(col.parent) /* root has no formula */, formula);
  }
  // We mainly care that this doesn't crash.
  function origFormulaStrForData(data: ChangeColumnData) {
    if (data.onObjectHeader) {
      return null;
    }
    let col = getColumn(data.columnId);
    if (col == null) return null;
    let formula = col.formula;
    return formula == null ? null : strForFormula(col, formula);
  }

  let changeColumnTemplateName = "changeColumn";
  let changeColumnTemplate: Blaze.Template1<ChangeColumnTemplateInstance> = Template[changeColumnTemplateName];
  changeColumnTemplate.created = function() {
    this.actionBar = <ActionBarTemplateInstance>findAncestorTemplateInstanceOfView(this.view, actionBarTemplateName);
  };
  changeColumnTemplate.rendered = function() {
    // XXX What if there are unsaved changes when the formula changes externally?
    this.autorun(() => {
      let data: ChangeColumnData = Template.currentData();
      let formulaStr = origFormulaStrForData(data);
      this.actionBar.origFormulaStr.set(formulaStr);
      this.actionBar.newFormulaStr.set(formulaStr);
    });
    this.autorun(() => {
      if (!isFormulaDebuggerActive(this)) {
        let info = this.actionBar.newFormulaInfo.get();
        if (info != null && info.selectedBand != null) {
          info.selectedBand.selected = false;
          info.selectedBand = undefined;
          // Retriggers this autorun, but it will do nothing the second time.
          this.actionBar.newFormulaInfo.set(info);
        }
        if (this.actionBar.tracingView != null) {
          this.actionBar.tracingView.destroy();
        }
        this.actionBar.tracingView = null;
      }
    });
    this.autorun(() => {
      let shouldShowFormulaBar = this.actionBar.newFormulaStr.get() != null;
      if (shouldShowFormulaBar && (this.codeMirror == null)) {
        // Have to wait for the template to re-render with the new div.
        // afterFlush will become unmaintainable if we push it much further, but
        // works for now and is easier than trying to figure out the referencing
        // for a child template.
        Tracker.afterFlush(() => {
          changeColumnInitFormulaBar(this);
        });
      } else if (!shouldShowFormulaBar && (this.codeMirror != null)) {
        // Should roughly reverse the things done in changeColumnInitFormulaBar.
        Blaze.remove(fixmeAssertNotNull(this.formulaBandsView));
        this.formulaBandsView = null;
        for (let c of fixmeAssertNotNull(this.formulaBarComputations)) {
          c.stop();
        }
        this.formulaBarComputations = null;
        this.actionBar.newFormulaInfo.set(null);
        // XXX Do we need to tear down the CodeMirror somehow?
        this.codeMirrorDoc = null;
        this.codeMirror = null;
      }
    });
  };

  changeColumnTemplate.destroyed = function() {
    // Try to avoid holding on to data that's no longer relevant.
    // XXX: Should we rather define the reactive vars on the template instance?
    // Then we'd need more Template.instance() from the helpers.
    if (this.actionBar.tracingView != null) {
      this.actionBar.tracingView.destroy();
    }
    this.actionBar.tracingView = null;
    this.actionBar.newFormulaInfo.set(null);
    this.actionBar.newFormulaStr.set(null);
    this.actionBar.origFormulaStr.set(null);
  };

  const TYPE_CHOICE_AUTO = "auto";
  type TypeChoice = OSType | typeof TYPE_CHOICE_AUTO;
  function origTypeChoice(col: Column) {
    return fallback<TypeChoice>(col.specifiedType, TYPE_CHOICE_AUTO);
  }
  function resetTypeChoice(template: ChangeColumnTemplateInstance) {
    let data: ChangeColumnData = template.data;
    selectOptionWithValue(template, "#changeColumn-type", origTypeChoice(getExistingColumn(data.columnId)));
  }
  function resetBackendChoice(template: ChangeColumnTemplateInstance) {
    let data: ChangeColumnData = template.data;
    (<HTMLInputElement>template.find("#changeColumn-computed")).checked =
      (getExistingColumn(data.columnId).formula != null);
  }
  function resetSingularChoice(template: ChangeColumnTemplateInstance) {
    let data: ChangeColumnData = template.data;
    (<HTMLInputElement>template.find("#changeColumn-multiple")).checked =
      !getExistingColumn(data.columnId).singular;
  }
  const RDC_CHOICE_AUTO = "auto";
  type RdcChoice = OSType | typeof RDC_CHOICE_AUTO;
  function origRdcChoice(col: Column) {
    return fallback<RdcChoice>(col.referenceDisplayColumn, RDC_CHOICE_AUTO);
  }
  function resetRdcChoice(template: ChangeColumnTemplateInstance) {
    let data: ChangeColumnData = template.data;
    selectOptionWithValue(template, "#changeColumn-referenceDisplayColumn",
      origRdcChoice(getExistingColumn(data.columnId)));
  }
  function isFormulaDebuggerActive(template: ChangeColumnTemplateInstance) {
    let data = <ChangeColumnData>Blaze.getData(template.view);
    return isExpansionModeOpen(template.actionBar, ExpansionMode.FORMULA_DEBUGGER) &&
      getExistingColumn(data.columnId).viewObjectType == null;
  }

  changeColumnTemplate.helpers<ChangeColumnData>({
    col: function() {
      return getColumn(this.columnId);
    },
    isFormulaModified: function() {
      let actionBar = Template.instance(this).actionBar;
      return actionBar.newFormulaStr.get() !== actionBar.origFormulaStr.get();
    },
    canSave: function() {
      let actionBar = Template.instance(this).actionBar;
      // Looks like this can be called before the autorun that sets newFormulaInfo.  Grr.
      let newFormulaInfo = actionBar.newFormulaInfo.get();
      return actionBar.newFormulaStr.get() !== actionBar.origFormulaStr.get() &&
        (newFormulaInfo != null && newFormulaInfo.formula != null);
    },
    columnName: function() {
      return stringifyColumnRef([this.columnId, !this.onObjectHeader]);
    },
    keyColumnName: function() {
      let c = getExistingColumn(this.columnId);
      return this.onObjectHeader && getColumnType(c) !== SpecialType.TOKEN ? c.fieldName : null;
    },
    typeMenu: function() {
      let actionBar = Template.instance(this).actionBar;
      let col = getExistingColumn(this.columnId);
      let items: HtmlSelectItem[] = [];
      if (col.formula != null) {
        // Note: Inferred type should match c.type if c.specifiedType is null and
        // there are no unsaved changes to the formula.
        let info = actionBar.newFormulaInfo.get();
        let inferredTypeDesc = info != null && info.type != null
          ? stringifyType(info.type) : SpecialType.ERROR;
        items.push(new HtmlOption(TYPE_CHOICE_AUTO, `auto (${inferredTypeDesc})`));
      }
      for (let item of actionBar.typeMenuCommonItems.get()) {
        items.push(item);
      }
      return new HtmlSelectData(items, origTypeChoice(col));
    },
    isComputed: function() {
      return getExistingColumn(this.columnId).formula != null;
    },
    isViewObjectRoot: function() {
      return getExistingColumn(this.columnId).isViewObjectRoot;
    },
    newFormulaInfo: function() {
      return Template.instance(this).actionBar.newFormulaInfo.get();
    },
    canDebug: function() {
      return getExistingColumn(this.columnId).viewObjectType == null;
    },
    isFormulaDebuggerActive: function() {
      return isFormulaDebuggerActive(Template.instance(this));
    },
    isEditConfigOpen: function() {
      return isExpansionModeOpen(Template.instance(this).actionBar, ExpansionMode.EDIT_CONFIG);
    },
    contextText: function() {
      let col = getExistingColumn(this.columnId);
      return col.isObject  // i.e., we are editing the formula of a key column
        ? objectNameForUI(getExistingColumn(col.parent))
        : null;
    },
    // Color-coding is much less useful with non-rainbow palettes.
    //contextColorIndex: ->
    //  col = getColumn(@columnId)
    //  if col.isObject
    //    colorIndexForDepth(columnDepth(col.parent))
    //  else null
    // Should only be called when onObjectHeader = true
    referenceDisplayColumnMenu: function() {
      let col = getExistingColumn(this.columnId);
      let defaultColId = defaultReferenceDisplayColumn(col);
      let defaultColDesc = defaultColId != null ?
        fallback(getExistingColumn(defaultColId).fieldName, "unnamed") : "none";
      let items = [new HtmlOption(RDC_CHOICE_AUTO, `Choose automatically (${defaultColDesc})`)];
      for (let displayColId of allowedReferenceDisplayColumns(col)) {
        let displayCol = getExistingColumn(displayColId);
        items.push(new HtmlOption(displayColId, fieldNameForUI(displayCol)));
      }
      return new HtmlSelectData(items, origRdcChoice(col));
    },

    // Edit config panel
    computedIsEditable: function() {
      return getEditabilityColumn(getExistingColumn(this.columnId)) != null;
    },
    editabilityFormulaStr: function() {
      let col = getExistingColumn(this.columnId);
      // Note: the original column and its editability column have the same parent.
      // Note: Template only calls this if `computedIsEditable` returned true.
      // Editability columns should be computed.
      return stringifyFormula(col.parent, assertNotNull(getEditabilityColumn(col)!.formula));
    },
    areTriggersDefault: function() {
      return getExistingColumn(this.columnId).areTriggersDefault;
    },
    triggerEntries: function() {
      let col = getExistingColumn(this.columnId);
      let entries = [];
      for (let triggerType of [TriggerType.NEW, TriggerType.ADD, TriggerType.REMOVE]) {
        let proc = Procedures.findOne({name: triggerName(col, triggerType)});
        if (proc != null) {
          // Don't bother to show the parameters because they are simple enough?
          entries.push({name: triggerType, body: stringifyProcedureBody(proc)});
        }
      }
      return entries;
    }
  });

  function changeColumnInitFormulaBar(template: ChangeColumnTemplateInstance) {
    template.codeMirror = CodeMirror(template.find("#changeFormula-formula"), {
      value: "",  // filled in by autorun below
      extraKeys: {
        Enter: (cm) => {
          template.find(".saveFormula").click();
        },
        Esc: (cm) => {
          template.find(".revertFormula").click();
        }
      }
    });
    template.codeMirrorDoc = template.codeMirror.getDoc();
    template.formulaBarComputations = [
      template.autorun(() => {
        // http://stackoverflow.com/a/15256593
        let height = fixmeAssertNotNull(template.codeMirror).defaultTextHeight() + 2 * 4;
        // Save room for a horizontal scrollbar for long formulas.  It's ugly to
        // have extra white space at other times, but I didn't see an obvious
        // good alternative.  (CodeMirror doesn't have an option to always show
        // the scrollbar, and when I tried to resize the CodeMirror based on the
        // presence of the scrollbar, I ran into some glitches.)
        // ~ Matt 2017-02-16
        height += (<fixmeAny>template.codeMirror).display.nativeBarWidth;
        if (isFormulaDebuggerActive(template)) {
          height += NESTED_UNDERLINING_PX_PER_LEVEL * NESTED_UNDERLINING_MAX_DEPTH;
        }
        fixmeAssertNotNull(template.codeMirror).setSize("100%", height);
      }), template.autorun(() => {
        let formulaStr = template.actionBar.newFormulaStr.get();
        if (formulaStr == null) {
          // When the formula is cleared, sometimes this runs before the autorun
          // that tears down the formula bar.  Grr, Meteor, how are we supposed to
          // avoid these problems in general?
          return;
        }
        // Avoid re-setting in response to user input, since this sends the cursor
        // back to the beginning.  Wish for a better two-way binding mechanism...
        if (formulaStr !== fixmeAssertNotNull(template.codeMirrorDoc).getValue()) {
          fixmeAssertNotNull(template.codeMirrorDoc).setValue(formulaStr);
        }
        template.actionBar.newFormulaInfo.set(generateFormulaInfo(template));
      })
    ];
    template.codeMirror.on("beforeChange", (cm, change) => {
      if (change.update != null) {
        let newtext = change.text.join("").replace(/\n/g, "");
        change.update(undefined, undefined, [newtext]);
      }
      // Otherwise, change is coming from undo or redo; hope it's OK.
    });
    template.codeMirror.on("changes", (cm) => {
      template.actionBar.newFormulaStr.set(fixmeAssertNotNull(template.codeMirrorDoc).getValue());
    });
    // Inject formula bands template into the scrollable part of the CodeMirror.
    // Relying on the CodeMirror-sizer having `position: relative`.
    template.formulaBandsView = Blaze.render(formulaBandsTemplate,
      template.find(".CodeMirror-sizer"), undefined, template.view);
  }

  interface FormulaInfo {
    formula?: Formula;
    type?: OSType;
    extrasMap?: FormulaExtrasMap;
    error?: string;
    root?: ExtendedSubformulaTree;
    bands?: FormulaBand[];
    selectedBand?: FormulaBand;
    haveTraced?: boolean;
  }
  interface FormulaBand {
    node: ExtendedSubformulaTree;
    selected: boolean;
    left: number;
    width: number;
    top: number;
    height: number;
  }
  interface ExtendedSubformulaTree extends SubformulaTree {
    height?: number;
    ch1?: number;
    ch2?: number;
    children: {paramName: string, node: ExtendedSubformulaTree}[];
  }
  function generateFormulaInfo(template: ChangeColumnTemplateInstance) {
    if (template.actionBar.tracingView != null) {
      template.actionBar.tracingView.destroy();
    }
    template.actionBar.tracingView = null;
    let formulaStr = fixmeAssertNotNull(template.actionBar.newFormulaStr.get());
    let formulaInfo: FormulaInfo = {};
    if (formulaStr === "") {
      formulaInfo.formula = DUMMY_FORMULA;
      formulaInfo.type = SpecialType.EMPTY;
      // Do not construct a subformula tree.
      return formulaInfo;
    }
    let parentColumnId = getExistingColumn(template.data.columnId).parent;
    try {
      let ret = parseFormulaWithExtras(parentColumnId, formulaStr);
      formulaInfo.formula = ret.formula;
      formulaInfo.extrasMap = ret.extrasMap;
      // Determine types of all subformulas.  Obviously in the future
      // we want to be able to help troubleshoot ill-typed formulas, but we punt
      // all error tolerance until later rather than fight that complexity now.
      formulaInfo.type = typecheckFormulaWithExtras(getReadableModel(), new JSONKeyedMap([["this", parentColumnId]]),
        formulaInfo.formula, formulaInfo.extrasMap);
    } catch (e) {
      if (!(e instanceof StaticError)) {
        throw e;
      }
      // TODO: More graceful error handling
      formulaInfo.error = e.message;
      return formulaInfo;
    }
    const extrasMap = formulaInfo.extrasMap;
    formulaInfo.root = getSubformulaTree(formulaInfo.formula);
    formulaInfo.bands = [];
    function layoutSubtree(node: ExtendedSubformulaTree) {
      // It looks silly to offer to debug a literal, though it's not harmful.  Open
      // to counterarguments. ~ Matt
      if (node.formula[0] === "lit") {
        node.height = -1;
        return;
      }
      let loc = fixmeAssertNotNull(assertNotNull(extrasMap.get(node.formula)).loc);
      node.ch1 = loc.first_column;
      node.ch2 = loc.last_column;
      node.height = 0;
      for (let childInfo of node.children) {
        // loc missing for implicit "this" inserted by resolveNavigation; other cases?
        if (assertNotNull(extrasMap.get(childInfo.node.formula)).loc != null) {
          layoutSubtree(childInfo.node);
          let isNavigationLhs = (node.formula[0] === "up" || node.formula[0] === "down")
            && childInfo.paramName === "start";
          if (isNavigationLhs) {
            node.ch1 = fixmeAssertNotNull(childInfo.node.ch2);
          }
          node.height = Math.max(node.height, fixmeAssertNotNull(childInfo.node.height) + (isNavigationLhs ? 0 : 1));
        }
      }
      let codeMirror = fixmeAssertNotNull(template.codeMirror);
      let top = 4 + codeMirror.defaultTextHeight() + NESTED_UNDERLINING_PX_PER_LEVEL * node.height;
      // Tweak for gaps in navigation chains.
      let x1 = codeMirror.cursorCoords({
        line: 0,
        ch: node.ch1
      }, "local").left;
      let x2 = codeMirror.cursorCoords({
        line: 0,
        ch: node.ch2
      }, "local").left;
      fixmeAssertNotNull(formulaInfo.bands).push({
        node: node,
        selected: false,
        left: x1,
        width: x2 - x1,
        top: top,
        height: NESTED_UNDERLINING_PX_PER_LEVEL
      });
    }
    layoutSubtree(formulaInfo.root);
    formulaInfo.selectedBand = undefined;
    formulaInfo.haveTraced = false;
    return formulaInfo;
  }

  class TracingView {
    public grid: ViewGrid;
    public hot: Handsontable;

    constructor(domElement: Element) {
      // TODO: Add a bunch more settings?
      this.grid = [];
      this.hot = new Handsontable(domElement, {
        readOnly: true,
        readOnlyCellClassName: "",  // Not useful to dim everything.
        cells: (row, col, prop) => {
          let gridRow = arrayGet(this.grid, row);
          let cell = gridRow != null ? arrayGet(gridRow, col) : null;
          if (!cell) {
            return {};  // Would like to understand why this is needed...
          }
          return {
            className: cell.cssClasses.join(" ")
          };
        }
      });
    }

    public show(node: ExtendedSubformulaTree, extrasMap: FormulaExtrasMap) {
      let formula = node.formula;
      let extras = assertNotNull(extrasMap.get(formula));
      function formatOutcome(outcome: Outcome) {
        if (outcome.result != null) {
          // TODO: Display in individual cells so we can support the
          // referent-related features.  Or do we like this better?
          // We could at least base the curly braces on
          // type-checking-level singular-ness once we have it.
          return tsetToTextIgnoreErrors(getReadableModel(), outcome.result);
        } else {
          return outcome.error;
        }
      }
      // Exclude subformulas with additional bound variables, e.g., filter
      // predicate.  Currently, the only way to debug them is to select them
      // directly.  If we add support for a persistent set of test cases that are
      // heterogeneous in the local variables they define, we can probably remove
      // this restriction.
      //
      // Also throw out implicit "this" again. :/
      let childrenToShow = node.children.filter((childInfo) => {
        let childExtra = assertNotNull(extrasMap.get(childInfo.node.formula));
        return (childExtra.loc != null) && EJSON.equals(childExtra.vars, extras.vars);
      });
      // TODO: Enforce outside-to-inside order ourselves rather than relying on it
      // as a side effect of object iteration order and the way we typecheck
      // formulas.
      let varsAndTypesList = fixmeAssertNotNull(extras.vars).entries().filter((e) => e[1] !== rootColumnId);
      this.grid = [[], []];
      function typeCell(type: OSType) {
        return new ViewCell(stringifyTypeForSheet(type), 1, 1, ["rsHeader"].concat(markDisplayClassesForType(type)));
      }
      for (let [name, type] of varsAndTypesList) {
        this.grid[0].push(new ViewCell(name, 1, 1, ["rsHeader"]));
        this.grid[1].push(typeCell(type));
      }
      for (let childInfo of childrenToShow) {
        this.grid[0].push(new ViewCell(childInfo.paramName, 1, 1, ["rsHeader"]));
        this.grid[1].push(typeCell(fixmeAssertNotNull(assertNotNull(extrasMap.get(childInfo.node.formula)).type)));
      }
      this.grid[0].push(new ViewCell("Result", 1, 1, ["rsHeader"]));
      this.grid[1].push(typeCell(fixmeAssertNotNull(extras.type)));
      // XXX It would be cleaner for traceColumnFormula to ensure "traces" was
      // created at least as an empty list on all subformulas, but more work to
      // implement.
      for (let [varValues, outcome] of extras.traces != null ? extras.traces.entries() : []) {
        let line = varsAndTypesList.map(([name, _]) =>
          new ViewCell(tsetToTextIgnoreErrors(getReadableModel(), assertNotNull(varValues.get(name)))));
        for (let childInfo of childrenToShow) {
          let childTraces = assertNotNull(extrasMap.get(childInfo.node.formula)).traces;
          let childOutcome = (childTraces == null) ? null : childTraces.get(varValues);
          // XXX Would we rather just evaluate cases that weren't originally reached?
          line.push(new ViewCell(childOutcome != null ? formatOutcome(childOutcome) : "(not reached)"));
        }
        line.push(new ViewCell(formatOutcome(outcome)));
        this.grid.push(line);
      }
      let data = this.grid.map((row) => row.map((cell) => <string>cell.value));
      this.hot.loadData(data);
    }

    public destroy() {
      this.hot.destroy();
    }
  }

  function updateTracingView(template: ChangeColumnTemplateInstance) {
    let formulaInfo = fixmeAssertNotNull(template.actionBar.newFormulaInfo.get());
    let columnId = template.actionBar.changeColumnArgs.get()[0].columnId;
    // Tracing can be slow, so do it only on first demand.  (Longer term, we should
    // optimize it!)
    if (!formulaInfo.haveTraced) {
      traceColumnFormula(fixmeAssertNotNull(formulaInfo.root).formula, columnId, assertNotNull(formulaInfo.extrasMap));
      formulaInfo.haveTraced = true;
    }
    if (template.actionBar.tracingView == null) {
      template.actionBar.tracingView = new TracingView(template.find("#TracingView"));
    }
    template.actionBar.tracingView.show(fixmeAssertNotNull(formulaInfo.selectedBand).node,
      assertNotNull(formulaInfo.extrasMap));
  }

  export function hasUnsavedData(template: ActionBarTemplateInstance) {
    return template.newFormulaStr.get() !== template.origFormulaStr.get();
  }

  export function isExpanded(template: ActionBarTemplateInstance) {
    return template.actionBarMode.get() == ActionBarMode.GLOBAL_MACROS || template.currentExpansionMode.get() != null;
  }

  changeColumnTemplate.events<ChangeColumnData>({
    "change #changeColumn-multiple": function(event, template) {
      let newIsSingular = !(<HTMLInputElement>template.find("#changeColumn-multiple")).checked;
      MeteorMethods.changeColumnIsSingular.call($$.id, this.columnId, newIsSingular,
        $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
          if (error) {
            // XXX This may not be foolproof against races but is better than
            // not trying.
            resetSingularChoice(template);
          }
        })));
    },
    "change #changeColumn-computed": function(event, template) {
      let newIsComputed = (<HTMLInputElement>template.find("#changeColumn-computed")).checked;
      let newFormula = newIsComputed ? DUMMY_FORMULA : null;
      let col = getExistingColumn(this.columnId);
      if (col.viewObjectType == null) {
        // With these conditions (plus the fact that DUMMY_FORMULA returns empty sets),
        // one can toggle between an empty state column and DUMMY_FORMULA without warnings.
        // If we add the ability to undo this operation, we can probably remove the warnings
        // (except the numErroneousFamilies one, which Matt believes deserves more respect
        // in general).
        if ((newFormula != null) && stateColumnHasValues(this.columnId) &&
          !window.confirm("This will delete all existing cells in the column.  Are you sure?")) {
          resetBackendChoice(template);
          return;
        }
        if ((newFormula == null) && !EJSON.equals(col.formula, DUMMY_FORMULA)) {
          // Currently no-op on the client but let's get in the habit of calling
          // it before reading AllFamilies.
          getReadableModel().evaluateAll();
          let numErroneousFamilies = AllFamilies.find({
            ...familySelectorForColumn(this.columnId),
            error: {
              $exists: true,
              $ne: null
            }
          }).count();
          let msg: string;
          if (getColumnTypecheckError(col) != null) {
            msg = "This will delete your formula.  ";
          } else {
            msg = "This will take a snapshot of the current computed data and delete your formula.";
            if (numErroneousFamilies) {
              msg += `\n\n${numErroneousFamilies} families are currently failing to evaluate; ` +
                "they will become empty, and you will not be able to distinguish them from " +
                "families that were originally empty.\n\n";
            } else {
              msg += "  ";
            }
          }
          msg += "Are you sure?";
          if (!window.confirm(msg)) {
            resetBackendChoice(template);
            return;
          }
        }
      }
      // Server checks for "state column as child of formula column" error.
      // XXX: Disallow converting keyed objects to state?
      MeteorMethods.changeColumnFormula.call($$.id, this.columnId, newFormula,
        $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
          if (error) {
            // XXX This may not be foolproof against races but is better than
            // not trying.
            resetBackendChoice(template);
          }
        })));
    },
    "change #changeColumn-type": function(event, template) {
      let col = getExistingColumn(this.columnId);
      let newTypeChoice = getValueOfSelectedOption(template, "#changeColumn-type");
      let newSpecifiedType = (newTypeChoice == TYPE_CHOICE_AUTO) ? null : <OSType>newTypeChoice;
      // If the new type is text, there is no risk of conversion failure, but I
      // think it's still valuable to explain what's happening.  Right?
      // ~ Matt 2015-12-04
      if (col.viewObjectType == null && col.formula == null && stateColumnHasValues(this.columnId) &&
        !window.confirm("This will attempt to reinterpret existing values as the new type.  " +
          "Any values that cannot be converted will be deleted.  Proceed?")) {
        resetTypeChoice(template);
        return;
      }
      MeteorMethods.changeColumnSpecifiedType.call($$.id, this.columnId, newSpecifiedType,
        $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
          if (error) {
            // XXX This may not be foolproof against races but is better than
            // not trying.
            resetTypeChoice(template);
          }
        })));
    },
    "change #changeColumn-referenceDisplayColumn": function(event, template) {
      let newRdcChoice = getValueOfSelectedOption(template, "#changeColumn-referenceDisplayColumn");
      let newReferenceDisplayColumn = (newRdcChoice == RDC_CHOICE_AUTO) ? null : <ColumnId>newRdcChoice;
      MeteorMethods.changeColumnReferenceDisplayColumn.call(
        $$.id, this.columnId, newReferenceDisplayColumn,
        $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
          if (error) {
            // XXX This may not be foolproof against races but is better than
            // not trying.
            resetRdcChoice(template);
          }
        })));
    },
    "click .saveFormula": function(event, template) {
      let col = getExistingColumn(this.columnId);
      // canSave ensures that this is defined.
      let formula = assertNotNull(assertNotNull(template.actionBar.newFormulaInfo.get()).formula);
      // Canonicalize the string in the field, otherwise the field might stay
      // yellow after successful submission.
      template.actionBar.newFormulaStr.set(strForFormula(col, formula));
      MeteorMethods.changeColumnFormula.call($$.id, this.columnId, formula,
        $$client.wrapCallback(standardServerCallback));
    },
    "click .revertFormula": (event, template) => {
      template.actionBar.newFormulaStr.set(template.actionBar.origFormulaStr.get());
    },
    "click .formulaDebuggerToggle": (event, template) => {
      toggleExpansionMode(template.actionBar, ExpansionMode.FORMULA_DEBUGGER);
    },
    "click #editconfig-link": (event, template) => {
      toggleExpansionMode(template.actionBar, ExpansionMode.EDIT_CONFIG);
    }
  });
  changeColumnTemplate.events<FormulaBand>({
    "click .formulaBand": function(event, template) {
      // Update selection.
      let formulaInfo = fixmeAssertNotNull(template.actionBar.newFormulaInfo.get());
      if (formulaInfo.selectedBand != null) {
        formulaInfo.selectedBand.selected = false;
      }
      formulaInfo.selectedBand = this;
      this.selected = true;
      template.actionBar.newFormulaInfo.set(formulaInfo);  // Trigger reactive dependents

      // XXX Might be nice to make this an autorun.
      updateTracingView(template);
    }
  });

  interface FormulaBandsTemplateInstance extends Blaze.TemplateInstance {
    actionBar: ActionBarTemplateInstance;
    changeColumnTemplateInstance: ChangeColumnTemplateInstance;
  }
  let formulaBandsTemplate: Blaze.Template1<FormulaBandsTemplateInstance> = Template["formulaBands"];
  formulaBandsTemplate.created = function() {
    this.actionBar = <ActionBarTemplateInstance>findAncestorTemplateInstanceOfView(this.view, actionBarTemplateName);
    this.changeColumnTemplateInstance =
      <ChangeColumnTemplateInstance>findAncestorTemplateInstanceOfView(this.view, changeColumnTemplateName);
  };
  formulaBandsTemplate.helpers<{}>({
    newFormulaInfo: function() {
      return Template.instance(this).actionBar.newFormulaInfo.get();
    },
    isFormulaDebuggerActive: function() {
      return isFormulaDebuggerActive(Template.instance(this).changeColumnTemplateInstance);
    },
  });

  interface GlobalMacrosTemplateInstance extends Blaze.TemplateInstance {
    actionBar: ActionBarTemplateInstance;
  }
  let globalMacrosTemplate: Blaze.Template1<GlobalMacrosTemplateInstance> = Template["globalMacros"];
  globalMacrosTemplate.created = function() {
    this.actionBar = <ActionBarTemplateInstance>findAncestorTemplateInstanceOfView(this.view, actionBarTemplateName);
  };
  globalMacrosTemplate.helpers<{}>({
    macrosText: () => {
      return Procedures.find().fetch()
        // Skip procedures that look like triggers.
        .filter((proc) => !/_(new|add|remove)$/.test(proc.name))
        .map(stringifyProcedureDefinition)
        .join("\n");
    }
  });

}
