namespace Objsheets {

  export namespace ClientAppHelpers {
    let openCallbacks: null | (() => void)[] = [];
    // XXX Does this belong in ClientTablespace?
    export let isReady = new ReactiveVar<boolean>(false);
    export function openOrReload(sheet: string) {
      // Once a tablespace is open, it's open.  Don't stop the subscriptions
      // because something else invalidates the active computation.
      return Tracker.nonreactive(() => {
        if (clientTablespace != null) {
          if (clientTablespace.id != sheet) {
            isReady.set(false);
            // Not location.reload because we may have reached this after a Router.go
            // and Iron Location doesn't update location.href until afterFlush.
            location.replace(Iron.Location.get().href);
            return false;
          }
          return true;
        }
        clientTablespace = ClientTablespace.get(sheet);
        return true;
      });
    }

    export function openCallback() {
      isReady.set(true);
      let saveOpenCallbacks = fixmeAssertNotNull(openCallbacks);
      openCallbacks = null;
      for (let callback of saveOpenCallbacks) {
        callback();
      }
    }
    // Note: This can be called with or without openCallback being called first.
    // We write the message to cover both cases. :/
    let showedError = false;
    export function errorCallback(error: Error) {
      // Show only the first error.  It's annoying to get multiple dialogs if
      // several subscriptions fail for the same reason.
      if (!showedError) {
        showedError = true;
        alert("Connection to sheet failed: " + error.message);
      }
    }

    export function onOpen(callback: () => void) {
      if (isReady.get()) {
        callback();
      } else {
        fixmeAssertNotNull(openCallbacks).push(callback);
      }
    }
    export type LooseProcedureArgsObj = {[name: string]: SimpleOSValue | SimpleOSValue[]};
    export function call(procName: string, argsObj: LooseProcedureArgsObj, callback: MeteorCallback<void>) {
      let proc = Procedures.findOne({name: procName});
      if (proc == null)
        throw new Error(`Failed to look up type information for procedure ${procName}`);
      // In principle, this could support view objects, but it's a deprecated
      // code path. ~ Matt 2018-05-09
      MeteorMethods.executeCannedTransaction.call(
        $$.id, procName, glue(proc, argsObj), null, standardServerCallbackThen(callback));
    }

    // rootCellId == null means return an object with all empty families.  This
    // is used when a top-level Mavo template is used to create a new object.
    // If we just use {}, then Mavo initializes nested collections to have one
    // element rather than zero (example: reviews of a paper in conf).  TODO:
    // Consider filing a bug if this still happens in the latest Mavo.
    export function readObj(layoutTree: Tree<ColumnId>, rootCellId: null | CellId) {
      // _ref and _display get added by the first addSingleToDump call below.
      let obj: ObjectDump = <ObjectDump>{};
      let addPropertyToDump = (prop: string, val: null | FamilyDump | SimpleOSValue) => {
        assert(!_.has(obj, prop), () => `readObj name collision: ${prop}`);
        obj[prop] = val;
      };
      // Ideas to remove this duplication without losing static typing or making
      // the code unreadable are welcome. :/
      let addSingleToDump = (name: string, type: OSType, value: null | OSValue) => {
        // Slight hack: name == "" represents the object's self-reference.
        addPropertyToDump(name || "_ref",
          value == null ? null : passSimpleOSValue(type, value));
        if (typeIsReference(type)) {
          addPropertyToDump(name + "_display",
            value == null ? null : valueToTextIgnoreErrors(getReadableModel(), type, value));
        }
      };
      let addArrayToDump = (name: string, type: OSType, values: OSValue[]) => {
        addPropertyToDump(name, values.map((value) => passSimpleOSValue(type, value)));
        if (typeIsReference(type)) {
          addPropertyToDump(name + "_display",
            values.map((value) => valueToTextIgnoreErrors(getReadableModel(), type, value)));
        }
      };

      addSingleToDump("", layoutTree.root, rootCellId);

      let keyField = getExistingColumn(layoutTree.root).fieldName;
      if (keyField != null) {
        let key = (rootCellId == null) ? null : cellIdLastStep(rootCellId);
        let keyType = getColumnType(getExistingColumn(layoutTree.root));
        addSingleToDump(keyField, keyType, key);
      }
      for (let x of layoutTree.subtrees) {
        let c = getExistingColumn(x.root);
        assert(c.parent == layoutTree.root);
        let vals: null | OSValue[];
        if (rootCellId != null) {
          let qFamilyId: QFamilyId = {
            parentCellId: rootCellId,
            columnId: <NonRootColumnId>x.root
          };
          vals = new FamilyHandle(qFamilyId).values();
          if (vals == null) {
            // XXX Unprincipled, but people will be angry if we fail the whole
            // readObj or we leave a null and their template code fails later.
            // :(  Revisit. ~ Matt 2016-10-13
            console.warn("readObj: erroneous family treated as empty", qFamilyId);
            vals = [];
          }
        } else {
          vals = [];
        }
        if (c.isObject) {
          let fam: ObjectFamilyDump = vals.map((v2) => readObj(x, cellIdChild(assertNotNull(rootCellId), v2)));
          // We could use objectOrBracketedFieldName, but this gives easier syntax for readers.
          let objectName = assertNotNull(objectOrFieldName(c));
          addPropertyToDump(objectName, fam);
        } else if (c.fieldName != null) {
          addArrayToDump(c.fieldName, getColumnType(c), vals);
        }
      }

      return obj;
    }
  }

  function glue(proc: ProcedureDoc, argsobj: ClientAppHelpers.LooseProcedureArgsObj) {
    let o: ProcedureArgsObj = {};
    for (let param of proc.params) {
      if (!_.has(argsobj, param.name))
        throw new Error(`Missing argument ${param.name} to ${proc.name}`);
      let v = argsobj[param.name];
      let arr = (v instanceof Array) ? v : [v];
      o[param.name] = arr.map((e) => acceptSimpleOSValue(param.type, e));
    }
    return o;
  }

  // origDump should be from readObj, or null/undefined when we process an add
  // of an entire object.  newDump should be from Mavo.
  //
  // This will return null if there's no diff, in which case the client should
  // skip the call to the server.
  export function diffObjectDumps(objectType: ColumnId, origDump: undefined | ObjectDump,
    newDump: MavoObjectDump): null | ObjectDiff {
    let column = getExistingColumn(objectType);
    let diff: ObjectDiff = {};
    for (let childColumnId of column.children) {
      let childColumn = getExistingColumn(childColumnId);
      let field = objectOrFieldName(childColumn);
      if (field == null) continue;  // Guess diffs can't target this column.
      // XXX This code will mess up with field name collisions.  readObj will
      // detect this in many cases but not all, e.g., the layoutTree might not
      // include the colliding fields or we might not be editing an existing
      // object.  We could add dedicated code to validate everything but I
      // suspect Shachar won't like that. ~ Matt 2017-04-05
      let fieldDiff: FamilyDiff = [];
      // When editing an existing object, origDump should contain all fields.
      let origData = (origDump == null) ? [] : assertNotNull(<FamilyDump>origDump[field]);
      // In the todo example, Mavo seems to give us null for the title of a task
      // if we leave the field blank.  XXX: Find the right solution.
      let newData1 = fallback<typeof newDump[string]>(newDump[field], []);
      // TypeScript is not smart enough to know that a single-element array of a union
      // type gives an array of /one of/ the types, not a heterogeneous array.
      let newData = <SimpleOSValue[] | MavoObjectDump[]>
        ((newData1 instanceof Array) ? newData1 : [newData1]);

      type KeyAndContent<OD> = [null | OSValue, null | OD];
      let convert = <OD extends ObjectDump | MavoObjectDump>(familyDump: SimpleOSValue[] | OD[]): KeyAndContent<OD>[] =>
        (childColumn.isObject)
          ? (<OD[]>familyDump).map<KeyAndContent<OD>>((objDump) =>
            [objDump._ref == null ? null :
              cellIdLastStep(<CellId>acceptSimpleOSValue(childColumnId, objDump._ref)),
              objDump])
          : (<SimpleOSValue[]>familyDump).map<KeyAndContent<OD>>(
              (value) => [acceptSimpleOSValue(getColumnType(childColumn), value), null]);
      let origEntries = convert(origData);
      let newEntries = convert(newData);

      let origMap = new JSONKeyedMap(origEntries);
      for (let [newKey, newContent] of newEntries) {
        let origContent = undefined;
        if (newKey != null) {
          origContent = origMap.get(newKey);
          origMap.delete(newKey);
        }

        let diffItem: FamilyDiffItem = {key: newKey};
        if (origContent === undefined) {
          diffItem.add = true;
        }
        if (childColumn.isObject) {
          if (origContent === null)  // narrowing
            throw new Error("Should not get null for an object column");
          let edit = diffObjectDumps(childColumnId, origContent, fixmeAssertNotNull(newContent));
          if (edit != null)
            diffItem.edit = edit;
        }

        if (!_.isEmpty(diffItem)) {
          fieldDiff.push(diffItem);
        }
      }
      for (let deletedKey of origMap.keys())
        fieldDiff.push({key: deletedKey, remove: true});

      if (fieldDiff.length > 0)
        diff[field] = fieldDiff;
    }
    return _.isEmpty(diff) ? null : diff;
  }

  let objsheetsAppTemplate: Blaze.Template1<Blaze.TemplateInstance> = Template["ObjsheetsApp"];
  objsheetsAppTemplate.created = function() {
    this.autorun(() => {
      let data = Template.currentData();
      ClientAppHelpers.openOrReload(data.sheet);
    });
  };
  objsheetsAppTemplate.helpers({
    isReady: () => ClientAppHelpers.isReady.get(),
  });
}
