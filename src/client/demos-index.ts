namespace Objsheets {

  function indexPathForExamplesNamePrefix(examplesNamePrefix: string) {
    // Based on the pathFor template helper, which doesn't appear to have an
    // equivalent accessible from code. :(
    return (<fixmeAny>Router.routes)["index"].path(null, {
      query: {
        examplesNamePrefix: examplesNamePrefix
      }
    });
  }

  let indexMultiuser = fallback<boolean>(Meteor.settings.public.indexMultiuser, false);

  interface IndexTemplateData {
    examplesNamePrefix: string;
  }
  Router.route("/", (function(this: fixmeAny) {
    let examplesNamePrefix = this.params.query.examplesNamePrefix;
    // Only redirect if examplesNamePrefix was unset.  Don't go wild if the user just
    // clears the input field.
    if (indexMultiuser && (examplesNamePrefix == null)) {
      Router.go(indexPathForExamplesNamePrefix(Random.id()));
    } else {
      if (examplesNamePrefix == null) {
        examplesNamePrefix = "";
      }
      let data: IndexTemplateData = {examplesNamePrefix};
      this.render("Index", {data: data});
    }
  }), <fixmeAny>{
    name: "index",
    onAfterAction: () => {
      document.title = "Espalier";
    }
  });

  let indexTemplate: Blaze.Template = Template["Index"];
  indexTemplate.helpers({
    // Since we aren't restricting what domain names can point to our hosted demo
    // servers, it's easiest to just base this on the URL of the current page.  To
    // use Meteor.absoluteUrl, we'd have to set the ROOT_URL environment variable
    // on each server.
    //
    // XXX: Is there a library that does the following in a more robust way?
    absUrl: (path: string) => location.protocol + "//" + location.host + path,
    indexMultiuser: () => indexMultiuser,
    newSheetName: () => Session.get("newSheetName")
  });

  let indexConditionalExampleLinkTemplate: Blaze.Template = Template["Index_ConditionalExampleLink"];
  indexConditionalExampleLinkTemplate.helpers({
    examplesNamePrefixWithDot: () => {
      let examplesNamePrefix = (<IndexTemplateData>Template.parentData()).examplesNamePrefix;
      return examplesNamePrefix ? examplesNamePrefix + "." : "";
    },
    shouldEnableExampleLinks: () => {
      let examplesNamePrefix = (<IndexTemplateData>Template.parentData()).examplesNamePrefix;
      return !indexMultiuser || examplesNamePrefix;
    }
  });

  indexTemplate.events<IndexTemplateData>({
    "input #examplesNamePrefix": (event, template) => {
      Router.go(indexPathForExamplesNamePrefix((<HTMLInputElement>event.target).value));
    },
    "input #newSheetName": (event, template) => {
      Session.set("newSheetName", (<HTMLInputElement>event.target).value);
    },
    "click #newSheetGo": (event, template) => {
      Router.go("/" + Session.get("newSheetName"));
    },
    "keypress #newSheetName": (event, template) => {
      if (event.which === 13) {
        Router.go("/" + Session.get("newSheetName"));
      }
    }
  });

}
