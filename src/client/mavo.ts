namespace Objsheets {

  // Mavo Objsheets Backend
  // Use a TypeScript class to get full type information.  We weren't using any
  // Bliss features anyway, but if we want to, we can find workarounds.
  class MavoObjsheetsBackend extends Mavo.Backend {
    data: Promise<ObjectDump>;
    viewLayout: fixmeUndefinable<Tree<ColumnId>>;
    // Exactly one of these will be set: rootObjQFamilyId if we're creating a new object.
    rootObjQCellId: undefined | QCellId;
    rootObjQFamilyId: undefined | QFamilyId;
    _data_fulfill: fixmeAny;

    id = "Objsheets";

    constructor(url: string, mavo: Mavo) {
      super(url, mavo);
      this.permissions.on(["read", "edit", "save"]);
      this.data = new Promise((fulfill, reject) => { this._data_fulfill = fulfill; });
      this.open();
    }

    open() {
      // Parse the URL:
      // "objsheets:<sheet>" = use master view and root object
      // "objsheets:<sheet>/views/<v>" = use view <v>
      // "objsheets:<sheet>/<columnStr>/<cellId>" = use given object
      // "objsheets:<sheet>/<columnStr>/new:<parentCellId>" = create new object of given type and parent
      let url = decodeURI(this.url);
      // https://github.com/Microsoft/TypeScript/issues/17053 :(
      let m = assertNotNull(/^objsheets:([^/]+)([/].*)?/.exec(url)), sheet = m[1], path = arrayGet(m, 2);
      // cannot be placed outside due to load order
      // (maybe some hack is doable but)
      Objsheets.ClientAppHelpers.openOrReload(sheet);
      // Notify Mavo when underlying data has changed
      ClientAppHelpers.onOpen(() => {
        if (path == null) {
          this.rootObjQCellId = rootQCellId;
          this.viewLayout = new View(null).def().layout;
        } else {
          let mo_view = /^[/]views[/](.+)$/.exec(path),
              mo_obj = /^[/]([^/]+)[/](.+)$/.exec(path);
          if (mo_view) {
            this.rootObjQCellId = rootQCellId;
            this.viewLayout = new View(mo_view[1]).def().layout;
          } else if (mo_obj) {
            let columnId = parseColumnRef(mo_obj[1])[0];
            let newMatch = /^new:(.*)$/.exec(mo_obj[2]);
            if (newMatch) {
              if (columnId === rootColumnId)  // narrowing
                throw new Error("Cannot create a new root object");
              this.rootObjQFamilyId = {columnId: columnId, parentCellId: JSON.parse(newMatch[1])};
            } else {
              this.rootObjQCellId = {columnId: columnId, cellId: JSON.parse(mo_obj[2])};
            }
            this.viewLayout = View.drillDown(columnId);
          } else {
            throw new Error("objsheets backend URL does not match a supported pattern: " + url);
          }
        }
        Tracker.autorun(() => {
          let newData = this.getSync();  // call sync version to stimulate tracker
          if (this._data_fulfill) {
            this._data_fulfill(newData);
            this._data_fulfill = null;
          } else {
            this.data = new Promise((fulfill, reject) => fulfill(newData));
            this.mavo.render(newData);
          }
        });
      });
    }

    /**
     * Handles the case where we have to create a new object, so the diff
     * bubbles to the parent object.
     */
    wrapDiffForNewObject(diff: ObjectDiff) {
      let newDiff: ObjectDiff = {};
      let field = assertNotNull(
        objectOrFieldName(getExistingColumn(fixmeAssertNotNull(this.rootObjQFamilyId).columnId)));
      newDiff[field] = [{key: null, add: true, edit: diff}];
      return newDiff;
    }

    /**
     * Handles the case where we have just created a new object, and got the
     * response.
     */
    processNewObjectResponse(resp: ObjectDiffResponse) {
      let rootObjQFamilyId = fixmeAssertNotNull(this.rootObjQFamilyId);
      let field = assertNotNull(
        objectOrFieldName(getExistingColumn(rootObjQFamilyId.columnId)));
      let newCellId = cellIdChild(rootObjQFamilyId.parentCellId,
        fixmeAssertNotNull(assertNotNull(resp[field])[0].key));
      // Navigate to the newly created object.
      //
      // FIXME: We do not know the URL format for the template (as opposed to
      // the backend).  For now, assume the object ID is the last path
      // component (currently true for papers in conf).  We can come up with a
      // more general solution later.
      window.location.href = encodeURI(JSON.stringify(newCellId));
    }

    get() {
      return this.data;
    }

    getSync(): ObjectDump {
      let cellId = this.rootObjQCellId == null ? null : this.rootObjQCellId.cellId;
      return Objsheets.ClientAppHelpers.readObj(fixmeAssertNotNull(this.viewLayout), cellId);
    }

    put(file = this.getFile()) {
      return new Promise((fulfill, reject) => {
        this.data.then((oldData: ObjectDump) => {
          let diff = diffObjectDumps(fixmeAssertNotNull(this.viewLayout).root, oldData, file.data);
          if (diff == null) {
            fulfill(true);
            return;
          }
          let root: QCellId;
          if (this.rootObjQFamilyId) {
            diff = this.wrapDiffForNewObject(diff);
            root = new FamilyHandle(this.rootObjQFamilyId).parent();
          } else {
            root = fixmeAssertNotNull(this.rootObjQCellId);
          }
          MeteorMethods.writeDiff.call($$.id, root, diff, (error, resp) => {
            if (error) {
              reject(error);
            } else {
              if (this.rootObjQFamilyId)
                this.processNewObjectResponse(assertNotNull(resp));
              fulfill(true);
            }
          });
        });
      });
    }

    call(procName: string, argsObj: ClientAppHelpers.LooseProcedureArgsObj) {
      console.log(procName, argsObj);
      ClientAppHelpers.call(procName, argsObj, () => {});
    }

    static test(value: string) {
      return /^objsheets:/.test(value);
    }
  }

  Mavo.Backend.register(MavoObjsheetsBackend);
}
