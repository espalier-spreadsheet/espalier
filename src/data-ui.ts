namespace Objsheets {
  // The next two definitions are exported for the action bar.

  export function allowedReferenceDisplayColumns(col: Column): ColumnId[] {
    let allowed: ColumnId[] = [];
    // XXX: Duplicating logic from columnLogicalChildrenByName?  (Avoiding this
    // would require a comprehensive emulation layer for keys as fields.)
    if (getColumnType(col) !== SpecialType.TOKEN) {
      allowed.push(col._id);
    }
    for (let childColId of col.children) {
      let childCol = getExistingColumn(childColId);
      if (childCol.userVisible && !childCol.isObject) {
        allowed.push(childColId);
      }
    }
    return allowed;
  }

  export function defaultReferenceDisplayColumn(col: Column): null | ColumnId {
    // Current heuristic: First allowed.
    //
    // NOTE: Formulas are allowed to depend on the default reference display column
    // via toText, so reordering columns may change the spreadsheet values!  This
    // is a little surprising, but I think it's better than any of the
    // alternatives. ~ Matt 2015-11-20
    //
    // Ideas:
    // - Prefer a tuple of fields declared unique, if and when we have that
    //   information.
    // - Require singular once we have that information.
    // - Automatically detect certain field names, e.g., "name" or "title"?  A hack
    //   but maybe the right thing in this context.

    let allowed = allowedReferenceDisplayColumns(col);
    return allowed.length > 0 ? allowed[0] : null;
  }

  export function effectiveReferenceDisplayColumn(col: Column): null | ColumnId {
    return fallback(col.referenceDisplayColumn, defaultReferenceDisplayColumn(col));
  }

  const DATE_FORMAT = "yyyy-MM-dd HH:mm";
  {
    // Monkeypatch for the part of
    // https://github.com/abritinthebay/datejs/issues/233 that affects us until
    // we upgrade to a version of DateJS with the fix.
    // https://github.com/abritinthebay/datejs/commit/bc72b270ca2177a5e35615a407e58e3831fd0cf0
    let $D = <fixmeAny>Date;
    let _ = $D.Parsing.Operators, g = $D.Grammar, t = $D.Translator;
    g["HH"] = _.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3])/), t.hour));
    g["mm"] = _.cache(_.process(_.rtoken(/^[0-5][0-9]/), t.minute));
  }

  // XXX: Currently all callers of {parse,stringify}Date also call
  // {pack,unpack}Date. Would it make more sense to have these functions do a
  // pack/unpack?
  export function parseDate(text: string): Date {
    // Let's err on the side of strictness until we have a use case to motivate
    // a different design.  Note that DateJS's Date.parse is potentially
    // dependent on the current time, so if formulas can invoke this function in
    // the future, we'd need to enforce the determinism flag.  It's less clear
    // if determinism is important to the other callers of this function (sheet
    // UI and Mavo templates).
    // ~ Matt 2017-04-07
    let date = Date.parseExact(text, DATE_FORMAT);
    if (date == null)
      throw new Error(`Invalid date: '${text}'.  Please use the following format: 2017-04-07 17:52`);
    else
      return date;
  }
  export function stringifyDate(date: Date) {
    return date.toString(DATE_FORMAT);
  }

  // Used on the server to reparse values in changeColumnSpecifiedType.
  export function parseValue(type: OSType, text: string): OSValue {
    if (typeIsReference(type)) {
      // Ignore erroneous families: they do not contain any objects we can match against.
      // Also ignore references that fail to convert to text.
      let matchingCells: CellId[] = [];
      for (let cellId of allCellIdsInColumnIgnoreErrors(type)) {
        try {
          if (text === valueToText(getReadableModel(), type, cellId)) {
            matchingCells.push(cellId);
          }
        } catch (e) {
          // Skip
        }
      }
      if (matchingCells.length === 1) {
        return matchingCells[0];
      } else if (matchingCells.length > 1) {
        throw new Error(`The entered text matches ${matchingCells.length} '${stringifyType(type)}' objects.  ` +
          `Choose a reference display column for '${stringifyType(type)}' that has unique values, ` +
          "or define a new computed column if necessary.");  // "or enter the @n notation instead"
      } else {
        throw new Error(`The entered text does not match any existing '${stringifyType(type)}' object.`);
      }
    }
    switch (type) {
      case SpecialType.UNIT:
        if (text != "X") throw new Error("The only allowed _unit value is \"X\".");
        return "X";
      case SpecialType.TEXT:
        return text;
      case SpecialType.DATE:
        return packDate(parseDate(text));
      case SpecialType.NUMBER:
      case SpecialType.BOOL:
        return JSON.parse(text);
      case SpecialType.IMAGE:
      case SpecialType.EMPTY:
      case SpecialType.ERROR:
      case SpecialType.TOKEN:
        throw new Error(`We should not be trying to parse ${type}.`);
      default:
        return assertUnreachable(type, `Unknown type ${type}`);
    }
  }

  // Even now that we only support a reference display column (not an arbitrary
  // formula), there's still a risk of infinite recursion because we allow the user
  // to choose a reference display column that contains another reference, which
  // we'd have to convert to text.  Ideally we'd bring the toText of each object
  // into the data cache so we could stop infinite recursion the same way we do for
  // formulas in general.  But I don't know how long we'll be keeping this
  // functionality, so for now I'm using the refsSeen set because it's easy.
  //
  // refsSeen contains qCellIds.  Since we have to use the equivalent of
  // EJSON.equals to compare them, we use a JSONSet rather than implementing
  // our own list membership test.
  export function valueToText(model: ReadableModel, type: OSType, value: OSValue,
    refsSeen: JSONSet<QCellId> = new JSONSet<QCellId>()): string {
    if (typeIsReference(type)) {
      let qCellId = {
        columnId: type,
        cellId: <CellId>value
      };
      if (refsSeen.has(qCellId)) {
        // C.f. readFamilyForFormula
        throw new RuntimeError("Circular dependency while converting object of type " +
          `'${stringifyType(type)}', ID ${JSON.stringify(value)}, to text`);
      }
      if (!(new CellHandle(qCellId).existsIgnoreErrors()))
        // This isn't quite accurate for computed objects, but it should be
        // easiest for non-programmers to understand.
        return "<deleted>";
      let newRefsSeen = refsSeen.shallowClone();
      newRefsSeen.add(qCellId);
      let col = getExistingColumn(type);
      let displayColId = effectiveReferenceDisplayColumn(col);
      let displayTset: TypedSet;
      if (displayColId === null) {
        // Really nothing we can use?
        return "<reference>";
      } else {
        displayTset = readField(model, col, value, displayColId);
      }
      return tsetToText(model, displayTset, newRefsSeen);
    }
    switch (type) {
      case SpecialType.NUMBER:
        // toLocaleString defaults to 3 decimal places.  This seems like the most
        // useful behavior for most UI purposes.  It loses data; if we get a clear
        // use case for a string representation that retains all data, we can add
        // another function.
        //
        // FIXME: A dummy edit to a state cell that has extra precision loses the
        // extra precision.  This would be unacceptable in a production tool, but
        // let's see how long we can get away with it in a research prototype. :/
        return (<number>value).toLocaleString(undefined, {useGrouping: false});
      case SpecialType.TEXT:
      case SpecialType.UNIT:
        return <string>value;
      case SpecialType.DATE:
        // Same remarks about data loss as SpecialType.NUMBER type.
        return stringifyDate(unpackDate(value));
      case SpecialType.BOOL:
        return JSON.stringify(value);
      case SpecialType.IMAGE:
        return "<image>";
      case SpecialType.EMPTY:
      case SpecialType.ERROR:
      case SpecialType.TOKEN:
        throw new Error(`We should not be trying to convert ${type} to text.`);
      default:
        return assertUnreachable(type, `Unknown type ${type}`);
    }
  }

  export function genericSetToText<T>(elements: T[], formatOne: (x: T) => string) {
    return elements.length === 1 ? formatOne(elements[0]) : "{" + (elements.map((e) => formatOne(e))).join(", ") + "}";
  }

  export function tsetToText(model: ReadableModel, tset: TypedSet,
    refsSeen: JSONSet<QCellId> = new JSONSet<QCellId>()) {
    return genericSetToText(tset.elements(), (e) => valueToText(model, tset.type, e, refsSeen));
  }

  // The ignoreErrors versions must not be used in formula evaluation, because the
  // ability to catch errors makes evaluation of cyclic dependencies
  // nondeterministic in the current implementation.
  //
  // Note: Callers for major tool features should rather catch the error themselves
  // and display it.  Ignoring it is OK for niche purposes like referring to values
  // in an error message.

  export function valueToTextIgnoreErrors(model: ReadableModel, type: OSType, value: OSValue) {
    try {
      return valueToText(model, type, value);
    } catch (e) {
      return "<?>";
    }
  }

  export function tsetToTextIgnoreErrors(model: ReadableModel, tset: TypedSet) {
    return genericSetToText(tset.elements(), (e) => valueToTextIgnoreErrors(model, tset.type, e));
  }

}
