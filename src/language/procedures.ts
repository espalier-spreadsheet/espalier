namespace Objsheets {

  // Wrappers for the server write APIs that the `OpHandler.execute`
  // implementations use but cannot reference directly from outside a
  // server-only file.  Given that the client needs `stringifyProcedure`; having
  // the `OpHandler.execute` and `OpHandler.stringify` implementations in
  // separate files would be a severe hit to understandability; and a
  // `DataWritableModel` is already passed to all the operations, this seems to
  // be the best solution, even though we seem to be wavering on whether we want
  // the model to be involved in data reads and writes.  In theory, this gets us
  // part of the way to being able to execute procedures on the client using a
  // special Model implementation, e.g., for a procedure debugger analogous to
  // the current formula debugger.
  // ~ Matt 2017-03-08
  export interface DataWritableModel extends ReadableModel {
    FamilyHandle_new(fam: FamilyHandle): OSValue;
    FamilyHandle_add(fam: FamilyHandle, value: OSValue): void;
    // One of FamilyHandle_remove, CellHandle_remove can be expressed in terms
    // of the other, but it's simpler to provide both than to duplicate the
    // statement of that relationship.
    FamilyHandle_remove(fam: FamilyHandle, value: OSValue): void;
    CellHandle_remove(ch: CellHandle): void;
    FamilyHandle_setValues(fam: FamilyHandle, values: OSValue[]): void;
  }

  // Argument adapters.
  interface ArgAdapter<A, T, E, S> {
    // - mutableVars contains all variables in scope
    // - mutableCurrentScopeVars contains all variables defined in the current
    //   scope, which are the variables that statements are allowed to assign to.
    //
    // The "mutable" in the names refers to the fact that both sets are mutated as
    // assignments are processed.  However, as per the comment on Statements
    // below, the arg adapters do not mutate them.

    // If `validate` completes without error, then `arg` is of type A.
    validate: (mutableVars: VarSet, mutableCurrentScopeVars: VarSet, arg: Unvalidated<A>) => void;
    typecheck: (model: ReadableModel, mutableVars: VarTypes, arg: A) => T;
    execute: (model: ReadableModel, mutableVars: VarValues, arg: A) => E;
    stringify: (model: ReadableModel, mutableVars: VarTypes, arg: A) => S;
  }
  interface VarNameStringifyInfo {
    name: string;
    str: string;
  }
  let VarName: ArgAdapter<string, string, string, VarNameStringifyInfo> = {
    validate: (mutableVars, mutableCurrentScopeVars, arg) => {
      staticCheck(_.isString(arg), () => "Variable name must be a string");
    },
    typecheck: (model, mutableVars, arg) => arg,
    execute: (model, mutableVars, arg) => arg,
    stringify: (model, mutableVars, arg): VarNameStringifyInfo => ({
      name: arg,
      str: FormulaInternals.stringifyIdent(arg)
    })
  };
  // This one is mostly for documentation purposes.  Validate, typecheck, and
  // execute for statements all have side effects; if we performed those side
  // effects during the arg adapter phase, we'd be behaving as if the statements
  // ran unconditionally before the control structure, and a control structure for
  // which that behavior is correct would be a pretty boring control structure.
  // Even for stringify, foreach needs to bind a variable first.
  //
  // We could have the arg adapters return partial applications of the
  // validateStatements, etc. functions, but I don't think this helps
  // understandability.
  //
  // FIXME: `validate` does not conform to the ArgAdapter interface.
  let Statements: ArgAdapter<Statement[], Statement[], Statement[], Statement[]> = {
    validate: (mutableVars, mutableCurrentScopeVars, arg) => arg,
    typecheck: (model, mutableVars, arg) => arg,
    execute: (model, mutableVars, arg) => arg,
    stringify: (model, mutableVars, arg) => arg
  };
  let OptionalVarName: ArgAdapter<null | string, null | string, null | string, null | VarNameStringifyInfo> = {
    validate: (mutableVars, mutableCurrentScopeVars, arg) => {
      if (arg != null) {
        VarName.validate(mutableVars, mutableCurrentScopeVars, arg);
      }
    },
    typecheck: (model, mutableVars, arg) => arg,
    execute: (model, mutableVars, arg) => arg,
    stringify: (model, mutableVars, arg) => arg != null ? VarName.stringify(model, mutableVars, arg) : null
  };
  interface EagerSubformulaStringifyInfo {
    formula: Formula;
    str: string;
  }
  let procedures_EagerSubformula: ArgAdapter<Formula, OSType, TypedSet, EagerSubformulaStringifyInfo> = {
    // Wrapper needed to strip mutableCurrentScopeVars argument.
    validate: (mutableVars, mutableCurrentScopeVars, arg) => {
      FormulaInternals.EagerSubformula.validate(mutableVars, arg);
    },
    // Eta-expanded to avoid a load order dependence.
    typecheck: (model, mutableVars, arg) => FormulaInternals.EagerSubformula.typecheck(model, mutableVars, arg),
    execute: (model, mutableVars, arg) => FormulaInternals.EagerSubformula.evaluate(model, mutableVars, arg),
    stringify: (model, mutableVars, arg) => {
      // Anything that binds a local variable needs the original formula in order
      // to get its type.  Follow the design of stringifySubformula.
      //
      // All contexts in which formulas appear in statements are safe for PRECEDENCE_LOWEST.
      return {
        formula: arg,
        str: FormulaInternals.EagerSubformula.stringify(model, mutableVars, arg)
          .strFor(FormulaInternals.PRECEDENCE_LOWEST)
      };
    }
  };

  interface EvaluatedFamilyRef {
    parentCellsTset: TypedSet;
    columnId: NonRootColumnId;
    // Relevant to "make".
    keysTset: null | TypedSet;
    // Relevant to "new", the only statement for which it isn't fixed.
    wantValues: boolean;
  }
  let EagerFamilyRef =
    (stmtIsNew: boolean, stmtWantsObjects: undefined | boolean,
      stmtWantsKeys: boolean): ArgAdapter<Formula, OSType, EvaluatedFamilyRef, EagerSubformulaStringifyInfo> =>
  ({
    validate: (mutableVars, mutableCurrentScopeVars, fmla) => {
      procedures_EagerSubformula.validate(mutableVars, mutableCurrentScopeVars, fmla);
      staticCheck(fmla[0] === "down", () => "Family reference must be a down navigation.");
      // If fmla passed validation and fmla[0] === "down", then this should work.
      let [/*op*/, /*startFmla*/, /*targetColumnId*/, keysFmla, /*wantValues*/] = fmla;
      staticCheck((keysFmla != null) === stmtWantsKeys,
        () => stmtWantsKeys ? "make requires a subscript expression."
          : "A subscript expression is not allowed.");
    },

    typecheck: (model, mutableVars, fmla) => {
      let type = procedures_EagerSubformula.typecheck(model, mutableVars, fmla);
      let [/*op*/, /*startFmla*/, targetColumnId, /*keysFmla*/, wantValues] = fmla;
      let targetCol = getExistingColumn(targetColumnId);
      staticCheck(!(wantValues && targetCol.isObject),
        () => "Cannot write to a key field.  Write to the object family instead.");
      // At this point, wantValues == !targetCol.isObject.
      if (stmtIsNew) {
        if (targetCol.formula == null)
          staticCheck(getColumnType(targetCol) == SpecialType.TOKEN,
            () => "new cannot be used for state data other than unkeyed objects.");
      } else {
        // make, :=, add, remove (NOT delete, which just takes an expression)

        // A single down navigation in concrete syntax will always have
        // wantValues = !isObject, but the simplification code in resolveNavigation
        // generates down navigations to object columns with wantValues = true, and
        // more generally either mismatch can arise from adding/removing object
        // types.
        staticCheck(!wantValues === stmtWantsObjects,
          () => stmtWantsObjects ? "new/make only work on object families."
            : "set/add/remove only work on leaf families.");
      }
      return type;
    },

    execute: (model, mutableVars,
      [op, startFmla, targetColumnId, keysFmla, wantValues]): EvaluatedFamilyRef => {
      return {
        parentCellsTset: evaluateFormula(model, mutableVars, startFmla),
        columnId: targetColumnId,
        keysTset: keysFmla != null ? evaluateFormula(model, mutableVars, keysFmla) : null,
        wantValues: wantValues
      };
    },

    stringify: procedures_EagerSubformula.stringify
  });

  function validateAssignment(mutableVars: VarSet, mutableCurrentScopeVars: VarSet, lhsName: string) {
    if (!mutableCurrentScopeVars.has(lhsName)) {
      staticCheck(!mutableVars.has(lhsName),
        () => `Assignment to ${lhsName} shadows a variable defined outside the current scope (foreach block).`);
      mutableCurrentScopeVars.add(lhsName);
      mutableVars.add(lhsName);
    }
  }

  function indent(str: string) {
    return str.replace(/^(?=.)/mg, "  ");
  }

  interface OpHandlerUntyped {
    argAdapters: ArgAdapter<fixmeAny, fixmeAny, fixmeAny, fixmeAny>[];
    validate?: (mutableVars: VarSet, mutableCurrentScopeVars: VarSet, ...adaptedArgs: fixmeAny[]) => void;
    typecheck: (model: ReadableModel, mutableVars: VarTypes, ...adaptedArgs: fixmeAny[]) => void;
    execute: (model: DataWritableModel, mutableVars: VarValues, ...adaptedArgs: fixmeAny[]) => void;
    stringify: (model: ReadableModel, mutableVars: VarTypes, ...adaptedArgs: fixmeAny[]) => string;
  }
  const OP_HANDLER_CHECKED_BRAND = Symbol();
  interface OpHandlerChecked extends OpHandlerUntyped {
    [OP_HANDLER_CHECKED_BRAND]: {};
  }

  function makeOpHandler<A1, T1, E1, S1>(opHandler: {
    argAdapters: [ArgAdapter<A1, T1, E1, S1>];
    validate?: (mutableVars: VarSet, mutableCurrentScopeVars: VarSet, a1: A1) => void;
    typecheck: (model: ReadableModel, mutableVars: VarTypes, a1: T1) => void;
    execute: (model: DataWritableModel, mutableVars: VarValues, a1: E1) => void;
    stringify: (model: ReadableModel, mutableVars: VarTypes, a1: S1) => string;
  }): OpHandlerChecked;
  function makeOpHandler<A1, T1, E1, S1, A2, T2, E2, S2>(opHandler: {
    argAdapters: [ArgAdapter<A1, T1, E1, S1>, ArgAdapter<A2, T2, E2, S2>];
    validate?: (mutableVars: VarSet, mutableCurrentScopeVars: VarSet, a1: A1, a2: A2) => void;
    typecheck: (model: ReadableModel, mutableVars: VarTypes, a1: T1, a2: T2) => void;
    execute: (model: DataWritableModel, mutableVars: VarValues, a1: E1, a2: E2) => void;
    stringify: (model: ReadableModel, mutableVars: VarTypes, a1: S1, a2: S2) => string;
  }): OpHandlerChecked;
  function makeOpHandler<A1, T1, E1, S1, A2, T2, E2, S2, A3, T3, E3, S3>(opHandler: {
    argAdapters: [ArgAdapter<A1, T1, E1, S1>, ArgAdapter<A2, T2, E2, S2>,
      ArgAdapter<A3, T3, E3, S3>];
    validate?: (mutableVars: VarSet, mutableCurrentScopeVars: VarSet, a1: A1, a2: A2, a3: A3) => void;
    typecheck: (model: ReadableModel, mutableVars: VarTypes, a1: T1, a2: T2, a3: T3) => void;
    execute: (model: DataWritableModel, mutableVars: VarValues, a1: E1, a2: E2, a3: E3) => void;
    stringify: (model: ReadableModel, mutableVars: VarTypes, a1: S1, a2: S2, a3: S3) => string;
  }): OpHandlerChecked;
  function makeOpHandler(opHandler: OpHandlerUntyped): OpHandlerChecked {
    return <OpHandlerChecked>opHandler;
  }

  let procedures_dispatch: {[opName: string]: OpHandlerChecked} = {
    "let": makeOpHandler({
      argAdapters: [VarName, procedures_EagerSubformula],
      validate: (mutableVars, mutableCurrentScopeVars, lhsName, rhsFmla) => {
        validateAssignment(mutableVars, mutableCurrentScopeVars, lhsName);
      },
      typecheck: (model, mutableVars, lhsName, rhsType) => {
        mutableVars.set(lhsName, rhsType);
      },
      execute: (model, mutableVars, lhsName, rhsTset) => {
        mutableVars.set(lhsName, rhsTset);
      },
      stringify: (model, mutableVars, lhsSinfo, rhsSinfo) => {
        mutableVars.set(lhsSinfo.name, FormulaInternals.tryTypecheckFormula(model, mutableVars, rhsSinfo.formula));
        return `let ${lhsSinfo.str} = ${rhsSinfo.str}\n`;
      }
    }),
    set: makeOpHandler({
      argAdapters: [EagerFamilyRef(false, false, false), procedures_EagerSubformula],
      typecheck: (model, mutableVars, lhsType, rhsType) => {
        staticExpectType("Right operand of ':='", rhsType, lhsType);
      },
      execute: (model, mutableVars, lhsFref, rhsTset) => {
        for (let parentCellId of (<JSONSet<CellId>>lhsFref.parentCellsTset.set).elements()) {
          let fam = new FamilyHandle({columnId: lhsFref.columnId, parentCellId: parentCellId});
          model.FamilyHandle_setValues(fam, rhsTset.elements());
        }
      },
      stringify: (model, mutableVars, lhsSinfo, rhsSinfo) =>
        `${lhsSinfo.str} := ${rhsSinfo.str}\n`
    }),
    add: makeOpHandler({
      argAdapters: [EagerFamilyRef(false, false, false), procedures_EagerSubformula],
      typecheck: (model, mutableVars, lhsType, rhsType) => {
        staticExpectType("Right operand of 'add'", rhsType, lhsType);
      },
      execute: (model, mutableVars, lhsFref, rhsTset) => {
        for (let parentCellId of (<JSONSet<CellId>>lhsFref.parentCellsTset.set).elements()) {
          let fam = new FamilyHandle({columnId: lhsFref.columnId, parentCellId: parentCellId});
          for (let val of rhsTset.elements())
            model.FamilyHandle_add(fam, val);
        }
      },
      stringify: (model, mutableVars, lhsSinfo, rhsSinfo) =>
        `to set ${lhsSinfo.str} add ${rhsSinfo.str}\n`
    }),
    remove: makeOpHandler({
      argAdapters: [EagerFamilyRef(false, false, false), procedures_EagerSubformula],
      typecheck: (model, mutableVars, lhsType, rhsType) => {
        staticExpectType("Right operand of 'remove'", rhsType, lhsType);
      },
      execute: (model, mutableVars, lhsFref, rhsTset) => {
        for (let parentCellId of (<JSONSet<CellId>>lhsFref.parentCellsTset.set).elements()) {
          let fam = new FamilyHandle({columnId: lhsFref.columnId, parentCellId: parentCellId});
          for (let val of rhsTset.elements())
            model.FamilyHandle_remove(fam, val);
        }
      },
      stringify: (model, mutableVars, lhsSinfo, rhsSinfo) =>
        `from set ${lhsSinfo.str} remove ${rhsSinfo.str}\n`
    }),
    "if": makeOpHandler({
      argAdapters: [procedures_EagerSubformula, Statements, Statements],
      validate: (mutableVars, mutableCurrentScopeVars, conditionFmla, thenBody, elseBody) => {
        // We can't simply validate the "then" part and then the "else" part,
        // because a loop in the "else" part shouldn't be reported as shadowing a
        // variable defined only in the "then" part.  Check each part separately;
        // then, all variables that were in the current scope after either part
        // should be in the current scope after the "if".
        let currentScopeVarsByBranch = [thenBody, elseBody].map((branch) => {
          let branchVars = mutableVars.shallowClone();
          let branchCurrentScopeVars = mutableCurrentScopeVars.shallowClone();
          validateStatements(branchVars, branchCurrentScopeVars, branch);
          return branchCurrentScopeVars;
        });
        for (let csv of currentScopeVarsByBranch) {
          for (let varName of csv.elements()) {
            mutableCurrentScopeVars.add(varName);
            mutableVars.add(varName);
          }
        }
      },
      typecheck: (model, mutableVars, conditionType, thenBody, elseBody) => {
        staticExpectType("if condition", conditionType, SpecialType.BOOL);
        let varsByBranch = [thenBody, elseBody].map((branch) => {
          let branchVars = mutableVars.shallowClone();
          typecheckStatements(model, branchVars, branch);
          return branchVars;
        });
        let mergedVars = mergeTypeMaps(varsByBranch[0], varsByBranch[1]);
        // Mutate mutableVars to match mergedVars.  (Maybe if we introduce a real
        // class to track the defined variables, this will be less hacky.)  Here,
        // mergedVars should contain a superset of the keys of mutableVars.
        for (let [k, v] of mergedVars.entries()) {
          mutableVars.set(k, v);
        }
      },
      execute: (model, mutableVars, conditionTset, thenBody, elseBody) => {
        executeStatements(model, mutableVars, singleElement(conditionTset) ? thenBody : elseBody);
      },
      stringify: (model, mutableVars, conditionSinfo, thenBody, elseBody) => {
        let varsAndStringByBranch = [thenBody, elseBody].map((branch) => {
          let branchVars = mutableVars.shallowClone();
          let str = stringifyStatements(model, branchVars, branch);
          return {
            branchVars: branchVars,
            str: str
          };
        });
        let mergedVars = mergeTypeMaps(varsAndStringByBranch[0].branchVars, varsAndStringByBranch[1].branchVars);
        for (let [k, v] of mergedVars.entries()) {
          mutableVars.set(k, v);
        }
        return `if (${conditionSinfo.str}) {\n` +
          indent(varsAndStringByBranch[0].str) +
          "}" +
          (elseBody.length
            ? " else {\n" +
              indent(varsAndStringByBranch[1].str) +
              "}\n"
            : "\n");
      }
    }),
    foreach: makeOpHandler({
      argAdapters: [VarName, procedures_EagerSubformula, Statements],
      validate: (mutableVars, mutableCurrentScopeVars,
        bindVarName, domainFmla, body) => {
        mutableVars = mutableVars.shallowClone();
        mutableCurrentScopeVars = new JSONSet<string>();
        staticCheck(!mutableVars.has(bindVarName),
          () => `foreach binding of ${bindVarName} shadows a variable defined ` +
          "outside the current scope (foreach block).");
        mutableCurrentScopeVars.add(bindVarName);
        mutableVars.add(bindVarName);
        validateStatements(mutableVars, mutableCurrentScopeVars, body);
      },
      typecheck: (model, mutableVars, bindVarName, domainType, body) => {
        mutableVars = mutableVars.shallowClone();
        mutableVars.set(bindVarName, domainType);
        typecheckStatements(model, mutableVars, body);
      },
      execute: (model, mutableVars, bindVarName, domainTset, body) => {
        for (let element of domainTset.elements()) {
          let newVars = mutableVars.shallowClone();
          newVars.set(bindVarName, new TypedSet(domainTset.type, set([element])));
          executeStatements(model, newVars, body);
        }
      },
      stringify: (model, mutableVars, bindVarSinfo: VarNameStringifyInfo, domainSinfo, body) => {
        mutableVars = mutableVars.shallowClone();
        mutableVars.set(bindVarSinfo.name,
          FormulaInternals.tryTypecheckFormula(model, mutableVars, domainSinfo.formula));
        return `foreach (${bindVarSinfo.str} : ${domainSinfo.str}) {\n` +
          indent(stringifyStatements(model, mutableVars, body)) + "}\n";
      }
    }),
    "delete": makeOpHandler({
      argAdapters: [procedures_EagerSubformula],
      typecheck: (model, mutableVars, objectsType) => {
        // XXX Duplicating functionality of EagerSubformulaCells in formulas.ts.
        // It's not worth providing a whole EagerSubformulaCells wrapper yet.
        if (!typeIsReference(objectsType))  // narrowing
          throw new StaticError(`Expected a set of cells, got set of '${objectsType}'`);
        staticCheck(objectsType != rootColumnId, () => "Cannot delete the root object.");
        staticCheck(!getExistingColumn(objectsType).isViewObjectRoot,
          () => "Deleting a view object root does not make sense.");
      },
      execute: (model, mutableVars, objectsTset) => {
        // XXX: We are relying on objectsTset.type being correct!  This
        // functionality is poorly tested since we introduced typechecking.
        for (let objectId of (<JSONSet<CellId>>objectsTset.set).elements()) {
          model.CellHandle_remove(new CellHandle({columnId: <NonRootColumnId>objectsTset.type, cellId: objectId}));
        }
      },
      stringify: (model, mutableVars, objectsSinfo) => `delete ${objectsSinfo.str}\n`
    }),
    "new": makeOpHandler({
      argAdapters: [OptionalVarName, EagerFamilyRef(true, undefined, false)],
      validate: (mutableVars, mutableCurrentScopeVars, bindVarName, fref) => {
        if (bindVarName != null) {
          validateAssignment(mutableVars, mutableCurrentScopeVars, bindVarName);
        }
      },
      typecheck: (model, mutableVars, bindVarName, familyType) => {
        if (bindVarName != null) {
          mutableVars.set(bindVarName, familyType);
        }
      },
      execute: (model, mutableVars, bindVarName, fref) => {
        let newCells: CellId[] = [];
        for (let parentCellId of (<JSONSet<CellId>>fref.parentCellsTset.set).elements()) {
          let fam = new FamilyHandle({columnId: fref.columnId, parentCellId: parentCellId});
          let key = model.FamilyHandle_new(fam);
          newCells.push(cellIdChild(parentCellId, key));
        }
        // This is a little inefficient but easier to verify than doing our own thing.
        let result = FormulaInternals.cellsToValuesIfWanted(model,
          fref.wantValues, new TypedSet(fref.columnId, set(newCells)));
        if (bindVarName != null) {
          mutableVars.set(bindVarName, result);
        }
      },
      stringify: (model, mutableVars, bindVarSinfo, frefSinfo) => {
        if (bindVarSinfo != null) {
          mutableVars.set(bindVarSinfo.name,
            FormulaInternals.tryTypecheckFormula(model, mutableVars, frefSinfo.formula));
        }
        return (bindVarSinfo != null ? `let ${bindVarSinfo.str} = ` : "") + `new ${frefSinfo.str}\n`;
      }
    }),
    make: makeOpHandler({
      argAdapters: [OptionalVarName, EagerFamilyRef(false, true, true)],
      validate: (mutableVars, mutableCurrentScopeVars, bindVarName, fref) => {
        if (bindVarName != null) {
          validateAssignment(mutableVars, mutableCurrentScopeVars, bindVarName);
        }
      },
      typecheck: (model, mutableVars, bindVarName, familyType) => {
        if (bindVarName != null) {
          mutableVars.set(bindVarName, familyType);
        }
      },
      execute: (model, mutableVars, bindVarName, fref) => {
        let objects: CellId[] = [];
        for (let parentCellId of (<JSONSet<CellId>>fref.parentCellsTset.set).elements()) {
          for (let key of /*validated*/ assertNotNull(fref.keysTset).elements()) {
            let fam = new FamilyHandle({columnId: fref.columnId, parentCellId: parentCellId});
            // No-op if already exists
            model.FamilyHandle_add(fam, key);
            objects.push(cellIdChild(parentCellId, key));
          }
        }
        if (bindVarName != null) {
          mutableVars.set(bindVarName, new TypedSet(fref.columnId, set(objects)));
        }
      },
      stringify: (model, mutableVars, bindVarSinfo, frefSinfo) => {
        if (bindVarSinfo != null) {
          mutableVars.set(bindVarSinfo.name,
            FormulaInternals.tryTypecheckFormula(model, mutableVars, frefSinfo.formula));
        }
        return (bindVarSinfo != null ? `let ${bindVarSinfo.str} = ` : "") + `make ${frefSinfo.str}\n`;
      }
    }),
    check: makeOpHandler({
      argAdapters: [procedures_EagerSubformula],
      typecheck: (model, mutableVars, conditionType) => {
        staticExpectType("check condition", conditionType, SpecialType.BOOL);
      },
      execute: (model, mutableVars, conditionTset) => {
        if (!singleElement(conditionTset)) {
          throw new RuntimeError("check condition failed");
        }
      },
      stringify: (model, mutableVars, conditionSinfo) => `check ${conditionSinfo.str}\n`
    })
  };

  function mergeTypeMaps(vars1: VarTypes, vars2: VarTypes): VarTypes {
    let mergedVars = new JSONKeyedMap<string, OSType>();
    for (let [varName, type1] of vars1.entries()) {
      let type2 = vars2.get(varName);
      mergedVars.set(varName, commonSupertype(type1, fallback<OSType>(type2, SpecialType.ERROR)));
    }
    for (let varName of vars2.keys()) {
      if (vars1.get(varName) == null) {
        mergedVars.set(varName, SpecialType.ERROR);
      }
    }
    return mergedVars;
  }

  function paramsToTypeMap(params: ProcedureParamInfo[]): VarTypes {
    return new JSONKeyedMap<string, OSType>(params.map<[string, OSType]>((p) => [p.name, p.type]));
  }

  interface ParserHelper extends FormulaInternals_ParserHelper {
    varsPreviousBranch: null | VarTypes;
    varsStack: [/*previous branch*/ null | VarTypes, /*current branch*/ VarTypes][];
    pushVars: () => void;
    rollbackVars: () => void;
    nextBranch: () => void;
    commitVars: () => void;
  }
  // params must already be in final format.
  export function parseProcedure(stringProc: {name: string, params: ProcedureParamInfo[], body: string}) {
    let bodyString = stringProc.body;
    if (!/(^|\n)$/.test(bodyString)) {
      bodyString += "\n";
    }

    let parser = setupParserCommon("ENTRY_PROCEDURE", paramsToTypeMap(stringProc.params));
    let parser_yy: ParserHelper = parser.yy;
    // The following duplicates the authoritative scoping rules expressed in the
    // statement handlers, but is needed to know the correct types during parsing
    // so we can interpret navigations correctly.
    parser_yy.varsStack = [];
    parser_yy.varsPreviousBranch = null;
    parser_yy.pushVars = function() {
      this.varsStack.push([this.varsPreviousBranch, this.vars]);
      this.varsPreviousBranch = null;
      this.vars = this.vars.shallowClone();
    };
    parser_yy.rollbackVars = function() {
      [this.varsPreviousBranch, this.vars] = assertNotNull(this.varsStack.pop());
    };
    parser_yy.nextBranch = function() {
      // assert !this.varsPreviousBranch?
      this.varsPreviousBranch = this.vars;
      this.vars = this.varsStack[this.varsStack.length - 1][1].shallowClone();
    };
    parser_yy.commitVars = function() {
      let newVars = mergeTypeMaps(assertNotNull(this.varsPreviousBranch), this.vars);
      this.varsPreviousBranch = assertNotNull(this.varsStack.pop())[0];
      this.vars = newVars;
    };

    try {
      return {
        name: stringProc.name,
        params: EJSON.clone(stringProc.params),
        body: parser.parse(bodyString)
      };
    } catch (e) {
      if (e instanceof SyntaxError) {
        throw new StaticError(e.message);
      } else {
        throw e;
      }
    }
  }

  // Based on validateSubformula
  //
  // Does not use dispatchStatement for the same reasons as validateSubformula.
  function validateStatement(mutableVars: VarSet, mutableCurrentScopeVars: VarSet, statement: Unvalidated<Statement>) {
    staticCheck(_.isArray(statement), () => "Statement must be an array.");
    staticCheck(_.isString(statement[0]), () => "Statement must begin with an operation name (a string).");
    let opName: string = statement[0];
    staticCheck(procedures_dispatch.hasOwnProperty(opName), () => `Unknown operation '${opName}'`);
    let d = procedures_dispatch[opName];
    let args = statement.slice(1);
    staticCheck(args.length === d.argAdapters.length,
      () => `Wrong number of arguments to '${opName}' (required ${d.argAdapters.length}, got ${args.length})`);
    d.argAdapters.forEach((adapter, i) => {
      adapter.validate(mutableVars, mutableCurrentScopeVars, args[i]);
    });
    if (d.validate != null) {
      d.validate(mutableVars, mutableCurrentScopeVars, ...args);
    }
  }

  // Copied from formulas.ts.  More generality than we need right now.
  function dispatchStatement(action: "typecheck" | "execute" | "stringify",
    statement: Statement, ...contextArgs: fixmeAny[]) {
    let d = procedures_dispatch[statement[0]];
    let args = statement.slice(1);
    let adaptedArgs = d.argAdapters.map((adapter, i) =>
      (<fixmeAny>adapter)[action] != null
        ? (<fixmeAny>adapter)[action].apply(adapter, contextArgs.concat([args[i]]))
        : args[i]);
    return (<fixmeAny>d)[action].apply(d, contextArgs.concat(adaptedArgs));
  }

  function validateStatements(mutableVars: VarSet, mutableCurrentScopeVars: VarSet, arg: Unvalidated<Statement[]>) {
    staticCheck(_.isArray(arg), () => "Expected a list of statements");
    for (let statement of arg) {
      validateStatement(mutableVars, mutableCurrentScopeVars, statement);
    }
  }

  function typecheckStatements(model: ReadableModel, mutableVars: VarTypes, arg: Statement[]) {
    for (let statement of arg) {
      dispatchStatement("typecheck", statement, model, mutableVars);
    }
  }

  function executeStatements(model: DataWritableModel, mutableVars: VarValues, arg: Statement[]) {
    for (let statement of arg) {
      dispatchStatement("execute", statement, model, mutableVars);
    }
  }

  function stringifyStatements(model: ReadableModel, mutableVars: VarTypes, arg: Statement[]) {
    return (arg.map((statement) => dispatchStatement("stringify", statement, model, mutableVars))).join("");
  }

  export interface Statement extends Array<fixmeAny> /* :( */ {}
  // XXX Duplicates Match pattern in validateProcedure.  Anyone want to find (or
  // write) a library that generates a pattern for Match (or some similar
  // library) from these definitions using TypeScript reflection?
  export type ProcedureDoc = {
    _id: string;
    name: string;
    params: ProcedureParamInfo[];
    body: Statement[];
  };
  export type ProcedureParamInfo = {
    name: string;
    type: OSType;
    singular: boolean;
  };

  export function validateProcedure(proc: Unvalidated<ProcedureDoc>) {
    check(proc, {
      _id: Match.Optional(String),
      name: String,
      params: [
        {
          name: String,
          type: String,
          singular: Boolean
        }
      ],
      body: Match.Any  // body is fully validated by validateStatements
    });
    try {
      let mutableCurrentScopeVars = new JSONSet<string>();
      for (let param of proc.params) {
        staticCheck(!mutableCurrentScopeVars.has(param.name), () => `Duplicate parameter name ${param.name}`);
        mutableCurrentScopeVars.add(param.name);
      }
      let mutableVars = mutableCurrentScopeVars.shallowClone();
      validateStatements(mutableVars, mutableCurrentScopeVars, proc.body);
    } catch (e) {
      // XXX: Want to do this here?
      if (e instanceof StaticError) {
        throw new Meteor.Error("invalid-procedure", "Invalid procedure: " + e.message);
      } else {
        throw e;
      }
    }
  }

  let RESULT_VAR_NAME = "result";
  let REDIRECT_VAR_NAME = "redirect";

  // Returns the return type or SpecialType.ERROR if none.
  //
  // returnRedirect means initialize the "redirect" var to the empty set of type
  // SpecialType.TEXT and let that, instead of "return", be the result.  A hack
  // but I don't have a better idea right now. ~ Matt 2018-09-08
  export function typecheckProcedure(model: ReadableModel, proc: ProcedureDoc, returnRedirect = false) {
    let mutableVars = new JSONKeyedMap<string, OSType>();
    for (let param of proc.params) {
      // XXX Duplicates logic from {Type,ColumnId}.typecheck
      if (typeIsReference(param.type)) {
        staticCheck(getColumn(param.type) != null, () => `No column exists with ID ${param.type}`);
      }
      mutableVars.set(param.name, param.type);
    }
    if (returnRedirect) {
      staticCheck(mutableVars.get(REDIRECT_VAR_NAME) == null, () => "'redirect' must not be declared as a parameter.");
      mutableVars.set(REDIRECT_VAR_NAME, SpecialType.TEXT);
    }
    typecheckStatements(model, mutableVars, proc.body);
    return fallback(mutableVars.get(returnRedirect ? REDIRECT_VAR_NAME : RESULT_VAR_NAME), SpecialType.ERROR);
  }

  // It is assumed that the procedure already passed typecheckProcedure against the
  // current schema.
  //
  // May throw EvaluationError and leave the sheet in an intermediate state.
  export function executeProcedure(model: DataWritableModel, proc: ProcedureDoc, args: VarValues,
    returnRedirect = false) {
    // TODO Validate correct set of arguments present with correct types?
    // Currently we consider that the caller's responsibility.
    for (let param of proc.params) {
      if (param.singular) {
        singleElement(assertNotNull(args.get(param.name)));  // Better error message?
      }
    }
    let vars = args.shallowClone();
    if (returnRedirect) {
      vars.set(REDIRECT_VAR_NAME, new TypedSet(SpecialType.TEXT, set([])));
    }
    executeStatements(model, vars, proc.body);
    // The caller has to know not to look at this if it doesn't have a
    // consistent type.  Model.executeCannedTransaction knows that currently.
    // XXX: Redesign.
    return vars.get(returnRedirect ? REDIRECT_VAR_NAME : RESULT_VAR_NAME);
  }

  // Currently used to show triggers in the action bar.
  export function stringifyProcedureBody(proc: ProcedureDoc) {
    return stringifyStatements(getReadableModel(), paramsToTypeMap(proc.params), proc.body);
  }

  // A reasonable format for "Global macros" in the action bar.  Subject to
  // redesign.  Not the opposite of parseProcedure (which we currently don't
  // need anywhere). ~ Matt 2017-03-08
  export function stringifyProcedureDefinition(proc: ProcedureDoc) {
    let paramsText = proc.params.map((p) =>
      // "one" here is like in Alloy.  It should make Daniel happy. :)
      `${p.name}: ${p.singular ? "one " : ""}${stringifyType(p.type)}`).join(", ");
    return `${proc.name}(${paramsText}) {\n${indent(stringifyProcedureBody(proc))}}\n`;
  }

}
