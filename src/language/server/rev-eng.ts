namespace Objsheets {

  const DEBUG_REVERSE_ENGINEERING = true;

  export function generateDefaultTriggers(col: NonRootColumn, spec: ServerAppHelpers.ColumnViewUpdateSpec) {
    let fmla = fixmeAssertNotNull(col.formula);

    // XXX: The procedural code snippets below may not parse the way we expect
    // for pathological inputs.  Ideally, we would directly generate ASTs, but
    // that will be hard to read.

    // TODO: Adopt a suitable pattern matching library?
    switch (fmla[0]) {
      case "down": {
        let [/*op*/, startFmla, targetColumnId, keysFmla, wantValues] = fmla;
        // Annoying special cases in our concrete syntax.
        // C.f. end of stringifyNavigation.
        let [setupContextCode, contextCode] =
          // Root or this doesn't have to be checked for being singular.
          EJSON.equals(startFmla, ["var", "this"]) ? ["", ""] :
          EJSON.equals(startFmla, ["lit", rootColumnId, [[]]]) ? ["", "$"] :
          [
            `
              let obj = ${stringifyFormula(col.parent, startFmla)}
              # Fail if the operation would be ambiguous.
              check count(obj) = 1
            `,
            "obj."
          ];
        let targetCol = getExistingColumn(targetColumnId);
        // If a developer just makes a column editable, we have to guess whether
        // they intended to allow new, add, or both.  For now, we only handle
        // the cases in which the derived family and the underlying family are
        // both value families (add) or both object families (new).
        if (!col.isObject && !targetCol.isObject &&
          // Redundant if the formula passed type-checking?
          keysFmla == null && wantValues == true) {
          // expr.field
          spec.addTrigger = `
            ${setupContextCode}
            to set ${contextCode}${assertNotNull(targetCol.fieldName)} add value
          `;
          // XXX It seems inconsistent not to check the count on this side.
          spec.removeTrigger = `
            ${setupContextCode}
            from set ${contextCode}${assertNotNull(targetCol.fieldName)} remove value
          `;
        } else if (col.isObject && targetCol.isObject &&
          wantValues == false && keysFmla == null) {
          // Computed objects based on expr.NestedObject
          spec.newTrigger = `
            ${setupContextCode}
            let result = new ${contextCode}${assertNotNull(targetCol.objectName)}
          `;
          spec.removeTrigger = `
            delete value
          `;
        }
        break;
      }
      case "revdown": {
        // Reverse down navigation (inverse relation): member.{domain by field}
        // Adding/removing "value" to this comprehension should add or remove "member" to "value.field".
        let [/*op*/, startFmla, domainFmla, fieldColumnId] = fmla;
        let fieldCol = getExistingColumn(fieldColumnId);
        // There would be no harm in doing the local variable and the "check" in
        // the "this" case.  For now, I omit it for consistency with the style of
        // generated triggers for down navigations. ~ Matt 2017-02-25
        let [setupMemberCode, memberCode] =
          EJSON.equals(startFmla, ["var", "this"]) ?
          ["", assertNotNull(getExistingColumn(col.parent).objectName)] :
          [
            `
              let member = ${stringifyFormula(col.parent, startFmla)};
              # Fail if the operation would be ambiguous.
              check count(member) = 1
            `,
            "member"
          ];
        spec.addTrigger = `
          check value in ${stringifyFormula(col.parent, domainFmla)}
          ${setupMemberCode}
          to set value.${assertNotNull(fieldCol.fieldName)} add ${memberCode}
        `;
        spec.removeTrigger = `
          # We can't get here unless (value in ${stringifyFormula(col.parent, domainFmla)}).
          ${setupMemberCode}
          from set value.${assertNotNull(fieldCol.fieldName)} remove ${memberCode}
        `;
        break;
      }
    }

    if (spec.newTrigger == null && spec.addTrigger == null && spec.removeTrigger == null) {
      throw new StaticError(`Don't know how to reverse-engineer column ${stringifyColumnRef([col._id, true])} ` +
        `(${stringifyFormula(col.parent, fmla)})`);
    }
    if (DEBUG_REVERSE_ENGINEERING) {
      console.log(`Reverse engineered column ${stringifyColumnRef([col._id, true])} ` +
        `(${stringifyFormula(col.parent, fmla)}):`);
      console.log("new: " + spec.newTrigger);
      console.log("add: " + spec.addTrigger);
      console.log("remove: " + spec.removeTrigger);
    }
  }

}
