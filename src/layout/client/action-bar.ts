// Fork of the action bar for the custom-layout UI.  I want to make changes here
// without affecting the old UI.  We can figure out later how to reconcile the
// forks. ~ Matt 2018-01-30

namespace Objsheets.LayoutActionBar {

  //
  // Template actionBar
  //
  export interface ActionBarData {
    fullTextToShow: null | string;
    isLoading: boolean;
    origLayoutTemplate: GIWrapped<F_OrigLayoutTemplate>;
    layoutName: ColumnId;
    goToLayout: (layoutName: ColumnId) => void;
    sheetDisplayMode: SheetDisplayMode;
    switchToSheetDisplayMode: (mode: SheetDisplayMode) => boolean;
    changeColumnArgs: ChangeColumnArgs[];
    // Weird hybrid of Blaze and React coding patterns; not worth worrying
    // about.
    notifyExpansionState: (isExpanded: boolean) => void;
    withDataAccessContext: <R>(cb: () => R) => R;
    viewObjectPending: boolean;  // for spinner

    // If non-null, we're waiting for the user to select the move destination of
    // a split or allocation.
    moveArgs: null | MoveArgs;
  }
  export interface ActionBarTemplateInstance extends Blaze.TemplateInstance {
    // Some of these might make more sense on the ChangeColumnTemplateInstance,
    // but I wanted to get rid of the global variables in the simplest way
    // possible without spending time redesigning this code. ~ Matt 2018-01-30
    actionBarMode: ReactiveVar<ActionBarMode>;
    currentExpansionMode: ReactiveVar<null | ExpansionMode>;
    changeColumnRef: ReactiveVar<null | ChangeColumnTemplateInstance>;
    spinner: ExternalModules.Spinner;
    prevOperationInProgress: boolean;

    pageMenuItems: ReactiveVar<HtmlSelectItem[]>;

    // This is only used by the changeColumn template, but we keep the cache
    // across destruction and creation of changeColumn template instances.
    typeMenuCommonItems: ReactiveVar<HtmlSelectItem[]>;
  }

  export interface MoveArgs {
    what: string;
    explanation?: string;  // Plain text is enough for now.
    canConfirm: boolean;
    confirmCallback: () => void;
    cancelCallback: () => void;
    savingMove: boolean;
  }

  enum ActionBarMode {
    STRUCTURE = "structure",
    GLOBAL_MACROS = "global-macros"
  }
  export enum SheetDisplayMode {
    NORMAL = "normal",
    DESIGN_VIEW = "design-view",
    LOCKDOWN = "lockdown"
  }
  enum ExpansionMode {
    FORMULA_DEBUGGER,
    EDIT_CONFIG
  }
  function toggleExpansionMode(template: ActionBarTemplateInstance, mode: ExpansionMode) {
    template.currentExpansionMode.set(isExpansionModeOpen(template, mode) ? null : mode);
  }
  function isExpansionModeOpen(template: ActionBarTemplateInstance, mode: ExpansionMode) {
    return template.currentExpansionMode.get() == mode;
  }

  export const actionBarTemplateName = "layout_actionBar";
  let actionBarTemplate: Blaze.Template1<ActionBarTemplateInstance> = Template[actionBarTemplateName];
  actionBarTemplate.created = function() {
    this.actionBarMode = new ReactiveVar(ActionBarMode.STRUCTURE);
    this.currentExpansionMode = new ReactiveVar(null);
    this.changeColumnRef = new ReactiveVar(null);
    this.autorun(() => {
      let data: ActionBarData = Template.currentData();
      data.notifyExpansionState(isExpanded(this));
    });
    this.spinner = new ExternalModules.Spinner({scale: 0.5, width: 3});
    this.prevOperationInProgress = false;
    this.pageMenuItems = new ReactiveVar([]);
    this.typeMenuCommonItems = new ReactiveVar([]);

    // Scanning for all possible reference types is slow enough to make the selection
    // feel laggy, so cache the menu and reuse it.
    ClientAppHelpers.onOpen(() => {
      this.autorun(() => {
        // Note: It's possible to create cycles in the "key + parent" relation on
        // object types.  This is a pointless thing to do but does not break our
        // tool; it's as if all of those reference types were merely empty.  So
        // don't try to prevent it for now.
        let refItems: HtmlOption[] = [];
        function scan(colId: ColumnId) {
          let c = getExistingColumn(colId);
          if (colId !== rootColumnId && c.isObject) {
            refItems.push(new HtmlOption(colId, stringifyType(colId)));
          }
          for (let childId of c.children) {
            scan(childId);
          }
        }
        scan(rootColumnId);

        let items: HtmlSelectItem[] = [];
        items.push(new HtmlOptgroup("Basic types", MAIN_PRIMITIVE_TYPES.map((t) => new HtmlOption(t, t))));
        items.push(new HtmlOptgroup("Reference to:", refItems));
        this.typeMenuCommonItems.set(items);
      });

      this.autorun(() => {
        let items = [new HtmlOption(rootColumnId, "Master data set")];
        for (let childColumnId of getExistingColumn(rootColumnId).children) {
          let childCol = getExistingColumn(childColumnId);
          if (childCol.isViewObjectRoot) {
            items.push(new HtmlOption(childColumnId, objectNameForUI(childCol)));
          }
        }
        if (!$$.layoutsAreHardcoded()) {
          items.push(new HtmlOption(PAGE_CHOICE_NEW, "New view object type"));
        }
        this.pageMenuItems.set(items);
      });
    });
  };
  actionBarTemplate.rendered = function() {
    this.autorun(() => {
      let data: ActionBarData = Template.currentData();
      let operationInProgress = $$client.numOperationsInProgress() > 0 || data.viewObjectPending;
      if (operationInProgress != this.prevOperationInProgress) {
        if (operationInProgress) {
          this.spinner.spin(this.find("#spinnerHost"));
        } else {
          this.spinner.stop();
        }
        this.prevOperationInProgress = operationInProgress;
      }
    });
  };

  const PAGE_CHOICE_NEW = "_new";

  actionBarTemplate.helpers<ActionBarData>({
    loading: function() { return this.isLoading; },
    fullTextToShow: function() { return this.fullTextToShow; },
    isGlobalMacrosOpen: function() {
      return Template.instance(this).actionBarMode.get() == ActionBarMode.GLOBAL_MACROS;
    },
    changeColumnData: function() {
      let datas: ChangeColumnData[] = [];
      for (const args of this.changeColumnArgs) {
        // Try to fetch the data needed by the changeColumn template in advance
        // and don't render it if some of the data is missing due to a race, to
        // reduce the exposure to races within the template.
        let allocKind = null, container = null, col = null,
          textCol = null, urlCol = null, procedureName = null;
        let columnId;
        if (args.onObjectHeader) {
          columnId = args.columnId;
        } else {
          if (!giRun((w) => {
            let origLayoutTemplate = w.unwrap<F_OrigLayoutTemplate>(this.origLayoutTemplate);
            let allocationDef = fixmeAssertNotNull(origLayoutTemplate.spec.allocations.get(args.allocationId));
            container = fixmeAssertNotNull(origLayoutTemplate.getExistingRectBinding(Layout.perAxis((a) =>
              origLayoutTemplate.archetype.containerOfMergedSpan(a, allocationDef.mergedSpans[a]))).trp).type;
            switch (allocationDef.kind) {
              case Layout.TemplateAllocationDefKind.LABEL:
                throw new Error("Assertion: action bar should not target label allocations");
              case Layout.TemplateAllocationDefKind.FIELD:
                columnId = allocationDef.field;
                break;
              case Layout.TemplateAllocationDefKind.HYPERLINK:
                textCol = getColumn(allocationDef.textField);
                urlCol = getColumn(allocationDef.urlField);
                if (textCol == null || urlCol == null) return false;
                break;
              case Layout.TemplateAllocationDefKind.BUTTON:
                textCol = getColumn(allocationDef.textField);
                procedureName = allocationDef.procedureName;
                if (textCol == null) return false;
                break;
              default:
                assertUnreachable(allocationDef);
            }
            allocKind = allocationDef.kind;
            return true;
          })) {
            continue;
          }
        }
        if (columnId != null) {
          col = getColumn(columnId);
          if (col == null) continue;
        }

        datas.push({
          _id: JSON.stringify(args),
          ref: Template.instance(this).changeColumnRef,
          layoutName: this.layoutName,
          withDataAccessContext: this.withDataAccessContext,
          args,
          allocKind,
          container,
          col,
          textCol,
          urlCol,
          procedureName,
        });
      }
      return datas;
    },
    canUndo: function() {
      return !hasUnsavedData(Template.instance(this)) && $$.canUndo();
    },
    canRedo: function() {
      return !hasUnsavedData(Template.instance(this)) && $$.canRedo();
    },
    showCollapseLink: function() {
      switch (Template.instance(this).currentExpansionMode.get()) {
      case ExpansionMode.FORMULA_DEBUGGER:
      case ExpansionMode.EDIT_CONFIG:
        let cca = arrayGet(this.changeColumnArgs, 0);
        return (cca == null || cca.onObjectHeader);
      case null:
        return false;
      }
    },
    hasUnsavedData: function() {
      return hasUnsavedData(Template.instance(this));
    },
    canConfirmMove: function() {
      let moveArgs = fixmeAssertNotNull(this.moveArgs);
      return moveArgs.canConfirm && !moveArgs.savingMove;
    },
    savingMove: function() {
      return fixmeAssertNotNull(this.moveArgs).savingMove;
    },
    pageMenu: function() {
      return new HtmlSelectData(Template.instance(this).pageMenuItems.get(), this.layoutName);
    },
    layoutsAreHardcoded: function() {
      return $$.layoutsAreHardcoded();
    },
    canDeletePage: function() {
      return !hasUnsavedData(Template.instance(this)) && this.layoutName != rootColumnId;
    },
    sheetDisplayModeMenu: function() {
      let items = [new HtmlOption(SheetDisplayMode.NORMAL, "Normal view"),
        new HtmlOption(SheetDisplayMode.DESIGN_VIEW, "Design view")];
      if (this.layoutName != rootColumnId)
        items.push(new HtmlOption(SheetDisplayMode.LOCKDOWN, "Unprivileged view"));  // Better wording??
      return new HtmlSelectData(items, this.sheetDisplayMode);
    },
    actionBarModeMenu: function() {
      let actionBar = Template.instance(this);
      let items = [new HtmlOption(ActionBarMode.STRUCTURE, "Structure"),
        new HtmlOption(ActionBarMode.GLOBAL_MACROS, "Global macros")];
      return new HtmlSelectData(items, actionBar.actionBarMode.get());
    },
  });

  actionBarTemplate.events<ActionBarData>({
    "change #pageMenu": function(event, template) {
      let newPage = getValueOfSelectedOption(template, "#pageMenu");
      if (newPage == PAGE_CHOICE_NEW) {
        MeteorMethods.layoutCreateViewObjectType.call($$.id,
          $$client.wrapCallback(standardServerCallbackThen((error, result) => {
            if (error == null) {
              result = result!;
              this.goToLayout(result);
            } else {
              selectOptionWithValue(template, "#pageMenu", this.layoutName);
            }
          })));
      } else {
        this.goToLayout(<ColumnId>newPage);
      }
    },
    "click #deletePage": function(event, template) {
      // This seems to work even though the action bar may get destroyed before the callback is called.
      MeteorMethods.layoutDeleteViewObjectType.call($$.id, <NonRootColumnId>this.layoutName,
        $$client.wrapCallback(standardServerCallbackThen((error, result) => {
          if (error == null) {
            this.goToLayout(rootColumnId);
          }
        })));
    },
    "change #sheetDisplayMode": function(event, template) {
      let newMode = <SheetDisplayMode>getValueOfSelectedOption(template, "#sheetDisplayMode");
      if (!this.switchToSheetDisplayMode(newMode)) {
        selectOptionWithValue(template, "#sheetDisplayMode", this.sheetDisplayMode);
      }
    },
    "change #actionBarMode": function(event, template) {
      let newMode = <ActionBarMode>getValueOfSelectedOption(template, "#actionBarMode");
      template.actionBarMode.set(newMode);
    },
    "click #undo": function(event, template) {
      MeteorMethods.undo.call($$.id, $$client.wrapCallback(standardServerCallback));
    },
    "click #redo": function(event, template) {
      MeteorMethods.redo.call($$.id, $$client.wrapCallback(standardServerCallback));
    },
    "click #cleanup": function(event, template) {
      MeteorMethods.executeCannedTransaction.call($$.id, TRIGGER_CLEANUP, {}, null,
        $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
          if (error == null) {
            alert("Successful cleanup.");
          }
        })));
    },
    "click #collapse-link": function(event, template) {
      template.currentExpansionMode.set(null);
    },
    "click #move-confirm": function(event, template) {
      fixmeAssertNotNull(this.moveArgs).confirmCallback();
    },
    "click #move-cancel": function(event, template) {
      fixmeAssertNotNull(this.moveArgs).cancelCallback();
    },
  });

  //
  // Template changeColumn
  //
  export interface ChangeColumnArgs_Object {
    onObjectHeader: true;
    columnId: NonRootColumnId;
  }
  export interface ChangeColumnArgs_Allocation {
    onObjectHeader: false;
    allocationId: Layout.TemplateAllocationId<Layout.OrigB>;
  }
  export type ChangeColumnArgs = ChangeColumnArgs_Object | ChangeColumnArgs_Allocation;
  type ChangeColumnData = {
    _id: string;  // JSON.stringify of args.
    ref: ReactiveVar<null | ChangeColumnTemplateInstance>;
    layoutName: ColumnId;
    args: ChangeColumnArgs;
    withDataAccessContext: <R>(cb: () => R) => R;

    // actionBar template looks up all the data to prevent skew within changeColumn template.
    allocKind: null | Layout.TemplateAllocationDefKind.FIELD |
      Layout.TemplateAllocationDefKind.HYPERLINK | Layout.TemplateAllocationDefKind.BUTTON;
    container: null | ColumnId;
    col: null | NonRootColumn;
    textCol: null | NonRootColumn;
    urlCol: null | NonRootColumn;
    procedureName: null | string;
  };
  interface ChangeColumnTemplateInstance extends Blaze.TemplateInstance {
    actionBar: ActionBarTemplateInstance;

    // Non-null when a column is applicable and it exists (no data skew)
    origColumnName: ReactiveVar<null | string>;
    newColumnName: ReactiveVar<null | string>;

    fieldFormulaFieldRef: ReactiveVar<null | FormulaFieldTemplateInstance>;  // Field only
    textFormulaFieldRef: ReactiveVar<null | FormulaFieldTemplateInstance>;  // Hyperlink and button only
    urlFormulaFieldRef: ReactiveVar<null | FormulaFieldTemplateInstance>;  // Hyperlink only

    tracingView: null | TracingView;
  }

  let NESTED_UNDERLINING_PX_PER_LEVEL = 4;
  let NESTED_UNDERLINING_MAX_DEPTH = 5;

  let changeColumnTemplate: Blaze.Template1<ChangeColumnTemplateInstance> = Template["layout_changeColumn"];
  changeColumnTemplate.created = function() {
    this.actionBar = <ActionBarTemplateInstance>findAncestorTemplateInstanceOfView(this.view, actionBarTemplateName);
    let data: ChangeColumnData = this.data;
    data.ref.set(this);
    this.origColumnName = new ReactiveVar(null);
    this.newColumnName = new ReactiveVar(null);
    this.fieldFormulaFieldRef = new ReactiveVar(null);
    this.textFormulaFieldRef = new ReactiveVar(null);
    this.urlFormulaFieldRef = new ReactiveVar(null);
    this.tracingView = null;

    this.autorun(() => {
      const data1: ChangeColumnData = Template.currentData();
      this.origColumnName.set(data1.col == null ? null :
        fallback(data1.args.onObjectHeader ? data1.col.objectName : data1.col.fieldName, ""));
    });
    this.autorun(() => {
      this.newColumnName.set(this.origColumnName.get());
    });
  };
  changeColumnTemplate.rendered = function() {
    this.autorun(() => {
      let ff = this.fieldFormulaFieldRef.get();
      let showTracing = false;
      if (ff != null) {
        let info = ff.newFormulaInfo.get();
        if (info != null && info.selectedBand != null) {
          if (isExpansionModeOpen(this.actionBar, ExpansionMode.FORMULA_DEBUGGER)) {
            showTracing = true;
          } else {
            info.selectedBand.selected = false;
            info.selectedBand = undefined;
            // Retriggers this autorun, but it will do nothing the second time.
            ff.newFormulaInfo.set(info);
          }
        }
      }
      if (showTracing) {
        updateTracingView(this);
      } else {
        if (this.tracingView != null) {
          this.tracingView.destroy();
        }
        this.tracingView = null;
      }
    });
  };

  changeColumnTemplate.destroyed = function() {
    if (this.tracingView != null) {
      this.tracingView.destroy();
    }
    let data: ChangeColumnData = this.data;
    data.ref.set(null);
  };

  const TYPE_CHOICE_AUTO = "auto";
  type TypeChoice = OSType | typeof TYPE_CHOICE_AUTO;
  function origTypeChoice(col: Column) {
    return fallback<TypeChoice>(col.specifiedType, TYPE_CHOICE_AUTO);
  }
  function resetTypeChoice(template: ChangeColumnTemplateInstance) {
    let data: ChangeColumnData = template.data;
    selectOptionWithValue(template, "#changeColumn-type", origTypeChoice(assertNotNull(data.col)));
  }
  const RDC_CHOICE_AUTO = "auto";
  type RdcChoice = OSType | typeof RDC_CHOICE_AUTO;
  function origRdcChoice(col: Column) {
    return fallback<RdcChoice>(col.referenceDisplayColumn, RDC_CHOICE_AUTO);
  }
  function resetRdcChoice(template: ChangeColumnTemplateInstance) {
    let data: ChangeColumnData = template.data;
    selectOptionWithValue(template, "#changeColumn-referenceDisplayColumn",
      origRdcChoice(assertNotNull(data.col)));
  }

  function resetAllocationKindChoice(template: ChangeColumnTemplateInstance) {
    let data: ChangeColumnData = template.data;
    selectOptionWithValue(template, "#changeColumn-allocationKind", assertNotNull(data.allocKind));
  }
  function resetButtonProcedureChoice(template: ChangeColumnTemplateInstance) {
    let data: ChangeColumnData = template.data;
    selectOptionWithValue(template, "#changeColumn-buttonProcedure", assertNotNull(data.procedureName));
  }

  function changeColumnCanSave(template: ChangeColumnTemplateInstance) {
    let isValid = changeColumnActiveFormulaFields(template).every(
      (f) => {
        let info = f.newFormulaInfo.get();
        return info != null && info.formula !== undefined;
      });
    return hasUnsavedData(template.actionBar) && isValid;
  }
  function changeColumnSave(template: ChangeColumnTemplateInstance) {
    let data: ChangeColumnData = template.data;
    let col = data.col;
    if (col != null) {
      let newName = template.newColumnName.get();
      if (newName == "")
        newName = null;
      if (data.args.onObjectHeader) {
        MeteorMethods.changeColumnObjectName.call($$.id, col._id, newName,
          $$client.wrapCallback(standardServerCallback));
      } else {
        let ff = assertNotNull(template.fieldFormulaFieldRef.get());
        // canSave ensures that this is defined.
        let newFormula = assertNotNull(ff.newFormulaInfo.get()).formula;
        if (newFormula === undefined) throw new Error("assertion failed");  // narrowing
        if (col.viewObjectType == null &&
          newFormula != null && col.formula == null && stateColumnHasValues(col._id) &&
          !window.confirm("This will delete all existing data in the field.  Are you sure?")) {
          return;
        }
        // For now, going computed -> state clears rather than snapshotting.
        // ~ Matt 2018-02-13

        // Canonicalize the string in the field, otherwise the field might stay
        // yellow after successful submission.
        ff.newFormulaStr.set(strForFormula(col, newFormula));
        MeteorMethods.layoutChangeColumnFieldNameAndFormula.call(
          $$.id, col._id, newName, newFormula,
          $$client.wrapCallback(standardServerCallback));
      }
    } else {
      let changes: Layout.ColumnIdAndFormula[] = [];
      for (let f of changeColumnActiveFormulaFields(template)) {
        let fData: FormulaFieldData = f.data;
        let newFormula = assertNotNull(f.newFormulaInfo.get()).formula;
        if (newFormula === undefined) throw new Error("assertion failed");  // narrowing
        f.newFormulaStr.set(strForFormula(fData.col, newFormula));  // Canonicalize as above.
        changes.push({columnId: fData.col._id, formula: newFormula});
      }
      MeteorMethods.layoutChangeMultipleColumnFormulas.call(
        $$.id, changes, $$client.wrapCallback(standardServerCallback));
    }
  }
  function changeColumnRevert(template: ChangeColumnTemplateInstance) {
    template.newColumnName.set(template.origColumnName.get());
    for (let f of changeColumnActiveFormulaFields(template)) {
      f.newFormulaStr.set(f.origFormulaStr.get());
    }
  }

  changeColumnTemplate.helpers<ChangeColumnData>({
    onObjectHeader: function() {
      return this.args.onObjectHeader;
    },
    shouldDisableAllocationKindMenu: function() {
      return hasUnsavedData(Template.instance(this).actionBar) ||
        // Cannot destroy the key of a keyed object.
        this.col != null && this.col.isObject;
    },
    allocationKindMenu: function() {
      let items = [
        new HtmlOption(Layout.TemplateAllocationDefKind.FIELD, "Field"),
        new HtmlOption(Layout.TemplateAllocationDefKind.HYPERLINK, "Hyperlink"),
        new HtmlOption(Layout.TemplateAllocationDefKind.BUTTON, "Button")
      ];
      return new HtmlSelectData(items, assertNotNull(this.allocKind));
    },
    isNameable: function() {
      return this.col != null;
    },
    columnNamePrefix: function() {
      let columnName = stringifyColumnRef([assertNotNull(this.col)._id, !this.args.onObjectHeader]);
      let lastColon = columnName.lastIndexOf(":");
      // If lastColon == -1, gives the empty string as desired. :)
      return columnName.substring(0, lastColon + 1);
    },
    newColumnName: function() {
      return Template.instance(this).newColumnName.get();
    },
    containerName: function() {
      return fullObjectNameForUI(assertNotNull(this.container));
    },
    isNameModified: function() {
      let template = Template.instance(this);
      return template.newColumnName.get() != template.origColumnName.get();
    },
    isComputed: function() {
      return assertNotNull(this.col).formula != null;
    },
    isViewObjectRoot: function() {
      return assertNotNull(this.col).isViewObjectRoot;
    },
    keyColumnName: function() {
      let col = assertNotNull(this.col);
      return this.args.onObjectHeader && getColumnType(col) !== SpecialType.TOKEN ? fieldNameForUI(col) : null;
    },
    canSave: function() {
      return changeColumnCanSave(Template.instance(this));
    },
    canRevert: function() {
      return hasUnsavedData(Template.instance(this).actionBar);
    },
    isField: function() {
      return this.allocKind == Layout.TemplateAllocationDefKind.FIELD;
    },
    // When a state <-> computed change is pending, we preview how the type will
    // change as a result of saving the formula.  It would be confusing to let
    // the user change the type at this time.
    //
    // XXX: This is a little weird.  Think if we can improve the flow.
    shouldDisableTypeMenu: function() {
      let template = Template.instance(this);
      let col = assertNotNull(this.col);
      let ff = template.fieldFormulaFieldRef.get();
      let info = (ff == null) ? null : ff.newFormulaInfo.get();
      return info == null || (col.formula != null) != (info.formula !== null);
    },
    typeMenu: function() {
      let template = Template.instance(this);
      let col = assertNotNull(this.col);
      let ff = template.fieldFormulaFieldRef.get();
      let info = (ff == null) ? null : ff.newFormulaInfo.get();
      let oldIsComputed = (col.formula != null);
      let newIsComputed = (info != null && info.formula !== null);
      let items: HtmlSelectItem[] = [];
      if (newIsComputed) {
        // Note: Inferred type should match c.type if c.specifiedType is null and
        // there are no unsaved changes to the formula.
        let inferredTypeDesc = info != null && info.type != null
          ? stringifyType(info.type) : SpecialType.ERROR;
        items.push(new HtmlOption(TYPE_CHOICE_AUTO, `auto (${inferredTypeDesc})`));
      }
      for (let item of template.actionBar.typeMenuCommonItems.get()) {
        items.push(item);
      }
      let origChoice =
        (oldIsComputed && !newIsComputed) ? DEFAULT_STATE_FIELD_TYPE :
        (newIsComputed && !oldIsComputed) ? TYPE_CHOICE_AUTO :
        origTypeChoice(col);
      return new HtmlSelectData(items, origChoice);
    },
    referenceDisplayColumnMenu: function() {
      let col = assertNotNull(this.col);
      let defaultColId = defaultReferenceDisplayColumn(col);
      let defaultColDesc = defaultColId != null ?
        fallback(getExistingColumn(defaultColId).fieldName, "unnamed") : "none";
      let items = [new HtmlOption(RDC_CHOICE_AUTO, `Choose automatically (${defaultColDesc})`)];
      for (let displayColId of allowedReferenceDisplayColumns(col)) {
        let displayCol = getExistingColumn(displayColId);
        items.push(new HtmlOption(displayColId, fieldNameForUI(displayCol)));
      }
      return new HtmlSelectData(items, origRdcChoice(col));
    },
    contextText: function() {
      let col = assertNotNull(this.col);
      return col.isObject  // i.e., we are editing the formula of a key column
        ? objectNameForUI(getExistingColumn(col.parent))
        : null;
    },
    fieldFormulaFieldData: function(): FormulaFieldData {
      let template = Template.instance(this);
      return {
        ref: template.fieldFormulaFieldRef,
        col: assertNotNull(this.col),
        showBands: isExpansionModeOpen(Template.instance(this).actionBar, ExpansionMode.FORMULA_DEBUGGER),
        trySaveCallback: () => { if (changeColumnCanSave(template)) changeColumnSave(template); },
        tryRevertCallback: () => { changeColumnRevert(template); },
      };
    },
    isHyperlink: function() {
      return this.allocKind == Layout.TemplateAllocationDefKind.HYPERLINK;
    },
    textFormulaFieldData: function(): FormulaFieldData {
      let template = Template.instance(this);
      return {
        ref: template.textFormulaFieldRef,
        col: assertNotNull(this.textCol),
        showBands: false,
        trySaveCallback: () => { if (changeColumnCanSave(template)) changeColumnSave(template); },
        tryRevertCallback: () => { changeColumnRevert(template); },
      };
    },
    urlFormulaFieldData: function(): FormulaFieldData {
      let template = Template.instance(this);
      return {
        ref: template.urlFormulaFieldRef,
        col: assertNotNull(this.urlCol),
        showBands: false,
        trySaveCallback: () => { if (changeColumnCanSave(template)) changeColumnSave(template); },
        tryRevertCallback: () => { changeColumnRevert(template); },
      };
    },
    buttonProcedureMenu: function() {
      let container = assertNotNull(this.container);
      let items = [new HtmlOption("", "(none)")];
      Procedures.find().forEach((proc) => {
        if (Layout.buttonProcedureIsValid(container, proc)) {
          items.push(new HtmlOption(proc.name, proc.name));
        }
      });
      return new HtmlSelectData(items, fallback(this.procedureName, ""));
    },
    isFormulaDebuggerOpen: function() {
      return isExpansionModeOpen(Template.instance(this).actionBar, ExpansionMode.FORMULA_DEBUGGER);
    },
    isEditConfigOpen: function() {
      let col = this.col;
      // Precondition: !onObjectHeader, but column may be a state column.
      return isExpansionModeOpen(Template.instance(this).actionBar, ExpansionMode.EDIT_CONFIG) &&
        col != null && col.formula != null;
    },
    // Edit config panel
    computedIsEditable: function() {
      return getEditabilityColumn(assertNotNull(this.col)) != null;
    },
    editabilityFormulaStr: function() {
      let col = assertNotNull(this.col);
      // Note: the original column and its editability column have the same parent.
      // Note: Template only calls this if `computedIsEditable` returned true.
      // Editability columns should be computed.
      return stringifyFormula(col.parent, assertNotNull(getEditabilityColumn(col)!.formula));
    },
    areTriggersDefault: function() {
      return assertNotNull(this.col).areTriggersDefault;
    },
    triggerEntries: function() {
      let col = assertNotNull(this.col);
      let entries = [];
      for (let triggerType of [TriggerType.NEW, TriggerType.ADD, TriggerType.REMOVE]) {
        let proc = Procedures.findOne({name: triggerName(col, triggerType)});
        if (proc != null) {
          // Don't bother to show the parameters because they are simple enough?
          entries.push({name: triggerType, body: stringifyProcedureBody(proc)});
        }
      }
      return entries;
    }
  });

  class TracingView {
    public grid: ViewGrid;
    public hot: Handsontable;

    constructor(domElement: Element) {
      // TODO: Add a bunch more settings?
      this.grid = [];
      this.hot = new Handsontable(domElement, {
        readOnly: true,
        readOnlyCellClassName: "",  // Not useful to dim everything.
        cells: (row, col, prop) => {
          let gridRow = arrayGet(this.grid, row);
          let cell = gridRow != null ? arrayGet(gridRow, col) : null;
          if (!cell) {
            return {};  // Would like to understand why this is needed...
          }
          return {
            className: cell.cssClasses.join(" ")
          };
        }
      });
    }

    public show(node: ExtendedSubformulaTree, extrasMap: FormulaExtrasMap) {
      let formula = node.formula;
      let extras = assertNotNull(extrasMap.get(formula));
      function formatOutcome(outcome: Outcome) {
        if (outcome.result != null) {
          // TODO: Display in individual cells so we can support the
          // referent-related features.  Or do we like this better?
          // We could at least base the curly braces on
          // type-checking-level singular-ness once we have it.
          return tsetToTextIgnoreErrors(getReadableModel(), outcome.result);
        } else {
          return outcome.error;
        }
      }
      // Exclude subformulas with additional bound variables, e.g., filter
      // predicate.  Currently, the only way to debug them is to select them
      // directly.  If we add support for a persistent set of test cases that are
      // heterogeneous in the local variables they define, we can probably remove
      // this restriction.
      //
      // Also throw out implicit "this" again. :/
      let childrenToShow = node.children.filter((childInfo) => {
        let childExtra = assertNotNull(extrasMap.get(childInfo.node.formula));
        return (childExtra.loc != null) && EJSON.equals(childExtra.vars, extras.vars);
      });
      // TODO: Enforce outside-to-inside order ourselves rather than relying on it
      // as a side effect of object iteration order and the way we typecheck
      // formulas.
      let varsAndTypesList = fixmeAssertNotNull(extras.vars).entries().filter((e) => e[1] !== rootColumnId);
      this.grid = [[], []];
      function typeCell(type: OSType) {
        return new ViewCell(stringifyTypeForSheet(type), 1, 1, ["rsHeader"].concat(markDisplayClassesForType(type)));
      }
      for (let [name, type] of varsAndTypesList) {
        this.grid[0].push(new ViewCell(name, 1, 1, ["rsHeader"]));
        this.grid[1].push(typeCell(type));
      }
      for (let childInfo of childrenToShow) {
        this.grid[0].push(new ViewCell(childInfo.paramName, 1, 1, ["rsHeader"]));
        this.grid[1].push(typeCell(fixmeAssertNotNull(assertNotNull(extrasMap.get(childInfo.node.formula)).type)));
      }
      this.grid[0].push(new ViewCell("Result", 1, 1, ["rsHeader"]));
      this.grid[1].push(typeCell(fixmeAssertNotNull(extras.type)));
      // XXX It would be cleaner for traceColumnFormula to ensure "traces" was
      // created at least as an empty list on all subformulas, but more work to
      // implement.
      for (let [varValues, outcome] of extras.traces != null ? extras.traces.entries() : []) {
        let line = varsAndTypesList.map(([name, _]) =>
          new ViewCell(tsetToTextIgnoreErrors(getReadableModel(), assertNotNull(varValues.get(name)))));
        for (let childInfo of childrenToShow) {
          let childTraces = assertNotNull(extrasMap.get(childInfo.node.formula)).traces;
          let childOutcome = (childTraces == null) ? null : childTraces.get(varValues);
          // XXX Would we rather just evaluate cases that weren't originally reached?
          line.push(new ViewCell(childOutcome != null ? formatOutcome(childOutcome) : "(not reached)"));
        }
        line.push(new ViewCell(formatOutcome(outcome)));
        this.grid.push(line);
      }
      let data = this.grid.map((row) => row.map((cell) => <string>cell.value));
      this.hot.loadData(data);
    }

    public destroy() {
      this.hot.destroy();
    }
  }

  function updateTracingView(template: ChangeColumnTemplateInstance) {
    let formulaInfo = assertNotNull(assertNotNull(template.fieldFormulaFieldRef.get()).newFormulaInfo.get());
    let data: ChangeColumnData = template.data;
    data.withDataAccessContext(() => {
      // Tracing can be slow, so do it only on first demand.  (Longer term, we should
      // optimize it!)
      if (!formulaInfo.haveTraced) {
        traceColumnFormula(fixmeAssertNotNull(formulaInfo.root).formula,
          assertNotNull(data.col)._id, assertNotNull(formulaInfo.extrasMap));
        formulaInfo.haveTraced = true;
      }
      if (template.tracingView == null) {
        template.tracingView = new TracingView(template.find("#TracingView"));
      }
      // Calls tsetToText, etc., which may depend on a view object.
      template.tracingView.show(fixmeAssertNotNull(formulaInfo.selectedBand).node,
        assertNotNull(formulaInfo.extrasMap));
    });
  }

  function changeColumnActiveFormulaFields(template: ChangeColumnTemplateInstance) {
    let ret = [];
    for (let ref of [template.fieldFormulaFieldRef, template.textFormulaFieldRef, template.urlFormulaFieldRef]) {
      let f = ref.get();
      if (f != null) ret.push(f);
    }
    return ret;
  }

  export function hasUnsavedData(template: ActionBarTemplateInstance) {
    let data = <ActionBarData>Blaze.getData(template.view);  // reactive
    let cc = template.changeColumnRef.get();
    return data.moveArgs != null ||
      cc != null &&
      (cc.newColumnName.get() != cc.origColumnName.get() ||
       changeColumnActiveFormulaFields(cc).some((f) => formulaFieldHasUnsavedData(f)));
  }

  export function isExpanded(template: ActionBarTemplateInstance) {
    return template.actionBarMode.get() == ActionBarMode.GLOBAL_MACROS || template.currentExpansionMode.get() != null;
  }

  changeColumnTemplate.events<ChangeColumnData>({
    "change #changeColumn-allocationKind": function(event, template) {
      giRun((w) => {
        if (this.args.onObjectHeader)  // narrowing
          throw new Error("assertion");
        let newKind = <Layout.TemplateAllocationDefKind>
          getValueOfSelectedOption(template, "#changeColumn-allocationKind");
        Layout.getGenericIndexLayoutMethods(w).layoutChangeAllocationKind.call(
          $$.id, this.layoutName, {
            allocationId: this.args.allocationId,
            newKind
          }, $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
            if (error) {
              // XXX This may not be foolproof against races but is better than
              // not trying.
              resetAllocationKindChoice(template);
            }
          })));
      });
    },
    "change #changeColumn-type": function(event, template) {
      let col = assertNotNull(this.col);
      let newTypeChoice = getValueOfSelectedOption(template, "#changeColumn-type");
      let newSpecifiedType = (newTypeChoice == TYPE_CHOICE_AUTO) ? null : <OSType>newTypeChoice;
      // If the new type is text, there is no risk of conversion failure, but I
      // think it's still valuable to explain what's happening.  Right?
      // ~ Matt 2015-12-04
      if (col.viewObjectType == null && col.formula == null && stateColumnHasValues(col._id) &&
        !window.confirm("This will attempt to reinterpret existing values as the new type.  " +
          "Any values that cannot be converted will be deleted.  Proceed?")) {
        resetTypeChoice(template);
        return;
      }
      MeteorMethods.changeColumnSpecifiedType.call($$.id, col._id, newSpecifiedType,
        $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
          if (error) {
            // XXX This may not be foolproof against races but is better than
            // not trying.
            resetTypeChoice(template);
          }
        })));
    },
    "change #changeColumn-referenceDisplayColumn": function(event, template) {
      let col = assertNotNull(this.col);
      let newRdcChoice = getValueOfSelectedOption(template, "#changeColumn-referenceDisplayColumn");
      let newReferenceDisplayColumn = (newRdcChoice == RDC_CHOICE_AUTO) ? null : <ColumnId>newRdcChoice;
      MeteorMethods.changeColumnReferenceDisplayColumn.call(
        $$.id, col._id, newReferenceDisplayColumn,
        $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
          if (error) {
            // XXX This may not be foolproof against races but is better than
            // not trying.
            resetRdcChoice(template);
          }
        })));
    },
    "change #changeColumn-buttonProcedure": function(event, template) {
      giRun((w) => {
        if (this.args.onObjectHeader)  // narrowing
          throw new Error("assertion");
        let newProcedureName: null | string = getValueOfSelectedOption(template, "#changeColumn-buttonProcedure");
        if (newProcedureName == "") newProcedureName = null;
        Layout.getGenericIndexLayoutMethods(w).layoutChangeAllocationButtonProcedure.call(
          $$.id, this.layoutName, {
            allocationId: this.args.allocationId,
            newProcedureName
          }, $$client.wrapCallback(standardServerCallbackThen<void>((error, result) => {
          if (error) {
            // XXX This may not be foolproof against races but is better than
            // not trying.
            resetButtonProcedureChoice(template);
          }
        })));
      });
    },
    "click .save": function(event, template) {
      changeColumnSave(template);
    },
    "click .revert": (event, template) => {
      changeColumnRevert(template);
    },
    "click .formulaDebuggerToggle": (event, template) => {
      toggleExpansionMode(template.actionBar, ExpansionMode.FORMULA_DEBUGGER);
    },
    "click #editconfig-link": (event, template) => {
      toggleExpansionMode(template.actionBar, ExpansionMode.EDIT_CONFIG);
    },
    "input .name-edit-input": (event, template) => {
      template.newColumnName.set((<HTMLInputElement>event.target).value);
    }
  });

  //
  // Template formulaField
  //
  interface FormulaFieldData {
    ref: ReactiveVar<null | FormulaFieldTemplateInstance>;
    col: NonRootColumn;
    showBands: boolean;
    trySaveCallback: () => void;
    tryRevertCallback: () => void;
  }
  interface FormulaFieldTemplateInstance extends Blaze.TemplateInstance {
    origFormulaStr: ReactiveVar<string>;
    newFormulaStr: ReactiveVar<string>;
    newFormulaInfo: ReactiveVar<null | FormulaInfo>;  // Only null for a brief time during creation
    codeMirror: CodeMirror.Editor;
    codeMirrorDoc: CodeMirror.Doc;
    formulaBandsView: undefined | Blaze.View;
  }
  let formulaFieldTemplate: Blaze.Template1<FormulaFieldTemplateInstance> = Template["layout_formulaField"];
  formulaFieldTemplate.created = function() {
    let data: FormulaFieldData = this.data;
    data.ref.set(this);
    this.origFormulaStr = new ReactiveVar("");
    this.newFormulaStr = new ReactiveVar("");
    this.newFormulaInfo = new ReactiveVar(null);
    // Separate these two computations so that changes that invalidate the first
    // computation without actually changing the formula (e.g., changing the
    // column type) don't blow away unsaved formula changes.  The old UI could
    // use this fix too, but I'm not bothering now. ~ Matt 2018-02-13
    this.autorun(() => {
      let data1: FormulaFieldData = Template.currentData();
      let formulaStr = strForFormula(data1.col, data1.col.formula);
      this.origFormulaStr.set(formulaStr);
    });
    this.autorun(() => {
      // XXX What if there are unsaved changes when the formula changes externally?
      this.newFormulaStr.set(this.origFormulaStr.get());
    });
  };
  formulaFieldTemplate.rendered = function() {
    this.codeMirror = CodeMirror(this.find("#changeFormula-formula"), {
      value: "",  // filled in by autorun below
      extraKeys: {
        Enter: (cm) => {
          let data: FormulaFieldData = this.data;
          data.trySaveCallback();
        },
        Esc: (cm) => {
          let data: FormulaFieldData = this.data;
          data.tryRevertCallback();
        }
      }
    });
    this.codeMirrorDoc = this.codeMirror.getDoc();
    this.autorun(() => {
      let data: FormulaFieldData = Template.currentData();
      // http://stackoverflow.com/a/15256593
      let height = this.codeMirror.defaultTextHeight() + 2 * 4;
      // Save room for a horizontal scrollbar for long formulas.  It's ugly to
      // have extra white space at other times, but I didn't see an obvious
      // good alternative.  (CodeMirror doesn't have an option to always show
      // the scrollbar, and when I tried to resize the CodeMirror based on the
      // presence of the scrollbar, I ran into some glitches.)
      // ~ Matt 2017-02-16
      height += (<fixmeAny>this.codeMirror).display.nativeBarWidth;
      if (data.showBands) {
        height += NESTED_UNDERLINING_PX_PER_LEVEL * NESTED_UNDERLINING_MAX_DEPTH;
      }
      this.codeMirror.setSize("100%", height);
    });
    // Generate the newFormulaInfo.
    this.autorun(() => {
      let data: FormulaFieldData = Template.currentData();
      let formulaStr = this.newFormulaStr.get();
      // Avoid re-setting in response to user input, since this sends the cursor
      // back to the beginning.  Wish for a better two-way binding mechanism...
      if (formulaStr !== this.codeMirrorDoc.getValue()) {
        this.codeMirrorDoc.setValue(formulaStr);
      }
      this.newFormulaInfo.set(generateFormulaInfo(this, data));
    });
    this.codeMirror.on("beforeChange", (cm, change) => {
      if (change.update != null) {
        let newtext = change.text.join("").replace(/\n/g, "");
        change.update(undefined, undefined, [newtext]);
      }
      // Otherwise, change is coming from undo or redo; hope it's OK.
    });
    this.codeMirror.on("changes", (cm) => {
      this.newFormulaStr.set(this.codeMirrorDoc.getValue());
    });
    // Inject formula bands this into the scrollable part of the CodeMirror.
    // Relying on the CodeMirror-sizer having `position: relative`.
    this.formulaBandsView = Blaze.renderWithData(formulaBandsTemplate, (): FormulaBandsData => {
      // Tested; this does seem to give us the FormulaFieldData, probably via this.view.
      let data: FormulaFieldData = Template.currentData();
      return {formulaInfo: data.showBands ? this.newFormulaInfo.get() : null};
    }, this.find(".CodeMirror-sizer"), undefined, this.view);
  };
  formulaFieldTemplate.destroyed = function() {
    Blaze.remove(assertNotNull(this.formulaBandsView));
    let data: FormulaFieldData = this.data;
    data.ref.set(null);
  };

  function strForFormula(col: NonRootColumn, formula: null | Formula) {
    return formula == null || EJSON.equals(formula, DUMMY_FORMULA) ? "" :
      stringifyFormula(col.parent, formula);
  }

  interface FormulaInfo {
    // null: no formula, i.e., state column
    // undefined: invalid edited formula, cannot save
    formula?: null | Formula;
    type?: OSType;
    extrasMap?: FormulaExtrasMap;
    error?: string;
    root?: ExtendedSubformulaTree;
    bands?: FormulaBand[];
    selectedBand?: FormulaBand;
    haveTraced?: boolean;
  }
  interface FormulaBand {
    node: ExtendedSubformulaTree;
    selected: boolean;
    left: number;
    width: number;
    top: number;
    height: number;
  }
  interface ExtendedSubformulaTree extends SubformulaTree {
    height?: number;
    ch1?: number;
    ch2?: number;
    children: {paramName: string, node: ExtendedSubformulaTree}[];
  }
  function generateFormulaInfo(template: FormulaFieldTemplateInstance, data: FormulaFieldData) {
    let formulaStr = template.newFormulaStr.get();
    let formulaInfo: FormulaInfo = {};
    let col = data.col;
    let origFormulaStr = template.origFormulaStr.get();
    let parentColumnId = col.parent;
    let parentCol = getExistingColumn(parentColumnId);
    if (formulaStr === "") {
      // The user wants to clear the formula.
      if (col.isObject) {
        // Don't let the user try to clear the key formula of a computed
        // collection, even to DUMMY_FORMULA, because a subsequent demotion of
        // the collection then would not be reversible by any single command,
        // which would be weird.  (Note that the custom-layout UI doesn't
        // support state keyed objects, so any key column is computed.  Demotion
        // of a state collection with an unused field can be reversed by adding
        // a collection.)
        formulaInfo.error = "Cannot clear the key formula of a computed collection.";
      } else if (parentCol.formula != null || !col.userVisible /* hack */) {
        // The field must remain computed, so use DUMMY_FORMULA.
        formulaInfo.formula = DUMMY_FORMULA;
        formulaInfo.type = SpecialType.EMPTY;
      } else {
        // The field can be changed to state.
        formulaInfo.formula = null;
      }
      // Do not construct a subformula tree.
      return formulaInfo;
    }
    try {
      // XXX: Doesn't take into account unsaved change to name of current column.
      let ret = parseFormulaWithExtras(parentColumnId, formulaStr);
      formulaInfo.formula = ret.formula;
      formulaInfo.extrasMap = ret.extrasMap;
      // Determine types of all subformulas.  Obviously in the future
      // we want to be able to help troubleshoot ill-typed formulas, but we punt
      // all error tolerance until later rather than fight that complexity now.
      formulaInfo.type = typecheckFormulaWithExtras(getReadableModel(), new JSONKeyedMap([["this", parentColumnId]]),
        formulaInfo.formula, formulaInfo.extrasMap);
    } catch (e) {
      if (!(e instanceof StaticError)) {
        throw e;
      }
      // TODO: More graceful error handling
      formulaInfo.error = e.message;
      // If the formula string is unchanged but can't be reparsed (due to
      // ambiguous names, etc.), carry over the old formula so the user can save
      // a rename of the field, which might be a step toward breaking a logjam
      // of unparseable formulas.
      if (formulaStr == origFormulaStr) {
        formulaInfo.formula = col.formula;
        if (formulaInfo.formula != null) {
          formulaInfo.type = typecheckFormula(getReadableModel(), new JSONKeyedMap([["this", parentColumnId]]),
          formulaInfo.formula);
        }
      }
      return formulaInfo;
    }
    const extrasMap = formulaInfo.extrasMap;
    formulaInfo.root = getSubformulaTree(formulaInfo.formula);
    formulaInfo.bands = [];
    function layoutSubtree(node: ExtendedSubformulaTree) {
      // It looks silly to offer to debug a literal, though it's not harmful.  Open
      // to counterarguments. ~ Matt
      if (node.formula[0] === "lit") {
        node.height = -1;
        return;
      }
      let loc = fixmeAssertNotNull(assertNotNull(extrasMap.get(node.formula)).loc);
      node.ch1 = loc.first_column;
      node.ch2 = loc.last_column;
      node.height = 0;
      for (let childInfo of node.children) {
        // loc missing for implicit "this" inserted by resolveNavigation; other cases?
        if (assertNotNull(extrasMap.get(childInfo.node.formula)).loc != null) {
          layoutSubtree(childInfo.node);
          let isNavigationLhs = (node.formula[0] === "up" || node.formula[0] === "down")
            && childInfo.paramName === "start";
          if (isNavigationLhs) {
            node.ch1 = fixmeAssertNotNull(childInfo.node.ch2);
          }
          node.height = Math.max(node.height, fixmeAssertNotNull(childInfo.node.height) + (isNavigationLhs ? 0 : 1));
        }
      }
      let top = 4 + template.codeMirror.defaultTextHeight() + NESTED_UNDERLINING_PX_PER_LEVEL * node.height;
      // Tweak for gaps in navigation chains.
      let x1 = template.codeMirror.cursorCoords({
        line: 0,
        ch: node.ch1
      }, "local").left;
      let x2 = template.codeMirror.cursorCoords({
        line: 0,
        ch: node.ch2
      }, "local").left;
      fixmeAssertNotNull(formulaInfo.bands).push({
        node: node,
        selected: false,
        left: x1,
        width: x2 - x1,
        top: top,
        height: NESTED_UNDERLINING_PX_PER_LEVEL
      });
    }
    layoutSubtree(formulaInfo.root);
    formulaInfo.selectedBand = undefined;
    formulaInfo.haveTraced = false;
    return formulaInfo;
  }

  formulaFieldTemplate.helpers<FormulaFieldData>({
    newFormulaInfo: function() {
      return Template.instance(this).newFormulaInfo.get();
    },
    isFormulaModified: function() {
      return formulaFieldHasUnsavedData(Template.instance(this));
    }
  });
  formulaFieldTemplate.events<FormulaBand>({
    "click .formulaBand": function(event, template) {
      // Update selection.
      let formulaInfo = fixmeAssertNotNull(template.newFormulaInfo.get());
      if (formulaInfo.selectedBand != null) {
        formulaInfo.selectedBand.selected = false;
      }
      formulaInfo.selectedBand = this;
      this.selected = true;
      template.newFormulaInfo.set(formulaInfo);  // Trigger reactive dependents
    }
  });

  function formulaFieldHasUnsavedData(template: FormulaFieldTemplateInstance) {
    return template.newFormulaStr.get() != template.origFormulaStr.get();
  }

  //
  // Template formulaBands
  //
  interface FormulaBandsData {
    formulaInfo: null | FormulaInfo;
  }
  let formulaBandsTemplate: Blaze.Template1<Blaze.TemplateInstance> = Template["layout_formulaBands"];

  //
  // Template globalMacros
  //
  interface GlobalMacrosTemplateInstance extends Blaze.TemplateInstance {}
  let globalMacrosTemplate: Blaze.Template1<GlobalMacrosTemplateInstance> = Template["layout_globalMacros"];
  globalMacrosTemplate.helpers<{}>({
    macrosText: () => {
      return Procedures.find().fetch()
        // Skip procedures that look like triggers.
        .filter((proc) => !/_(new|add|remove)$/.test(proc.name))
        .map(stringifyProcedureDefinition)
        .join("\n");
    }
  });

}
