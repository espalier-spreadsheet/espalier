namespace Objsheets.Layout {

  // See comment about structure editing caveats in src/layout/operations.ts .

  function deinstantiateMergedSpanId<A extends AxisL>(mergedSpanId: MergedSpanId<DataS, VisualB, A>) {
    if (singleOfPerExtreme(perExtreme((e) => mergedSpanId[e].objectId)) != null) {
      return perExtreme((e) => templateSpanOfDataSpan(mergedSpanId[e]));
    } else {
      return null;
    }
  }

  interface ContextMenuCommand {
    name: string;
    callback?: () => void;
    disabled?: () => boolean;
  }

  export function buildContextMenu<_>(component: LayoutUIComponentWithSizedData<_>) {
    if (component.state.pendingMove != null) {
      return false;
    }

    let layoutInfo = component.props.layoutInfo;
    const selectionInfo = component.state.selectionInfo;
    let contextMenuItems: ContextMenuCommand[] = [];

    if (selectionInfo != null) {

      let objectTypeName = (columnId: ColumnId) =>
        objectNameForUI(getExistingColumn(columnId));

      // If objects are selected, find the actual objects.
      let objects: CellIdWithSpares[] = [],
        objectsCanDelete: CellHandle[] = [], objectsCanAdd: CellHandle[] = [];
      if (selectionInfo.objectsBinding.trp != null) {
        let spanObjects: {[A in AxisG<_>]: CellIdWithSpares[]} = perAxis((a) =>
          // Little optimization.  The only way we can have multiple objects on
          // an axis is if each constitutent span of the merged span is an
          // entire object.
          selectionInfo.selection[a][Extreme.FIRST].templateSpan == selectionInfo.objectsBinding.origRectangle[a]
          ? layoutInfo.dataLayout.getSpansInMergedSpan(a, selectionInfo.selection[a])
            // Filter out the extra space span, if present.
            .filter((s) => s.templateSpan == selectionInfo.objectsBinding.origRectangle[a])
            .map((s) => s.objectId)
          : [selectionInfo.selection[a][Extreme.FIRST].objectId]);
        for (let rowSpanObject of spanObjects[Axis.ROW]) {
          for (let colSpanObject of spanObjects[Axis.COL]) {
            objects.push(layoutInfo.dataLayoutSpec.cross(
              selectionInfo.objectsBinding.origRectangle,
              {[Axis.ROW]: rowSpanObject, [Axis.COL]: colSpanObject}));
          }
        }
        if (selectionInfo.canAddOrDeleteSelectedObjects) {
          for (let obj of objects) {
            if (!cellIdIsReal(obj)) continue;
            let ch = new CellHandle({columnId: selectionInfo.objectsBinding.trp.type, cellId: obj});
            let fam = ch.containingFamily();
            try {
              if (fam.isEditable()) {
                // If the object is erroneous, then we can neither add nor delete it.
                if (ch.existsPropagateErrors()) {
                  objectsCanDelete.push(ch);
                } else {
                  objectsCanAdd.push(ch);
                }
              }
            } catch (e) {
              if (!(e instanceof RuntimeError)) throw e;
            }
          }
        }
      }

      if (objectsCanAdd.length > 0) {
        // The only kind of nonexistent object we can select should be a cross object.
        assert(selectionInfo.objectsBinding && selectionInfo.objectsBinding.objectAxis == null);
        if (objectsCanAdd.length == 1) {
          let [ch] = objectsCanAdd;
          let fam = ch.containingFamily();
          let keyText = valueToTextIgnoreErrors(getReadableModel(), fam.type(), ch.value());
          contextMenuItems.push({
            name: `Add ${objectTypeName(fixmeAssertNotNull(selectionInfo.objectsBinding.trp).type)} ` +
              `in '${fixmeAssertNotNull(ch.parent()).refToTextIgnoreErrors()}' for '${keyText}'`,
            // Hm, should we just use Meteor.bindEnvironment or fix the APIs to
            // pass the parameters explicitly?
            callback: () => component.withDataAccessContext(() =>
              ch.add_Client($$client.wrapCallback(standardServerCallback)))
          });
        } else {
          // We currently don't have a good API to add/delete multiple objects
          // in a single transaction, and using multiple transactions messes up
          // the undo history, so for now we don't support it at all.
        }
      }
      if (selectionInfo.addInfo) {
        selectionInfo.addInfo((ai) => {
          if (ai.addFamily == null)
            return;
          // Should never have both objectsCanAdd and addFamily
          assert(objectsCanAdd.length == 0);
          let fam = ai.addFamily;
          let ch = fam.parent();
          if (fam.values() != null && fam.isEditable()) {
            let desc = "New " + objectTypeName(fam.columnId);
            if (ch.columnId != rootColumnId) {
              desc += ` in '${ch.refToTextIgnoreErrors()}'`;
            }
            contextMenuItems.push({
              name: desc,
              callback: () => {
                let updateSelectionCB = component.wrapCallbackForAfterLayoutUpdate<OSValue>((error, newKey) => {
                  if (error != null) return;
                  newKey = newKey!;
                  // Select the new object.  In design view, on the scope axis, I
                  // think it looks better to select only out to the main spans,
                  // not the handle spans. ~ Matt 2018-03-04
                  let addAxis = ai.addAxis;
                  let newObjectId = cellIdChild(ch.cellId, newKey);
                  let newLayoutInfo = component.props.layoutInfo;
                  let split = newLayoutInfo.origLayoutTemplate.spec.splits[addAxis].get(ai.addOrigSplitId);
                  if (split == null || split.kind != TemplateSplitDefKind.VARIABLE) return;
                  // Hiding note: if either axis of this rectangle is hidden,
                  // then the collection should have been hidden, so an object
                  // couldn't have been added.
                  let sel = newLayoutInfo.dataLayoutSpec.calculateMergedRectIdForRectObject(
                    axisCons(addAxis,
                      mergedSpanOfSingleSpan(origToVisualSpanId(addAxis, split.child, newLayoutInfo)),
                      origToVisualMergedSpanId(otherAxis(addAxis), split.mergedScope, newLayoutInfo)),
                    newObjectId);
                  if (!newLayoutInfo.dataLayout.isMergedRectValid(sel)) return;
                  component.setSelection(sel);
                });
                component.withDataAccessContext(() =>
                  fam.new_Client($$client.wrapCallback(standardServerCallbackThen(updateSelectionCB))));
              }
            });
          }
        });
      }

      if (objectsCanDelete.length > 0) {
        if (objectsCanDelete.length == 1) {
          let [ch] = objectsCanDelete;
          contextMenuItems.push({
            name: `Delete ${objectTypeName(fixmeAssertNotNull(selectionInfo.objectsBinding.trp).type)} ` +
              `'${ch.refToTextIgnoreErrors()}'`,
            callback: () => component.withDataAccessContext(() =>
              ch.remove_Client($$client.wrapCallback(standardServerCallback)))
          });
        } else {
          // Not yet supported: see above.
        }
      }

      // Structure edits
      // TODO: Make a "design" section of the menu, etc.
      if (component.props.lockdown) {
        // No structure editing
      } else if ($$.layoutsAreHardcoded()) {
        contextMenuItems.push({
          name: "Structure editing disabled for hard-coded layout",
          disabled: () => true
        });
      } else {
        const visualMergedRect: NullableMergedSpanIds<_, TemplateS, VisualB> =
          // Cannot infer <typeof a> because of unsimplified indexed access type.
          perAxis((a) => deinstantiateMergedSpanId<typeof a>(selectionInfo.selection[a]));
        const origMergedRect: NullableMergedSpanIds<_, TemplateS, OrigB> =
          perAxis((a) => {
            let visualMergedSpan: null | MergedSpanId<TemplateS, VisualB, typeof a> = visualMergedRect[a];
            return visualMergedSpan == null ? null : visualToOrigMergedSpanId(a, visualMergedSpan, layoutInfo);
          });

        // Split-related commands
        //
        // To select an existing split to move or delete, you have to select the
        // parent span x the scope (main spans optionally plus handle spans).  I
        // considered implementing an outward search similar to the one for "add
        // object" but was unsure if it would be what users would expect and
        // thus did the easier thing for now. ~ Matt 2018-03-27
        forAxes((splitAxis) => {
          let scopeAxis = otherAxis(splitAxis);
          let origMergedSpan: null | MergedSpanId<TemplateS, OrigB, typeof splitAxis> = origMergedRect[splitAxis];
          let visualMergedScope: null | MergedSpanId<TemplateS, VisualB, typeof scopeAxis> =
            visualMergedRect[scopeAxis];
          if (origMergedSpan == null || visualMergedScope == null)
            return;
          const parentSpan = singleOfPerExtreme(origMergedSpan);
          const mergedScope = visualToOrigMergedSpanId(scopeAxis, visualMergedScope,
            layoutInfo, /*removeHandles*/ true);
          if (parentSpan == null || mergedScope == null)
            return;
          let coveringSplit = layoutInfo.origLayoutTemplate.archetype.splitCoveringMergedScope(
            splitAxis, parentSpan, mergedScope);
          if (coveringSplit == null) {
            // Add split
            //
            // XXX: Make it smarter and don't show this command if there's any conflicting split?
            //
            // Current design: "Add split" does not allow the selection to include handle spans.
            //
            // https://github.com/palantir/tslint/issues/3667
            // tslint:disable-next-line:strict-type-predicates
            if (origMergedRect[scopeAxis] == null)
              return;
            let container: RectangleId<_, TemplateS, OrigB> = axisCons(splitAxis, parentSpan,
              layoutInfo.origLayoutTemplate.archetype.containerOfMergedSpan(scopeAxis, mergedScope));
            if (getTemplateRectBindingIfGood(layoutInfo.origLayoutTemplate, container) == null)
              return;
            assert(objects.length == 1);
            contextMenuItems.push({
              name: "Add " + axisToCollectionDirection[splitAxis] + " collection",
              callback: () => attemptAddSplit(component, objects[0],
                {splitAxis, parentSpan, mergedScope, isVariable: true})
            });
            contextMenuItems.push({
              name: "Add nested " + uiTermForSplit(splitAxis, false),
              callback: () => attemptAddSplit(component, objects[0],
                {splitAxis, parentSpan, mergedScope, isVariable: false})
            });
          } else {
            if (!EJSON.equals(mergedScope, coveringSplit.def.mergedScope))
              return;
            // The area must be bound if there's an existing split.
            assert(objects.length == 1);
            let templateSplitId = coveringSplit.def.templateSplitId;
            let templateSplit = layoutInfo.origLayoutTemplate.templateSplitOfArchetypalSplit(splitAxis, coveringSplit);
            let what = (templateSplit.kind == TemplateSplitDefKind.VARIABLE)
              ? objectTypeName(templateSplit.nestedObjectType) + " collection"
              : uiTermForSplit(splitAxis, false);

            // Move
            contextMenuItems.push({
              name: "Move/resize " + what,
              callback: () => {
                // Canonicalize the selection to show while the user chooses the
                // move target: remove any handle spans, and may as well replace
                // a first-to-last-margin selection on the split axis with the
                // parent span.
                //
                // Hiding note: if either axis of this rectangle is hidden, then
                // the split should have been hidden and couldn't have been
                // moved.
                component.setSelection(layoutInfo.dataLayoutSpec.calculateMergedRectIdForRectObject(
                  axisCons(splitAxis,
                    mergedSpanOfSingleSpan(origToVisualSpanId(splitAxis, parentSpan, layoutInfo)),
                    perExtreme((e) => origToVisualSpanId(otherAxis(splitAxis), mergedScope[e], layoutInfo))),
                  objects[0]));
                component.setState({pendingMove: new PendingSplitMove(component.props.giWrapper,
                  splitAxis, templateSplitId, templateSplit.kind == TemplateSplitDefKind.VARIABLE)});
              }
            });

            // Demote (keyed or unkeyed object type)
            if (templateSplit.kind == TemplateSplitDefKind.VARIABLE) {
              contextMenuItems.push({
                name: "Reduce " + what + " to single cell",
                callback: () =>
                  getGenericIndexLayoutMethods<_>(component.props.giWrapper).layoutDemote.call<typeof splitAxis>(
                  $$.id, component.props.layoutName, {splitAxis, splitId: templateSplitId},
                  $$client.wrapCallback(standardServerCallback))
              });
            }

            // Delete
            contextMenuItems.push({
              name: "Delete " + what,
              callback: () =>
                getGenericIndexLayoutMethods<_>(component.props.giWrapper).layoutDeleteSplit.call<typeof splitAxis>(
                $$.id, component.props.layoutName, {splitAxis, splitId: templateSplitId},
                $$client.wrapCallback(standardServerCallback))
            });
          }
        });

        // Allocation-related commands
        let alloc = layoutInfo.dataLayout.coveringAllocationOfMergedRect(selectionInfo.selection);
        if (alloc != null) {
          const origAllocId = visualToOrigAllocationId(alloc.def.templateAllocationId, layoutInfo);
          if (origAllocId != null) {
            let templateAlloc = assertNotNull(layoutInfo.origLayoutTemplate.spec.allocations.get(origAllocId));
            let isKeyField = allocationIsKeyField(templateAlloc);
            let contentUsed = allocationContentIsUsed(templateAlloc);
            let container = layoutInfo.origLayoutTemplate.containerOfTemplateAlloc(templateAlloc);
            let nonLeaf = allocationIsNonLeaf(container, layoutInfo.origLayoutTemplate);
            if (contentUsed) {
              // Move cell
              //
              // There's not much point in moving a cell whose content is
              // unused.  This also blocks the edge case of moving cells in
              // unbound and extra-space areas, which I can't be bothered to
              // implement. ~ Matt 2018-03-18
              contextMenuItems.push({
                name: "Move cell / change merge area",
                callback: () => {
                  component.setState({pendingMove: new PendingAllocationMove(component.props.giWrapper, origAllocId)});
                }
              });

              // Promote.  Similar remarks re contentUsed.
              forAxes((a) => {
                let parentSpan = singleOfPerExtreme(templateAlloc.mergedSpans[a]);
                if (!isKeyField && parentSpan != null) {
                  contextMenuItems.push({
                    name: `Expand cell to ${axisToCollectionDirection[a]} collection`,
                    callback: () =>
                      getGenericIndexLayoutMethods<_>(component.props.giWrapper).layoutPromote.call<typeof a>(
                      $$.id, component.props.layoutName, {allocationId: origAllocId, axis: a},
                      $$client.wrapCallback(standardServerCallback))
                  });
                }
              });
            }
            // Purge/unmerge cell
            let purgeDesc =
              (contentUsed && nonLeaf) ? "Purge and unmerge cell" :
              contentUsed ? "Purge cell" :
              nonLeaf ? "Unmerge cell" :
              null;
            if (purgeDesc != null && !isKeyField) {
              contextMenuItems.push({
                name: purgeDesc,
                callback: () => getGenericIndexLayoutMethods<_>(component.props.giWrapper).layoutPurgeAllocation.call(
                  $$.id, component.props.layoutName, origAllocId,
                  $$client.wrapCallback(standardServerCallback))
              });
            }
          }
        } else if (axesEveryNotNull(origMergedRect)) {
          contextMenuItems.push({
            name: "Make merged cell",
            callback: () => getGenericIndexLayoutMethods<_>(component.props.giWrapper).layoutMakeAllocation.call(
              $$.id, component.props.layoutName, origMergedRect,
              $$client.wrapCallback(standardServerCallback))
          });
        }

        // Insert/delete spans in a containing fixed split
        forAxes((splitAxis) => {
          const origMergedSpan: null | MergedSpanId<TemplateS, OrigB, typeof splitAxis> = origMergedRect[splitAxis];
          if (origMergedSpan == null)
            return;
          const origSpan = singleOfPerExtreme(origMergedSpan);
          if (origSpan == null)
            return;
          let def = layoutInfo.origLayoutTemplate.parentSplitDefOfSpan(splitAxis, origSpan);
          if (def == null || def.kind != TemplateSplitDefKind.FIXED)
            return;
          // This is the best way I can think of to find the correct span object
          // given a first-to-last-margin selection.  We don't want to use the
          // cross object because the command can be run on an unbound cross.
          //
          // ~ Matt 2018-03-14
          let sb = layoutInfo.dataLayoutSpec.getSpanBinding(splitAxis,
            selectionInfo.selection[splitAxis][Extreme.FIRST]);
          for (let insertOnSide of extremes) {
            contextMenuItems.push({
              name: `Insert ${axisSpanNames[splitAxis]} ${sideNames[splitAxis][insertOnSide]}`,
              callback: () => {
                let updateSelectionCB =
                  component.wrapCallbackForAfterLayoutUpdate<TemplateSpanId<OrigB, typeof splitAxis>>(
                  (error, newSpan) => {
                  if (error != null) return;
                  newSpan = newSpan!;
                  let newLayoutInfo = component.props.layoutInfo;
                  // Hiding note: We just inserted this span, and it is shown by default.
                  let sel: MergedRectId<_, DataS, VisualB> = axisCons(splitAxis,
                    mergedSpanOfSingleSpan(
                      newLayoutInfo.dataLayoutSpec.calculateSpanId(splitAxis,
                        origToVisualSpanId(splitAxis, newSpan, newLayoutInfo), sb.rpm)),
                    newLayoutInfo.dataLayoutSpec.internMergedSpan(
                      selectionInfo.selection[otherAxis(splitAxis)]));
                  if (!newLayoutInfo.dataLayout.isMergedRectValid(sel)) return;
                  component.setSelection(sel);
                });
                getGenericIndexLayoutMethods<_>(component.props.giWrapper).layoutInsertSpan.call<typeof splitAxis>(
                  $$.id, component.props.layoutName,
                  {splitAxis, neighbor: origSpan, insertOnSide},
                  $$client.wrapCallback(standardServerCallbackThen(updateSelectionCB)));
              }
            });
          }

          // We can't delete the last child of a fixed split.
          if (def.children.length > 1) {
            contextMenuItems.push({
              name: "Delete " + axisSpanNames[splitAxis],
              callback: () => attemptDeleteSpan(component, {splitAxis, spanToDelete: origSpan})
            });
          }
        });

      }

    }

    if (contextMenuItems.length == 0) {
      return {items: {nothing: {
        name: "No actions available here",
        disabled: () => true
      }}};
    }
    let itemsObj: {[k: string]: ContextMenuCommand} = {};
    // We don't have meaningful keys.
    contextMenuItems.forEach((item, i) => itemsObj["item" + i] = item);
    return {items: itemsObj};
  }

  function attemptAddSplit<_, A extends Axis>(component: LayoutUIComponentWithSizedData<_>,
    object: CellIdWithSpares, params: AddSplitParams<A>) {
    let layoutTemplate = component.props.layoutInfo.origLayoutTemplate;
    let what = uiTermForSplit(params.splitAxis, params.isVariable);
    if (!requireNoSplits(layoutTemplate, params.splitAxis, params.parentSpan, params.mergedScope)) {
      alert(`There are existing row/column groups or collections blocking the new ${what}.  ` +
        "Please move or delete them first.");
      return;
    }
    if (prepareToClearArea(layoutTemplate, axisCons(
      params.splitAxis, mergedSpanOfSingleSpan(params.parentSpan), params.mergedScope))
      .usedAllocationIds.size > 0 &&
      !confirm(`Cells in use that overlap the selected area will overlap the new ${what}.  ` +
        "You can move or delete them later.  Proceed?")) {
      return;
    }
    let updateSelectionCB =
      component.wrapCallbackForAfterLayoutUpdate<AddSplitResponse<A>>((error, result) => {
      if (error != null) return;
      result = result!;
      // See remarks re concurrency in updateSelectionCB for "new span object".
      let newLayoutInfo = component.props.layoutInfo;
      let split = assertNotNull(newLayoutInfo.origLayoutTemplate.spec
        .splits[params.splitAxis].get(result.newSplitId));
      // Hiding note: In order for us to select the parent span and merged
      // scope, the parent span and both ends of the merged scope shouldn't have
      // been hidden.
      let splitAxisSel =
        (split.kind == TemplateSplitDefKind.FIXED) ?
          origToVisualSpanId(params.splitAxis, split.children[0], newLayoutInfo) :
        (newLayoutInfo.designViewInfo != null) ? nameVariableSplitBodySpan(split.child) :
        // Selection update is no-op in this case.
        origToVisualSpanId(params.splitAxis, split.parentSpan, newLayoutInfo);
      let sel = newLayoutInfo.dataLayoutSpec.calculateMergedRectIdForRectObject(
        axisCons(params.splitAxis,
          mergedSpanOfSingleSpan(splitAxisSel),
          perExtreme((e) => origToVisualSpanId(otherAxis(params.splitAxis), split.mergedScope[e], newLayoutInfo))),
        object);
      if (!newLayoutInfo.dataLayout.isMergedRectValid(sel)) return;
      component.setSelection(sel);
    });
    getGenericIndexLayoutMethods<_>(component.props.giWrapper).layoutAddSplit.call<A>(
      $$.id, component.props.layoutName, params,
      $$client.wrapCallback(standardServerCallbackThen(updateSelectionCB)));
  }

  // PendingMove is guaranteed not to need re-interning.
  export abstract class PendingMove<_> {
    abstract move(component: LayoutUIComponentWithSizedData<_>,
      origTarget: MergedRectId<_, TemplateS, OrigB>, callback: MeteorCallback<void>): void;
    abstract readonly what: string;
    readonly explanation: undefined | string;  // optional
    protected wrapCallback(component: LayoutUIComponentWithSizedData<_>, cb: MeteorCallback<void>):
      MeteorCallback<void> {
      // XXX Encapsulation, but I'm not defining another 2 methods just for this. ~ Matt 2018-08-11
      component.setState({savingMove: true});
      return $$client.wrapCallback((error, result) => {
        cb(error, result);
        component.setState({savingMove: false});
      });
    }
  }
  class PendingAllocationMove<_> extends PendingMove<_> {
    constructor(public giWrapper: GIWrapper<_>, public allocationId: TemplateAllocationId<OrigB>) {
      super();
    }
    readonly what = "cell";
    move(component: LayoutUIComponentWithSizedData<_>,
      origTarget: MergedRectId<_, TemplateS, OrigB>, callback: MeteorCallback<void>) {
      getGenericIndexLayoutMethods<_>(this.giWrapper).layoutMoveAllocation.call($$.id, component.props.layoutName, {
        allocationId: this.allocationId, newMergedSpans: origTarget
      }, this.wrapCallback(component, callback));
    }
  }
  class PendingSplitMove<_, A extends Axis> extends PendingMove<_> {
    readonly what: string;
    readonly explanation: string;
    constructor(public giWrapper: GIWrapper<_>, public splitAxis: A, public splitId: TemplateSplitId<OrigB, A>,
      public isVariable: boolean) {
      super();
      this.what = uiTermForSplit(splitAxis, isVariable);
      let scopeAxis = otherAxis(splitAxis);
      // This is admittedly complicated for users to deal with, but let's
      // implement something that works and then see where improvements fall on
      // the priority list.
      //
      // This text is inserted into HTML and the line breaks don't matter.
      this.explanation = `
(Selecting a different parent ${axisSpanNames[splitAxis]} will move the
${this.what} and its content to that ${axisSpanNames[splitAxis]}.  Selecting a
different ${axisSpanNames[scopeAxis]} range will resize the ${this.what}; all
existing descendant used cells and nested collections and row/column groups will
remain in the same ${axisSpanNames[scopeAxis]}s, so the new range must include
these ${axisSpanNames[scopeAxis]}s.  If you need to move descendant items to
different ${axisSpanNames[scopeAxis]}s, you'll need to do that as a separate
operation.  You may need to expand the ${this.what}, move descendant items, and
then shrink the ${this.what} to its final range.)
`;
    }
    move(component: LayoutUIComponentWithSizedData<_>,
      origTarget: MergedRectId<_, TemplateS, OrigB>, callback: MeteorCallback<void>) {
      let newParentSpan = singleOfPerExtreme(origTarget[this.splitAxis]);
      if (newParentSpan == null) {
        alert(`Please select a single destination ${axisSpanNames[this.splitAxis]}.`);
        return;
      }
      let newMergedScope = origTarget[otherAxis(this.splitAxis)];
      getGenericIndexLayoutMethods<_>(this.giWrapper).layoutMoveSplit.call<A>($$.id, component.props.layoutName, {
        splitAxis: this.splitAxis,
        splitId: this.splitId,
        newParentSpan,
        newMergedScope
      }, this.wrapCallback(component, callback));
    }
  }

  export function attemptMove<_>(component: LayoutUIComponentWithSizedData<_>) {
    assert(component.state.moveTargetInfo != null);
    let layoutInfo = component.props.layoutInfo;
    let target = assertNotNull(component.state.moveTargetInfo).selection;

    let visualTarget: NullableMergedSpanIds<_, TemplateS, VisualB> =
      perAxis((a) => deinstantiateMergedSpanId(target[a]));
    let origTarget: NullableMergedSpanIds<_, TemplateS, OrigB> = perAxis((a) => {
      let visualTargetSpan: null | MergedSpanId<TemplateS, VisualB, typeof a> = visualTarget[a];
      return visualTargetSpan == null ? null : visualToOrigMergedSpanId(a, visualTargetSpan, layoutInfo);
    });
    if (axesEveryNotNull(origTarget)) {
      let updateSelectionCB = component.wrapCallbackForAfterLayoutUpdate<void>((error) => {
        if (error != null) return;
        // Update the primary selection to the move target.  This might look a
        // bit silly if the move target was in a different object instance from
        // the source; blame the user.
        //
        // I don't think there's any guarantee about whether the layoutInfo
        // has been updated yet.  Reintern to be safe.
        let newLayoutInfo = component.props.layoutInfo;
        let sel = newLayoutInfo.dataLayoutSpec.internMergedRect(target);
        if (newLayoutInfo.dataLayout.isMergedRectValid(sel)) {
          component.setSelection(sel);
        }
        component.endMove();
      });
      assertNotNull(component.state.pendingMove).move(component, origTarget,
        standardServerCallbackThen(updateSelectionCB));
    } else {
      alert("Please select a rectangle of original (non-design-view) " +
        "rows and columns belonging to a single object.");
    }
  }

  function attemptDeleteSpan<_, A extends Axis>(component: LayoutUIComponentWithSizedData<_>,
    params: DeleteSpanParams<A>) {
    let layoutTemplate = component.props.layoutInfo.origLayoutTemplate;
    let prepareResult = prepareToDeleteSpan(layoutTemplate, params);
    if (prepareResult.fail) {
      alert(`There are row/column groups, collections, or key fields of ` +
        `computed collections in this ` +
        `${axisSpanNames[params.splitAxis]}.  Please move or delete them first.`);
      return;
    }
    if (prepareResult.deletingUsedAllocations &&
      !confirm(`Cells in use in this ${axisSpanNames[params.splitAxis]} ` +
        `will be deleted.  Proceed?`)) {
      return;
    }
    getGenericIndexLayoutMethods<_>(component.props.giWrapper).layoutDeleteSpan.call<A>(
      $$.id, component.props.layoutName, params,
      $$client.wrapCallback(standardServerCallback));
  }

}
