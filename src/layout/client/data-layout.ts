namespace Objsheets.Layout {

  // "Spare" object references: used to show a dummy object in an empty variable
  // split in design view.  Since we need to generate unique IDs for descendant
  // spans, this approach seems least likely to cause problems.
  export type SpareKey = {spare: true};
  export type OSValueWithSpares = OSValue | SpareKey | CellIdWithSpares;
  export interface CellIdWithSpares extends Array<OSValueWithSpares> {}
  export type QCellIdWithSpares = { columnId: ColumnId, cellId: CellIdWithSpares };

  export function cellIdChildWithSpares(cellId: CellIdWithSpares, value: OSValueWithSpares): CellIdWithSpares {
    return cellId.concat([value]);
  }
  export function cellIdParentWithSpares(cellId: CellIdWithSpares): CellIdWithSpares {
    return cellId.slice(0, -1);
  }
  export function cellIdIsReal(cellId: CellIdWithSpares): cellId is CellId {
    return cellId.every((value) =>
      (!(<Partial<SpareKey>>value).spare &&
        (!(value instanceof Array) || cellIdIsReal(value))));
  }
  export function qCellIdIsReal(qCellId: QCellIdWithSpares): qCellId is QCellId {
    return cellIdIsReal(qCellId.cellId);
  }

  // Data layout is only used with B = VisualB.

  // Intering the DataSpanIds in the DataLayoutSpec is the easiest way to make
  // them equal by identity.
  export type UninternedDataS = {templateSpan: TemplateS, objectId: CellIdWithSpares};
  export type DataS = UninternedDataS & Interned;
  export type UninternedDataSpanId<A extends AxisL> = SpanId<UninternedDataS, VisualB, A>;
  export type DataSpanId<A extends AxisL> = SpanId<DataS, VisualB, A>;
  export function templateSpanOfDataSpan<B, A extends AxisL>(dataSpan: DataSpanId<A>): TemplateSpanId<VisualB, A> {
    return visualBrander(axisBrander<typeof dataSpan.templateSpan, A>(dataSpan.templateSpan));
  }

  export class RefPatternMapper {
    // We can use Map for extra speed because TemplateSpanId compares by identity.
    matches = new Map<RefPatternPlaceholder, OSValueWithSpares>();
    load(pattern: RefPattern, ref: CellIdWithSpares) {
      pattern.forEach((patternElt, i) => {
        let refElt = ref[i];
        if (patternElt instanceof Array) {
          if (refElt instanceof Array) {
            this.load(patternElt, refElt);
          } else {
            throw new RuntimeError("Invalid reference loaded in RefPatternProcessor: not an array");
          }
        } else {
          let oldRefElt = this.matches.get(patternElt);
          if (oldRefElt == null) {
            this.matches.set(patternElt, refElt);
          } else {
            runtimeCheck(EJSON.equals(refElt, oldRefElt),
              () => "Invalid reference loaded in RefPatternProcessor: " +
              "the same placeholder corresponds to two different elements.");
          }
        }
      });
    }
    calculate(pattern: RefPattern): CellIdWithSpares {
      return pattern.map((patternElt) =>
        (patternElt instanceof Array)
          ? this.calculate(patternElt)
          : assertNotNull(this.matches.get(patternElt))
      );
    }
  }

  // Not named "DataRectangleId" because it isn't unique for unbound template
  // rectangles. :/
  export type DataRectangleBinding<_> = {
    templateRect: RectangleId<_, TemplateS, VisualB>,
    // Usable even for unbound template rectangles.
    rpm: RefPatternMapper,
    // Null if the template rectangle is unbound.  If non-null, the object still
    // may not exist if it is a cross object.
    qCellId: null | QCellIdWithSpares
  };
  export type DataSpanBinding<A extends Axis> = {
    templateSpan: TemplateSpanId<VisualB, A>,
    rpm: RefPatternMapper
  };
  export type DataSplitBinding<A extends Axis> = {
    templateSplit: TemplateSplitDef<VisualB, A>,
    qCellId: QCellIdWithSpares  // Cannot have a split in an unbound area.
  };
  export type DataAllocationBinding<_> = {
    templateAlloc: TemplateAllocationDef<_, VisualB>,
    qCellId: null | QCellIdWithSpares
  };

  const ERROR_COLOR = "#F00";
  const HYPERLINK_COLOR = "#00F";
  const NON_EDITABLE_COLOR = "#808080";
  const GHOST_COLOR = "#808080";

  enum AllocContentWidgetType {
    HYPERLINK = "_hyperlink",
    BUTTON = "_button"
  }
  type AllocContentType = OSType | AllocContentWidgetType;

  type AllocInfo = {
    isEditable: boolean;
    type: AllocContentType;
    content: AllocationContent;
    allocStatus: AllocStatus;
    specialTextColor: null | string;
  };

  export function buttonProcedureIsValid(container: ColumnId, proc: ProcedureDoc) {
    return EJSON.equals(proc.params, [{name: "this", type: container, singular: true}]);
  }

  export class DataLayoutSpec<_> implements ILayoutSpec<_, DataS, VisualB> {
    // If showEntireTemplate = true, ensure that all elements of the layout
    // template are visible by adding a dummy object to every empty variable
    // split and not allowing extra space spans to shrink to zero size. The
    // dummy objects in particular are important so we have a way to edit the
    // key formula of a computed collection even when the formula is currently
    // returning an empty set.
    constructor(public template: LayoutTemplate<_, VisualB>, public viewObjectId: null | string,
      public showEntireTemplate = false, public minimalStyling = false) {
      assert((viewObjectId != null) == (template.spec._id != rootColumnId));
    }

    internPool = new JSONableInternPool();
    internSpan<A extends AxisG<_>>(span: UninternedDataSpanId<A>): DataSpanId<A> {
      return this.internPool.intern(span);
    }
    internMergedSpan<A extends AxisG<_>>(mergedSpan: MergedSpanId<UninternedDataS, VisualB, A>):
      MergedSpanId<DataS, VisualB, A> {
      return perExtreme((e) => this.internSpan(mergedSpan[e]));
    }
    internMergedRect(mergedRect: MergedRectId<_, UninternedDataS, VisualB>): MergedRectId<_, DataS, VisualB> {
      return perAxis((a) => this.internMergedSpan(mergedRect[a]));
    }

    instantiateSpanId<A extends AxisG<_>>(
      templateSpanId: TemplateSpanId<VisualB, A>, spanObject: CellIdWithSpares): DataSpanId<A> {
      let d = {templateSpan: templateSpanId, objectId: spanObject};
      return this.internSpan(visualBrander(axisBrander<typeof d, A>(d)));
    }
    calculateSpanId<A extends Axis>(a: A,
      templateSpanId: TemplateSpanId<VisualB, A>, rpm: RefPatternMapper): DataSpanId<A> {
      return this.instantiateSpanId(templateSpanId,
        rpm.calculate(this.template.getSpanBinding(a, templateSpanId).refPattern));
    }
    calculateMergedSpanId<A extends Axis>(a: A,
      templateMergedSpanId: MergedSpanId<TemplateS, VisualB, A>, rpm: RefPatternMapper):
      MergedSpanId<DataS, VisualB, A> {
      let obj = rpm.calculate(this.template.getSpanBinding(a, templateMergedSpanId[Extreme.FIRST]).refPattern);
      return perExtreme((e) => this.instantiateSpanId(templateMergedSpanId[e], obj));
    }
    calculateMergedRectId(
      templateMergedRectId: MergedRectId<_, TemplateS, VisualB>, rpm: RefPatternMapper):
      MergedRectId<_, DataS, VisualB> {
      return perAxis((a) => this.calculateMergedSpanId(a, templateMergedRectId[a], rpm));
    }
    calculateMergedRectIdForRectObject(
      templateMergedRectId: MergedRectId<_, TemplateS, VisualB>, rectObject: CellIdWithSpares):
      MergedRectId<_, DataS, VisualB> {
      let representativeTemplateRect: RectangleId<_, TemplateS, VisualB> =
        perAxis((a) => templateMergedRectId[a][Extreme.FIRST]);
      let rpm = new RefPatternMapper();
      rpm.load(fixmeAssertNotNull(this.template.getExistingRectBinding(representativeTemplateRect).trp).refPattern,
        rectObject);
      return this.calculateMergedRectId(templateMergedRectId, rpm);
    }

    cross(templateRect: RectangleId<_, TemplateS, VisualB>, ids: IndependentPerAxis<CellIdWithSpares>) {
      let rpm = new RefPatternMapper();
      forAxes((a) => {
        rpm.load(this.template.getSpanBinding(a, templateRect[a]).refPattern, ids[a]);
      });
      return rpm.calculate(fixmeAssertNotNull(this.template.getExistingRectBinding(templateRect).trp).refPattern);
    }
    uncross(templateRect: RectangleId<_, TemplateS, VisualB>, id: CellIdWithSpares) {
      let rpm = new RefPatternMapper();
      rpm.load(fixmeAssertNotNull(this.template.getExistingRectBinding(templateRect).trp).refPattern, id);
      return independentPerAxis((a) =>
        rpm.calculate(this.template.getSpanBinding(a, templateRect[a]).refPattern));
    }

    // Moderately annoying: RectangleId will just come back as a pair of SpanIds
    // and we have to reconstruct the object reference of the right type.
    getRectBinding(rect: RectangleId<_, DataS, VisualB>): DataRectangleBinding<_> {
      let templateRect: RectangleId<_, TemplateS, VisualB> = perAxis((a) => templateSpanOfDataSpan(rect[a]));
      let tb = this.template.getExistingRectBinding(templateRect);
      let rpm = new RefPatternMapper();
      forAxes((a) => {
        rpm.load(this.template.getSpanBinding(a, templateRect[a]).refPattern, rect[a].objectId);
      });
      let qCellId = (tb.trp == null) ? null :
        {columnId: tb.trp.type, cellId: rpm.calculate(tb.trp.refPattern)};
      return {templateRect, rpm, qCellId};
    }
    getSpanBinding<A extends Axis>(a: A, span: DataSpanId<A>): DataSpanBinding<A> {
      let templateSpan = templateSpanOfDataSpan(span);
      let tb = this.template.getSpanBinding(a, templateSpan);
      let rpm = new RefPatternMapper();
      rpm.load(tb.refPattern, span.objectId);
      return {templateSpan, rpm};
    }
    getSplitBinding<A extends Axis>(a: A, split: SplitInfo<_, DataS, VisualB, A>): DataSplitBinding<A> {
      let {qCellId} = this.getRectBinding(split.container);
      // Some duplication with LayoutTemplate.templateSplitOfArchetypalSplit.
      let templateSplit = assertNotNull(this.template.spec.splits[a].get(split.def.templateSplitId));
      return {templateSplit, qCellId: fixmeAssertNotNull(qCellId)};
    }
    getAllocBinding(alloc: AllocationInfo<_, DataS, VisualB>): DataAllocationBinding<_> {
      let {qCellId} = this.getRectBinding(alloc.container);
      // Some duplication with LayoutTemplate.templateAllocationOfArchetypalAllocation.
      let templateAlloc = assertNotNull(this.template.spec.allocations.get(alloc.def.templateAllocationId));
      return {templateAlloc, qCellId};
    }

    private spanStatus: {[A in AxisG<_>]: Map<DataSpanId<A>, ObjectStatus>} =
      perAxis((a) => new Map<DataSpanId<typeof a>, ObjectStatus>());

    private objectStatusCache = new JSONKeyedMap<QCellId, ObjectStatus>();

    private registerSpanId<A extends AxisG<_>>(a: A,
      templateSpanId: TemplateSpanId<VisualB, A>, spanObject: CellIdWithSpares, status: ObjectStatus): DataSpanId<A> {
      let dataSpan = this.instantiateSpanId(templateSpanId, spanObject);
      this.spanStatus[a].set(dataSpan, status);  // should be idempotent
      return dataSpan;
    }

    getRoot<A extends AxisG<_>>(a: A): DataSpanId<A> {
      return this.registerSpanId(a, rootTemplateSpanId(DUMMY),
        this.template.spec._id != rootColumnId ? [{viewObjectRoot: true}] : rootCellId,
        ObjectStatus.NORMAL);
    }
    getSpanStyle<A extends Axis>(a: A, spanId: DataSpanId<A>): SpanStyle {
      let origStyle = this.template.archetype.getSpanStyle(a, templateSpanOfDataSpan(spanId));
      // When showEntireTemplate = true, force all extra space spans to be visible.
      if (this.showEntireTemplate && origStyle.isExtraSpace) {
        return {...origStyle, minSize: 12};
      } else {
        return origStyle;
      }
    }

    private getObjectStatus(ch: CellHandle): ObjectStatus {
      let qCellId = ch.id();
      let status = this.objectStatusCache.get(qCellId);
      if (status != null) {
        return status;
      }
      if (ch.columnId == rootColumnId) {
        status = ObjectStatus.NORMAL;
      } else {
        let parentStatus = this.getObjectStatus(assertNotNull(ch.parent()));
        if (parentStatus != ObjectStatus.NORMAL) {
          status = parentStatus;
        } else {
          try {
            status = ch.existsPropagateErrors() ? ObjectStatus.NORMAL : ObjectStatus.NONEXISTENT;
          } catch (e) {
            if (!(e instanceof RuntimeError)) throw e;
            status = ObjectStatus.ERROR;
          }
        }
      }
      this.objectStatusCache.set(qCellId, status);
      return status;
    }

    private getSplitChildren<A extends Axis>(a: A, rect: RectangleId<_, DataS, VisualB>,
      split: TemplateSplitDef<VisualB, A>, qCellId: QCellIdWithSpares):
      DataSpanId<A>[] {
      let parentStatus = assertNotNull(this.spanStatus[a].get(rect[a]));
      if (split.kind == TemplateSplitDefKind.FIXED) {
        return split.children.map((x) => this.registerSpanId(a, x, qCellId.cellId, parentStatus));
      } else {
        let keys: OSValueWithSpares[];
        let newStatus: null | ObjectStatus;
        if (qCellIdIsReal(qCellId)) {
          // Since the parent of a split cannot be an extra space span, if
          // qCellId is real, then the status so far must be normal on both
          // axes.
          assert(parentStatus == ObjectStatus.NORMAL);
          let ch = new CellHandle(qCellId);
          let crossStatus = this.getObjectStatus(ch);
          if (crossStatus != ObjectStatus.NORMAL) {
            keys = [];
            newStatus = crossStatus;
          } else {
            let fam = ch.childFamily(split.nestedObjectType).read();
            if (fam.values != null) {
              keys = fam.values.slice();
              newStatus = keys.length == 0 ? ObjectStatus.NONEXISTENT : ObjectStatus.NORMAL;  // for any spare
            } else {
              keys = [];
              newStatus = ObjectStatus.ERROR;
            }
          }
        } else {
          // There is a special status already in effect on at least one axis.
          // We shouldn't set another one here.
          keys = [];
          newStatus = null;
        }
        if (this.showEntireTemplate && keys.length == 0) {
          keys.push({spare: true});
        }
        let children = keys.map((v) => this.registerSpanId(a,
          split.child, cellIdChildWithSpares(qCellId.cellId, v), fallback(newStatus, parentStatus)));
        let extraSpaceSpan = this.registerSpanId(a, split.extraSpaceChild, qCellId.cellId,
          newStatus == null ? parentStatus :
          newStatus == ObjectStatus.NORMAL ? ObjectStatus.NONEXISTENT :
          newStatus);
        children.push(extraSpaceSpan);
        return children;
      }
    }

    // Precondition: object exists.
    // Returns the FamilyHandle if editable in the UI, null if not.
    editableFamilyForField(object: QCellId, field: NonRootColumnId) {
      if (field == object.columnId) {
        // Key field: not editable
        return null;
      }
      let fam = new FamilyHandle({columnId: field, parentCellId: object.cellId});
      // Logic ripped off of src/client/sheet.ts.
      //
      // Disallow "editing" the key of a keyed object because that will delete
      // the object and insert a new one, losing all descendant data, which
      // probably isn't what the user expects.
      if (!getExistingColumn(field).isObject && StateEdit.typeIsEditable(field) && fam.isEditable()) {
        return fam;
      } else {
        return null;
      }
    }

    private getAllocInfo(rect: RectangleId<_, DataS, VisualB>,
      templateAlloc: TemplateAllocationDef<_, VisualB>, qCellId: null | QCellIdWithSpares): AllocInfo {
      // Initialize to defaults.
      let info: AllocInfo = {
        isEditable: false,
        type: SpecialType.TEXT,
        content: "",
        allocStatus: {
          spanObjectStatus: independentPerAxis((a) => assertNotNull(this.spanStatus[a].get(rect[a]))),
          areaIsUnbound: false,
          crossObjectStatus: ObjectStatus.NORMAL
        },
        specialTextColor: null
      };
      // Set this first: design view wants to show labels on spare subcollections.
      if (templateAlloc.kind == TemplateAllocationDefKind.LABEL) {
        info.isEditable = true;  // Not really but don't gray the text.
        info.content = templateAlloc.text;
      }
      if (axesSome((a) => info.allocStatus.spanObjectStatus[a] != ObjectStatus.NORMAL)) {
        return info;
      }
      if (qCellId == null) {
        info.allocStatus.areaIsUnbound = true;
        return info;
      }
      if (!qCellIdIsReal(qCellId)) {
        throw new Error("qCellId is spare but we didn't get a status on either axis?  This shouldn't happen.");
      }
      info.allocStatus.crossObjectStatus = this.getObjectStatus(new CellHandle(qCellId));
      if (info.allocStatus.crossObjectStatus != ObjectStatus.NORMAL) {
        return info;
      }
      if (templateAlloc.kind == TemplateAllocationDefKind.LABEL) {
        return info;
      }
      let model = getReadableModel();
      let objectCol = getExistingColumn(qCellId.columnId);
      let readFieldOfThisObject = (fieldColumnId: ColumnId) =>
        readField(model, objectCol, qCellId.cellId, fieldColumnId);
      switch (templateAlloc.kind) {
        case TemplateAllocationDefKind.FIELD:
          info.isEditable = this.editableFamilyForField(qCellId, templateAlloc.field) != null;
          let tset;
          try {
            // We should not get a StaticError for invalid field binding because
            // LayoutTemplate has already checked for that.
            tset = readFieldOfThisObject(templateAlloc.field);
          } catch (e) {
            if (!(e instanceof RuntimeError)) throw e;
            info.content = "error";
            info.specialTextColor = ERROR_COLOR;
            return info;
          }
          info.type = tset.type;
          if (tset.type == SpecialType.IMAGE) {
            let dataurl = arrayGet(tset.set.elements(), 0);
            if (typeof dataurl === "string" || dataurl === undefined) {
              info.content = {type: "image", dataurl};
            } else {
              throw new Error("Rendering: retrieved dataurl is not string or undefined");
            }
            return info;
          }
          if (tset.set.size() == 0)
            return info;
          try {
            let text = tsetToText(model, tset);
            if (text == "") {
              info.specialTextColor = GHOST_COLOR;
              info.content = "“”";
            } else {
              info.content = text;
            }
            return info;
          } catch (e) {
            if (!(e instanceof RuntimeError)) throw e;
            info.content = "<?>";
            info.specialTextColor = ERROR_COLOR;
            return info;
          }
        case TemplateAllocationDefKind.HYPERLINK:
          try {
            let text = /*validated*/ <string>singleElement(readFieldOfThisObject(templateAlloc.textField));
            let url = /*validated*/ <string>singleElement(readFieldOfThisObject(templateAlloc.urlField));
            if (text == "") {
              text = "“”";
              info.specialTextColor = GHOST_COLOR;
            }
            info.type = AllocContentWidgetType.HYPERLINK;
            info.content = {type: "hyperlink", text, url};
          } catch (e) {
            if (!(e instanceof RuntimeError)) throw e;
            info.content = "error";
            info.specialTextColor = ERROR_COLOR;
          }
          return info;
        case TemplateAllocationDefKind.BUTTON:
          try {
            let text = /*validated*/ <string>singleElement(readFieldOfThisObject(templateAlloc.textField));
            let procedureName = runtimeCheckNotNull(templateAlloc.procedureName,
              () => "Procedure name not specified");
            let proc = runtimeCheckNotNull(Procedures.findOne({name: procedureName}),
              () => "Procedure does not exist");
            // XXX: Maybe we should revalidate this on the server at call time.
            runtimeCheck(buttonProcedureIsValid(qCellId.columnId, proc),
              () => "Procedure does not take the correct parameters");
            info.type = AllocContentWidgetType.BUTTON;
            info.content = {type: "button", procedureName, thisObj: qCellId, text};
          } catch (e) {
            if (!(e instanceof RuntimeError)) throw e;
            info.content = "error";
            info.specialTextColor = ERROR_COLOR;
          }
          return info;
        default:
          return assertUnreachable(templateAlloc, "Invalid template allocation type");
      }
    }

    getRectContentSpec(rect: RectangleId<_, DataS, VisualB>): RectContentSpec<_, DataS, VisualB> {
      let {templateRect, rpm, qCellId} = this.getRectBinding(rect);
      let templateContent = this.template.getRectContentSpec(templateRect);

      // TODO: Support an alternate sub-template for nonexistent cross objects?
      // For now, we use the regular template but everything will be empty.

      let viewObjectContext: null | ViewObjectContext =
        this.template.spec._id != rootColumnId
        ? {type: this.template.spec._id, id: assertNotNull(this.viewObjectId),
            queryParams: {} /* not used here */}
        : null;

      return currentViewObject.withValue(viewObjectContext, (): RectContentSpec<_, DataS, VisualB> => ({
        splitDefs: perAxis((a) => templateContent.splitDefs[a].entries()
          .map<SplitDef<DataS, VisualB, typeof a>>(([id, split]) => ({
          parentSpan: rect[a],
          mergedScope: this.calculateMergedSpanId(otherAxis(a), split.mergedScope, rpm),
          children: this.getSplitChildren(a, rect, split, fixmeAssertNotNull(qCellId)),
          templateSplitId: id
        }))),
        allocationDefs: templateContent.allocationDefs.entries().map(([id, alloc]) => {
          let info = this.getAllocInfo(rect, alloc, qCellId);
          let defaultStyle: AllocationStyle = {};
          // Set default text alignment
          if (info.type == SpecialType.NUMBER || info.type == SpecialType.DATE || info.type == SpecialType.BOOL) {
            defaultStyle.textAlignment = TextAlignment.Right;
          } else {
            defaultStyle.textAlignment = TextAlignment.Left;
          }
          // Set image editability
          if (info.type == SpecialType.IMAGE) {
            defaultStyle.isImageEditable = info.isEditable;
          }
          // Set default text underline
          if (info.type == AllocContentWidgetType.HYPERLINK ||
            (!this.minimalStyling && info.type != AllocContentWidgetType.BUTTON && typeIsReference(info.type))) {
            defaultStyle.isTextUnderlined = true;
          }
          // Set default text color
          defaultStyle.textColor =
            info.type == AllocContentWidgetType.HYPERLINK ? HYPERLINK_COLOR :
            (!this.minimalStyling && !info.isEditable && info.type != AllocContentWidgetType.BUTTON)
              ? NON_EDITABLE_COLOR
              : undefined;

          let style: AllocationStyle = {...defaultStyle, ...alloc.style,
            allocStatus: info.allocStatus, specialTextColor: info.specialTextColor};

          return {
            mergedSpans: this.calculateMergedRectId(alloc.mergedSpans, rpm),
            backgroundAllocationInfo: alloc.backgroundAllocationInfo,
            style,
            templateAllocationId: id,
            content: info.content
          };
        })
      }));
    }

  }

}
