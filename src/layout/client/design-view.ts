namespace Objsheets.Layout {

  const VISUAL_BRAND = Symbol();
  export type VisualB = {[VISUAL_BRAND]: undefined};
  export const origBrander = makeBrander<OrigB>();
  export const visualBrander = makeBrander<VisualB>();

  function computeHandleHeights<_, A extends Axis>(origLayout: LayoutTemplate<_, OrigB>, a: A, e: Extreme) {
    let archetype = origLayout.archetype;
    let oa = otherAxis(a);
    // We're adding a-spans for handles for scope e-boundaries of
    // oa-splits.
    let handleHeights = new JSONKeyedMap<RectangleId<_, TemplateS, OrigB>, number>();
    archetype.traverseAllWithMajorAxis({
      postRect: (rect) => {
        // Partial rectangles?
        // - If rect is a-partial, then none of its oa-descendants can have
        //   handleHere because the split would conflict with the split that
        //   caused the partiality, so this code will set all the handle heights
        //   to 0; we don't have to do anything special.
        // - If rect is oa-partial, then it won't have handleHere and it just
        //   aggregates the heights of its children; we don't have to do
        //   anything special.
        let rectInfo = archetype.getRectInfo(rect);
        let split: null | SplitInfo<_, TemplateS, OrigB, OtherAxis<A>> = rectInfo.coveringSplits[oa];
        let height;
        if (split == null) {
          height = 0;
        } else {
          let handleHere = EJSON.equals(
            extremeOfMergedSpan(split.def.mergedScope, e), rect[a]);
          let childRects = archetype.getRectChildrenBySplit(rect, split, oa);
          height = (handleHere ? 1 : 0) +
            // Defend against splits with zero children, even though they should
            // be disallowed now.
            Math.max(0, ...childRects.map((childRect) => assertNotNull(handleHeights.get(childRect))));
        }
        assert(!isNaN(height));
        handleHeights.set(rect, height);
      }
    }, a);
    return handleHeights;
  }

  function nameMainSpan<A extends Axis>(orig: TemplateSpanId<OrigB, A>) {
    return templateSpanIdBrander<VisualB, A>(orig + ".m");
  }
  function unwrapMainSpan<A extends Axis>(visual: TemplateSpanId<VisualB, A>) {
    let match = /^(.*)\.m$/.exec(visual);
    return match ? templateSpanIdBrander<OrigB, A>(match[1]) : null;
  }
  function nameHandleSpan<A extends Axis>(orig: TemplateSpanId<OrigB, A>, extreme: Extreme, height: number) {
    return templateSpanIdBrander<VisualB, A>(orig + "." + extreme.charAt(0) + height);
  }
  function unwrapHandleSpan<A extends Axis>(visual: TemplateSpanId<VisualB, A>, extreme: Extreme) {
    let match = /^(.*)\.([fl])[0-9]+$/.exec(visual);
    return (match && match[2] == extreme.charAt(0)) ? templateSpanIdBrander<OrigB, A>(match[1]) : null;
  }
  function nameVariableItemWrapperSpan<A extends Axis>(orig: TemplateSpanId<OrigB, A>) {
    return templateSpanIdBrander<VisualB, A>(orig + ".v");
  }
  export function nameVariableSplitBodySpan<A extends Axis>(orig: TemplateSpanId<OrigB, A>) {
    return templateSpanIdBrander<VisualB, A>(orig + ".vb");
  }
  function nameMarginSpan<A extends Axis>(origSplitId: TemplateSplitId<OrigB, A>, extreme: Extreme) {
    return templateSpanIdBrander<VisualB, A>(origSplitId + ".s" + extreme.charAt(0));
  }
  function unwrapMarginSpan<A extends Axis>(visual: TemplateSpanId<VisualB, A>, extreme: Extreme) {
    let match = /^(.*)\.s([fl])$/.exec(visual);
    return (match && match[2] == extreme.charAt(0)) ? templateSplitIdBrander<OrigB, A>(match[1]) : null;
  }

  function nameUserSplit<A extends Axis>(orig: TemplateSplitId<OrigB, A>) {
    return templateSplitIdBrander<VisualB, A>("u." + orig);
  }
  function nameUserAlloc(orig: TemplateAllocationId<OrigB>) {
    return templateAllocationIdBrander<VisualB>("u." + orig);
  }
  function unwrapUserSplit<A extends Axis>(visual: TemplateSplitId<VisualB, A>) {
    let match = /^u\.(.*)$/.exec(visual);
    return match ? templateSplitIdBrander<OrigB, A>(match[1]) : null;
  }
  function unwrapUserAlloc(visual: TemplateAllocationId<VisualB>) {
    let match = /^u\.(.*)$/.exec(visual);
    return match ? templateAllocationIdBrander<OrigB>(match[1]) : null;
  }

  export function origToVisualSpanId<_, A extends Axis>(
    a: A, span: TemplateSpanId<OrigB, A>, layoutInfo: DataLayoutInfo<_>): TemplateSpanId<VisualB, A> {
    if (layoutInfo.designViewInfo != null)
      return nameMainSpan(span);
    let visualSpan = visualBrander(span);
    assert(layoutInfo.visualLayoutTemplate.archetype.hasSpan(a, visualSpan),
      () => "Tried to call origToVisualSpanId on a hidden span.  This should not happen.");
    return visualSpan;
  }
  export function origToVisualMergedSpanId<_, A extends Axis>(a: A, mergedSpan: MergedSpanId<TemplateS, OrigB, A>,
    layoutInfo: DataLayoutInfo<_>): MergedSpanId<TemplateS, VisualB, A> {
    if (layoutInfo.designViewInfo != null)
      return perExtreme((e) => nameMainSpan(mergedSpan[e]));
    // XXX Code duplication vs. hiding.ts
    let shown = layoutInfo.origLayoutTemplate.archetype.getSpansInMergedSpan(a, mergedSpan)
      .map(visualBrander).filter((s) => layoutInfo.visualLayoutTemplate.archetype.hasSpan(a, s));
    assert(shown.length > 0,
      () => "Tried to call origToVisualSpanId on a completely hidden merged span.  This should not happen.");
    return {[Extreme.FIRST]: shown[0], [Extreme.LAST]: shown[shown.length - 1]};
  }
  export function visualToOrigMergedSpanId<_, A extends Axis>(a: A,
    mergedSpan: MergedSpanId<TemplateS, VisualB, A>,
    layoutInfo: DataLayoutInfo<_>, removeHandles = false): null | MergedSpanId<TemplateS, OrigB, A> {
    const designViewInfo = layoutInfo.designViewInfo;
    if (designViewInfo == null) {
      return perExtreme((e) => origBrander(mergedSpan[e]));
    }
    let origSpans = perExtreme((e) => {
      let visualSpan = mergedSpan[e];
      // Case 1: main span
      let origSpan = unwrapMainSpan(visualSpan);
      if (origSpan != null) return origSpan;
      if (removeHandles) {
        // Case 2: handle span on the proper extreme
        let maybeHandleSpan = (e == Extreme.FIRST)
          ? fallback(designViewInfo.spanToAliasMap[a].get(visualSpan), visualSpan)
          : visualSpan;
        origSpan = unwrapHandleSpan(maybeHandleSpan, e);
        if (origSpan != null) return origSpan;
      }
      // Give up.
      return null;
    });
    if (extremesEveryNotNull(origSpans)) {
      try {
        layoutInfo.origLayoutTemplate.archetype.getSpansInMergedSpan(a, origSpans);
        return origSpans;
      } catch (e) {
        if (!(e instanceof StaticError)) throw e;
        // When removeHandles = true and the visual merged span consists of
        // handles between two original spans A and B, origSpans will be [B,A]
        // and we'll get "Invalid MergedSpanId".  In this case, there's no
        // original merged span.  There doesn't seem to be a cleaner way to
        // detect this case than just validating origSpans.  XXX: This code
        // might hide bugs that lead to invalid origSpans in other cases.
        return null;
      }
    }
    // Special case: first through last margin span of a split, which is
    // visually identical to the parent span.  Return the parent span.
    let splitId = singleOfPerExtreme(perExtreme((e) => unwrapMarginSpan(mergedSpan[e], e)));
    if (splitId != null) {
      let parentSpan = assertNotNull(layoutInfo.origLayoutTemplate.spec.splits[a].get(splitId)).parentSpan;
      return mergedSpanOfSingleSpan(parentSpan);
    }
    return null;
  }
  export function origToVisualSplitId<_, A extends Axis>(
    splitId: TemplateSplitId<OrigB, A>, layoutInfo: DataLayoutInfo<_>): TemplateSplitId<VisualB, A> {
    return (layoutInfo.designViewInfo != null) ? nameUserSplit(splitId) : visualBrander(splitId);
  }
  export function visualToOrigSplitId<_, A extends Axis>(
    splitId: TemplateSplitId<VisualB, A>, layoutInfo: DataLayoutInfo<_>): null | TemplateSplitId<OrigB, A> {
    return (layoutInfo.designViewInfo != null) ? unwrapUserSplit(splitId) : origBrander(splitId);
  }
  export function visualToOrigAllocationId<_>(
    allocationId: TemplateAllocationId<VisualB>, layoutInfo: DataLayoutInfo<_>): null | TemplateAllocationId<OrigB> {
    return (layoutInfo.designViewInfo != null) ? unwrapUserAlloc(allocationId) : origBrander(allocationId);
  }

  export type DesignViewSelectableSplitInfo<A extends AxisL> = {
    origId: TemplateSplitId<OrigB, A>;
    designViewSplit: TemplateSplitDef<VisualB, A>;
  };

  export type DesignViewInfo<_> = {
    // We may as well return a LayoutTemplate rather than a LayoutTemplateSpec
    // as long as we are creating one anyway.  Subject to change.
    newLayoutTemplate: LayoutTemplate<_, VisualB>;

    // The UI wants to be able to select a variable split via its design view
    // margins, which are not part of the variable split in the transformed
    // layout template.  This map takes the place of RectInfo.coveringSplits to
    // determine what variable split should be selected for a given selection.
    variableSplitSelectionMap: {[A in AxisG<_>]:
      JSONKeyedMap<RectangleId<_, TemplateS, VisualB>, DesignViewSelectableSplitInfo<A>>}

    // Takes an interior handle span (by its canonical .l* name) and returns its
    // alias .f* name.
    spanToAliasMap: {[A in AxisG<_>]: Map<TemplateSpanId<VisualB, A>, TemplateSpanId<VisualB, A>>};
  };

  const BASE_Z_INDEX = -100;
  const designViewLightGray = "#E8E8E8";
  //Color: retro
  // I added a third background color.  It could probably use adjustment.
  // ~ Matt 2018-03-06
  const backgroundColors = ["#F9FAF0", "#F0F4DB", "#E8F0C8"];
  const originalSpanHandleColor = "#F96161";
  const designViewLabelsFont = "Roboto";

  export function generateDesignView<_>(origLayout: LayoutTemplate<_, OrigB>): DesignViewInfo<_> {
    let archetype = origLayout.archetype;
    let handleHeights: {[A in AxisG<_>]: PerExtreme<JSONKeyedMap<RectangleId<_, TemplateS, OrigB>, number>>} =
      perAxis((a) => perExtreme((e) => computeHandleHeights(origLayout, a, e)));
    // Design view transformation currently is losing SpanStyles from the original layout template.
    // The only property currently supported on user spans, `hide`, is handled specially in design view.
    let newSpec = emptyLayoutTemplateSpec<_, VisualB>(origLayout.spec._id);
    enum BoxStyle {
      DesignViewSpan = "designViewSpan",
      OriginalSpan = "originalSpan",
      RootSpan = "rootSpan",
    }
    // Specify border styles for handleBox(contains two brackets on both end) allocations
    let variableSplitHandleStyle = {color: originalSpanHandleColor, visibility: true, isDouble: true};
    let objectSideBorderStyle = {color: originalSpanHandleColor, visibility: true};
    let grayBorderStyle = {visibility: true};
    let fixedSplitHandleStyle = {visibility: true, isDouble: true};
    let invisibleBorderStyle = {visibility: false};

    // Generation of arbitrary IDs for splits and allocations added by design
    // view, not corresponding to one of the user's original splits or
    // allocations.
    let nextSyntheticIdNumber = 0;
    let generateSyntheticId = () => "dv." + nextSyntheticIdNumber++;
    let addSyntheticSplit = <A extends Axis>(a: A, split: TemplateSplitDef<VisualB, A>) =>
      newSpec.splits[a].set(templateSplitIdBrander(generateSyntheticId()), split);
    let addSyntheticAllocation = (alloc: TemplateAllocationDef<_, VisualB>) =>
      newSpec.allocations.set(templateAllocationIdBrander(generateSyntheticId()), alloc);

    let setupHandleSpan = <A extends Axis>(a: A, span: TemplateSpanId<VisualB, A>) => {
      newSpec.spanStyles[a].set(span, {minSize: 16});
    };

    let spanAliasMap: {[A in AxisG<_>]: Map<TemplateSpanId<VisualB, A>, TemplateSpanId<VisualB, A>>} =
      perAxis((a) => new Map<TemplateSpanId<VisualB, typeof a>, TemplateSpanId<VisualB, typeof a>>());
    let spanToAliasMap: {[A in AxisG<_>]: Map<TemplateSpanId<VisualB, A>, TemplateSpanId<VisualB, A>>} =
      perAxis((a) => new Map<TemplateSpanId<VisualB, typeof a>, TemplateSpanId<VisualB, typeof a>>());
    let resolveSpanAlias = <A extends Axis>(a: A, span: TemplateSpanId<VisualB, A>) =>
      fallback(spanAliasMap[a].get(span), span);
    let translatedFixedSplitChildren:
      {[A in AxisG<_>]: Map<TemplateSplitId<OrigB, A>, TemplateSpanId<VisualB, A>[]>} =
      perAxis((a) => new Map<TemplateSplitId<OrigB, typeof a>, TemplateSpanId<VisualB, typeof a>[]>());

    // Given a span list representing the children of an a-split (or in a special
    // case, just the root span), add enough handle spans to fit all the handles
    // appearing on the a-sides of the given spans anywhere in the given oa-scope.
    let addHandleSpansToSpanList = <A extends Axis>(
      scope: MergedSpanId<TemplateS, OrigB, OtherAxis<A>>, spanList: TemplateSpanId<OrigB, A>[], a: A) => {
      let newChildren: TemplateSpanId<VisualB, A>[] = [];
      let scopeAxis = otherAxis(a);
      let scopeParts = archetype.getSpansInMergedSpan(scopeAxis, scope);
      {
        // Handles before the first span.
        let firstSpan = spanList[0];
        // handleHeights already tells us the greatest height of handles
        // appearing anywhere in a single oa-span in the scope, but if the scope is
        // a merged scope, we have to take the max over the spans in it.
        let hh = Math.max(...scopeParts.map((scopePart) =>
          assertNotNull(handleHeights[a][Extreme.FIRST].get(axisCons(a, firstSpan, scopePart)))));
        if (hh > 0) {
          // Add one more span than the handleHeight because each handle needs
          // an inner span and an outer span (the outer span of one handle is
          // the inner span of the next).
          for (let h = hh; h >= 0; h--) {
            let handleSpan = nameHandleSpan(firstSpan, Extreme.FIRST, h);
            setupHandleSpan(a, handleSpan);
            newChildren.push(handleSpan);
          }
        }
      }
      spanList.forEach((span, i) => {
        newChildren.push(nameMainSpan(span));
        if (i < spanList.length - 1) {
          // Handles between two successive a-spans.
          //
          // We want to pack in the handles as well as possible and allow
          // closing handles for one oa-span to appear in the same handle spans
          // as opening handles for another oa-span as long as the actual
          // handles do not overlap.  As we descend the tree of oa-spans, when
          // we find an oa-split that covers both a-spans (it's sufficient to
          // check that it covers the previous a-span but does not end there),
          // no handle can cross this split, so we can evaluate the total height
          // we need independently for each child of the split.  Once there are
          // no more such splits, a given oa-span on a given side either is a
          // leaf or has a split with one big handle, so no further packing is
          // possible.
          //
          // XXX: I suspect there's no problem with partial rectangles, but
          // confirm it?  Just wait and see if things break. ~ Matt 2018-01-23
          //
          // So that we can place the handles later, we assign each handle span
          // a real ID with respect to the previous a-span and an "alias" ID
          // with respect to the next a-span, stored in the spanAliasMap for
          // later use.
          let nextSpan = spanList[i + 1];
          let totalHeightForScopeSpan = (scopeSpan: TemplateSpanId<OrigB, OtherAxis<A>>): number => {
            let split = origLayout.archetype.getRectInfo(axisCons(a, span, scopeSpan))
              .coveringSplits[scopeAxis];
            // https://github.com/palantir/tslint/issues/3667
            // tslint:disable-next-line:strict-type-predicates
            if (split != null && !EJSON.equals(
              extremeOfMergedSpan(split.def.mergedScope, Extreme.LAST), span)) {
              return Math.max(...split.def.children.map(totalHeightForScopeSpan));
            } else {
              let heightPrev = assertNotNull(handleHeights[a][Extreme.LAST].get(axisCons(a, span, scopeSpan)));
              let heightNext = assertNotNull(handleHeights[a][Extreme.FIRST].get(axisCons(a, nextSpan, scopeSpan)));
              return (heightPrev > 0 ? heightPrev + 1 : 0) + (heightNext > 0 ? heightNext + 1 : 0);
            }
          };
          let ht = Math.max(...scopeParts.map(totalHeightForScopeSpan));
          for (let h = 0; h < ht; h++) {
            let actualSpan = nameHandleSpan(span, Extreme.LAST, h);
            setupHandleSpan(a, actualSpan);
            newChildren.push(actualSpan);
            let alias = nameHandleSpan(nextSpan, Extreme.FIRST, ht - 1 - h);
            spanAliasMap[a].set(alias, actualSpan);
            spanToAliasMap[a].set(actualSpan, alias);
          }
        }
      });
      {
        // Handles after the last span.
        let lastSpan = spanList[spanList.length - 1];
        let hh = Math.max(...scopeParts.map((scopePart) =>
          assertNotNull(handleHeights[a][Extreme.LAST].get(axisCons(a, lastSpan, scopePart)))));
        if (hh > 0) {
          for (let h = 0; h <= hh; h++) {
            let handleSpan = nameHandleSpan(lastSpan, Extreme.LAST, h);
            setupHandleSpan(a, handleSpan);
            newChildren.push(handleSpan);
          }
        }
      }

      return newChildren;
    };

    let selectableVariableSplits: {[A in AxisG<_>]: {
      dvssi: DesignViewSelectableSplitInfo<A>,
      selectableParentSpan: TemplateSpanId<VisualB, A>,
      selectableScope: PerExtreme<TemplateSpanId<VisualB, OtherAxis<A>>>
    }[]} = perAxis((a) => []);

    let drawHandleBoxSlice = <A extends Axis>(newMergedScope: PerExtreme<TemplateSpanId<VisualB, OtherAxis<A>>>,
      childSpanName: TemplateSpanId<VisualB, A>, splitAxis: A, boxStyle: BoxStyle,
      originalChildName: TemplateSpanId<OrigB, A>, /* only for depth */
      originalScope: PerExtreme<TemplateSpanId<OrigB, OtherAxis<A>>>, isVariable: boolean) => {
      let correspondingRectangle: MergedRectId<_, TemplateS, VisualB> =
        axisCons(splitAxis, perExtreme((e) => childSpanName), newMergedScope);
      // Compute depth used for deciding background colors
      let orginalRectangle: RectangleId<_, TemplateS, OrigB> =
        axisCons(splitAxis, originalChildName, originalScope[Extreme.FIRST]);
      let depth = fixmeAssertNotNull(origLayout.rectDepths.get(orginalRectangle));
      // Set up border parameters
      let specialBorderStyle: BorderStyle;
      switch (boxStyle) {
        case BoxStyle.DesignViewSpan:
        case BoxStyle.OriginalSpan:
          specialBorderStyle = isVariable ? variableSplitHandleStyle : fixedSplitHandleStyle; break;
        case BoxStyle.RootSpan:
          specialBorderStyle = grayBorderStyle;
      }
      let sideBorderStyle: BorderStyle =
        isVariable ? objectSideBorderStyle :
        (boxStyle == BoxStyle.DesignViewSpan) ? invisibleBorderStyle : grayBorderStyle;
      let backgroundColor = backgroundColors[depth % backgroundColors.length];
      // Add the corresponding allocation (with all borders invisible)
      let correspondingAllocation: LabelAllocationDef<_, VisualB> = {
        mergedSpans: correspondingRectangle,
        kind: TemplateAllocationDefKind.LABEL,
        text: "",
        backgroundAllocationInfo: {zIndex: BASE_Z_INDEX + depth},
        style: {
          backgroundColor: backgroundColor,
          borderStyle: independentPerAxis((a) => perExtreme((e) => invisibleBorderStyle))
        }
      };
      addSyntheticAllocation(correspondingAllocation);

      // add another transparent corresponding allocation right below forground for borders
      let transparentForBorderAllocation: LabelAllocationDef<_, VisualB> = {
        mergedSpans: correspondingRectangle,
        kind: TemplateAllocationDefKind.LABEL,
        text: "",
        backgroundAllocationInfo: {zIndex: -1},
        style: {
          backgroundColor: "transparent",
          borderStyle:
            independentAxisCons(splitAxis, perExtreme((e) => sideBorderStyle), perExtreme((e) => specialBorderStyle))
        }
      };
      addSyntheticAllocation(transparentForBorderAllocation);
    };



    let translateSplit = <A extends Axis>(id: TemplateSplitId<OrigB, A>,
      split: TemplateSplitDef<OrigB, A>, splitAxis: A) => {
      // Propagate hiding.
      let parentStyle = newSpec.spanStyles[splitAxis].get(nameMainSpan(split.parentSpan));
      for (let child of archetypalChildrenOfTemplateSplit(split)) {
        let childOrigStyle = origLayout.spec.spanStyles[splitAxis].get(child);
        let hideChild = (parentStyle != null && parentStyle.hide) || (childOrigStyle != null && childOrigStyle.hide);
        if (hideChild) {
          newSpec.spanStyles[splitAxis].set(nameMainSpan(child), {hide: true});
        }
      }
      let scopeAxis = otherAxis(splitAxis);
      let scope = split.mergedScope;
      // The new split will get a merged scope that includes the original scope
      // plus the inner handles of the split and any handles inside them.
      let outerHandleHeights = perExtreme((e) => {
        let extremeSpan = extremeOfMergedSpan(scope, e);
        let extremeRect: RectangleId<_, TemplateS, OrigB> = axisCons(splitAxis, split.parentSpan, extremeSpan);
        return assertNotNull(handleHeights[scopeAxis][e].get(extremeRect));
      });
      let outerHandleSpans = perExtreme((e) =>
        resolveSpanAlias(scopeAxis, nameHandleSpan(extremeOfMergedSpan(scope, e), e, outerHandleHeights[e])));
      let innerHandleSpans = perExtreme((e) =>
        resolveSpanAlias(scopeAxis, nameHandleSpan(extremeOfMergedSpan(scope, e), e, outerHandleHeights[e] - 1)));
      let newMergedScope = innerHandleSpans;  // Remember, merged spans are inclusive.

      let newSplitDef: TemplateSplitDef<VisualB, A>;
      if (split.kind == TemplateSplitDefKind.FIXED) {
        let marginSpans = perExtreme((e) => nameMarginSpan(id, e));
        for (let e of extremes) {
          setupHandleSpan(splitAxis, marginSpans[e]);
        }
        let translatedChildren = fixmeAssertNotNull(translatedFixedSplitChildren[splitAxis].get(id));
        let newChildren = [marginSpans[Extreme.FIRST], ...translatedChildren, marginSpans[Extreme.LAST]];
        newSplitDef = {
          parentSpan: nameMainSpan(split.parentSpan),
          kind: TemplateSplitDefKind.FIXED,
          children: newChildren,
          mergedScope: newMergedScope
        };
        for (let newChild of translatedChildren) {
          drawHandleBoxSlice(innerHandleSpans, newChild, splitAxis,
            (unwrapMainSpan(newChild) != null ? BoxStyle.OriginalSpan : BoxStyle.DesignViewSpan),
            split.children[0] /*only for depth*/, scope, false);
        }

        // Ensure we have a border around the outer handle spans.
        addSyntheticAllocation({
          mergedSpans: axisCons(splitAxis,
            perExtreme((e) => extremeOfSpanList(translatedChildren, e)), innerHandleSpans),
          kind: TemplateAllocationDefKind.LABEL,
          backgroundAllocationInfo: {zIndex: -1},
          style: {
            backgroundColor: "transparent",
            borderStyle:
              independentAxisCons(splitAxis, perExtreme((e) => grayBorderStyle),
                perExtreme((e) => invisibleBorderStyle))
          },
          text: ""
        });
      } else {
        // Generate a nested fixed split to hold handles for the child of the
        // variable split, since handles can't be added to the variable split.
        let wrapperSpanId = nameVariableItemWrapperSpan(split.child);
        addSyntheticSplit(splitAxis, {
          parentSpan: wrapperSpanId,
          kind: TemplateSplitDefKind.FIXED,
          children: addHandleSpansToSpanList(scope, [split.child], splitAxis),
          mergedScope: newMergedScope
        });
        // Make a box around each object.
        drawHandleBoxSlice(innerHandleSpans, wrapperSpanId, splitAxis, BoxStyle.OriginalSpan,
          split.child, scope, true);
        // Side borders around the original span.
        addSyntheticAllocation({
          mergedSpans: axisCons(splitAxis,
            mergedSpanOfSingleSpan(nameMainSpan(split.child)), innerHandleSpans),
          kind: TemplateAllocationDefKind.LABEL,
          // Lower z index than the object box, which should take priority if there are no handle spans.
          backgroundAllocationInfo: {zIndex: -2},
          style: {
            backgroundColor: "transparent",
            borderStyle:
              independentAxisCons(splitAxis, perExtreme((e) => grayBorderStyle),
                perExtreme((e) => invisibleBorderStyle))
          },
          text: ""
        });
        // Make the collection label.
        let column = getExistingColumn(split.nestedObjectType);
        let label_name = fallback(column.objectName, "");
        let parentSpanOfSplit = split.parentSpan;
        let labelMergedRect: MergedRectId<_, TemplateS, VisualB> = axisCons(splitAxis,
          mergedSpanOfSingleSpan(nameMainSpan(parentSpanOfSplit)),
          mergedSpanOfSingleSpan(outerHandleSpans[Extreme.FIRST]));
        // Add allocation to cover the label cell for this variable split
        let labelAllocation: LabelAllocationDef<_, VisualB> = {
          mergedSpans: labelMergedRect,
          kind: TemplateAllocationDefKind.LABEL,
          text: label_name,
          backgroundAllocationInfo: null,  // Auto sizing works only for foreground allocations.
          style: {
            backgroundColor: "transparent",
            textOrientation: (splitAxis == Axis.ROW) ? TextOrientation.SidewaysLeft : TextOrientation.Normal,
            isTextItalic: true,
            textFont: designViewLabelsFont,
            textAlignment: TextAlignment.Center,
            fontSize: "13px",
            borderStyle: independentPerAxis((a) => perExtreme((e) => invisibleBorderStyle))
          }
        };
        addSyntheticAllocation(labelAllocation);


        let bodySpanId = nameVariableSplitBodySpan(split.child);
        let variableSplitDef: VariableSplitDef<VisualB, A> = {
          parentSpan: bodySpanId,
          kind: TemplateSplitDefKind.VARIABLE,
          child: wrapperSpanId,
          extraSpaceChild: nameMainSpan(split.extraSpaceChild),
          nestedObjectType: split.nestedObjectType,
          mergedScope: newMergedScope
        };
        newSplitDef = variableSplitDef;
        let marginSpans = perExtreme((e) => nameMarginSpan(id, e));
        for (let e of extremes) {
          setupHandleSpan(splitAxis, marginSpans[e]);
        }
        addSyntheticSplit(splitAxis, {
          parentSpan: nameMainSpan(split.parentSpan),
          mergedScope: newMergedScope,
          kind: TemplateSplitDefKind.FIXED,
          children: [marginSpans[Extreme.FIRST], bodySpanId, marginSpans[Extreme.LAST]]
        });
        selectableVariableSplits[splitAxis].push({
          dvssi: {
            origId: id,
            designViewSplit: newSplitDef
          },
          selectableParentSpan: nameMainSpan(split.parentSpan),
          selectableScope: outerHandleSpans
        });
      }
      newSpec.splits[splitAxis].set(nameUserSplit(id), newSplitDef);
    };

    let translateAllocation = (id: TemplateAllocationId<OrigB>, allocation: TemplateAllocationDef<_, OrigB>) => {
      let fieldNameLabelInfo: null | FieldNameLabelInfo;
      if (allocation.kind == TemplateAllocationDefKind.FIELD) {
        let containerBinding = origLayout.getExistingRectBinding(
          origLayout.containerOfTemplateAlloc(allocation));
        fieldNameLabelInfo = {
          expansionAxis: allocation.field == assertNotNull(containerBinding.trp).type
            ? containerBinding.objectAxis : null,
          fieldName: fallback(getExistingColumn(allocation.field).fieldName, "")
        };
      } else if (allocation.kind != TemplateAllocationDefKind.LABEL) {
        // Show an empty field name label to maintain consistency and alignment.
        fieldNameLabelInfo = {expansionAxis: null, fieldName: ""};
      } else {
        // We don't want field name labels in extra space spans.  Guess we won't
        // get them in unbound areas either.
        fieldNameLabelInfo = null;
      }
      let defCommon: AllocationDefCommon<_, TemplateS, VisualB> = {
        mergedSpans: perAxis((a) => perExtreme((e) => nameMainSpan(allocation.mergedSpans[a][e]))),
        backgroundAllocationInfo: allocation.backgroundAllocationInfo /* should be null */,
        style: Object.assign({fieldNameLabelInfo}, allocation.style)
      };
      // TypeScript does a great job of checking this for all cases of `allocation`!
      let def: TemplateAllocationDef<_, VisualB> = {...allocation, ...defCommon};
      newSpec.allocations.set(nameUserAlloc(id), def);
    };

    for (let [rect, crossBinding] of origLayout.spec.crossBindings.entries()) {
      let newCrossRect: RectangleId<_, TemplateS, VisualB> = perAxis((a) => nameVariableItemWrapperSpan(rect[a]));
      newSpec.crossBindings.set(newCrossRect, crossBinding);
    }

    // By default, add a default border around rectagnle defined by root.m and root.m
    drawHandleBoxSlice(perExtreme((e) => nameMainSpan(rootTemplateSpanId<OrigB, Axis.COL>(DUMMY))),
      nameMainSpan(rootTemplateSpanId<OrigB, Axis.ROW>(DUMMY)),
      Axis.ROW /*arbitrary*/, BoxStyle.RootSpan,
      rootTemplateSpanId(DUMMY), perExtreme((e) => rootTemplateSpanId(DUMMY)), false);

    // Since translateSplit needs to place handles, which may require that the
    // spanAliasMap entries for other fixed splits already exist, the easiest
    // solution is to do addHandleSpansToSpanList for all fixed splits first and
    // save the results, then go back and translate all the splits.  (The
    // synthetic fixed splits generated when translating variable splits don't
    // generate any aliases.)
    forAxes((a) => {
      for (let [id, split] of origLayout.spec.splits[a].entries()) {
        if (split.kind == TemplateSplitDefKind.FIXED) {
          translatedFixedSplitChildren[a].set(id, addHandleSpansToSpanList(split.mergedScope, split.children, a));
        }
      }
    });
    forAxes((a) => {
      for (let [id, split] of origLayout.spec.splits[a].entries()) {
        translateSplit(id, split, a);
      }
    });
    for (let [id, alloc] of origLayout.spec.allocations.entries()) {
      translateAllocation(id, alloc);
    }

    // Draw the correct background color based on depth for each intersection of
    // two perpendicular handle boxes.  We draw the intersection when we visit
    // the rectangle formed by the parent spans of both splits.
    origLayout.archetype.traverseAll({
      preRect: (rect) => {
        let ri = origLayout.archetype.getRectInfo(rect);
        const coveringSplits = ri.coveringSplits;
        if (!axesEveryNotNull(coveringSplits))
          return;
        // Note: Partial rectangles cannot have a "covering split" on both axes,
        // so they won't get here.
        let mergedSpans: MergedRectId<_, TemplateS, VisualB> = perAxis((a) => {
          let archetypalSplit = coveringSplits[a];
          let templateSplit = origLayout.templateSplitOfArchetypalSplit(a, archetypalSplit);
          if (templateSplit.kind == TemplateSplitDefKind.FIXED) {
            let translatedChildren =
              fixmeAssertNotNull(translatedFixedSplitChildren[a].get(archetypalSplit.def.templateSplitId));
            return perExtreme((e) => extremeOfSpanList(translatedChildren, e));
          } else {
            return mergedSpanOfSingleSpan(nameVariableItemWrapperSpan(templateSplit.child));
          }
        });
        let representativeOrigRect: RectangleId<_, TemplateS, OrigB> =
          perAxis((a) => coveringSplits[a].def.children[0]);
        let depth = fixmeAssertNotNull(origLayout.rectDepths.get(representativeOrigRect));
        let backgroundColor = backgroundColors[depth % backgroundColors.length];
        addSyntheticAllocation({
          mergedSpans,
          kind: TemplateAllocationDefKind.LABEL,
          backgroundAllocationInfo: {zIndex: BASE_Z_INDEX + depth},
          style: {
            backgroundColor: backgroundColor,
            borderStyle: independentPerAxis((a) => perExtreme((e) => invisibleBorderStyle))
          },
          text: ""
        });
      }
    });

    // Overall background.
    addSyntheticAllocation({
      mergedSpans: perAxis((a) => mergedSpanOfSingleSpan(rootTemplateSpanId<VisualB, typeof a>(DUMMY))),
      kind: TemplateAllocationDefKind.LABEL,
      backgroundAllocationInfo: {zIndex: BASE_Z_INDEX},
      style: {
        backgroundColor: designViewLightGray,
        borderStyle: independentPerAxis((a) => perExtreme((e) => invisibleBorderStyle))
      },
      text: ""
    });

    forAxes((a) => {
      // Generate "root splits" to add handle spans to the original root spans.
      //
      // TODO: Implement "special fixed splits at the root" if desired.  We
      // might want to disable them for custom app UIs anyway.
      addSyntheticSplit(a, {
        parentSpan: rootTemplateSpanId(DUMMY),
        mergedScope: mergedSpanOfSingleSpan(rootTemplateSpanId(DUMMY)),
        kind: TemplateSplitDefKind.FIXED,
        children: addHandleSpansToSpanList(
          mergedSpanOfSingleSpan(rootTemplateSpanId(DUMMY)), [rootTemplateSpanId(DUMMY)], a),
      });
    });

    let newLayoutTemplate = new LayoutTemplate(newSpec);

    let variableSplitSelectionMap: {[A in AxisG<_>]:
      JSONKeyedMap<RectangleId<_, TemplateS, VisualB>, DesignViewSelectableSplitInfo<A>>} =
      perAxis((a) => new JSONKeyedMap<RectangleId<_, TemplateS, VisualB>, DesignViewSelectableSplitInfo<typeof a>>());
    forAxes((a) => {
      let oa = otherAxis(a);
      for (let svs of selectableVariableSplits[a]) {
        let fill = (oaSpan: TemplateSpanId<VisualB, OtherAxis<typeof a>>) => {
          let rect: RectangleId<_, TemplateS, VisualB> = axisCons(a, svs.selectableParentSpan, oaSpan);
          variableSplitSelectionMap[a].set(rect, svs.dvssi);
          let ri = newLayoutTemplate.archetype.getRectInfo(rect);
          let oaSplit = ri.coveringSplits[oa];
          // https://github.com/palantir/tslint/issues/3667
          // tslint:disable-next-line:strict-type-predicates
          if (oaSplit != null) {
            for (let child of oaSplit.def.children) {
              fill(child);
            }
          }
        };
        for (let scopePart of newLayoutTemplate.archetype.getSpansInMergedSpan(oa, svs.selectableScope)) {
          fill(scopePart);
        }
      }
    });

    return {
      newLayoutTemplate,
      variableSplitSelectionMap,
      spanToAliasMap
    };
  }

}
