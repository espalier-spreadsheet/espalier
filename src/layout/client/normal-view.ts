namespace Objsheets.Layout {

  export function generateNormalView<_>(origLayout: LayoutTemplate<_, OrigB>, minimalStyling: boolean) {
    let newSpec = emptyLayoutTemplateSpec<_, OrigB>(origLayout.spec._id);
    let shownSpans: {[A in AxisG<_>]: Set<TemplateSpanId<OrigB, A>>} =
      perAxis((a) => new Set([rootTemplateSpanId<OrigB, typeof a>(DUMMY)]));
    // C.f. adjustMergedSpan in prepareToDeleteSpan
    let adjustMergedSpan = <A extends Axis>(a: A, mergedSpan: MergedSpanId<TemplateS, OrigB, A>):
      null | MergedSpanId<TemplateS, OrigB, A> => {
      let shown = origLayout.archetype.getSpansInMergedSpan(a, mergedSpan).filter((s) => shownSpans[a].has(s));
      return shown.length == 0 ? null : {[Extreme.FIRST]: shown[0], [Extreme.LAST]: shown[shown.length - 1]};
    };
    origLayout.archetype.traverseAll({
      preRect(rect) {
        if (axesEvery((a) => shownSpans[a].has(rect[a]))) {
          let ri = origLayout.archetype.getRectInfo(rect);
          let processSplit = <A extends Axis>(a: A, split: SplitInfo<_, TemplateS, OrigB, A>) => {
            let templateSplit = origLayout.templateSplitOfArchetypalSplit(a, split);
            let oa = otherAxis(a);
            let newMergedScope = adjustMergedSpan(oa, templateSplit.mergedScope);
            if (newMergedScope != null) {
              let newTemplateSplit = EJSON.clone(templateSplit);
              newTemplateSplit.mergedScope = newMergedScope;
              if (newTemplateSplit.kind == TemplateSplitDefKind.FIXED) {
                newTemplateSplit.children =
                  newTemplateSplit.children.filter((s) => !isSpanStyleHidden(origLayout.getSpanStyle(a, s)));
              }
              newSpec.splits[a].set(split.def.templateSplitId, newTemplateSplit);
              for (let newChild of archetypalChildrenOfTemplateSplit(newTemplateSplit)) {
                shownSpans[a].add(newChild);
              }
            }
          };
          // As in Layout constructor, we have to process covering splits before
          // those with merged scopes (which might be affected by hiding), and
          // allocations last of all.
          forAxes((a) => {
            for (let split of ri.containedSplits[a]) {
              if (mergedSpanIsSingleSpan(split.def.mergedScope)) {
                processSplit(a, split);
              }
            }
          });
          forAxes((a) => {
            for (let split of ri.containedSplits[a]) {
              if (!mergedSpanIsSingleSpan(split.def.mergedScope)) {
                processSplit(a, split);
              }
            }
          });
          for (let alloc of ri.containedAllocations) {
            let newMergedSpans: NullableMergedSpanIds<_, TemplateS, OrigB> =
              perAxis((a) => adjustMergedSpan(a, alloc.def.mergedSpans[a]));
            if (axesEveryNotNull(newMergedSpans)) {
              let newTemplateAlloc = EJSON.clone(origLayout.templateAllocationOfArchetypalAllocation(alloc));
              newTemplateAlloc.mergedSpans = newMergedSpans;
              if (minimalStyling) {
                let style = newTemplateAlloc.style;
                let borderStyle_ = style.borderStyle;
                if (borderStyle_ == null) {
                  borderStyle_ = style.borderStyle = independentPerAxis((a) => perExtreme((e) => ({})));
                }
                const borderStyle = borderStyle_;
                forAxes((a) => {
                  for (let e of extremes) {
                    if (borderStyle[a][e].visibility == null) {
                      borderStyle[a][e].visibility = false;
                    }
                  }
                });
              }
              newSpec.allocations.set(alloc.def.templateAllocationId, newTemplateAlloc);
            }
          }
          let crossBinding = origLayout.spec.crossBindings.get(rect);
          if (crossBinding != null) {
            newSpec.crossBindings.set(rect, crossBinding);
          }
          // As in design view, we don't yet need to preserve span styles. :/
        }
      }
    });

    return new LayoutTemplate(newSpec) as {} as LayoutTemplate<_, VisualB>;
  }

}
