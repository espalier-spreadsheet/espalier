namespace Objsheets.Layout {

  export const BORDER_WIDTH_PX = 1;  // Keep consistent with layout-rect CSS rules

  // Terminology is hard when we have many similar data types... :/
  export type CSSOffsetsAndSizes = {
    height: number;
    width: number;
    top: number;
    left: number;
  };

  export function offsetsAndSizesToCSS(offsetsAndSizes: IndependentPerAxis<OffsetAndSize>): CSSOffsetsAndSizes {
    return {
      // Add back border width to get values that will work with `box-sizing: border-box`.
      height: offsetsAndSizes[Axis.ROW].size + BORDER_WIDTH_PX,
      width: offsetsAndSizes[Axis.COL].size + BORDER_WIDTH_PX,
      top: offsetsAndSizes[Axis.ROW].offset,
      left: offsetsAndSizes[Axis.COL].offset
    };
  }
  export function getCSSOffsetsAndSizes<_>(
    sizings: AxisSizings<_>, mergedRect: MergedRectId<_, DataS, VisualB>): CSSOffsetsAndSizes {
    let offsetsAndSizes = independentPerAxis((a) => sizings[a].getOffsetAndSize(mergedRect[a]));
    return offsetsAndSizesToCSS(offsetsAndSizes);
  }

  // Handles most AllocationStyle properties; not text orientation or borders.
  export function allocationStyleToCSS(allocStyle: AllocationStyle) {
    let cssStyle: React.CSSProperties = {};
    cssStyle.backgroundColor = fallback(allocStyle.backgroundColor, "white");
    if (allocStyle.textAlignment) cssStyle.textAlign = allocStyle.textAlignment;
    if (allocStyle.isTextBold) cssStyle.fontWeight = "bold";
    if (allocStyle.isTextItalic) cssStyle.fontStyle = "italic";
    if (allocStyle.isTextUnderlined) cssStyle.textDecoration = "underline";
    cssStyle.fontFamily = fallback(allocStyle.textFont, "Source Sans Pro");
    if (allocStyle.fontSize) cssStyle.fontSize = allocStyle.fontSize;
    let textColor = fallback(allocStyle.specialTextColor, allocStyle.textColor);
    if (textColor) cssStyle.color = textColor;
    return cssStyle;
  }

  type ButtonCallback = (button: Button) => void;

  type InnerRectangleToRender = {
    isExtraSpace: boolean;
    style: AllocationStyle;
    hide: IndependentPerAxis<boolean>;
    content: AllocationContent;
  };

  function rectangleIsExtraSpace<_>(container: RectangleId<_, DataS, VisualB>, layout: Layout<_, DataS, VisualB>) {
    // Because an extra space span shouldn't be part of a merged span, we can
    // just check the container.
    return axesSome((a) => fallback(layout.getSpanStyle(a, container[a]).isExtraSpace, false));
  }

  function makeInnerRectangleToRender<_>(
    alloc: AllocationInfo<_, DataS, VisualB>, layout: Layout<_, DataS, VisualB>,
    textOverride: null | string): InnerRectangleToRender {
    let isSpanHidden = <A extends Axis>(a: A, s: DataSpanId<A>) => {
      let style = layout.getSpanStyle(a, s);
      return fallback(style.hide, false);
    };
    return {
      isExtraSpace: rectangleIsExtraSpace(alloc.container, layout),
      style: (textOverride != null)
        ? {...alloc.def.style, specialTextColor: null}
        : alloc.def.style,
      hide: independentPerAxis((a) =>
        layout.getSpansInMergedSpan(a, alloc.def.mergedSpans[a]).every((s) => isSpanHidden(a, s))),
      content: fallback(textOverride, alloc.def.content)
    };
  }

  // Returns an absolute URL for convenience of LayoutUIComponentWithSizedData.onUserButtonClick.
  export function sanitizeURL(urlText: string) {
    try {
      let u = new URL(urlText, location.href);
      // We mainly want to block `javascript:`, but who knows what other
      // URL schemes might be dangerous in particular configurations.
      if (u.protocol == "http:" || u.protocol == "https:") {
        return u.href;
      }
    } catch (e) {}
    return new URL("../unsafe-link?link=" + encodeURIComponent(urlText), location.href).href;
  }

  // Shared logic between automatic sizing and actual rendering of rectangles.
  class InnerRectangleComponent extends React.PureComponent<{
    rectangle: InnerRectangleToRender,
    extraStyle: React.CSSProperties,
    buttonCallback: null | ButtonCallback,
    divRef?: React.Ref<HTMLDivElement>
  }> {
    imageElement: null | HTMLImageElement = null;
    imageResolver: undefined | (() => void);
    knownSizePromise: undefined | Promise<void>;
    render() {
      // By default
      this.knownSizePromise = Promise.resolve();
      this.imageResolver = undefined;

      let rectangle = this.props.rectangle;
      let style = Object.assign(this.props.extraStyle, allocationStyleToCSS(rectangle.style));
      let classNames = ["layout-rect-common"];
      if (!rectangle.isExtraSpace)
        classNames.push("layout-rect-real");

      // Try to make sure all cells are tall enough for a line of text even if
      // they currently contain no text.  This seems to force the same height as
      // adding a Latin-script letter without affecting the width; I haven't
      // researched how this works according to the specs. ~ Matt 2017-10-23
      //
      // XXX: This might affect copy and paste.  There's probably a well-known
      // workaround for that; use it.
      let adjustText = (text: string) => rectangle.isExtraSpace ? "" : "\u200B" + text;

      const content = rectangle.content;
      let renderedContent;
      if (typeof content == "string") {
        renderedContent = adjustText(content);
      } else if (content.type == "hyperlink") {
        // XXX: How is tab focus supposed to work for hyperlinks and buttons?
        // Not worth worrying about until we support keyboard navigation of
        // the sheet.
        renderedContent =
          <a href={sanitizeURL(content.url)} style={{
            pointerEvents: "auto",
            textDecoration: "inherit",
            color: "inherit"
          }}>{adjustText(content.text)}</a>;
      } else if (content.type == "button") {
        // XXX: How is styling supposed to work for buttons?
        renderedContent =
          <button style={{pointerEvents: "auto"}}
            onClick={(e) => assertNotNull(this.props.buttonCallback)(content)}
          >{adjustText(content.text)}</button>;
      } else if (content.type == "image") {
        if (content.dataurl !== undefined) {
          // XXX This code pattern may not be right for React asynchronous
          // rendering, but we are far away from adopting it anyway.
          this.knownSizePromise = new Promise((resolve) => this.imageResolver = resolve);
          renderedContent = <img src={content.dataurl}
            ref={(i) => this.imageElement = i} onLoad={(e) => this.imageLoaded()}/>;
        } else if (rectangle.style.isImageEditable) {
          // Override size to something more reasonable than the original 145x145.
          // Since we're setting the size, we don't need to make a knownSizePromise.
          renderedContent = <img width="40" height="40" src="/uploadIcon.png"/>;
        } else {
          renderedContent = undefined;
        }
      } else {
        assertUnreachable(content);
      }

      return <div ref={this.props.divRef} className={classNames.join(" ")} style={style}>
        {renderedContent}
      </div>;
    }
    checkImageComplete() {
      if (this.imageElement != null && this.imageElement.complete) {
        assertNotNull(this.imageResolver)();
      }
    }
    private imageLoaded() {
      assertNotNull(this.imageResolver)();
    }
    componentDidMount() {
      this.checkImageComplete();
    }
    componentDidUpdate() {
      this.checkImageComplete();
    }
  }

  export class FieldNameLabelComponent extends React.PureComponent<{
    fieldNameLabelInfo: FieldNameLabelInfo,
    objectExists: boolean,
    extraStyle: React.CSSProperties,
    divRef?: React.Ref<HTMLDivElement>
  }> {
    render() {
      return <div ref={this.props.divRef} className="field-name-label" style={this.props.extraStyle}>
        { /* Opacity is the easiest way to lighten both arrows and text. */ }
        <span style={{opacity: this.props.objectExists ? 1 : 0.5}}>
          { this.props.fieldNameLabelInfo.expansionAxis != null
            // If we don't specify the image size, then we can compute the wrong
            // size for the label while the image is still loading.  XXX: Would
            // we rather preload the image so it doesn't flash when loaded?
            ? <img width="8" height="8" style={{marginRight: "2px"}} src={
                {[Axis.ROW]: "/arrows-vert.png", [Axis.COL]: "/arrows-horiz.png"}
                  [this.props.fieldNameLabelInfo.expansionAxis] }/>
            : <></> }
          {this.props.fieldNameLabelInfo.fieldName}
        </span>
      </div>;
    }
  }

  export type BasicLayoutInfo<_> = {
    dataLayout: Layout<_, DataS, VisualB>;
  };
  export type SizedLayoutInfo<_> = BasicLayoutInfo<_> & {
    sizings: AxisSizings<_>;
  };

  // Default minimum span sizes.  If a user wants a span to be smaller than
  // this, the user has to both customize the minimum span size and ensure that
  // all allocations in the span can shrink.
  //
  // Without minimum span sizes, assuming we automatically create allocations to
  // cover the sheet, automatic allocation sizing would impose a reasonable
  // minimum row height, but the columns would be still very narrow and would
  // look bad unless we do something.  For now, just do this.
  //
  // TODO: Harmonize these with the default font.
  let DEFAULT_MIN_SPAN_SIZES: IndependentPerAxis<number>;
  Meteor.startup(() => {  // Load order for Axis (!)
    DEFAULT_MIN_SPAN_SIZES = {[Axis.ROW]: 24, [Axis.COL]: 48};
  });
  export const FIELD_NAME_LABEL_HEIGHT = 16;

  // Since we want to use InnerRectangleComponent to render rectangles for
  // automatic sizing, but React doesn't let us synchronously render and measure
  // an InnerRectangleComponent during the rendering of another React component,
  // the best we can do is this wrapper component that renders all the
  // rectangles we need, measures them, and passes the sizings to the actual
  // component on the next cycle.
  //
  // Would we rather the sizings be a separate prop and not injected into the
  // layoutInfo?  Not important.
  export type LayoutAutoSizingWrappedComponentProps<_, P extends {layoutInfo: BasicLayoutInfo<_>}> = P & {
    layoutInfo: SizedLayoutInfo<_>,
    // Is there a more proper way to do this with React??
    runAfterSizingUpdate(callback: () => void): void
  };
  // NOTE: Subclasses should not mess with anything except to override innerRender.
  export abstract class LayoutAutoSizingWrapperComponent<_, P extends {layoutInfo: BasicLayoutInfo<_>}>
    extends React.PureComponent<P, {subProps: null | LayoutAutoSizingWrappedComponentProps<_, P>}> {
    private fontsLoaded = false;
    private autoSizeComponent: null | AutoSizeRectanglesComponent<_> = null;
    private autoSizeDivs = new JSONKeyedMap<MergedRectId<_, DataS, VisualB>, null | HTMLDivElement>();
    private fieldNameLabelAutoSizeDivs = new JSONKeyedMap<FieldNameLabelInfo, null | HTMLDivElement>();
    private currentSizingsUpdateMarker: undefined | {};
    private sizings: undefined | AxisSizings<_>;
    constructor(props: P) {
      super(props);
      this.state = {subProps: null};
    }
    render() {
      // XXX: The wrapped component may be invisible for a moment before we
      // compute the first sizings.  We argue it's not any worse than the
      // wrapped component being out of date for a moment later on.
      //
      // There's some bug in contextual typing of `ref` in TypeScript 3.1.6 if
      // the type argument to `AutoSizeRectanglesComponent` isn't specified.
      // ~ Matt 2018-11-23
      return <>
        <AutoSizeRectanglesComponent<_> layoutInfo={this.props.layoutInfo}
          ref={(c) => this.autoSizeComponent = c}
          autoSizeDivs={this.autoSizeDivs}
          fieldNameLabelAutoSizeDivs={this.fieldNameLabelAutoSizeDivs}/>
        {this.state.subProps == null ? <></> : this.innerRender(this.state.subProps)}
      </>;
    }
    protected abstract innerRender(innerProps: LayoutAutoSizingWrappedComponentProps<_, P>): React.ReactNode;
    private scheduleSizingsUpdate() {
      let myMarker = {};
      this.currentSizingsUpdateMarker = myMarker;
      assertNotNull(assertNotNull(this.autoSizeComponent).knownSizesPromise).then(() => {
        if (this.currentSizingsUpdateMarker === myMarker) {
          this.currentSizingsUpdateMarker = undefined;
          this.updateSizings();
          this.updateSubProps(this.props);
        }
      });
    }
    private afterSizingUpdateCallbacks: (() => void)[] = [];
    private runAfterSizingUpdate = (callback: () => void) => {
      if (this.state.subProps != null &&
        this.props.layoutInfo == this.state.subProps.layoutInfo) {
        callback();
      } else {
        this.afterSizingUpdateCallbacks.push(callback);
      }
    }
    private updateSizings() {
      // Measure the rectangles, construct sizings, and set state.
      let layout = this.props.layoutInfo.dataLayout;
      // https://github.com/Microsoft/TypeScript/issues/26119
      this.sizings = perAxis<_, AxisSizings<_>>((a) => new AxisSizing(layout, a,
        (span) => fallback(layout.getSpanStyle(a, span).minSize,
          layout.getSpanStyle(a, span).isExtraSpace ? 0 : DEFAULT_MIN_SPAN_SIZES[a]),
        (alloc) => {
          let div = this.autoSizeDivs.get(alloc.def.mergedSpans);
          if (div == null)
            return 0;  // Extra space allocations
          let mainSize;
          let unrotatedAxis = (textOrientationOfAllocationStyle(alloc.def.style) == TextOrientation.Normal)
            ? a : otherAxis(a);
          switch (unrotatedAxis) {
            case Axis.ROW:
              // Subtract border width to achieve the effect of border collapsing.
              mainSize = div.offsetHeight - BORDER_WIDTH_PX;
              break;
            case Axis.COL:
              // Add an extra pixel to try to stop text from spilling out in
              // Firefox print to file or in Chrome.  Hope nobody notices. (Some
              // rounding issue?  I know browsers have plenty of them.  Not
              // investigating.) ~ Matt 2018-01-06
              mainSize = div.offsetWidth - BORDER_WIDTH_PX + 1;
              break;
            default:
              // https://github.com/Microsoft/TypeScript/issues/11572
              throw new Error("not reached");
          }
          if (alloc.def.style && alloc.def.style.fieldNameLabelInfo != null) {
            switch (a) {
              case Axis.COL:
                let fieldNameLabelDiv = this.fieldNameLabelAutoSizeDivs.get(alloc.def.style.fieldNameLabelInfo);
                return Math.max(mainSize, fixmeAssertNotNull(fieldNameLabelDiv).offsetWidth - BORDER_WIDTH_PX + 1);
              case Axis.ROW:
                return FIELD_NAME_LABEL_HEIGHT + mainSize;
              default:
                // https://github.com/Microsoft/TypeScript/issues/11572
                throw new Error("not reached");
            }
          } else {
            return mainSize;
          }
        }));
      }
    updateSubProps(newProps: P) {
      // Spread of type parameter not supported:
      // https://github.com/Microsoft/TypeScript/issues/10727
      let layoutInfo = Object.assign({sizings: fixmeAssertNotNull(this.sizings)}, newProps.layoutInfo);
      let subProps = Object.assign({}, newProps,
        {layoutInfo, runAfterSizingUpdate: this.runAfterSizingUpdate});
      this.setState({subProps});
      for (let callback of this.afterSizingUpdateCallbacks) {
        callback();
      }
      this.afterSizingUpdateCallbacks = [];
    }
    componentDidMount() {
      // Loads Font, and when loading is finished, call updateSizings
      let fontFamiliesToLoad = ["Roboto", "Source Sans Pro"];
      // Load specified fonts
      WebFont.load({
        google: {
          families: fontFamiliesToLoad
        },
        // Called when all fonts finished loading
        active: () => {
          this.fontsLoaded = true;
          this.scheduleSizingsUpdate();
        }
      });
    }
    componentWillReceiveProps(nextProps: P) {
      if (this.fontsLoaded) {
        if (nextProps.layoutInfo == this.props.layoutInfo) {
          // No need to update sizings.  We can update the subProps immediately
          // and save a re-render.
          this.updateSubProps(nextProps);
        } else {
          // Cancel any scheduled sizings update based on the previous props.
          this.currentSizingsUpdateMarker = undefined;
        }
      }
    }
    componentDidUpdate(prevProps: P) {
      if (this.fontsLoaded && this.props.layoutInfo != prevProps.layoutInfo) {
        // Note: if this.props.layoutInfo == prevProps.layoutInfo, then we
        // would have already updated subProps in componentWillReceiveProps.
        this.scheduleSizingsUpdate();
      }
    }
    componentWillUnmount() {
      this.currentSizingsUpdateMarker = undefined;
    }
  }

  // Separate component from withLayoutAutoSizing so it doesn't re-render after
  // withLayoutAutoSizing sets the subProps.
  class AutoSizeRectanglesComponent<_> extends React.PureComponent<{
    layoutInfo: BasicLayoutInfo<_>,
    autoSizeDivs: JSONKeyedMap<MergedRectId<_, DataS, VisualB>, null | HTMLDivElement>,
    fieldNameLabelAutoSizeDivs: JSONKeyedMap<FieldNameLabelInfo, null | HTMLDivElement>
  }> {
    ircs = new JSONKeyedMap<MergedRectId<_, DataS, VisualB>, null | InnerRectangleComponent>();
    knownSizesPromise: undefined | Promise<unknown>;
    render() {
      let layout = this.props.layoutInfo.dataLayout;
      let realAllocations: AllocationInfo<_, DataS, VisualB>[] = [];
      let fieldNameLabelInfos = new JSONSet<FieldNameLabelInfo>();
      layout.traverseAll({
        preRect: (rect) => {
          if (!rectangleIsExtraSpace(rect, layout)) {
            let allocationsHere = layout.getRectInfo(rect).containedAllocations;
            for (let alloc of allocationsHere) {
              if (alloc.def.backgroundAllocationInfo == null) {
                realAllocations.push(alloc);
              }
              if (alloc.def.style && alloc.def.style.fieldNameLabelInfo != null) {
                fieldNameLabelInfos.add(alloc.def.style.fieldNameLabelInfo);
              }
            }
          }
        }
      });
      let autoSizeRectangles = realAllocations.map((alloc) =>
        <InnerRectangleComponent
          key={JSON.stringify(alloc.def.mergedSpans)}
          rectangle={makeInnerRectangleToRender(alloc, layout, null)}
          extraStyle={{visibility: "hidden", whiteSpace: "nowrap", position: "absolute"}}
          ref={(c) => this.ircs.set(alloc.def.mergedSpans, c)}
          divRef={(d) => this.props.autoSizeDivs.set(alloc.def.mergedSpans, d)}
          buttonCallback={null}  // Not reached: button is hidden and can't be clicked.
        />
      );
      let fieldNameLabelAutoSizeDivs = Array.from(fieldNameLabelInfos.elements(), (fnli) =>
        <FieldNameLabelComponent
          key={JSON.stringify(fnli)}
          fieldNameLabelInfo={fnli}
          objectExists={true}  // Does not affect sizing
          extraStyle={{visibility: "hidden", whiteSpace: "nowrap", position: "absolute"}}
          divRef={(d) => this.props.fieldNameLabelAutoSizeDivs.set(fnli, d)}
        />);
      // Put the autoSizeRectangles in a div to keep them out of the way in
      // the developer tools.
      return <div>{autoSizeRectangles}{fieldNameLabelAutoSizeDivs}</div>;
    }
    private updateKnownSizesPromise() {
      this.knownSizesPromise = Promise.all(
        this.ircs.entries().map(([k, v]) => v == null ? Promise.resolve() : assertNotNull(v.knownSizePromise)));
    }
    componentDidMount() {
      this.updateKnownSizesPromise();
    }
    componentDidUpdate() {
      this.updateKnownSizesPromise();
    }
  }

  type RectangleToRender = InnerRectangleToRender & {
    key: string;  // for React
    backgroundAllocationInfo: null | BackgroundAllocationInfo;
    cssGeometry: CSSOffsetsAndSizes;
  };

  function orientationToRotationDegree(orientation: TextOrientation): number {
    // Positive degree in CSS is for rotation in clockwise direction. Negative is for counter-clockwise
    switch (orientation) {
      case TextOrientation.SidewaysLeft:
        return -90;
      case TextOrientation.SidewaysRight:
        return 90;
      case TextOrientation.Normal:
        return 0;
    }
  }

  class RectangleInteriorComponent extends React.PureComponent<{
    rectangle: RectangleToRender,
    buttonCallback: ButtonCallback
  }> {
    render() {
      let rectangle = this.props.rectangle;
      let allocStyle = rectangle.style || {};
      let extraStyle: React.CSSProperties = {
        pointerEvents: "none",
        position: "absolute",
        zIndex: rectangle.backgroundAllocationInfo ? rectangle.backgroundAllocationInfo.zIndex : 0
      };
      let cssGeometry = rectangle.cssGeometry;
      if (allocStyle.fieldNameLabelInfo != null) {
        cssGeometry = Object.assign({}, cssGeometry);
        cssGeometry.top += FIELD_NAME_LABEL_HEIGHT;
        cssGeometry.height -= FIELD_NAME_LABEL_HEIGHT;
      }
      if (allocStyle.backgroundColor == null) {
        // Special default backgrounds.
        //
        // XXX Fails if we rotate the whole div because text is rotated.  Maybe
        // we can just disallow that.
        let backgroundNames: string[] = [];
        forAxes((a) => {
          if (rectangle.hide[a]) {
            backgroundNames.push(`hidden-${a}-span`);
          }
        });
        const allocStatus = rectangle.style.allocStatus;
        if (allocStatus != null) {
          // Error takes priority over nonexistent.
          forAxes((a) => {
            if (allocStatus.spanObjectStatus[a] == ObjectStatus.ERROR)
              backgroundNames.push(`error-${a}-span`);
          });
          forAxes((a) => {
            if (allocStatus.spanObjectStatus[a] == ObjectStatus.NONEXISTENT)
              backgroundNames.push(`extra-space-${a}-span`);
          });
          if (allocStatus.areaIsUnbound)
            backgroundNames.push("unbound-area");
          switch (allocStatus.crossObjectStatus) {
            case ObjectStatus.NONEXISTENT:
              backgroundNames.push("nonexistent-cross-object");
              break;
            case ObjectStatus.ERROR:
              backgroundNames.push("erroneous-cross-object");
              break;
          }
        }
        // Make the backgrounds line up across cells, but don't use
        // `border-attachment: fixed` because the scrolling behavior is too
        // weird.
        extraStyle.background = backgroundNames.map((n) =>
          `url(/${n}-bg.png) border-box top ${-cssGeometry.top}px left ${-cssGeometry.left}px, `)
          .join("") + "white";
      }
      let orientation = textOrientationOfAllocationStyle(allocStyle);
      if (orientation == TextOrientation.Normal) {
        Object.assignTo(extraStyle, cssGeometry);
      } else {
        let rotationDegree = orientationToRotationDegree(orientation);
        // Computes style of the div such that it ends up in the right place after rotation
        // Because center of rotation is the center of the div, so position adjustment is needed for alignment
        //
        // Math.floor seems to work around a rounding issue in Firefox without
        // breaking Chrome.  Not investigating further. ~ Matt 2018-02-21
        Object.assignTo(extraStyle, {
          height: cssGeometry.width,
          width: cssGeometry.height,
          top: cssGeometry.top + Math.floor((cssGeometry.height - cssGeometry.width) / 2),
          left: cssGeometry.left + Math.floor((cssGeometry.width - cssGeometry.height) / 2),
          transform: "rotate(" + rotationDegree + "deg)",
        });
      }
      let inner = <InnerRectangleComponent rectangle={rectangle} extraStyle={extraStyle}
        buttonCallback={this.props.buttonCallback}/>;
      if (allocStyle.fieldNameLabelInfo == null) {
        return inner;
      } else {
        // The field name label is rendered specially rather than added to the
        // design-view layout template as a separate allocation because the
        // split we would have to add for a user allocation with a non-leaf row
        // span might conflict with a user split.  Although special rendering
        // means more code to maintain, it may also have a lower performance hit
        // than making a separate allocation.
        //
        // The field name label overlaps the top row of pixels of the inner
        // rectangle.
        return <>
          {inner}
          <FieldNameLabelComponent fieldNameLabelInfo={allocStyle.fieldNameLabelInfo}
            objectExists={allocStatusIsNormal(fixmeAssertNotNull(allocStyle.allocStatus))}
            extraStyle={{
              pointerEvents: "none",
              position: "absolute",
              height: FIELD_NAME_LABEL_HEIGHT + BORDER_WIDTH_PX,
              width: cssGeometry.width,
              top: rectangle.cssGeometry.top,  // original
              left: cssGeometry.left
            }}/>
        </>;
      }
    }
  }

  class RectangleBorderComponent extends React.PureComponent<{
    rectangle: RectangleToRender
  }> {
    render() {
      let rectangle = this.props.rectangle;
      let classNames = ["layout-rect-common"];
      let style: React.CSSProperties = {
        pointerEvents: "none",
        position: "absolute",
        zIndex: rectangle.backgroundAllocationInfo ? rectangle.backgroundAllocationInfo.zIndex : 0
      };
      Object.assignTo(style, rectangle.cssGeometry);
      style.borderStyle = "solid";
      let defaultBorderColor = "#CCC";
      let defaultBorderWidth = "1px";
      // Double borders do not show up unless border width is at least 4px at 100% zoom
      let defaultDoubleBorderWidth = "4px";
      let defaultBorderStyle = "solid";

      let rectangleStyle = rectangle.style || {};
      let emptyBorderStyles = independentPerAxis((a) => perExtreme<BorderStyle>((e) => ({})));
      let borderStyles = rectangleStyle.borderStyle || emptyBorderStyles;
      // BorderStyles of edges in the order we need for the CSS shorthand
      // properties (Top, Right, Bottom, Left).
      let parsedBorderStyles = [
        borderStyles[Axis.ROW][Extreme.FIRST],
        borderStyles[Axis.COL][Extreme.LAST],
        borderStyles[Axis.ROW][Extreme.LAST],
        borderStyles[Axis.COL][Extreme.FIRST]
      ];
      style.borderColor = parsedBorderStyles.map(
        (s) => fallback(s && s.color, defaultBorderColor)).join(" ");
      style.borderWidth = parsedBorderStyles.map(
        (s) => fallback(s && s.visibility, true) ?
          (fallback(s && s.isDouble, false) ? defaultDoubleBorderWidth : defaultBorderWidth) : "0px"
      ).join(" ");
      style.borderStyle = parsedBorderStyles.map(
        (s) => fallback(s && s.isDouble, false) ? "double" : defaultBorderStyle).join(" ");
      return <div className={classNames.join(" ")} style={style}></div>;
    }
  }

  // Interiors + borders of background allocations; interiors of non-background allocations
  class MainLayerComponent extends React.PureComponent<{
    rectanglesToRender: RectangleToRender[],
    buttonCallback: ButtonCallback
  }> {
    render() {
      return this.props.rectanglesToRender.map((rectangle) => {
        return <React.Fragment key={rectangle.key}>
          <RectangleInteriorComponent rectangle={rectangle}
            buttonCallback={this.props.buttonCallback}/>
          {rectangle.backgroundAllocationInfo
            ? <RectangleBorderComponent rectangle={rectangle} />
            : <></> }
        </React.Fragment>;
      });
    }
  }

  // Borders of non-background allocations
  class BordersLayerComponent extends React.PureComponent<{
    rectanglesToRender: RectangleToRender[]
  }> {
    render() {
      return this.props.rectanglesToRender.map((rectangle) => {
        return rectangle.backgroundAllocationInfo
          ? <React.Fragment key={rectangle.key} />
          : <RectangleBorderComponent key={rectangle.key} rectangle={rectangle} />;
      });
    }
  }

  // Overrides the text of an allocation.  Used to show text edits made by the
  // user from the time they are saved (and the edit controls are hidden) until
  // a new layout with the edits is rendered.
  export type AllocTextOverride<_> = {
    mergedRect: MergedRectId<_, DataS, VisualB>,
    text: string,
    // Used to identify the AllocTextOverride that should be cleaned up on
    // completion of a given edit, since the AllocTextOverride object itself may
    // be recreated when the mergedRect is re-interned.
    handle: object
  };
  export type LayoutComponentProps<_> = {
    layoutInfo: SizedLayoutInfo<_>,
    allocTextOverrides: AllocTextOverride<_>[],
    buttonCallback: ButtonCallback
  };
  export class LayoutComponent<_> extends React.PureComponent<LayoutComponentProps<_>> {
    private prevLayoutInfo: null | SizedLayoutInfo<_> = null;
    private rectanglesToRender: fixmeUndefinable<RectangleToRender[]>;

    private addRectangleObj(rectanglesToRender: RectangleToRender[],
      alloc: AllocationInfo<_, DataS, VisualB>,
      mergedRect: MergedRectId<_, DataS, VisualB>, textOverride: null | string) {
      let layout = this.props.layoutInfo.dataLayout;
      let sizings = this.props.layoutInfo.sizings;

      let offsetsAndSizes =
        independentPerAxis((a) => sizings[a].getOffsetAndSize(mergedRect[a]));
      if (axesSome((a) => offsetsAndSizes[a].size == 0)) {
        // Just drop zero-size rectangles for now, otherwise we'd have to do
        // something to avoid the two 1-pixel borders forcing them to a larger
        // size than we want.

        // When a rectangle (whether extra space or otherwise) shrinks to zero
        // size on an axis, any borders configured on the perpendicular axis
        // will be lost, even though logically they should appear.  We let
        // this be the user's problem for now.

        // Note: A reasonable use case for allowing a non-extra-space span to
        // shrink to zero size is the current idiom for conditional UI with
        // two variable splits, one of which shrinks to zero size.  We may or
        // may not ever add true conditionals to the layout model.
      } else {
        rectanglesToRender.push({
          key: JSON.stringify(
            // Multiple foreground and background allocations may have the same
            // mergedRect.
            {templateAllocationId: alloc.def.templateAllocationId, mergedRect}),
          cssGeometry: offsetsAndSizesToCSS(offsetsAndSizes),
          backgroundAllocationInfo: alloc && alloc.def.backgroundAllocationInfo,
          ...makeInnerRectangleToRender(alloc, layout, textOverride)
        });
      }
    }
    private buildRectanglesToRender() {
      let layout = this.props.layoutInfo.dataLayout;

      // Contains a parallel array javascript objects that represent the rectangles in our sheet
      let rectanglesToRender: RectangleToRender[] = [];

      // Traverses and gathers every rectangle in our sheet as a javascript obj into rectangles
      // Assigns a size and position to each child rectangle relative to its layout container
      layout.traverseAll({
        preRect: (rect) => {
          // We no longer render uncovered leaves as cells because many design
          // view added rectangles are intentionally uncovered by foreground
          // allocations.  LayoutUIComponent checks that all user rectangles are
          // covered.

          for (let alloc of layout.getRectInfo(rect).containedAllocations) {
            this.addRectangleObj(rectanglesToRender, alloc, alloc.def.mergedSpans, null);
          }
        }
      });

      this.rectanglesToRender = rectanglesToRender;
    }
    render() {
      let layoutInfo = this.props.layoutInfo;
      // This seems to be a good way to cache data derived from the props that
      // we use in rendering. If we update the data in
      // componentWillReceiveProps, it's easy to accidentally use this.props
      // instead of nextProps.
      if (layoutInfo != this.prevLayoutInfo) {
        this.prevLayoutInfo = layoutInfo;
        this.buildRectanglesToRender();
      }
      let overrideRectanglesToRender: RectangleToRender[] = [];
      for (let {mergedRect, text} of this.props.allocTextOverrides) {
        try {
          let alloc = assertNotNull(layoutInfo.dataLayout.coveringAllocationOfMergedRect(mergedRect));
          assert(EJSON.equals(alloc.def.mergedSpans, mergedRect));
          this.addRectangleObj(overrideRectanglesToRender, alloc, mergedRect, text);
        } catch (e) {
          if (!(e instanceof StaticError)) throw e;
          // We can get "Invalid rectangle ID" if the object has been
          // concurrently deleted.
        }
      }
      // Since we want re-rendering with a text override to be very fast when
      // the layout hasn't changed, we leave the main components completely
      // unchanged and just stick an extra RectangleInteriorsComponent over the
      // main interiors but under the main borders.  A hack, but it works.
      // Transient glitches may be visible in the background color at a hidden
      // border, but that's a minor issue.  The approach is subject to change as
      // this code is restructured for overall better performance.
      return <>
        <MainLayerComponent rectanglesToRender={fixmeAssertNotNull(this.rectanglesToRender)}
          buttonCallback={this.props.buttonCallback} />
        <MainLayerComponent rectanglesToRender={overrideRectanglesToRender}
          buttonCallback={this.props.buttonCallback} />
        <BordersLayerComponent rectanglesToRender={fixmeAssertNotNull(this.rectanglesToRender)} />
      </>;
    }
  }

}
