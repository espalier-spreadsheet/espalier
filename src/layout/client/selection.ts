namespace Objsheets.Layout {

  export type SelectionAddInfo<A extends Axis> = {
    // Split and family to which to add an object, if applicable.  (The split is
    // used to update the selection.)
    addAxis: A;
    addOrigSplitId: TemplateSplitId<OrigB, A>;
    // Null if we can't actually add due to a spare object.
    addFamily: null | FamilyHandle;
  };
  export type SelectionExistentialAddInfo = <R>(f: <A extends Axis>(x: SelectionAddInfo<A>) => R) => R;
  export type SelectionInfo<_> = {
    selection: MergedRectId<_, DataS, VisualB>;
    selectionPixels: IndependentPerAxis<OffsetAndSize>;

    changeColumnArgs: LayoutActionBar.ChangeColumnArgs[];

    // TemplateRectBinding for the object(s) intersected by the selection.  We
    // don't make a list of the actual objects until the context menu is built,
    // in case doing so might slow down selection performance noticeably for
    // large selections.
    objectsBinding: TemplateRectBinding<_, VisualB>;

    // Stuff that serves the needs of the add/delete object commands.

    // Currently, we display an "add/delete target" that is either (1) a merged
    // rectangle of objects (maybe with extra space spans) that the user can
    // delete (or add if they are cross objects) or (2) a variable split in
    // which the user can add an object. The add/delete target is the innermost
    // region of one of these two types that contains the original selection.
    // (That's the intuition; more precisely, case 2 is used only if case 1
    // would identify a single object and there is a unique innermost variable
    // split inside that object that contains the selection.)
    //
    // For case 1, if the objects are span (not cross) objects, the user can
    // also add a sibling object, which is equivalent to adding an object to the
    // containing variable split under case 2.  Case 2 is used only when the
    // selection includes a part of the variable split that isn't part of any
    // object, such as the extra space span or a design view annotation.
    addDeleteTargetPixels: null | IndependentPerAxis<OffsetAndSize>;

    // Used by the add/delete objects implementation in commands.ts:

    canAddOrDeleteSelectedObjects: boolean;

    // I wanted to use an existential type defined by the type-funcs library,
    // but it wouldn't work because at some point we would need a conditional
    // type like `X extends Axis ? SelectionAddInfo<X> : never`, which won't
    // reduce when we set `X = A` in calling code because definite assignability
    // doesn't consider the constraint of `A`. ~ Matt 2018-07-23
    addInfo: null | SelectionExistentialAddInfo;
  };

  export function resolveSelection<_>(
    selection: MergedRectId<_, DataS, VisualB>, layoutInfo: SizedDataLayoutInfo<_>): SelectionInfo<_> {

    let changeColumnArgs: LayoutActionBar.ChangeColumnArgs[] = [];

    // TODO: In design view, we probably want to disallow useless selections of
    // design view annotations.  Leave this until we start implementing editing
    // operations.

    // If the selection lies within a single allocation, expand it to the
    // whole allocation, since there's no command that can be executed on part
    // of a single allocation.
    //
    // Future: If we add keyboard navigation, we should remember the original
    // selection and display it as a dotted rectangle.
    const alloc = layoutInfo.dataLayout.coveringAllocationOfMergedRect(selection);
    if (alloc != null) {
      selection = perAxis((a) => alloc.def.mergedSpans[a]);
      let origAllocationId = visualToOrigAllocationId(alloc.def.templateAllocationId, layoutInfo);
      let templateAlloc = layoutInfo.dataLayoutSpec.getAllocBinding(alloc).templateAlloc;
      if (origAllocationId != null && templateAlloc.kind != TemplateAllocationDefKind.LABEL) {
        // The action bar targets the allocation.
        changeColumnArgs = [{
          onObjectHeader: false,
          allocationId: origAllocationId
        }];
      }
    }
    let selectionPixels =
      independentPerAxis((a) => layoutInfo.sizings[a].getOffsetAndSize(selection[a]));

    // Calculate info for add/delete object commands.

    let selectionUpperLeft: RectangleId<_, DataS, VisualB> =
      perAxis((a) => extremeOfMergedSpan(selection[a], Extreme.FIRST));
    let templateSelection: MergedRectId<_, TemplateS, VisualB> =
      perAxis((a) => perExtreme((e) => templateSpanOfDataSpan(selection[a][e])));
    let templateSelectionUpperLeft: RectangleId<_, TemplateS, VisualB> =
      perAxis((a) => extremeOfMergedSpan(templateSelection[a], Extreme.FIRST));
    // Here and in expandAxisSelectionToWholeObjects below, we are taking
    // advantage of the fact that in a merged span including both object spans
    // and an extra space span, the first span is always an object span.  Once
    // we add more kinds of margins to variable splits, we'll have to do
    // something more sophisticated.
    let objectsBinding = layoutInfo.visualLayoutTemplate.getExistingRectBinding(templateSelectionUpperLeft);

    // Look for a case-2 variable split on each axis.
    type VariableSplitInfo<A extends AxisL> = {
      origSplitId: TemplateSplitId<OrigB, A>;
      visualSplit: VariableSplitDef<VisualB, A>;
      family: null | FamilyHandle;
    };
    let variableSplitInfo: {[A in AxisG<_>]: null | VariableSplitInfo<A>} =
      // XXX: Why do I get an error without this type annotation?
      // https://github.com/Microsoft/TypeScript/issues/9659 maybe.
      perAxis<_, {[A in AxisG<_>]: null | VariableSplitInfo<A>}>((a) => {
      if (objectsBinding.trp == null) return null;  // Unbound area
      let oa = otherAxis(a);
      if (axesSome((a2) => !mergedSpanIsSingleSpan(selection[a2]) &&
        templateSelectionUpperLeft[a2] == objectsBinding.origRectangle[a2])) {
        // Case-1 target is not a single object.  Bail out.
        return null;
      }
      // Check candidate rectangles with successively larger a-spans starting
      // with the containing a-span of the selection and see if any of them has
      // a covering variable split.  After checking a rectangle, if it has no
      // a-parent, move to the nearest oa-ancestor that does.  If
      // `templateSelection[oa]` is a nontrivial merged span, then we allow its
      // first extreme `templateSelectionUpperLeft[oa]` to stand in for it in
      // candidateTemplateRect for the purpose of this traversal, but when we
      // check for a covering variable split, we use the appropriate
      // `splitCoveringMergedScope` or analogue.
      //
      // Since we are only looking at rectangles bound to a single object, we
      // can gain a little performance by doing the computation in terms of
      // template rectangles.
      let visualTemplateArchetype = layoutInfo.visualLayoutTemplate.archetype;
      let candidateTemplateRect: RectangleId<_, TemplateS, VisualB> = axisCons(a,
        visualTemplateArchetype.containerOfMergedSpan(a, templateSelection[a]),
        templateSelectionUpperLeft[oa]);
      for (;;) {
        let mustCheckMergedOaSpan =
          !mergedSpanIsSingleSpan(templateSelection[oa]) &&
          candidateTemplateRect[oa] == templateSelectionUpperLeft[oa];
        let origSplitId: null | TemplateSplitId<OrigB, typeof a> = null,
          visualSplit: null | TemplateSplitDef<VisualB, typeof a> = null;
        // TODO: Instead of having 2 cases here, we should probably have a
        // "dummy" design view transformation that generates the same data
        // structures without adding anything to the layout template.
        const designViewInfo = layoutInfo.designViewInfo;
        if (designViewInfo) {
          let dvssi;
          if (mustCheckMergedOaSpan) {
            // Reimplementing the idea of splitCoveringMergedScope against variableSplitSelectionMap.
            dvssi = singleOfPerExtreme(perExtreme((e) =>
              designViewInfo.variableSplitSelectionMap[a].get(
                axisCons(a, candidateTemplateRect[a], templateSelection[oa][e]))));
          } else {
            dvssi = designViewInfo.variableSplitSelectionMap[a].get(candidateTemplateRect);
          }
          if (dvssi != null) {
            origSplitId = dvssi.origId;
            visualSplit = dvssi.designViewSplit;
          }
        } else {
          let archetypalSplit: null | SplitInfo<_, TemplateS, VisualB, typeof a> = mustCheckMergedOaSpan
            ? visualTemplateArchetype.splitCoveringMergedScope(a, candidateTemplateRect[a], templateSelection[oa])
            : visualTemplateArchetype.getRectInfo(candidateTemplateRect).coveringSplits[a];
          if (archetypalSplit != null) {
            origSplitId = assertNotNull(visualToOrigSplitId(archetypalSplit.def.templateSplitId, layoutInfo));  // no-op
            visualSplit =
              layoutInfo.visualLayoutTemplate.templateSplitOfArchetypalSplit(a, archetypalSplit);
          }
        }
        if (visualSplit != null && visualSplit.kind == TemplateSplitDefKind.VARIABLE) {
          // We know the candidate rectangle is inside the case-1 target, but make sure
          // the scope of the candidate split is.
          let containerScope = visualTemplateArchetype.containerOfMergedSpan(oa, visualSplit.mergedScope);
          for (let search = candidateTemplateRect[oa]; search != containerScope;
            search = assertNotNull(visualTemplateArchetype.getParentSplitOfSpan(oa, search)).container[oa]) {
            if (search == objectsBinding.origRectangle[oa]) {
              // We hit the oa-span of the case-1 target before we reached the
              // scope of the candidate split.
              return null;
            }
          }
          let parentCellId = assertNotNull(layoutInfo.dataLayoutSpec.getRectBinding(selectionUpperLeft).qCellId).cellId;
          let family = cellIdIsReal(parentCellId)
            ? new FamilyHandle({columnId: visualSplit.nestedObjectType, parentCellId})
            : null;
          return {
            origSplitId: assertNotNull(origSplitId),  // insufficiently smart CFA
            visualSplit,
            family
          };
        }
        // We didn't find a variable split.  Go to the next candidate rectangle,
        // but stop if we hit the span of the case-1 target object rectangle on
        // either axis.
        if (candidateTemplateRect[a] == objectsBinding.origRectangle[a])
          return null;
        let aParent;
        while ((aParent = visualTemplateArchetype.getRectParentOnAxis(candidateTemplateRect, a)) == null) {
          if (candidateTemplateRect[oa] == objectsBinding.origRectangle[oa])
            return null;
          candidateTemplateRect = assertNotNull(visualTemplateArchetype.getRectParentOnAxis(candidateTemplateRect, oa));
        }
        candidateTemplateRect = aParent;
      }
    });

    let expandAxisSelectionToWholeObjects = <A extends Axis>(a: A) =>
      templateSelectionUpperLeft[a] == objectsBinding.origRectangle[a]
      ? selection[a]  // Preserve extra space span
      : mergedSpanOfSingleSpan(layoutInfo.dataLayoutSpec.instantiateSpanId(
        objectsBinding.origRectangle[a], selectionUpperLeft[a].objectId));

    // Now work out which case we are in.
    let canAddOrDeleteSelectedObjects = false, useContainingType = false;
    let addInfo: null | SelectionExistentialAddInfo = null;
    let addDeleteTarget: null | MergedRectId<_, DataS, VisualB> = null;
    // https://github.com/palantir/tslint/issues/3667
    // tslint:disable-next-line:strict-type-predicates
    if (axesEvery((a) => variableSplitInfo[a] != null)) {
      // Corner case: the user selected the entire intersection of two variable
      // splits.  Don't show a target or offer any commands.
    } else if (axesSome((addAxis) => {
      let vsi = variableSplitInfo[addAxis];
      // https://github.com/palantir/tslint/issues/3667
      // tslint:disable-next-line:strict-type-predicates
      if (vsi == null) return false;
      // Case 2
      let addOrigSplitId = vsi.origSplitId;
      let addFamily = vsi.family;
      // Target is the instantiation of vsi.visualSplit.
      addDeleteTarget = layoutInfo.dataLayoutSpec.calculateMergedRectId(
        axisCons(addAxis, mergedSpanOfSingleSpan(vsi.visualSplit.parentSpan),
          vsi.visualSplit.mergedScope),
        layoutInfo.dataLayoutSpec.getRectBinding(selectionUpperLeft).rpm);
      // If we already selected a non-leaf allocation (e.g.,
      // ProjDivSupportInfo:effort in the financial-assistant example), keep it.
      if (changeColumnArgs.length == 0) {
        // The action bar targets the object type of the variable split.
        changeColumnArgs = [{
          columnId: vsi.visualSplit.nestedObjectType,
          onObjectHeader: true
        }];
      }
      addInfo = (f) => f({addAxis, addOrigSplitId, addFamily});
      return true;
    })) {
      // Done
    } else if (objectsBinding.objectAxis != null) {
      // Case 1 with span object(s)
      canAddOrDeleteSelectedObjects = useContainingType = true;
      (<A extends Axis>(addAxis: A) => {
        let objectAxisTarget = expandAxisSelectionToWholeObjects(addAxis);
        let parentSplit = assertNotNull(
          layoutInfo.dataLayout.getParentSplitOfSpan(addAxis, objectAxisTarget[Extreme.FIRST]));
        let addOrigSplitId = assertNotNull(visualToOrigSplitId(parentSplit.def.templateSplitId, layoutInfo));
        let addParent = cellIdParentWithSpares(objectAxisTarget[Extreme.FIRST].objectId);
        let addFamily: null | FamilyHandle;
        if (cellIdIsReal(addParent)) {
          addFamily = new FamilyHandle({
            columnId: <NonRootColumnId>assertNotNull(objectsBinding.trp).type,
            parentCellId: addParent
          });
        } else {
          addFamily = null;
        }
        addInfo = (f) => f({addAxis, addOrigSplitId, addFamily});
        addDeleteTarget = axisCons(addAxis, objectAxisTarget, parentSplit.def.mergedScope);
      })(objectsBinding.objectAxis);
    } else if (objectsBinding.trp != null) {
      // The action bar can target the root object type of the layout if it is a
      // view object type rather than the root of the schema.  This is important
      // so the user can rename the view object type.
      useContainingType = true;
      if (!EJSON.equals(objectsBinding.origRectangle,
        perAxis<_, RectangleId<_, TemplateS, OrigB>>((a) => rootTemplateSpanId(DUMMY)))) {
        // Case 1 with cross object(s)
        canAddOrDeleteSelectedObjects = true;
        addDeleteTarget = perAxis((a) => expandAxisSelectionToWholeObjects(a));
      }
    } else {
      // Unbound region.  No available commands, so don't show a target.
    }

    // For any case-1 selection other than a single field allocation, the action
    // bar targets the containing object type.
    //
    // FIXME: When a cross object is covered by a single field allocation,
    // there's no way to make the action bar target the object type instead of
    // the allocation, even in design view.
    let type;
    if (changeColumnArgs.length == 0 && useContainingType &&
      (type = assertNotNull(objectsBinding.trp).type) != rootColumnId) {
      changeColumnArgs = [{
        columnId: type,
        onObjectHeader: true
      }];
    }

    const finalAddDeleteTarget = addDeleteTarget;
    return {
      selection,
      selectionPixels,
      changeColumnArgs,
      objectsBinding,
      addDeleteTargetPixels: (finalAddDeleteTarget == null) ? null
        : independentPerAxis((a) => layoutInfo.sizings[a].getOffsetAndSize(finalAddDeleteTarget[a])),
      canAddOrDeleteSelectedObjects,
      addInfo
    };
  }

}
