namespace Objsheets.Layout {

  function arraySum(a: number[]) {
    return a.reduce((x, y) => x + y);
  }

  export type OffsetAndSize = {offset: number, size: number};
  export class AxisSizing<_, A extends AxisG<_>> {
    // Maps span to its size
    sizeMap = new Map<DataSpanId<A>, number>();
    offsetMap = new Map<DataSpanId<A>, number>();

    constructor(public layout: Layout<_, DataS, VisualB>, public a: A,
      minSpanSize: (spanId: DataSpanId<A>) => number,
      allocSize: (alloc: AllocationInfo<_, DataS, VisualB>) => number) {
      // Compute sizes.
      // Currently: top-to-bottom (or left-to-right) greedy.
      // last -> first -> size
      let tempSizeMap = new Map<DataSpanId<A>, Map<DataSpanId<A>, number>>();
      let offerSize = (first: DataSpanId<A>, last: DataSpanId<A>, size: number) => {
        let mLast = tempSizeMap.get(last);
        if (mLast == null) {
          mLast = new Map<DataSpanId<A>, number>();
          tempSizeMap.set(last, mLast);
        }
        let prev = fallback<number>(mLast.get(first), 0);
        if (size > prev)
          mLast.set(first, size);
      };
      layout.traverseAxis(a, {
        visitAllocation: (alloc) => {
          let size = allocSize(alloc);
          let msi = alloc.def.mergedSpans[a];
          offerSize(extremeOfMergedSpan(msi, Extreme.FIRST), extremeOfMergedSpan(msi, Extreme.LAST), size);
        },
        postSplit: (split) => {
          let children = split.def.children;
          // index -> cumulative size to that index (exclusive)
          let cumSize = 0;
          let cumSizes = [0];
          for (let iLast = 0; iLast < children.length; iLast++) {
            let last = children[iLast];
            let mLast = tempSizeMap.get(last);
            let lastSize = minSpanSize(last);
            if (mLast != null) {
              for (let [first, mergedSize] of mLast.entries()) {
                let iFirst = ejsonIndexOf(children, first);
                assert(iFirst >= 0 && iFirst <= iLast);
                lastSize = Math.max(lastSize, mergedSize - (cumSize - cumSizes[iFirst]));
              }
            }
            this.sizeMap.set(last, lastSize);
            cumSize += lastSize;
            cumSizes.push(cumSize);
          }
          offerSize(split.container[a], split.container[a], cumSizes[children.length]);
        }
      });
      let root = layout.getRoot(a);
      offerSize(root, root, minSpanSize(root));
      this.sizeMap.set(root, assertNotNull(assertNotNull(tempSizeMap.get(root)).get(root)));
      // Compute offsets.  (With a richer visitor interface, sizes and offsets
      // could probably be computed in a single pass.)
      this.offsetMap.set(root, 0);
      layout.traverseAxis(a, {
        preSplit: (split) => {
          let children = split.def.children;
          let offset = assertNotNull(this.offsetMap.get(split.container[a]));
          let childrenSize = 0;
          children.forEach((c, i) => {
            this.offsetMap.set(c, offset);
            if (i == children.length - 1) {
              // Add any extra space to the last child.
              this.sizeMap.set(c, assertNotNull(this.sizeMap.get(split.container[a])) - childrenSize);
            } else {
              let childSize = assertNotNull(this.sizeMap.get(c));
              offset += childSize;
              childrenSize += childSize;
            }
          });
        }
      });
    }

    getOffsetAndSize(mergedSpan: MergedSpanId<DataS, VisualB, A>): OffsetAndSize {
      let spans = this.layout.getSpansInMergedSpan(this.a, mergedSpan);
      return {
        offset: assertNotNull(this.offsetMap.get(spans[0])),
        size: arraySum(spans.map((s) => assertNotNull(this.sizeMap.get(s))))
      };
    }
  }

  export type AxisSizings<_> = {[A in AxisG<_>]: AxisSizing<_, A>};

  export function pickCoords<_>(
    layout: Layout<_, DataS, VisualB>, sizings: AxisSizings<_>,
    coords: IndependentPerAxis<PerExtreme<number>>):
    null | MergedRectId<_, DataS, VisualB> {

    let coordInSpan = <A extends Axis>(a: A, spanId: DataSpanId<A>, coord: number) => {
      let offset = assertNotNull(sizings[a].offsetMap.get(spanId));
      let size = assertNotNull(sizings[a].sizeMap.get(spanId));
      return (coord >= offset && coord <= offset + size);
    };

    if (!axesEvery((a) => extremes.every((e) => coordInSpan(a, layout.getRoot(a), coords[a][e])))) {
      return null;
    }
    let mergedRect: MergedRectId<_, DataS, VisualB> = perAxis((a) => mergedSpanOfSingleSpan(layout.getRoot(a)));
    let alreadyStraddled = independentPerAxis((a) => false);
    // Look for a split we can descend.
    while (axesSome((a) => {
      if (alreadyStraddled[a])
        return false;
      let oa = otherAxis(a);
      const split = layout.splitCoveringMergedScope(a, mergedRect[a][Extreme.FIRST], mergedRect[oa]);
      if (split == null)
        return false;
      let pickedMergedSpan = perExtreme((e) => assertNotNull(
        split.def.children.find((child) => coordInSpan(a, child, coords[a][e]))));
      mergedRect[a] = pickedMergedSpan;
      if (!EJSON.equals(pickedMergedSpan[Extreme.FIRST], pickedMergedSpan[Extreme.LAST])) {
        alreadyStraddled[a] = true;
      }
      return true;
    })) ;
    return mergedRect;
  }
}
