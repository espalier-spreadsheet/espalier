namespace Objsheets.Layout {

  export type LayoutTemplateInfo<_> = {
    templateReadyReactiveVar: ReactiveVar<boolean>;
    origLayoutTemplate: LayoutTemplate<_, OrigB>;
    designViewInfo: null | DesignViewInfo<_>;
    visualLayoutTemplate: LayoutTemplate<_, VisualB>;  // Design view, if enabled
  };
  // XXX Better name?
  export type DataLayoutInfo<_> = LayoutTemplateInfo<_> & BasicLayoutInfo<_> & {
    dataLayoutSpec: DataLayoutSpec<_>;  // More precise type than dataLayout.spec
  };
  export type SizedDataLayoutInfo<_> = DataLayoutInfo<_> & SizedLayoutInfo<_>;

  type LayoutUIComponentProps = {
    sheet: string;
    layoutName: ColumnId;
    designView: boolean;
    lockdown: boolean;
    viewObjectData: null | ViewObjectDump;
    viewObjectQueryParams: ViewObjectQueryParams;
  };
  type LayoutUIComponentPropsAndDataPreSizing<_> =
    LayoutUIComponentWithGIProps<_> & {layoutInfo: DataLayoutInfo<_>} &
      ViewObjectRequestProps & {serverViewObjectId: null | ViewObjectId};
  type LayoutUIComponentPropsAndData<_> =
    LayoutAutoSizingWrappedComponentProps<_, LayoutUIComponentPropsAndDataPreSizing<_>>;

  function mouseEventCoordsRelativeToCurrentTarget(mouseEvent: React.MouseEvent<Element>): IndependentPerAxis<number> {
    // Note: mouseEvent.offset{X,Y} are with respect to /original/ target.
    let targetOffset = fixmeAssertNotNull($(mouseEvent.currentTarget).offset());
    return {
      [Axis.ROW]: mouseEvent.pageY - targetOffset.top,
      [Axis.COL]: mouseEvent.pageX - targetOffset.left
    };
  }

  type MoveTargetInfo<_> = {
    selection: MergedRectId<_, DataS, VisualB>;
    selectionPixels: IndependentPerAxis<OffsetAndSize>;
  };
  type EditInfo<_> = {
    alloc: AllocationInfo<_, DataS, VisualB>,
    family: FamilyHandle,
    text: string,
  };

  type LayoutUIComponentState<_> = {
    // The action bar is authoritative on this.  We duplicate the state here to
    // trigger re-rendering.
    isActionBarExpanded: boolean,

    selectionInfo: null | SelectionInfo<_>,
    pendingMove: null | PendingMove<_>,
    moveTargetInfo: null | MoveTargetInfo<_>,
    savingMove: boolean,

    editInfo: null | EditInfo<_>,
    // Defined when in the middle of editing images
    imageFamInfo: null | FamilyHandle,

    allocTextOverridesForRendering: AllocTextOverride<_>[]
  };

  // Exported for commands.ts.
  export class LayoutUIComponentWithSizedData<_> extends
    React.PureComponent<LayoutUIComponentPropsAndData<_>, LayoutUIComponentState<_>> {
    private id = Random.id();
    private pendingEditOverrides = new JSONKeyedMap<MergedRectId<_, DataS, VisualB>, AllocTextOverride<_>>();
    private fileInput = React.createRef<HTMLInputElement>();
    constructor(props: LayoutUIComponentPropsAndData<_>) {
      super(props);
      this.state = {
        isActionBarExpanded: false,
        selectionInfo: null,
        pendingMove: null,
        moveTargetInfo: null,
        savingMove: false,
        editInfo: null,
        imageFamInfo: null,
        allocTextOverridesForRendering: this.makeAllocTextOverridesForRendering()
      };
    }

    private makeAllocTextOverridesForRendering() {
      return this.pendingEditOverrides.values();
    }
    private updateAllocTextOverridesForRendering() {
      this.setState({
        allocTextOverridesForRendering: this.makeAllocTextOverridesForRendering()
      });
    }
    private layoutContainer: null | HTMLDivElement = null;
    render() {
      let rootCSSGeometry = getCSSOffsetsAndSizes(this.props.layoutInfo.sizings,
        perAxis((a) => mergedSpanOfSingleSpan(this.props.layoutInfo.dataLayout.getRoot(a))));
      let actionBarData: LayoutActionBar.ActionBarData = {
        fullTextToShow: null,
        isLoading: false,
        origLayoutTemplate: this.props.giWrapper.wrap<F_OrigLayoutTemplate>(this.props.layoutInfo.origLayoutTemplate),
        layoutName: this.props.layoutName,
        goToLayout: (name) => {
          Router.go(this.sheetLayoutsUrlPrefix() + uiUrl(name, null, this.props.viewObjectQueryParams));
        },
        sheetDisplayMode: this.props.lockdown ? LayoutActionBar.SheetDisplayMode.LOCKDOWN :
          this.props.designView ? LayoutActionBar.SheetDisplayMode.DESIGN_VIEW :
          LayoutActionBar.SheetDisplayMode.NORMAL,
        switchToSheetDisplayMode: (mode) => {
          if (mode == LayoutActionBar.SheetDisplayMode.LOCKDOWN &&
            !confirm("To return to the developer interface, go back or remove '&lockdown=1' from the URL.")) {
            return false;
          }
          let queryParams: ViewObjectQueryParams = {
            ...(_.omit(this.props.viewObjectQueryParams, "designView", "lockdown")),
            ...(mode == LayoutActionBar.SheetDisplayMode.LOCKDOWN ? {lockdown: 1} :
              mode == LayoutActionBar.SheetDisplayMode.DESIGN_VIEW ? {designView: 1} : {})
          };
          Router.go(this.sheetLayoutsUrlPrefix() +
            uiUrl(this.props.layoutName, this.props.viewObjectData, queryParams));
          return true;
        },
        changeColumnArgs: this.state.selectionInfo ? this.state.selectionInfo.changeColumnArgs : [],
        notifyExpansionState: (isExpanded) => {
          this.setState({isActionBarExpanded: isExpanded});
        },
        withDataAccessContext: (cb) => this.withDataAccessContext(cb),
        viewObjectPending: this.props.viewObjectPending,
        moveArgs: this.state.pendingMove == null ? null : {
          what: this.state.pendingMove.what,
          explanation: this.state.pendingMove.explanation,
          canConfirm: this.state.moveTargetInfo != null,
          confirmCallback: this.confirmMove,
          cancelCallback: this.endMove,
          savingMove: this.state.savingMove
        },
      };
      let actionBarExpansionClass = this.props.lockdown ? "actionBarHidden" :
        this.state.isActionBarExpanded ? "actionBarExpanded" : "";
      // For now, we are relying on some of the rules in src/client/sheet.css.
      // If we need to fork them in the future, we will.
      return <>
        {
          this.props.lockdown ? <></>
          : <div id="ActionBar" className={actionBarExpansionClass}>
              <BlazeComponent template="layout_actionBar" {...actionBarData} />
            </div>
        }
        <div id="ViewWrapper" className={actionBarExpansionClass}>
          <input id="imageUploader" type="file" accept="image/*" style={{display: "none"}}
            ref={this.fileInput}
            onChange={(e) => this.onImageSubmit(e)}/>
          <div id="View" style={{
            // Selection and edit controls are positioned relative to this div.
            position: "relative",
            overflow: "auto"
          }}>
            <div
              ref={(div) => this.layoutContainer = div}
              id={this.id}
              className="layout-container"
              style={{
                height: rootCSSGeometry.height,
                width: rootCSSGeometry.width
              }}
              // Allow the layout container to receive keyboard focus.
              tabIndex={0}
              onMouseDown={(e) => this.onMouseDown(e)}
              onContextMenu={(e) => this.onContextMenu(e)}
              onMouseMove={(e) => this.onMouseMove(e)}
              onKeyDown={(e) => this.onKeyDown(e)}
              onDoubleClick={(e) => this.onDoubleClick(e)}>
              <LayoutComponent layoutInfo={this.props.layoutInfo}
                allocTextOverrides={this.state.allocTextOverridesForRendering}
                buttonCallback={this.onUserButtonClick} />
            </div>
            {
              // Selection frame has to be last to appear on top.
              this.state.editInfo != null ? this.renderEditControls() :
              this.state.selectionInfo != null ? this.renderSelection() :
              <></>
            }
          </div>
        </div>
      </>;
    }
    private renderSelection() {
      let selectionInfo = assertNotNull(this.state.selectionInfo);
      // Selection frame goes on top of add/delete frame.
      return <>
        { selectionInfo.addDeleteTargetPixels != null && this.state.pendingMove == null
          ? <div className="add-delete-frame"
              style={{
                pointerEvents: "none",
                position: "absolute",
                ...offsetsAndSizesToCSS(selectionInfo.addDeleteTargetPixels)
              }} />
          : <></> }
        <div className={
          this.state.pendingMove != null ? "selection-frame-inactive" : "selection-frame" }
          style={{
            pointerEvents: "none",
            position: "absolute",
            ...offsetsAndSizesToCSS(selectionInfo.selectionPixels)
          }} />
        { this.state.moveTargetInfo != null
          ? <div className="move-target-frame"
              style={{
                pointerEvents: "none",
                position: "absolute",
                ...offsetsAndSizesToCSS(this.state.moveTargetInfo.selectionPixels)
              }} />
          : <></> }
        </>;
    }
    private sheetLayoutsUrlPrefix() {
      return "/" + encodeURIComponent(this.props.sheet) + "/layouts/";
    }
    private onUserButtonClick = (button: Button) => {
      MeteorMethods.executeCannedTransactionWithRedirect.call($$.id,
        button.procedureName, {this: [button.thisObj.cellId]},
        this.props.clientViewObjectId,
        standardServerCallbackThen((error, url) => {
          if (error == null && url != null) {
            url = sanitizeURL(url);
            if (Iron.Url.isSameOrigin(url, location.href)) {
              Router.go(url);
            } else {
              location.href = url;
            }
          }
        }));
    }
    // Must be non-null if dragSelecting is true.
    private selectionAnchorCoords: null | IndependentPerAxis<number> = null;
    private dragSelecting = false;
    private updateDragSelection(mouseCoords: IndependentPerAxis<number>) {
      let selectionAnchorCoords = assertNotNull(this.selectionAnchorCoords);
      let coords = independentPerAxis((a) => ({
        [Extreme.FIRST]: Math.min(selectionAnchorCoords[a], mouseCoords[a]),
        [Extreme.LAST]: Math.max(selectionAnchorCoords[a], mouseCoords[a]),
      }));
      let layoutInfo = this.props.layoutInfo;
      const selection = pickCoords(layoutInfo.dataLayout, layoutInfo.sizings, coords);
      if (selection == null)
        // Out of bounds.  Never seems to happen anyway: because the layout container doesn't get a mouse event?
        return;
      if (this.state.pendingMove != null) {
        let selectionPixels = independentPerAxis((a) => layoutInfo.sizings[a].getOffsetAndSize(selection[a]));
        this.setState({moveTargetInfo: {selection, selectionPixels}});
      } else {
        this.setSelection(selection);
      }
    }
    // Public for commands.ts.
    setSelection(selection: null | MergedRectId<_, DataS, VisualB>, newLayoutInfo?: SizedDataLayoutInfo<_>) {
      this.setState({selectionInfo: selection == null ? null :
        resolveSelection(selection, newLayoutInfo || this.props.layoutInfo)});
    }
    private stopDragSelecting() {
      this.dragSelecting = false;
    }

    private onImageSubmit(changeEvent: React.ChangeEvent<HTMLInputElement>) {
      this.withDataAccessContext(() => {
        let fam = fixmeAssertNotNull(this.state.imageFamInfo);
        let files = changeEvent.target.files;
        let fileToUpload = files ? files[0] : null;
        if (fileToUpload) {
          // read file as dataurl and save
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.withDataAccessContext(() => {
              let dataUrl = reader.result;
              if (typeof dataUrl === "string") {
                let oldValues = assertNotNull(fam.values());  // Checked in editability, modulo a potential race
                assert(oldValues.length <= 1);  // Template validation
                let oldValue = undefinedToNull(arrayGet(oldValues, 0));
                this.updateFamily(fam, oldValue, dataUrl, () => {});
              } else {
                throw new Error("Updating: dataUrl from uploaded image is not string");
              }
            });
          }, false);
          reader.readAsDataURL(fileToUpload);
        }
      });
      this.setState({imageFamInfo: null});
    }
    private onMouseDown(mouseEvent: React.MouseEvent<HTMLDivElement>) {
      // Do not swallow a mousedown intended for a button.
      // XXX: Do other event handlers need the same logic?
      if (mouseEvent.currentTarget != mouseEvent.target) return;
      if (mouseEvent.button == MOUSE_BUTTON_LEFT) {
        mouseEvent.preventDefault();
        // But do make sure the layout container is focused.
        fixmeAssertNotNull(this.layoutContainer).focus();
        this.dragSelecting = true;
        let mouseCoords = mouseEventCoordsRelativeToCurrentTarget(mouseEvent);
        if (this.selectionAnchorCoords == null || !mouseEvent.getModifierState("Shift")) {
          this.selectionAnchorCoords = mouseCoords;
        }
        this.updateDragSelection(mouseCoords);
      } else if (mouseEvent.button == MOUSE_BUTTON_RIGHT && this.state.pendingMove == null) {
        // If the current selection does not already contain the location of the
        // mouse click, then select that location before opening the context
        // menu.
        //
        // We are relying on this event hander running _and the state change
        // completing_ before the context menu handler runs.  I understand the
        // order of the handlers: I presume they run in the order they were
        // attached, and this handler is attached during render and the context
        // menu handler is attached later during componentDidMount.  I don't
        // know if it's guaranteed that the state change completes before the
        // next event handler is called, but it seems to work.
        // https://yiochen.github.io/blog/post/setstate-might-be-synchronous/
        // may be related. ~ Matt 2018-01-15
        let coords = mouseEventCoordsRelativeToCurrentTarget(mouseEvent);
        const selectionInfo = this.state.selectionInfo;
        if (!(selectionInfo != null &&
          axesEvery((a) => {
            let offsetAndSize = selectionInfo.selectionPixels[a];
            // XXX: Factor out with coordInSpan in sizing.ts?
            return coords[a] >= offsetAndSize.offset && coords[a] <= offsetAndSize.offset + offsetAndSize.size;
          }))) {
          this.selectionAnchorCoords = coords;
          this.updateDragSelection(coords);
        }
      }
    }
    private onContextMenu(contextMenuEvent: React.MouseEvent<HTMLDivElement>) {
      if (contextMenuEvent.target instanceof HTMLAnchorElement) {
        // Bypass the `preventDefault` in the jQuery-contextMenu listener to
        // allow the native context menu to show so the user can "open in new
        // tab" or the like.  This is the right call because React and jQuery
        // are each emulating their own event model using a single native
        // listener on the document.
        contextMenuEvent.nativeEvent.stopImmediatePropagation();
      }
    }
    private onMouseMove(mouseEvent: React.MouseEvent<HTMLDivElement>) {
      if (this.dragSelecting) {
        if (mouseEvent.buttons & (1 << MOUSE_BUTTON_LEFT)) {
          this.updateDragSelection(mouseEventCoordsRelativeToCurrentTarget(mouseEvent));
        } else {
          // Can happen if the mouseup was lost because of a window.alert box.
          // Best effort; right now the fact that selection is "still in
          // progress" until we get a mousemove event is unobservable to the
          // user, but that might not be the case in the future.
          this.stopDragSelecting();
        }
      }
    }
    private onKeyDown(event: React.KeyboardEvent<HTMLDivElement>) {
      // None of the existing keybindings apply with a pending move.  Of course,
      // if we add ones that do, we'll change this.
      if (this.state.pendingMove != null) return;
      let charTyped = charTypedOfKeyEvent(event);
      if (charTyped != null) {
        event.preventDefault();
        this.tryStartEdit(charTyped);
      } else {
        switch (modifiedKeyOfKeyEvent(event)) {
          case "Enter":
            event.preventDefault();
            this.tryStartEdit();
            break;
          case "Backspace":
          case "Delete":
            event.preventDefault();
            this.clearCell();
            break;
          case "Control-z":
            if (!this.props.lockdown) {
              event.preventDefault();
              MeteorMethods.undo.call($$.id, $$client.wrapCallback(standardServerCallback));
            }
            break;
          case "Control-y":
            if (!this.props.lockdown) {
              event.preventDefault();
              MeteorMethods.redo.call($$.id, $$client.wrapCallback(standardServerCallback));
            }
            break;
        }
      }
    }

    private prepareForEditOrClear(isEdit: boolean):
      null | {alloc: AllocationInfo<_, DataS, VisualB>, fam: FamilyHandle} {
      let selectionInfo = this.state.selectionInfo;
      if (selectionInfo == null) return null;
      let layoutInfo = this.props.layoutInfo;
      let alloc = layoutInfo.dataLayout.coveringAllocationOfMergedRect(selectionInfo.selection);
      if (alloc == null) return null;
      // At this point, we know the selection is within a single allocation, so
      // selectCoords will have expanded it to the entire allocation.
      let ab = layoutInfo.dataLayoutSpec.getAllocBinding(alloc);
      // Under the current design, LabelAllocationDefs are never user-editable.
      if (ab.templateAlloc.kind != TemplateAllocationDefKind.FIELD)
        return null;
      if (ab.qCellId == null)  // narrowing
        throw new Error("Should not have FieldAllocationDef in unbound area");
      if (!qCellIdIsReal(ab.qCellId)) {
        alert("Automatic creation of parent objects is not yet supported.");
        return null;
      }
      let parentObj = new CellHandle(ab.qCellId);
      if (!parentObj.existsIgnoreErrors()) {
        alert("Automatic creation of parent objects is not yet supported.");
        return null;
      }
      let fam = layoutInfo.dataLayoutSpec.editableFamilyForField(parentObj, ab.templateAlloc.field);
      if (fam == null) return null;
      let col = getExistingColumn(ab.templateAlloc.field);
      if (this.props.serverViewObjectId != null && col.fieldName == null) {
        if (!this.props.lockdown) {
          alert("Data cannot be added to an unnamed state field in a view object.  " +
            "Either name the field or, if you are trying to create a label " +
            "that appears on all view objects of the type, use a constant formula.");
        }
        return null;
      }
      return {alloc, fam};
    }
    // Like this.state.editing but updated synchronously.  Used to prevent the
    // blur triggered by endEdit from causing a saveEdit before the setState
    // goes through.
    private editingSync = false;
    private tryStartEdit(initialText?: string) {
      this.withDataAccessContext(() => {
        let obj = this.prepareForEditOrClear(true);
        if (obj == null) return;
        let {alloc, fam} = obj;
        this.editingSync = true;
        if ((typeof alloc.def.content != "string") && alloc.def.content.type == "image") {
          // Want to trigger click only if user double clicked to upload
          if (initialText == null) {
            fixmeAssertNotNull(this.fileInput.current).click();
            this.setState({imageFamInfo: fam});
          }
        } else {
          if (initialText == null) {
            let override = this.pendingEditOverrides.get(alloc.def.mergedSpans);
            if (typeof alloc.def.content != "string")  // narrowing
              throw new Error("Assertion: Only allocations with string content should get here.");
            initialText = (override != null) ? override.text :
              (alloc.def.style && alloc.def.style.specialTextColor != null) ? "" : alloc.def.content;
          }
          this.setState({editInfo: {alloc, family: fam, text: initialText}});
        }
      });
    }
    public withDataAccessContext<R>(cb: () => R): R {
      let viewObjectContext = this.props.serverViewObjectId == null ? null : {
        type: this.props.layoutInfo.origLayoutTemplate.spec._id as NonRootColumnId,
        id: this.props.serverViewObjectId,
        queryParams: this.props.viewObjectQueryParams
      };
      return currentViewObject.withValue(viewObjectContext,
        () => autoCleanup.withValue(this.props.lockdown, cb));
    }
    private updateFamily(fam: FamilyHandle, oldValue: null | OSValue, newValue: null | OSValue,
      cleanupOverrideCB: MeteorCallback<void>) {
      let col = getExistingColumn(fam.columnId);
      if (col.viewObjectType != null && col.formula == null) {
        // This code will go away once we have support for propagating view
        // object edits back from the server. ~ Matt 2018-05-09
        let newViewObjectData = fallback(EJSON.clone(this.props.viewObjectData), {});
        let findObject = (ch: CellHandle): ViewObjectDump => {
          if (ch.columnId == col.viewObjectType) return newViewObjectData;
          // XXX assertion?
          return (findObject(assertNotNull(ch.parent()))[ch.columnId] as {[stringifiedKey: string]: ViewObjectDump})
            [JSON.stringify(ch.value())];
        };
        findObject(fam.parent())[fam.columnId] = newValue == null ? [] : [newValue];
        // XXX: Garbage collect empty objects?
        let path = this.sheetLayoutsUrlPrefix() +
          uiUrl(this.props.layoutName, newViewObjectData, this.props.viewObjectQueryParams);
        Router.go(path);
        // Wait until LayoutUIComponentWithTemplate has received the new dump
        // and set the viewObjectPending flag.
        Tracker.afterFlush(() => {
          if (this.props.viewObjectPending) {
            // XXX The dummy arguments are tiresome.  Can we generalize wrapCallbackForAfterLayoutUpdate?
            this.viewObjectUpdateCallbacks.push(() => cleanupOverrideCB(null, undefined));
          } else {
            cleanupOverrideCB(null, undefined);
          }
        });
      } else {
        let callback = $$client.wrapCallback(standardServerCallbackThen(cleanupOverrideCB));
        if (oldValue == null && newValue != null) {
          fam.cell(newValue).add_Client(callback);
        } else if (oldValue != null && newValue != null) {
          fam.cell(oldValue).setValue_Client(newValue, callback);
        } else if (oldValue != null && newValue == null) {
          fam.cell(oldValue).remove_Client(callback);
        } else {
          throw new Error("not reached");
        }
      }
    }
    private saveEdit() {
      if (!this.editingSync) return;
      // Needed at least by fam.values() .
      this.withDataAccessContext(() => {
        let editInfo = fixmeAssertNotNull(this.state.editInfo);
        let fam = editInfo.family;
        let oldValues = assertNotNull(fam.values());  // Checked in editability, modulo a potential race
        assert(oldValues.length <= 1);  // Template validation
        let oldValue = undefinedToNull(arrayGet(oldValues, 0));
        let type = fam.type();
        let newValue;
        if (editInfo.text != "" || type == SpecialType.TEXT || typeIsReference(type)) {
          try {
            newValue = StateEdit.parseValueUi(fam.type(), editInfo.text);
          } catch (e) {
            // XXX Define the type of exception that parseValueUi throws.
            this.endEdit();
            return;
          }
        } else {
          // Entering the empty string into a field is an alternative way to make
          // it null, except for a field of text or reference type, where we need
          // to provide a way to actually set it to an empty string.  (Currently,
          // references can have empty display strings.  That's unfortunate, but
          // trying to ban it may lead to corner-case bugs in applications that
          // could be triggered by untrusted users, which I think is worse.)
          //
          // ~ Matt 2018-03-02
          newValue = null;
        }
        if (!EJSON.equals(oldValue, newValue)) {
          let editAlloc = editInfo.alloc;
          let editOverride = {mergedRect: editAlloc.def.mergedSpans, text: editInfo.text, handle: new Object()};
          let cleanupOverrideCB = this.addPendingEdit(editOverride);
          this.updateFamily(fam, oldValue, newValue, cleanupOverrideCB);
        }
      });
      this.endEdit();
    }
    private endEdit() {
      this.editingSync = false;
      this.setState({editInfo: null});
      fixmeAssertNotNull(this.layoutContainer).focus();
    }
    private addPendingEdit(editOverride: AllocTextOverride<_>): MeteorCallback<void> {
      this.pendingEditOverrides.set(editOverride.mergedRect, editOverride);
      this.updateAllocTextOverridesForRendering();
      let cleanupOverrideCB = this.wrapCallbackForAfterLayoutUpdate<void>((error) => {
        let currentOverride = this.pendingEditOverrides.get(editOverride.mergedRect);
        if (currentOverride != null && currentOverride.handle == editOverride.handle) {
          this.pendingEditOverrides.delete(editOverride.mergedRect);
          this.updateAllocTextOverridesForRendering();
        }
      });
      return cleanupOverrideCB;
    }
    private clearCell() {
      this.withDataAccessContext(() => {
        let obj = this.prepareForEditOrClear(false);
        if (obj == null) return;
        let {alloc, fam} = obj;
        let oldValues = assertNotNull(fam.values());  // Checked in editability
        assert(oldValues.length <= 1);  // Template validation
        let oldValue = arrayGet(oldValues, 0);
        if (oldValue != null) {
          let editOverride = {mergedRect: alloc.def.mergedSpans, text: "", handle: new Object()};
          let cleanupOverrideCB = this.addPendingEdit(editOverride);
          this.updateFamily(fam, oldValue, null, cleanupOverrideCB);
        }
      });
    }
    private onDoubleClick(mouseEvent: React.MouseEvent<HTMLDivElement>) {
      if (this.state.pendingMove != null) return;
      if (mouseEvent.button == MOUSE_BUTTON_LEFT) {
        mouseEvent.preventDefault();
        // Assume mousedown handler has already set the selection.
        // XXX: Is this always true, or do we want to reselect based on the location of the event?
        this.tryStartEdit();
      }
    }
    // Want to move the edit controls to a separate React component?  I don't
    // think it's a big enough win yet. ~ Matt 2018-01-10
    private editInput: null | HTMLInputElement = null;
    private renderEditControls() {
      let selectionInfo = assertNotNull(this.state.selectionInfo),
        editInfo = assertNotNull(this.state.editInfo);
      // No support for orientation: we always edit text in normal orientation.
      //
      // Since a text input field cannot be automatically sized based on its
      // content, we maintain an automatically sized div with the same text and
      // then set the input field to be 100% of the size of the div.
      // Alternatively, we could determine the size for the current text and set
      // that size on the input field directly, but that would require multiple
      // passes of rendering, like the withLayoutAutoSizing wrapper.

      let cellCSSGeometry = offsetsAndSizesToCSS(selectionInfo.selectionPixels);
      let allocStyle = editInfo.alloc.def.style;
      let allocStyleCSS = allocationStyleToCSS({...allocStyle, specialTextColor: null});
      return <div className="cell-edit-backing-div layout-rect-common layout-rect-real" style={{
        position: "absolute",
        top: cellCSSGeometry.top,
        left: cellCSSGeometry.left,
        height: cellCSSGeometry.height,
        // minWidth instead of width so the input field can grow!
        minWidth: cellCSSGeometry.width
        // Don't put allocStyleCSS here: it will affect the field name label!
      }}>
        <span style={allocStyleCSS}>{editInfo.text}</span>
        <div style={Object.assign({
          visibility: "visible",
          position: "absolute",
          top: allocStyle.fieldNameLabelInfo != null ? FIELD_NAME_LABEL_HEIGHT : 0,
          bottom: 0,
          left: 0,
          width: "100%"
        },
        // We need to set the background here because the input doesn't cover
        // the entire height of this div.
        allocStyleCSS)}>
          <input
            ref={(input) => this.editInput = input}
            className="cell-edit-input layout-rect-common layout-rect-real"
            // <input> seems to default to the default font rather than its
            // parent's font, so we either need this or to set a bunch of
            // properties to `inherit`.
            style={allocStyleCSS}
            type="text"
            autoFocus
            onBlur={(e) => this.saveEdit()}
            value={editInfo.text}
            // A "controlled component".
            // React's onChange is DOM onInput: https://github.com/facebook/react/issues/3964
            //
            // XXX: Is it possible in our scenario for the state to change between the call to
            // setState and when the state change is applied? ~ Matt 2018-08-02
            onChange={(e) => this.setState({editInfo: {...assertNotNull(this.state.editInfo), text: e.target.value}})}
            // onKeyPress does not work:
            // tslint:disable-next-line:max-line-length
            // https://stackoverflow.com/questions/37440408/how-to-detect-esc-key-press-in-react-and-how-to-handle-it#comment77751556_41797287
            onKeyDown={(e) => this.onEditFieldKeyDown(e)}
          />
        </div>
        {allocStyle.fieldNameLabelInfo != null
          ? <FieldNameLabelComponent
              fieldNameLabelInfo={allocStyle.fieldNameLabelInfo}
              objectExists={true}
              extraStyle={{
                visibility: "visible",
                position: "absolute",
                top: 0,
                height: FIELD_NAME_LABEL_HEIGHT + BORDER_WIDTH_PX,
                left: 0,
                width: "100%"
              }}/>
          : <></>}
        { /* The selection frame must be on top of the field name label and the input field. */ }
        <div className="selection-frame" style={{
          visibility: "visible",
          position: "absolute",
          pointerEvents: "none",
          top: 0,
          height: "100%",
          left: 0,
          width: "100%"
        }}/>
      </div>;
    }
    private onEditFieldKeyDown(event: React.KeyboardEvent<HTMLInputElement>) {
      switch (modifiedKeyOfKeyEvent(event)) {
        case "Enter":
          event.preventDefault();
          this.saveEdit();
          break;
        case "Escape":
          event.preventDefault();
          this.endEdit();
          break;
      }
    }

    private confirmMove = () => {
      attemptMove(this);
    }
    endMove = () => {
      this.setState({pendingMove: null, moveTargetInfo: null});
    }

    // Wrap the callback for a server call to run after our props have been
    // updated to reflect the new layout.  That is guaranteed to have happened
    // by the time the original callback is called (so we know the client
    // collections have the new data), the autorun has called forceUpdate on the
    // ReactMeteorData component to pass the new layout info to the
    // LayoutAutoSizingWrapperComponent, and auto sizing is completed.
    //
    // Letting this component render before calling the original callback (which
    // typically changes the state of this component) wastes a render, but it's
    // a minor cost and avoiding it would be complicated.
    //
    // ~ Matt 2018-03-02
    //
    // Public for commands.ts.
    wrapCallbackForAfterLayoutUpdate<R>(callback: MeteorCallback<R>):
      MeteorCallback<R> {
      return (error, result) =>
        Tracker.afterFlush(() =>
          this.props.runAfterSizingUpdate(() =>
            callback(error, result)));
    }
    private viewObjectUpdateCallbacks: (() => void)[] = [];
    componentDidMount() {
      // Make sure we reset the selection state when the mouse button goes up,
      // even if the mouse moved out of the layout container.  Ideally we'd have
      // capture-like behavior for mousemove too, but it looks nontrivial to
      // implement and I don't want to bother now. ~ Matt 2017-12-13
      // https://github.com/meteor/blaze/issues/152#issuecomment-260565124
      $(window).on(`mouseup.${this.id}`, () => {
        this.stopDragSelecting();
      });
      // Too bad we have to specify a selector and not just do
      // $(this.layoutContainer).contextMenu(...) .
      ($ as fixmeAny).contextMenu({
        selector: "#" + this.id,
        hideOnSecondTrigger: true,
        // Matt's system is slow enough that the default animation is just
        // annoying.  Disable the animation.
        animation: {duration: 0, show: "show", hide: "hide"},
        events: {
          show: () => {
            // Otherwise keys pressed to navigate the context menu get passed to
            // the layout container too.
            fixmeAssertNotNull(this.layoutContainer).blur();
            return true;
          },
          hide: () => {
            // Automatically refocus the layout container when we're done with
            // the menu.  Do it synchronously (unlike the contextmenu:hidden
            // event) to prevent a potential race where the user starts editing
            // and then our attempt to focus the layout container causes a
            // saveEdit. Synchronous _after_ the menu is hidden would make more
            // sense, but it shouldn't matter.
            fixmeAssertNotNull(this.layoutContainer).focus();
            return true;
          }
        },
        build: () => this.withDataAccessContext(() => buildContextMenu(this))
      });
      fixmeAssertNotNull(this.layoutContainer).focus();
    }
    componentWillUnmount() {
      $(window).off(`mouseup.${this.id}`);
    }
    componentWillReceiveProps(nextProps: LayoutUIComponentPropsAndData<_>) {
      if (!nextProps.viewObjectPending) {
        for (let cb of this.viewObjectUpdateCallbacks)
          cb();
        this.viewObjectUpdateCallbacks = [];
      }
      let newLayoutInfo = nextProps.layoutInfo;
      if (newLayoutInfo.visualLayoutTemplate != this.props.layoutInfo.visualLayoutTemplate) {
        // Hide any pending edits when the layout template changes.  This seems
        // a lesser evil than potentially really weird glitches.
        this.pendingEditOverrides = new JSONKeyedMap();
        this.updateAllocTextOverridesForRendering();
        this.endMove();
      }
      if (newLayoutInfo != this.props.layoutInfo) {
        // Make sure that all DataSpanIds stored by this class are either
        // cleared or re-interned!
        this.dragSelecting = false;
        this.selectionAnchorCoords = null;
        if (this.editingSync) {
          this.endEdit();
        }
        let oldSelectionInfo = this.state.selectionInfo;
        if (oldSelectionInfo != null) {
          let newSelection: null | MergedRectId<_, DataS, VisualB> =
            newLayoutInfo.dataLayoutSpec.internMergedRect(oldSelectionInfo.selection);
          if (!newLayoutInfo.dataLayout.isMergedRectValid(newSelection)) {
            newSelection = null;
          }
          this.setSelection(newSelection, newLayoutInfo);
        }
        for (let v of this.pendingEditOverrides.values()) {
          this.pendingEditOverrides.set(v.mergedRect,
            {mergedRect: newLayoutInfo.dataLayoutSpec.internMergedRect(v.mergedRect), text: v.text, handle: v.handle});
        }
        this.updateAllocTextOverridesForRendering();
        // I can't be bothered to try to preserve a move target. ~ Matt 2018-07-05
        this.setState({moveTargetInfo: null});
      }
    }
    componentDidUpdate(prevProps: LayoutUIComponentPropsAndData<_>, prevState: LayoutUIComponentState<_>) {
      if (prevState.editInfo == null && this.state.editInfo != null) {
        // When starting an edit, move the cursor to the end of the input field.
        let editInput = fixmeAssertNotNull(this.editInput);
        editInput.selectionStart = editInput.selectionEnd = editInput.value.length;
      }
    }
  }

  class LayoutUIComponentWithData<_> extends
    LayoutAutoSizingWrapperComponent<_, LayoutUIComponentPropsAndDataPreSizing<_>> {
    innerRender(innerProps: LayoutAutoSizingWrappedComponentProps<_,
      LayoutUIComponentPropsAndDataPreSizing<_>>) {
      return <LayoutUIComponentWithSizedData {...innerProps}/>;
    }
  }
  class LayoutUIComponentWithViewObjectRequest<_> extends SelectiveReactMeteorDataPureComponent<
    LayoutUIComponentWithTemplate_Props<_> & ViewObjectRequestProps,
    {innerData?: {layoutInfo: DataLayoutInfo<_>, serverViewObjectId: null | ViewObjectId}}, {}> {
    shouldGetMeteorData(
      nextProps: LayoutUIComponentWithTemplate_Props<_> & ViewObjectRequestProps, nextState: {}) {
      // As an optimization, avoid triggering directly on a change to
      // viewObjectData.  The change will cause a new view object request, which
      // will ultimately invalidate Tracker data that this component reads.
      return nextProps.clientViewObjectId != this.props.clientViewObjectId
        || nextProps.layoutInfo != this.props.layoutInfo
        || nextProps.designView != this.props.designView
        || nextProps.lockdown != this.props.lockdown;
    }
    getMeteorDataReal() {
      // If the template is out of date with the sheet collections, then don't
      // try to generate a data layout based on that template and the data in
      // the sheet collections: it's wasted work and will likely even crash.
      // Instead, hold over the previous data; the reactive var will ensure that
      // we run again after the template is updated.  If the template is out of
      // date the first time `getMeteorDataReal` is called, then `innerData` may
      // be null, so check for that in `render`.
      if (!this.props.layoutInfo.templateReadyReactiveVar.get()) {
        return {innerData: this.getData().innerData};
      }
      let serverViewObjectId: null | ViewObjectId;
      if (this.props.clientViewObjectId != null) {
        // Register a Tracker dependency that will (hopefully) cover changes to
        // Meteor.connection._lastSessionId.
        Meteor.status();
        serverViewObjectId = (Meteor.connection as fixmeAny)._lastSessionId + ":" + this.props.clientViewObjectId;
      } else {
        serverViewObjectId = null;
      }
      let layoutTemplateInfo = this.props.layoutInfo;
      let dataLayoutSpec = new DataLayoutSpec(layoutTemplateInfo.visualLayoutTemplate,
        serverViewObjectId, /*showEntireTemplate*/ this.props.designView, /*minimalStyling*/ this.props.lockdown);
      let dataLayout = new Layout(dataLayoutSpec, /*validateLayout*/ false);
      return {innerData: {layoutInfo: {...layoutTemplateInfo, dataLayoutSpec, dataLayout}, serverViewObjectId}};
    }
    render() {
      let data = this.getData();
      return data.innerData == null ? <></> : <LayoutUIComponentWithData {...this.props} {...data.innerData}/>;
    }
  }

  type ViewObjectRequestProps = {
    clientViewObjectId: null | string;
    // Means we have an active view object request of the correct type.
    viewObjectInitialized: boolean;
    viewObjectPending: boolean;
  };

  type LayoutUIComponentWithTemplate_Props<_> = LayoutUIComponentWithGIProps<_> & {layoutInfo: LayoutTemplateInfo<_>};

  class LayoutUIComponentWithTemplate<_> extends
    React.PureComponent<LayoutUIComponentWithTemplate_Props<_>, ViewObjectRequestProps> {
    private clientViewObjectId: string;
    private currentSubscription: null | Meteor.SubscriptionHandle = null;
    private requestViewObject(nextProps?: LayoutUIComponentWithTemplate_Props<_>) {
      let props = nextProps || this.props;
      this.setState({viewObjectPending: true});
      let oldSubscription = this.currentSubscription;
      let req: ViewObjectRequest = {
        type: props.layoutInfo.origLayoutTemplate.spec._id as NonRootColumnId,
        stateDump: fallback(props.viewObjectData, {}),
        queryParams: props.viewObjectQueryParams
      };
      let newSubscription = Tablespace.viewObjectPub.subscribe(
        $$.id, this.clientViewObjectId, req, {
        onReady: () => {
          if (req.type == this.props.layoutInfo.origLayoutTemplate.spec._id) {
            this.setState({viewObjectInitialized: true});
          }
          if (this.currentSubscription == newSubscription) {
            this.setState({viewObjectPending: false});
          }
        },
        onStop: (error) => {
          if (error) {
            // XXX Is this reasonable?  The request will just stay pending
            // unless/until a later request succeeds.
            ClientAppHelpers.errorCallback(error);
          }
        }
      });
      this.currentSubscription = newSubscription;
      if (oldSubscription != null) {
        oldSubscription.stop();
      }
    }
    constructor(props: LayoutUIComponentWithTemplate_Props<_>) {
      super(props);
      this.clientViewObjectId = Random.id();
      if (this.props.layoutInfo.origLayoutTemplate.spec._id != rootColumnId) {
        this.state = {
          clientViewObjectId: this.clientViewObjectId,
          viewObjectInitialized: false,
          viewObjectPending: true
        };
      } else {
        assert(this.props.viewObjectData == null);
        this.state = {
          clientViewObjectId: null,
          viewObjectInitialized: true,
          viewObjectPending: false
        };
      }
    }
    componentDidMount() {
      if (this.props.layoutInfo.origLayoutTemplate.spec._id != rootColumnId) {
        // Latest React advice is to make requests from componentDidMount.
        //
        // TODO: Consider migrating to
        // https://github.com/facebook/react/tree/master/packages/create-subscription
        // or similar for compatibility with React asynchronous rendering, but
        // ReactMeteorData is probably going to be the bottleneck anyway.
        this.requestViewObject();
      }
    }
    componentWillReceiveProps(nextProps: LayoutUIComponentWithTemplate_Props<_>) {
      let oldRoot = this.props.layoutInfo.origLayoutTemplate.spec._id,
        newRoot = nextProps.layoutInfo.origLayoutTemplate.spec._id;
      if (newRoot == rootColumnId) {
        if (this.currentSubscription != null) {
          this.currentSubscription.stop();
          this.currentSubscription = null;
        }
        assert(nextProps.viewObjectData == null);
        this.setState({
          clientViewObjectId: null,
          viewObjectInitialized: true,
          viewObjectPending: false
        });
      } else {
        this.setState({clientViewObjectId: this.clientViewObjectId});
        if (newRoot != oldRoot) {
          this.setState({viewObjectInitialized: false});
        }
        if (newRoot != oldRoot ||
          nextProps.viewObjectData != this.props.viewObjectData ||
          nextProps.viewObjectQueryParams != this.props.viewObjectQueryParams) {
          this.requestViewObject(nextProps);
        }
      }
    }
    componentWillUnmount() {
      if (this.currentSubscription != null) {
        this.currentSubscription.stop();
        this.currentSubscription = null;
      }
    }
    render() {
      return this.state.viewObjectInitialized
        ? <LayoutUIComponentWithViewObjectRequest {...this.props} {...this.state}/>
        : <div>Generating page...</div>;
    }
  }

  // So that downstream components (LayoutUIComponentWithTemplate, etc.) don't
  // have to check for layoutInfo being null.
  function ConditionalLayoutUIComponent<_>(
    props: LayoutUIComponentWithGIProps<_> & {layoutInfo: null | LayoutTemplateInfo<_>}) {
    if (props.layoutInfo != null) {
      if (props.lockdown && props.designView) {
        return <div>Lockdown does not make sense with design view.</div>;
      } else if (props.lockdown && props.layoutInfo.origLayoutTemplate.spec._id == rootColumnId) {
        return <div>Lockdown only makes sense with view objects.</div>;
      } else {
        return <LayoutUIComponentWithTemplate {...props}
          // https://github.com/Microsoft/TypeScript/issues/25930
          layoutInfo={props.layoutInfo}/>;
      }
    } else if (!$$.layoutsAreHardcoded() && props.layoutName == rootColumnId) {
      return <div>Initializing layout...</div>;
    } else {
      return <div>The requested layout does not exist on sheet "{props.sheet}".{" "}
        <a href={uiUrl(rootColumnId, null, props.viewObjectQueryParams)}>Return to the master data set</a>.</div>;
    }
  }

  type LayoutUIComponentWithGIProps<_> = LayoutUIComponentProps & {giWrapper: GIWrapper<_>};

  // XXX Now that we had to make this a class (because withTracker proved
  // inadequate), maybe ConditionalLayoutUIComponent could be inlined into it.
  class LayoutUIComponentWithGI<_> extends SelectiveReactMeteorDataPureComponent<LayoutUIComponentWithGIProps<_>,
    {layoutInfo: null | LayoutTemplateInfo<_>}, {}> {
    templateReadyReactiveVar = new ReactiveVar(false);
    shouldGetMeteorData(nextProps: LayoutUIComponentProps, nextState: {}) {
      // In particular, we do NOT want to trigger on changes to viewObjectDump,
      // because this will lose an edit override.
      return nextProps.layoutName != this.props.layoutName
        || nextProps.designView != this.props.designView
        || nextProps.lockdown != this.props.lockdown;
    }
    getMeteorDataReal() {
      let origLayoutTemplateSpec = getLayoutsCollection(this.props.giWrapper).findOne(this.props.layoutName);
      let layoutInfo: null | LayoutTemplateInfo<_>;
      if (origLayoutTemplateSpec == null) {
        layoutInfo = null;
        if (!$$.layoutsAreHardcoded() && this.props.layoutName == rootColumnId) {
          // XXX: The user can "undo" this and we'll just do it again, which is
          // silly.  Long term, we should make sure that gets fixed.
          MeteorMethods.layoutInit.call($$.id, standardServerCallback);
        }
      } else {
        // Put this in a separate withTracker from the instantiation so it doesn't
        // re-run if the data changes and the original layout template doesn't.
        // Obviously nesting components like this isn't a general approach to
        // incrementalization, but it reduces the running time a little for now.
        let origLayoutTemplate = new LayoutTemplate(origLayoutTemplateSpec);
        // XXX: Better place for this?
        let uncoveredLeaves = fixmeAssertNotNull(origLayoutTemplate.archetype.uncoveredLeaves);
        staticCheck(uncoveredLeaves.length == 0,
          () => "Layout template has uncovered leaves, including " +
          JSON.stringify(uncoveredLeaves[0]));
        let designViewInfo = this.props.designView ? generateDesignView(origLayoutTemplate) : null;
        let visualLayoutTemplate = designViewInfo != null
          ? designViewInfo.newLayoutTemplate
          : generateNormalView(origLayoutTemplate, this.props.lockdown);
        layoutInfo = {
          templateReadyReactiveVar: this.templateReadyReactiveVar,
          origLayoutTemplate,
          designViewInfo,
          visualLayoutTemplate
        };
      }
      this.templateReadyReactiveVar.set(true);
      Tracker.onInvalidate(() => this.templateReadyReactiveVar.set(false));
      return {layoutInfo};
    }
    render() {
      return <ConditionalLayoutUIComponent {...this.props} {...this.getData()}/>;
    }
  }

  function LayoutUIComponent(props: LayoutUIComponentProps) {
    return giRun((w) => <LayoutUIComponentWithGI {...props} giWrapper={w}/>);
  }

  Router.route("/:sheet/layouts/:layout", function(this: fixmeAny) {
    let layoutName = this.params.layout;
    let props: ReactWrapperTemplateData<LayoutUIComponentProps> = {
      component: LayoutUIComponent,
      sheet: this.params.sheet,
      layoutName: layoutName,
      designView: this.params.query.designView != null,
      lockdown: this.params.query.lockdown != null,
      viewObjectData: this.params.query.viewObjectData == null ? null :
        // XXX Catch error?  Maybe when we validate values.
        JSON.parse(this.params.query.viewObjectData),
      viewObjectQueryParams: _.pick(this.params.query, "designView", "lockdown")
    };
    this.render("layout-wrapper", {data: props});
  });

  Router.route("/:sheet/unsafe-link", function(this: fixmeAny) {
    this.render("layout-unsafe-link", {data: {link: this.params.query.link}});
  });
}
