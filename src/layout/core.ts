namespace Objsheets.Layout {

  // Definitions and logic for scoped-splits layouts without regard to data
  // binding (that part is in binding.ts).

  // Basic definitions for axes and rectangles.

  export enum Axis {
    ROW = "row", COL = "col"
  }

  // We want to brand span IDs with the axis of the span, write functions that
  // are generic over an axis, and check that those functions do not mix up
  // spans on the two axes.  For this to work, every data structure that
  // contains an axis-branded item for each axis must be generic-index (see
  // src/lib/generic-index.ts).  We set this up here.
  export type AxisG<_> = GenericIndex<_, Axis>;
  export type AxisL = LooseIndex<Axis>;
  // Public for example-helpers PA
  export type TypePerAxis<_> = {[A in AxisG<_>]: unknown};
  export type IndependentPerAxis<T> = {[A in Axis]: T};

  export interface Failed_to_infer_A_parameter {
    [ERROR_INTERFACE_DUMMY]: never;
  }

  const AXIS_BRAND = Symbol();
  export type AxisBranded<A extends AxisL> = {[AXIS_BRAND]: A};
  // TODO: Take advantage of partial type inference once supported by TypeScript.
  export function axisBrander<T, A extends AxisL>(arg: T) {
    return <T & AxisBranded<A>>arg;
  }

  // Fully branded span ID.
  //
  // - S ("span") is the representation of the span ID that underlies the brands
  //   B and A: TemplateS or DataS.
  // - B distinguishes between spans in an original or visual layout template:
  //   OrigB or VisualB.  (B is for "brand": a term of poor specificity but Matt
  //   didn't have a better idea.)
  // - A is the axis of the span.
  export type SpanId<S, B, A extends AxisL> = S & B & AxisBranded<A>;

  // We'd prefer:
  //
  // {[A in AxisG<_>]: A extends Axis ? SpanId<S, A> : undefined}
  //
  // to stop AxisG/AxisL from leaking everywhere, but indexing that with `A
  // extends Axis` fails to reduce the conditional type because definite
  // assignability doesn't consider the constraint. !@#$%
  export type RectangleId<_, S, B> = {[A in AxisG<_>]: SpanId<S, B, A>};

  export type OtherAxis<A extends AxisL> =
    A extends Axis.ROW ? Axis.COL :
    A extends Axis.COL ? Axis.ROW :
    never;
  export function otherAxis<A extends AxisL>(a: A): OtherAxis<A> {
    switch (a) {
      // https://github.com/Microsoft/TypeScript/issues/24085
      case Axis.ROW: return <OtherAxis<A>>Axis.COL;
      case Axis.COL: return <OtherAxis<A>>Axis.ROW;
    }
    throw new Error("not reached");
  }
  // Because TypeScript is not able to reason that OtherAxis<OtherAxis<A>> = A,
  // we use these functions to convert back and forth in the relatively few
  // cases in which it's needed.
  export function unwrapDoubleOtherAxis<_ = never, R extends TypePerAxis<_> = never,
    A extends AxisG<_> = never, G extends Do_not_mess_with_this_type_parameter = never>
    (arg: R[OtherAxis<OtherAxis<A>>] & Check_<_, G>): R[A] {
    return <R[A]>arg;
  }
  export function wrapDoubleOtherAxis<_ = never, R extends TypePerAxis<_> = never,
    A extends AxisG<_> = never, G extends Do_not_mess_with_this_type_parameter = never>
    (arg: R[A] & Check_<_, G>): R[OtherAxis<OtherAxis<A>>] {
    return <R[OtherAxis<OtherAxis<A>>]>arg;
  }

  // `perAxis` typically needs the return type to be known, via either an
  // annotation or passing directly to a function of known parameter type.
  //
  // `R |` in return helps type inference.
  export function perAxis<_ = never, R extends TypePerAxis<_> = never,
    G extends Do_not_mess_with_this_type_parameter = never>
    (f: (<A extends Axis>(a: A) => R[A]) & Check_<_, G>): R | Pick<R, AxisG<_>> {
    return <Pick<R, AxisG<_>>>{[Axis.ROW]: f(Axis.ROW), [Axis.COL]: f(Axis.COL)};
  }
  export function independentPerAxis<R>(f: <A extends Axis>(a: A) => R): IndependentPerAxis<R> {
    return {[Axis.ROW]: f(Axis.ROW), [Axis.COL]: f(Axis.COL)};
  }
  export function forAxes(f: <A extends Axis>(a: A) => void) {
    f(Axis.ROW);
    f(Axis.COL);
  }
  export function axesEvery(f: <A extends Axis>(a: A) => boolean) {
    return f(Axis.ROW) && f(Axis.COL);
  }
  export function axesEveryNotNull<_ = never, R extends TypePerAxis<_> = never,
    G extends Do_not_mess_with_this_type_parameter = never>
    (arg: R | Pick<R, AxisG<_>>): arg is {[A in AxisG<_>]: NonNullable<R[A]>} {
    // https://github.com/palantir/tslint/issues/3667
    // tslint:disable-next-line:strict-type-predicates
    return axesEvery((a) => arg[a] != null);
  }
  export function axesSome(f: <A extends Axis>(a: A) => boolean) {
    return f(Axis.ROW) || f(Axis.COL);
  }
  export function axesMap<T>(f: <A extends Axis>(a: A) => T) {
    return [f(Axis.ROW), f(Axis.COL)];
  }

  // Like `perAxis`, typically needs a known return type.
  export function axisCons<_ = never, R extends TypePerAxis<_> = never, A extends AxisG<_> = never,
    G extends Do_not_mess_with_this_type_parameter = never>(
    a: A & Check_<_, G>, par: R[A], perp: R[OtherAxis<A>]): R | Pick<R, AxisG<_>> {
    switch (a) {
      case Axis.ROW:
        // We have to double-cast because TypeScript 3.5 and newer knows that
        // Axis.ROW and A are unrelated... I think?  I haven't fully researched
        // the behavior. ~ Matt 2019-08-27
        return <Pick<R, AxisG<_>>><unknown>{[Axis.ROW]: par, [Axis.COL]: perp};
      case Axis.COL:
        // At one point I reversed the order of the properties, triggering the
        // key order sensitivity bug in JSONKeyedMap.  Leave it like this until
        // we decide what to do about JSONKeyedMap (and supposedly for better
        // optimization by JavaScript engines). ~ Matt 2017-12-07
        return <Pick<R, AxisG<_>>><unknown>{[Axis.ROW]: perp, [Axis.COL]: par};
    }
    throw new Error("not reached");
  }
  export function independentAxisCons<T>(a: Axis, par: T, perp: T): IndependentPerAxis<T> {
    switch (a) {
      case Axis.ROW:
        return {[Axis.ROW]: par, [Axis.COL]: perp};
      case Axis.COL:
        return {[Axis.ROW]: perp, [Axis.COL]: par};
    }
  }

  export enum Extreme {
    FIRST = "first", LAST = "last"
  }
  export const extremes: Extreme[] = [Extreme.FIRST, Extreme.LAST];
  export type PerExtreme<T> = {[E in Extreme]: T};
  export function perExtreme<T>(f: (e: Extreme) => T): PerExtreme<T> {
    return {[Extreme.FIRST]: f(Extreme.FIRST), [Extreme.LAST]: f(Extreme.LAST)};
  }
  export function extremesEveryNotNull<T>(arg: PerExtreme<T>): arg is PerExtreme<NonNullable<T>> {
    return extremes.every((e) => arg[e] != null);
  }
  export function singleOfPerExtreme<T extends undefined | EJSONable>(x: PerExtreme<T>) {
    return (EJSON.equals(x[Extreme.FIRST], x[Extreme.LAST])) ? x[Extreme.FIRST] : null;
  }

  export class RectangleMap<_ = never, S extends EqualByIdentity = never, B = never, V = never,
    G extends Do_not_mess_with_this_type_parameter = never> {
    private curriedMap = new Map<SpanId<S, B, Axis.ROW>, Map<SpanId<S, B, Axis.COL>, V>>();

    constructor(dummy: typeof DUMMY & Check_<_, G>) {}

    get(rect: RectangleId<_, S, B>) {
      let m = this.curriedMap.get(rect[Axis.ROW]);
      if (m == null) return undefined;
      return m.get(rect[Axis.COL]);
    }
    set(rect: RectangleId<_, S, B>, value: V) {
      let row = rect[Axis.ROW];
      let m = this.curriedMap.get(row);
      if (m == null) {
        m = new Map<SpanId<S, B, Axis.COL>, V>();
        this.curriedMap.set(row, m);
      }
      m.set(rect[Axis.COL], value);
    }
    entries() {
      let entries: [RectangleId<_, S, B>, V][] = [];
      this.curriedMap.forEach((m, row) => {
        m.forEach((value, col) =>
          entries.push([axisCons(Axis.ROW, row, col), value]));
      });
      return entries;
    }
  }

  // Merged spans.

  // A _merged span_ consists of a single span (a "trivial" merged span) or a
  // range of two or more sibling spans in a split (the _referenced split_).  It
  // is represented as a pair of first and last spans, which are equal if the
  // merged span consists of a single span.  A merged span has a _container_
  // span; the container of a trivial merged span is the single span, and the
  // container of a nontrivial merged span is the parent of the referenced
  // split.
  //
  // A nontrivial merged span that forms part of the specification of a layout
  // template may only reference a fixed split, since it needs to have a
  // well-defined meaning as the data set changes in size.  However, other parts
  // of the codebase (e.g., selection in the UI) may use merged spans that refer
  // to sibling spans in a data layout that were instantiated from a variable
  // split.

  export type MergedSpanId<S, B, A extends AxisL> = PerExtreme<SpanId<S, B, A>>;

  // A valid merged rectangle will always have one axis `a` on which its extent
  // is valid across the entire oa-span of the containing rectangle.  (I.e., if
  // the merged rectangle has a nontrivial merged span on `a`, then the scope of
  // the referenced a-split includes the oa-span of the containing rectangle,
  // and if not, then the scope of the containing rectangle's a-span includes
  // its oa-span, i.e., the containing rectangle is not oa-partial).  The
  // oa-extent of the merged rectangle may not be valid across the entire a-span
  // of the rectangle, either because there is a nontrivial merged span on `oa`
  // that refers to an oa-split with the same containing rectangle and a merged
  // a-scope, or because the containing rectangle itself is a-partial.
  export type MergedRectId<_, S, B> = {[A in AxisG<_>]: MergedSpanId<S, B, A>};

  export type NullableMergedSpanIds<_, S, B> = {[A in AxisG<_>]: null | MergedSpanId<S, B, A>};

  export function extremeOfSpanList<S, B, A extends AxisL>(
    spans: SpanId<S, B, A>[], extreme: Extreme) {
    switch (extreme) {
      case Extreme.FIRST:
        return spans[0];
      case Extreme.LAST:
        return spans[spans.length - 1];
    }
  }
  export function mergedSpanOfSingleSpan<S, B, A extends AxisL>(
    span: SpanId<S, B, A>): MergedSpanId<S, B, A> {
    return perExtreme((e) => span);
  }
  // Note: In cases where we know the container of the merged span, this
  // function may be used to test whether the span is the entire container.  Not
  // explicit, I know. ~ Matt 2018-01-20
  export function mergedSpanIsSingleSpan<S extends EqualByIdentity, B, A extends AxisL>(
    mergedSpan: MergedSpanId<S, B, A>) {
    return mergedSpan[Extreme.FIRST] == mergedSpan[Extreme.LAST];
  }
  // TODO: Consider inlining once we're reasonably confident we won't change the
  // data structure again.
  export function extremeOfMergedSpan<S, B, A extends AxisL>(
    mergedSpan: MergedSpanId<S, B, A>, extreme: Extreme): SpanId<S, B, A> {
    return mergedSpan[extreme];
  }

  // Data structures for specification of a concrete layout.

  // The extent of a split on its scope axis or of an allocation on either axis
  // is a merged span.  In a concrete layout, a split or allocation appears in
  // the RectContentSpec of its containing rectangle.

  // For X in {Split, Allocation}, XDefCommon is the common part of XDef (for
  // concrete layouts) and TemplateXDef (for layout templates).

  export type AllocationDefCommon<_, S, B> = {
    mergedSpans: MergedRectId<_, S, B>;
    // If non-null, this is a special "background" allocation that does not
    // count as a covering allocation and thus can overlap other allocations.
    // Only for design view added allocations.
    backgroundAllocationInfo: null | BackgroundAllocationInfo;
    style: AllocationStyle;
  };
  export type AllocationDef<_, S, B> = AllocationDefCommon<_, S, B> & {
    templateAllocationId: TemplateAllocationId<B>;
    content: AllocationContent;
  };
  export type SplitDefCommon<S, B, A extends AxisL> = {
    parentSpan: SpanId<S, B, A>;
    mergedScope: MergedSpanId<S, B, OtherAxis<A>>;
  };
  export type SplitDef<S, B, A extends AxisL> = SplitDefCommon<S, B, A> & {
    templateSplitId: TemplateSplitId<B, A>;
    children: SpanId<S, B, A>[];
  };
  export type RectContentSpec<_, S, B> = {
    splitDefs: {[A in AxisG<_>]: SplitDef<S, B, A>[]};
    allocationDefs: AllocationDef<_, S, B>[];
  };
  // Currently, we require the SpanId type to be the same for both axes, but we
  // allow the root span ID to be different.
  export interface ILayoutSpec<_, S, B> {
    getRoot<A extends AxisG<_>>(a: A): SpanId<S, B, A>;
    getRectContentSpec(rect: RectangleId<_, S, B>): RectContentSpec<_, S, B>;
    getSpanStyle<A extends Axis>(a: A, spanId: SpanId<S, B, A>): SpanStyle;
  }
  export function getRootRectangle<_, S, B>(spec: ILayoutSpec<_, S, B>): RectangleId<_, S, B> {
    return perAxis((a) => spec.getRoot(a));
  }

  // Derived data about concrete layouts.

  export type SplitInfo<_, S, B, A extends AxisL> = {
    container: RectangleId<_, S, B>;
    def: SplitDef<S, B, A>;
  };
  export type AllocationInfo<_, S, B> = {
    container: RectangleId<_, S, B>;
    def: AllocationDef<_, S, B>;
  };

  export type RectInfo<_, S, B> = {
    coveringSplits: {[A in AxisG<_>]: null | SplitInfo<_, S, B, A>};
    coveringAllocation: null | AllocationInfo<_, S, B>;

    containedSplits: {[A in AxisG<_>]: SplitInfo<_, S, B, A>[]};
    containedAllocations: AllocationInfo<_, S, B>[];
  };

  // Visitor interface for ILayoutInfo.traverseAll.
  //
  // Assuming majorAxis = Axis.ROW (otherwise switch "row" and "column"):
  //
  // "Row-major" traversal: each rectangle is traversed via its column-wise
  // parent if it has one, otherwise via its row-wise parent.  (The
  // "column-wise parent" is the one that spans more columns and the same
  // rows; see the figure on `getRectParentOnAxis`.)  Since we follow column
  // splits before row splits, this traversal has the property that each
  // rectangle is visited after /all/ of its parents (and after the splits
  // that define its spans).
  export interface GeneralLayoutVisitor<_, S, B> {
    // preRect receives each valid and partially valid rectangle exactly once.
    // Use getRectValidityOnAxis to check for partial rectangles.
    preRect?(rect: RectangleId<_, S, B>): void;

    // "Post" is with respect to the axis-major traversal order.  If a rectangle
    // has an inherited split on the major axis, then postRect may be called on
    // it before the cross rectangles are visited!  This is good enough for
    // AxisSizing, which just chooses the major axis accordingly.  If we need a
    // traversal with a postRect that runs after /all/ descendants, we can
    // implement one.
    postRect?(rect: RectangleId<_, S, B>): void;
  }

  // Visitor interface for ILayoutInfo.traverseAxis.  Traverses the
  // decomposition of the given axis `a`; mainly intended for sizing.
  //
  // Guarantees:
  // - For each split on the requested axis, preSplit and postSplit are called
  //   between the preSplit and postSplit calls for the split defining the
  //   parent span.
  // - For each allocation that is merged on the requested axis, visitAllocation
  //   is called between the preSplit and postSplit calls for the split across
  //   which the allocation is merged.
  // - For each allocation that is not merged on the requested axis,
  //   visitAllocation is called between the preSplit and postSplit calls for
  //   the split defining the span of the allocation.
  export interface AxisVisitor<_, S, B, A extends AxisG<_>> {
    visitAllocation?(alloc: AllocationInfo<_, S, B>): void;
    preSplit?(split: SplitInfo<_, S, B, A>): void;
    postSplit?(split: SplitInfo<_, S, B, A>): void;
  }

  export interface ILayoutInfo<_, S, B> extends ILayoutSpec<_, S, B> {
    // ILayoutInfo includes facilities to walk "up" the layout, as counterparts
    // to ILayoutSpec, which walks "down" it (which is the natural way to give a
    // non-redundant specification).

    // TODO: Move the methods that are easily implemented in terms of the others
    // into an abstract class to leave focus on the ones that require lookups to
    // stored data?

    getParentSplitOfSpan<A extends Axis>(a: A, span: SpanId<S, B, A>):
      null /* if span == getRoot(a) */ | SplitInfo<_, S, B, A>;

    // Throws an exception if the rectangle is invalid.
    getRectInfo(rect: RectangleId<_, S, B>): RectInfo<_, S, B>;

    // If `rect` has a parent rectangle on `a` (i.e., the rectangle having the
    // parent span on `a` and the same span on the other axis, if that rectangle
    // is at least partially valid), then return the parent rectangle, otherwise
    // return null.
    //
    // Every rectangle has a parent on at least one axis.  A rectangle is called
    // a "cross" if it has parents on both axes.
    //
    //     +----------+-----------+----------+
    //     |          | row-wise  |          |
    //     |          | parent    |          |
    //     +----------+-----------+----------+
    //     | col-wise | rectangle |          |
    //     | parent   |           |          |
    //     +----------+-----------+----------+
    //     |          |           |          |
    //     |          |           |          |
    //     +----------+-----------+----------+
    getRectParentOnAxis<A extends Axis>(rect: RectangleId<_, S, B>, a: A): null | RectangleId<_, S, B>;

    getRectValidityOnAxis<A extends Axis>(rect: RectangleId<_, S, B>, a: A): MergedSpanId<S, B, A>;

    // See documentation on GeneralLayoutVisitor.
    traverseAllWithMajorAxis<MA extends Axis>(visitor: GeneralLayoutVisitor<_, S, B>,
      majorAxis: MA, start?: RectangleId<_, S, B>): void;
    traverseAll(visitor: GeneralLayoutVisitor<_, S, B>, start?: RectangleId<_, S, B>): void;
    // See documentation on AxisVisitor.
    traverseAxis<A extends Axis>(a: A, visitor: AxisVisitor<_, S, B, A>): void;
  }

  // Class Layout: validates a concrete layout and computes the derived data.

  // Future: Rename to MaterializedLayout once we have an ILayoutInfo
  // implementation for a data layout that refers to a MaterializedLayout for
  // the layout template without materializing the whole data layout.

  // Require SpanIds to be EqualByIdentity so we can use Map (and RectangleMap)
  // and == rather than JSONKeyedMap and EJSON.equals, which are slow.  We could
  // do interning in this class, but as of 2018-02-23, it seems much cleaner to
  // let the relevant ILayoutSpec implementation (i.e., DataLayoutSpec) do it.
  export class Layout<_, S extends JSONable & EqualByIdentity, B> implements ILayoutInfo<_, S, B> {
    private spec: ILayoutSpec<_, S, B>;
    // Only filled when fullValidation = true.
    uncoveredLeaves?: RectangleId<_, S, B>[];

    getRoot<A extends AxisG<_>>(a: A) {
      return this.spec.getRoot(a);
    }
    getRectContentSpec(rect: RectangleId<_, S, B>) {
      return this.spec.getRectContentSpec(rect);
    }
    getSpanStyle<A extends Axis>(a: A, spanId: SpanId<S, B, A>): SpanStyle {
      return this.spec.getSpanStyle(a, spanId);
    }

    private spanParentInfo: {[A in AxisG<_>]: Map<SpanId<S, B, A>, SplitInfo<_, S, B, A>>};
    private rectInfo: RectangleMap<_, S, B, RectInfo<_, S, B>>;

    hasSpan<A extends AxisG<_>>(a: A, span: SpanId<S, B, A>): boolean {
      return this.spanParentInfo[a].get(span) != null;
    }
    getParentSplitOfSpan<A extends AxisG<_>>(a: A, span: SpanId<S, B, A>): null | SplitInfo<_, S, B, A> {
      if (span == this.getRoot(a)) return null;
      let spi = this.spanParentInfo[a].get(span);
      return staticCheckNotNull(spi, () => "Invalid span ID");
    }
    getContainerScopeOfSpan<A extends AxisG<_>>(a: A, span: SpanId<S, B, A>): SpanId<S, B, OtherAxis<A>> {
      let oa = otherAxis(a);
      let spi = this.getParentSplitOfSpan(a, span);
      return (spi == null) ? this.getRoot(oa) : spi.container[oa];
    }
    getRectInfo(rect: RectangleId<_, S, B>) {
      let ri = this.rectInfo.get(rect);
      // XXX It would be more explicit to set a "valid" flag when a rectangle is
      // actually visited.  Then we could catch bugs where some code calls
      // getOrCreateRectInfo on an invalid rectangle.
      return staticCheckNotNull(ri, () => "Invalid rectangle ID");
    }

    traverseAllWithMajorAxis<MA extends AxisG<_>>(visitor: GeneralLayoutVisitor<_, S, B>, majorAxis: MA,
      start = getRootRectangle(this.spec)) {
      let minorAxis = otherAxis(majorAxis);
      let traverse = (rect: RectangleId<_, S, B>) => {
        if (visitor.preRect)
          visitor.preRect(rect);
        let ri = this.getOrCreateRectInfo(rect);
        let followSplit = <A extends AxisG<_>>(a: A, def: SplitInfo<_, S, B, A>) => {
          for (let childRect of this.getRectChildrenBySplit(rect, def, a))
            traverse(childRect);
        };
        // Follow any in-scope column split.
        let minorSplit: null | SplitInfo<_, S, B, OtherAxis<MA>> = ri.coveringSplits[minorAxis];
        if (minorSplit != null) {
          followSplit(minorAxis, minorSplit);
        }
        for (let split of ri.containedSplits[minorAxis]) {
          // Split with trivial merged scope should be in ri.coveringSplits[minorAxis].
          if (!mergedSpanIsSingleSpan(split.def.mergedScope))
            followSplit(minorAxis, split);
        }
        // Follow a row split only on the rectangle R where it is defined.  If
        // the current rectangle is a column-wise descendant of R, then the
        // child rectangles we would reach if we followed the split here should
        // instead be reached as column-wise descendants of the children from
        // following the split at R.
        let majorSplit: null | SplitInfo<_, S, B, MA> = ri.coveringSplits[majorAxis];
        if (majorSplit != null &&
          (rect[minorAxis] == majorSplit.container[minorAxis] ||
           rect[minorAxis] == start[minorAxis])) {
          followSplit(majorAxis, majorSplit);
        }
        for (let split of ri.containedSplits[majorAxis]) {
          // Split with trivial merged scope should be in ri.coveringSplits[majorAxis].
          if (!mergedSpanIsSingleSpan(split.def.mergedScope))
            followSplit(majorAxis, split);
        }
        if (visitor.postRect)
          visitor.postRect(rect);
      };
      traverse(start);
    }
    traverseAll(visitor: GeneralLayoutVisitor<_, S, B>, start?: RectangleId<_, S, B>) {
      this.traverseAllWithMajorAxis(visitor, Axis.ROW, start);
    }

    private getOrCreateRectInfo(rect: RectangleId<_, S, B>) {
      // TODO: Replace with defaultdict analogue?
      let ri = this.rectInfo.get(rect);
      if (ri == null) {
        ri = {
          coveringSplits: perAxis((a) => null),
          coveringAllocation: null,
          containedSplits: perAxis((a) => []),
          containedAllocations: []
        };
        this.rectInfo.set(rect, ri);
      }
      return ri;
    }
    private recordCoveringSplit<A extends Axis>(rect: RectangleId<_, S, B>, a: A, csi: SplitInfo<_, S, B, A>) {
      let ri = this.getOrCreateRectInfo(rect);
      if (this.validateLayout) {
        // TODO: Print a human-readable representation of the conflicting splits.
        //
        // https://github.com/palantir/tslint/issues/3667
        // tslint:disable-next-line:strict-type-predicates
        staticCheck(ri.coveringSplits[a] == null, () => `Conflicting splits`);
      }
      ri.coveringSplits[a] = csi;
    }
    private recordCoveringAllocation(rect: RectangleId<_, S, B>, cai: AllocationInfo<_, S, B>,
      ignoreDuplicate = false) {
      let ri = this.getOrCreateRectInfo(rect);
      if (this.validateLayout) {
        if (ignoreDuplicate && EJSON.equals(ri.coveringAllocation, cai))
          return;
        // TODO: Print a human-readable representation of the conflicting allocations.
        staticCheck(ri.coveringAllocation == null, () => `Conflicting allocations`);
      }
      ri.coveringAllocation = cai;
    }

    private resolveMergedSpanId<A extends AxisG<_>>(split: SplitDef<S, B, A>, mergedSpanId: MergedSpanId<S, B, A>,
      validity: null | MergedSpanId<S, B, A> = null): SpanId<S, B, A>[] {
      let availableChildren =
        (validity == null || mergedSpanIsSingleSpan(validity)) ? split.children
        : this.resolveMergedSpanId(split, validity);
      let firstIndex = availableChildren.indexOf(mergedSpanId[Extreme.FIRST]);
      let lastIndex = availableChildren.indexOf(mergedSpanId[Extreme.LAST]);
      // XXX: Want to return null and leave the decision of what error to throw
      // to callers?
      staticCheck(firstIndex >= 0 && lastIndex >= 0 &&
        // We currently disallow a "merged span" of a single span to avoid
        // having multiple ways to do the same thing.
        firstIndex < lastIndex,
        () => "Invalid MergedSpanId");
      return availableChildren.slice(firstIndex, lastIndex + 1);
    }

    // NOTE: Does not fully validate mergedSpan
    referencedSplitOfMergedSpan<A extends AxisG<_>>(a: A, mergedSpan: MergedSpanId<S, B, A>):
      null | SplitInfo<_, S, B, A> {
      if (mergedSpanIsSingleSpan(mergedSpan)) {
        return null;
      } else {
        return this.getParentSplitOfSpan(a, mergedSpan[Extreme.FIRST]);
      }
    }
    // NOTE: Does not fully validate mergedSpan
    containerOfMergedSpan<A extends Axis>(a: A, mergedSpan: MergedSpanId<S, B, A>): SpanId<S, B, A> {
      let referencedSplit = this.referencedSplitOfMergedSpan(a, mergedSpan);
      if (referencedSplit == null) {
        return mergedSpan[Extreme.FIRST];
      } else {
        return referencedSplit.def.parentSpan;
      }
    }
    getSpansInMergedSpan<A extends AxisG<_>>(a: A, mergedSpan: MergedSpanId<S, B, A>): SpanId<S, B, A>[] {
      let referencedSplit = this.referencedSplitOfMergedSpan(a, mergedSpan);
      if (referencedSplit == null) {
        return [mergedSpan[Extreme.FIRST]];
      } else {
        return this.resolveMergedSpanId(referencedSplit.def, mergedSpan);
      }
    }
    getRectsInMergedRect(mergedRect: MergedRectId<_, S, B>) {
      let spans: {[A in AxisG<_>]: SpanId<S, B, A>[]} =
        perAxis((a) => this.getSpansInMergedSpan(a, mergedRect[a]));
      let rects: RectangleId<_, S, B>[] = [];
      for (let rsp of spans[Axis.ROW]) {
        for (let csp of spans[Axis.COL]) {
          rects.push(axisCons(Axis.ROW, rsp, csp));
        }
      }
      return rects;
    }

    // Annotating the return type seems to work around
    // https://github.com/Microsoft/TypeScript/issues/26130 in sizing.ts.
    // ~ Matt 2018-08-01
    private splitCoveringMergedScopeInternal<A extends Axis>(splitAxis: A, parentSpan: SpanId<S, B, A>,
      mergedScope: MergedSpanId<S, B, OtherAxis<A>>, getRectInfoCB = (r: RectangleId<_, S, B>) => this.getRectInfo(r)):
      null | SplitInfo<_, S, B, A> {
      let extremeCoveringSplits = perExtreme((e) => getRectInfoCB(
        axisCons(splitAxis, parentSpan, extremeOfMergedSpan(mergedScope, e))
      ).coveringSplits[splitAxis]);
      return singleOfPerExtreme(extremeCoveringSplits);
    }
    splitCoveringMergedScope<A extends Axis>(splitAxis: A, parentSpan: SpanId<S, B, A>,
      mergedScope: MergedSpanId<S, B, OtherAxis<A>>) {
      return this.splitCoveringMergedScopeInternal(splitAxis, parentSpan, mergedScope);
    }
    coveringAllocationOfMergedRect(mergedRect: MergedRectId<_, S, B>) {
      let cornerAllocs = perExtreme((e) => this.getRectInfo(
        perAxis((a) => extremeOfMergedSpan(mergedRect[a], e))
      ).coveringAllocation);
      return singleOfPerExtreme(cornerAllocs);
    }

    private validateMergedRectInternal(mergedRect: MergedRectId<_, S, B>,
      container: RectangleId<_, S, B>,
      getRectInfoCB = (r: RectangleId<_, S, B>) => this.getRectInfo(r)) {
      // Implicitly validates the container if getRectInfoCB is the default getRectInfo.
      let containerRI = getRectInfoCB(container);

      let dependentAxis: null | Axis = null;
      // First pass attempt at resolving merged spans.
      forAxes((a) => {
        let validity = this.getRectValidityOnAxis(container, a);
        if (mergedSpanIsSingleSpan(mergedRect[a])) {
          staticCheck(mergedSpanIsSingleSpan(validity),
            () => `Merged rectangle specifies a span of its entire container on axis ${a}, ` +
            `but the container is partial`);
        } else {
          let theSplit = containerRI.coveringSplits[a];
          // https://github.com/palantir/tslint/issues/3667
          // tslint:disable-next-line:strict-type-predicates
          if (theSplit != null) {
            // Validation only; discard result.
            this.resolveMergedSpanId(theSplit.def, mergedRect[a], validity);
          } else {
            // Presumably the merged rectangle is trying to span part of a split
            // with a merged scope that includes the rectangle's merged
            // span on oa.  Defer to second pass.
            let oa = otherAxis(a);
            // https://github.com/palantir/tslint/issues/3667
            // tslint:disable-next-line:strict-type-predicates
            staticCheck(containerRI.coveringSplits[oa] != null,
              () => `Merged rectangle specifies a merged span on axis ${a} dependent on the other axis, ` +
              `which has no covering split.`);
            staticCheck(!mergedSpanIsSingleSpan(mergedRect[oa]),
              () => `Merged rectangle specifies a merged span on axis ${a} dependent on the other axis, ` +
              `which has a trivial merged span specified.`);
            assert(dependentAxis == null);
            dependentAxis = a;
          }
        }
      });
      // Second pass: validate a merged span that is not valid across
      // the entire container but is valid across the merged rectangle's
      // perpendicular span.
      forAxes((a) => {
        if (a == dependentAxis) {
          let oa = otherAxis(a);
          let coveringSplit = this.splitCoveringMergedScopeInternal(
            a, container[a], mergedRect[oa], getRectInfoCB);
          coveringSplit = staticCheckNotNull(coveringSplit,
            () => `Merged rectangle specifies a merged span on axis ${a}, but this is meaningless ` +
            `because the allocation's merged span on axis ${oa} does not lie in the scope of a single split.`);
          // Validation only; discard result.
          this.resolveMergedSpanId(coveringSplit.def, mergedRect[a]);
        }
      });
    }
    validateMergedRect(mergedRect: MergedRectId<_, S, B>) {
      this.validateMergedRectInternal(mergedRect,
        perAxis((a) => this.containerOfMergedSpan(a, mergedRect[a])));
    }
    isMergedRectValid(mergedRect: MergedRectId<_, S, B>) {
      try {
        this.validateMergedRect(mergedRect);
        return true;
      } catch (e) {
        if (!(e instanceof StaticError)) throw e;
        return false;
      }
    }

    // validateLayout may be set to false to bypass certain layout validity
    // checks to save time.
    //
    // Careful: validateLayout = false should not stop APIs such as
    // validateMergedRect from fulfilling their intended purpose.
    constructor(spec: ILayoutSpec<_, S, B>, public validateLayout = true) {
      this.spec = spec;

      if (this.validateLayout)
        this.uncoveredLeaves = [];
      this.spanParentInfo = perAxis((a) =>
        new Map<SpanId<S, B, typeof a>, SplitInfo<_, S, B, typeof a>>());
      this.rectInfo = new RectangleMap(DUMMY);
      // Use traverseAll to drive the initialization of rectInfo and the
      // validation of the layout.  The important thing is that each rectangle
      // is visited exactly once, after its parent(s).  traverseAll itself uses
      // rectInfo, but we'll finalize each entry in rectInfo just before it is
      // used.
      this.traverseAll({
        preRect: (rect) => {
          let contentSpec = this.getRectContentSpec(rect);
          let thisRI = this.getOrCreateRectInfo(rect);
          // It's easiest to process all relevant splits before all allocations
          // defined on rect, because a merged allocation can depend on any
          // split with the same container.

          // Inherit splits and allocations.
          forAxes((a) => {
            let oa = otherAxis(a);
            let parent = this.getRectParentOnAxis(rect, oa);
            if (parent != null) {
              let parentRI = this.getOrCreateRectInfo(parent);
              if (parentRI.coveringAllocation != null) {
                // Ignore duplicates because we could inherit the same non-leaf allocation from both parents.
                this.recordCoveringAllocation(rect, parentRI.coveringAllocation, true);
              }
              let splitToInherit: null | SplitInfo<_, S, B, typeof a> = parentRI.coveringSplits[a];
              if (splitToInherit != null) {
                this.recordCoveringSplit(rect, a, splitToInherit);
              }
            }
          });

          // Look for covering splits defined here.
          forAxes((a) => {
            let oa = otherAxis(a);
            for (let split of contentSpec.splitDefs[a]) {
              staticCheck(split.parentSpan == rect[a],
                () => "SplitDef.parentSpan does not match specified container");
              let splitInfo = {container: rect, def: split};
              // sizing.ts tries to make every split fill the parent span by
              // giving any extra space to the last child.  Hence, there must be
              // a last child.
              staticCheck(split.children.length > 0,
                () => "A concrete split must have at least one child.");
              for (let child of split.children) {
                staticCheck(this.spanParentInfo[a].get(child) == null,
                  () => `Span ID ${JSON.stringify(child)} used more than once in layout`);
                this.spanParentInfo[a].set(child, splitInfo);
              }
              thisRI.containedSplits[a].push(splitInfo);
              if (mergedSpanIsSingleSpan(split.mergedScope)) {
                if (this.validateLayout) {
                  // The corresponding check when
                  // !mergedSpanIsSingleSpan(split.mergedScope) is part of
                  // resolveMergedSpanId.
                  staticCheck(split.mergedScope[Extreme.FIRST] == rect[oa],
                    () => "SplitDef.mergedScope does not match specified container");
                  staticCheck(mergedSpanIsSingleSpan(this.getRectValidityOnAxis(rect, oa)),
                    () => "Split specifies a scope of its entire container, but the container is partial");
                  // Note: We do not need to check if rect is partial on `a`.
                  // That will be detected as a conflict with the outer split.
                }
                this.recordCoveringSplit(rect, a, splitInfo);
              }
            }
          });

          // Now look for splits with merged scopes, which may reference
          // covering splits that we just found.
          forAxes((a) => {
            let oa = otherAxis(a);
            for (let split of thisRI.containedSplits[a]) {
              if (!mergedSpanIsSingleSpan(split.def.mergedScope)) {
                let otherSplit = thisRI.coveringSplits[oa];
                otherSplit = staticCheckNotNull(otherSplit,
                  () => "Split definition with a mergedScope, but there's no orthogonal split to reference");
                let spans = this.resolveMergedSpanId(
                  otherSplit.def, split.def.mergedScope,
                  this.validateLayout ? this.getRectValidityOnAxis(rect, oa) : null);
                for (let sp of spans) {
                  this.recordCoveringSplit(axisCons(a, rect[a], sp), a, split);
                }
              }
            }
          });

          // Next: allocations, which can depend on all the splits!
          for (let alloc of contentSpec.allocationDefs) {
            let allocInfo: AllocationInfo<_, S, B> = {
              container: rect,
              def: alloc
            };
            if (this.validateLayout) {
              forAxes((a) => {
                if (mergedSpanIsSingleSpan(allocInfo.def.mergedSpans[a])) {
                  // Like for splits, the corresponding check when
                  // !mergedSpanIsSingleSpan(allocInfo.def.mergedSpans[a]) is part
                  // of a resolveMergedSpanId inside validateMergedRectInternal,
                  // based on the container we passed in.
                  staticCheck(allocInfo.def.mergedSpans[a][Extreme.FIRST] == rect[a],
                    () => "AllocationDef.mergedSpans does not match specified container");
                }
              });
              this.validateMergedRectInternal(allocInfo.def.mergedSpans, rect, (r) => this.getOrCreateRectInfo(r));
            }
            thisRI.containedAllocations.push(allocInfo);
            if (allocInfo.def.backgroundAllocationInfo == null) {
              for (let coveredRect of this.getRectsInMergedRect(alloc.mergedSpans)) {
                // Do not ignore duplicates so that if someone specifies two
                // identical allocation definitions, we'll catch it here.  This
                // is a little kludgy.  A better way might be to have allocaton
                // IDs, check that they are unique, and then ignore duplicates
                // by ID.
                this.recordCoveringAllocation(coveredRect, allocInfo);
              }
            }
          }

          if (this.validateLayout && thisRI.coveringAllocation == null &&
            // https://github.com/palantir/tslint/issues/3667
            // tslint:disable-next-line:strict-type-predicates
            axesEvery((a) => thisRI.coveringSplits[a] == null)) {
            fixmeAssertNotNull(this.uncoveredLeaves).push(rect);
          }
        }
      });

      if (this.validateLayout) {
        // Second traversal after all inheritance of covering allocations is
        // complete.  XXX: Could move to postRect of the first pass if postRect
        // ran after /all/ descendant rectangles.
        this.traverseAll({
          preRect: (rect) => {
            let rectInfo = this.getRectInfo(rect);
            forAxes((a) => {
              let oa = otherAxis(a);
              // For each split, check if its end caps are covered by allocations.
              for (let split of rectInfo.containedSplits[a]) {
                let msi = split.def.mergedScope;
                for (let e of extremes) {
                  // If this end cap is covered by an allocation, then that
                  // allocation must be the covering allocation of the extreme
                  // rectangle of the split's scope.  Furthermore, if that
                  // rectangle has a covering allocation, then the allocation
                  // covers the end cap iff the extreme span of the allocation is
                  // not the same as that of the split scope.  (The extreme of the
                  // allocation cannot be further inside the end cap of the split
                  // because then the allocation wouldn't have been the covering
                  // allocation.)
                  let extremeSpan = extremeOfMergedSpan(msi, e);
                  let extremeRect: RectangleId<_, S, B> = axisCons(a, rect[a], extremeSpan);
                  let ca = this.getRectInfo(extremeRect).coveringAllocation;
                  staticCheck(
                    ca == null ||
                    extremeOfMergedSpan(ca.def.mergedSpans[oa], e) == extremeSpan,
                    () => "Allocation covers split end cap");
                }
              }
            });
          }
        });
      }
    }

    getRectParentOnAxis<A extends Axis>(rect: RectangleId<_, S, B>, a: A): null | RectangleId<_, S, B> {
      let oa = otherAxis(a);
      // XXX: Want to assert that `rect` is valid?

      // Given that `rect` itself is valid, the putative parent rectangle is (at
      // least partially) valid iff the parent of the a-span is still in the
      // (container) scope of the oa-span.  Given that the scope of the oa-span
      // is either the a-span or an ancestor, the condition will be met iff the
      // scope of the oa-span is not the a-span itself.
      let containerScopeOfOaSpan =
        unwrapDoubleOtherAxis<_, RectangleId<_, S, B>, typeof a>(this.getContainerScopeOfSpan(oa, rect[oa]));
      if (containerScopeOfOaSpan != rect[a])
        return axisCons(a, assertNotNull(this.getParentSplitOfSpan(a, rect[a])).container[a], rect[oa]);
      else
        return null;
    }

    getRectValidityOnAxis<A extends AxisG<_>>(rect: RectangleId<_, S, B>, a: A): MergedSpanId<S, B, A> {
      // Note the similarity to getRectParentOnAxis.
      let oa = otherAxis(a);
      let spi = this.getParentSplitOfSpan(oa, rect[oa]);
      if (spi == null) {
        // rect[oa] is root.
        return mergedSpanOfSingleSpan(rect[a]);
      } else if (spi.container[a] != rect[a]) {
        // Container scope of oa-span is a proper ancestor of a-span.  `rect`
        // must be fully valid on `a`.
        return mergedSpanOfSingleSpan(rect[a]);
      } else {
        // Container scope of oa-span is exactly the a-span.  The a-validity of
        // `rect` will be the actual scope of the oa-span.
        return unwrapDoubleOtherAxis<_, MergedRectId<_, S, B>, A>(spi.def.mergedScope);
      }
    }

    // `split` can be the covering split of `rect` on `splitAxis` (if any) or a
    // split defined on `rect` with a merged scope (which will result in partial
    // rectangles).
    getRectChildrenBySplit<A extends AxisG<_>>(rect: RectangleId<_, S, B>, split: SplitInfo<_, S, B, A>, splitAxis: A):
      RectangleId<_, S, B>[] {
      let scopeAxis = otherAxis(splitAxis);
      assert(rect[splitAxis] == split.container[splitAxis]);
      // TODO: Assert that rect is in the container scope of the split?
      // Desirable for "untrusted" callers but might be too slow coming from
      // traverseAll.
      let validity = this.getRectValidityOnAxis(rect, splitAxis);
      let children = mergedSpanIsSingleSpan(validity) ? split.def.children :
        this.getSpansInMergedSpan(splitAxis, validity);
      return children.map((child) => axisCons(splitAxis, child, rect[scopeAxis]));
    }

    // TODO: This is simple enough, want to move it to top level of Layout
    // namespace or just inline into AxisSizing?
    traverseAxis<A extends AxisG<_>>(a: A, v: AxisVisitor<_, S, B, A>) {
      this.traverseAllWithMajorAxis({
        preRect: (rect) => {
          let thisRI = this.getRectInfo(rect);
          if (v.preSplit) {
            for (let split of thisRI.containedSplits[a]) {
              v.preSplit(split);
            }
          }
          if (v.visitAllocation) {
            for (let alloc of thisRI.containedAllocations) {
              v.visitAllocation(alloc);
            }
          }
        },
        postRect: (rect) => {
          let thisRI = this.getRectInfo(rect);
          if (v.postSplit) {
            for (let split of thisRI.containedSplits[a]) {
              v.postSplit(split);
            }
          }
        }
      }, a);
    }
  }

}
