// Helpers to reduce boilerplate in hard-coded example layouts.
//
// Hard-coded example layouts can be written in the Objsheets.Layout.Examples
// namespace, and they will automatically have unqualified access to everything
// in Layout as well as the helpers in this file.  We may as well use this trick
// as long as it works; it won't work if, for example, we want unqualified
// access to two unrelated namespaces.
namespace Objsheets.Layout.Examples {

  export function R(x: string) {
    return templateSpanIdBrander<OrigB, Axis.ROW>(x);
  }
  export function C(x: string) {
    return templateSpanIdBrander<OrigB, Axis.COL>(x);
  }

  export let rRoot: TemplateSpanId<OrigB, Axis.ROW>, cRoot: TemplateSpanId<OrigB, Axis.COL>;
  Meteor.startup(() => {  // load order for rootTemplateSpanId
    rRoot = rootTemplateSpanId<OrigB, Axis.ROW>(DUMMY);
    cRoot = rootTemplateSpanId<OrigB, Axis.COL>(DUMMY);
  });

  // Literal PerAxis values.
  export function PA<_ = never, R_ extends TypePerAxis<_> = never,
    G extends Do_not_mess_with_this_type_parameter = never>
    (xRow: R_[Axis.ROW] & Check_<_, G>, xCol: R_[Axis.COL]): R_ | Pick<R_, AxisG<_>> {
    return axisCons(Axis.ROW, xRow, xCol);
  }

  // Literal PerExtreme values.
  export function PE<T>(xFirst: T, xLast: T): PerExtreme<T> {
    return {[Extreme.FIRST]: xFirst, [Extreme.LAST]: xLast};
  }

  // Try making this naming convention mandatory for the examples.
  export function nameExtraSpaceSpan<A extends Axis>(child: TemplateSpanId<OrigB, A>) {
    return templateSpanIdBrander<OrigB, A>(child + ".extra");
  }

  type SingleOrMergedTemplateSpanId<A extends Axis> = TemplateSpanId<OrigB, A> | MergedSpanId<TemplateS, OrigB, A>;
  function ensureMergedSpan<A extends Axis>(x: SingleOrMergedTemplateSpanId<A>): MergedSpanId<TemplateS, OrigB, A> {
    // There's no simple type guard that narrows properly with our brand, so use a cast. :/
    // TODO Investigate tslint issue.
    // tslint:disable-next-line:strict-type-predicates
    return (typeof x == "string") ? PE(x, x) : <MergedSpanId<TemplateS, OrigB, A>>x;
  }

  export function FixedSplit<A extends Axis>(parentSpan: TemplateSpanId<OrigB, A>,
    scope: SingleOrMergedTemplateSpanId<OtherAxis<A>>,
    children: TemplateSpanId<OrigB, A>[]): FixedSplitDef<OrigB, A> {
    return {
      parentSpan,
      mergedScope: ensureMergedSpan(scope),
      kind: TemplateSplitDefKind.FIXED,
      children
    };
  }

  export function VariableSplit<A extends Axis>(parentSpan: TemplateSpanId<OrigB, A>,
    scope: SingleOrMergedTemplateSpanId<OtherAxis<A>>,
    child: TemplateSpanId<OrigB, A>,
    nestedObjectTypeRef: string): VariableSplitDef<OrigB, A> {
    let nestedObjectType = parseObjectTypeRef(nestedObjectTypeRef);
    if (nestedObjectType === rootColumnId)  // narrowing
      throw new Error("The root column cannot be a nested object type");
    return {
      parentSpan,
      mergedScope: ensureMergedSpan(scope),
      kind: TemplateSplitDefKind.VARIABLE,
      child,
      extraSpaceChild: nameExtraSpaceSpan(child),
      nestedObjectType
    };
  }

  export function LabelAlloc<_ = never, G extends Do_not_mess_with_this_type_parameter = never>(
    rowSpan: SingleOrMergedTemplateSpanId<Axis.ROW> & Check_<_, G>,
    colSpan: SingleOrMergedTemplateSpanId<Axis.COL>,
    text: string,
    style?: AllocationStyle): LabelAllocationDef<_, OrigB> {
    return {
      mergedSpans: PA(ensureMergedSpan(rowSpan), ensureMergedSpan(colSpan)),
      kind: TemplateAllocationDefKind.LABEL,
      text: text,
      backgroundAllocationInfo: null,
      style: fallback(style, {})
    };
  }

  export function FieldAlloc<_ = never, G extends Do_not_mess_with_this_type_parameter = never>(
    rowSpan: SingleOrMergedTemplateSpanId<Axis.ROW> & Check_<_, G>,
    colSpan: SingleOrMergedTemplateSpanId<Axis.COL>,
    fieldRef: string,
    style?: AllocationStyle): FieldAllocationDef<_, OrigB> {
    return {
      mergedSpans: PA(ensureMergedSpan(rowSpan), ensureMergedSpan(colSpan)),
      kind: TemplateAllocationDefKind.FIELD,
      field: parseFieldRef(fieldRef),
      backgroundAllocationInfo: null,
      style: fallback(style, {})
    };
  }

  export function ExampleLayout<_ = never, G extends Do_not_mess_with_this_type_parameter = never>(data: {
    _id: ColumnId & Check_<_, G>,
    splits: {[A in AxisG<_>]: TemplateSplitDef<OrigB, A>[]},
    allocations: TemplateAllocationDef<_, OrigB>[],
    crossBindings?: [RectangleId<_, TemplateS, OrigB>, CrossBinding][],
    spanStyles?: {[A in AxisG<_>]: [TemplateSpanId<OrigB, A>, SpanStyle][]}
  }): LayoutTemplateSpec<_, OrigB> {
    let spec: LayoutTemplateSpec<_, OrigB> = {
      _id: data._id,
      splits: perAxis((a) => new JSONKeyedMap(data.splits[a].map
        <[TemplateSplitId<OrigB, typeof a>, TemplateSplitDef<OrigB, typeof a>]>
        ((split, i) => [templateSplitIdBrander<OrigB, typeof a>(a + "_split_" + i), split]))),
      allocations: new JSONKeyedMap(data.allocations.map
        <[TemplateAllocationId<OrigB>, TemplateAllocationDef<_, OrigB>]>
        ((alloc, i) => [templateAllocationIdBrander<OrigB>("alloc_" + i), alloc])),
      // Cross bindings are rare enough, no sugar for now.
      crossBindings: new JSONKeyedMap(data.crossBindings || []),
      spanStyles: perAxis((a) => new JSONKeyedMap(data.spanStyles && data.spanStyles[a] || []))
    };
    let layoutTemplate = new LayoutTemplate(spec);
    // Don't use coverLeaves because we prefer to (1) use blank labels rather
    // than add fields to the schema and (2) use reproducible names.
    let newSpec = shallowishCloneLayoutTemplateSpec(spec);
    fixmeAssertNotNull(layoutTemplate.archetype.uncoveredLeaves).forEach((rect, i) => {
      newSpec.allocations.set(templateAllocationIdBrander<OrigB>("uncovered_leaf_" + i), {
        mergedSpans: perAxis((a) => mergedSpanOfSingleSpan(rect[a])),
        kind: TemplateAllocationDefKind.LABEL,
        text: "",
        backgroundAllocationInfo: null,
        style: {}
      });
    });
    return newSpec;
  }

}
