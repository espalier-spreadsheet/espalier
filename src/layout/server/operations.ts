namespace Objsheets.Layout {

  // See comment about structure editing caveats in src/layout/operations.ts .

  const NUM_INITIAL_SPANS = 10;

  let addAllocationServerCreateFieldCB: AddAllocationCreateFieldCallback = (parent) => {
    let formula = getExistingColumn(parent).formula != null ? DUMMY_FORMULA : null;
    return $$server.model.defineColumn({
      parentId: parent,
      index: -1,  // Hmm, deprecate eventually?
      fieldName: null,
      specifiedType: formula != null ? null : DEFAULT_STATE_FIELD_TYPE,
      isObject: false,
      objectName: null,
      formula,
      viewId: null,
      singular: true,
      isViewObjectRoot: false,
      userVisible: true
    });
  };

  function coverLeaves<_>(layoutTemplate: LayoutTemplate<_, OrigB>): LayoutTemplateSpec<_, OrigB> {
    return coverLeavesGeneric(layoutTemplate, addAllocationServerCreateFieldCB);
  }

  function addAllocation<_>(prevLayoutTemplate: LayoutTemplate<_, OrigB>, spec: LayoutTemplateSpec<_, OrigB>,
    mergedRect: MergedRectId<_, TemplateS, OrigB>) {
    addAllocationGeneric(prevLayoutTemplate, spec, mergedRect, addAllocationServerCreateFieldCB);
  }

  function deleteAllocation<_>(spec: LayoutTemplateSpec<_, OrigB>, id: TemplateAllocationId<OrigB>) {
    let allocDef = assertNotNull(spec.allocations.get(id));
    switch (allocDef.kind) {
      case TemplateAllocationDefKind.LABEL:
        break;
      case TemplateAllocationDefKind.FIELD:
        // Key fields cannot be deleted this way.
        assert(!getExistingColumn(allocDef.field).isObject);
        $$server.model.deleteColumn(allocDef.field);
        break;
      case TemplateAllocationDefKind.HYPERLINK:
        $$server.model.deleteColumn(allocDef.textField);
        $$server.model.deleteColumn(allocDef.urlField);
        break;
      case TemplateAllocationDefKind.BUTTON:
        $$server.model.deleteColumn(allocDef.textField);
        break;
      default:
        assertUnreachable(allocDef);
    }
    spec.allocations.delete(id);
  }

  function checkMoveTarget<_>(layoutTemplate: LayoutTemplate<_, OrigB>, what: string,
    oldContainer: RectangleId<_, TemplateS, OrigB>, newContainer: RectangleId<_, TemplateS, OrigB>) {
    let oldContainerBinding = layoutTemplate.getExistingRectBinding(oldContainer);
    let newContainerBinding = layoutTemplate.getExistingRectBinding(newContainer);

    staticCheck(axesEvery((a) => !layoutTemplate.extraSpaceSpans[a].has(newContainer[a])),
      () => `Cannot move a ${what} into an extra space span.`);

    if (oldContainerBinding.trp == null)  // narrowing
      throw new Error("Nothing movable should be in an unbound area.");
    let collectionName = (trp: null | TypeAndRefPattern) =>
      (trp == null) ? "(none)" :
      objectNameForUI(getExistingColumn(trp.type));
    staticCheck(newContainerBinding.trp != null && newContainerBinding.trp.type == oldContainerBinding.trp.type,
      () => "The new location does not belong to the same collection (" +
        collectionName(newContainerBinding.trp) +
        ") as the old location (" +
        collectionName(oldContainerBinding.trp) + ").");
  }

  type LayoutMethodResult<_, R> = {
    response: R;
    newLayout: LayoutTemplateSpec<_, OrigB>;
  };
  export interface GenericIndexLayoutMethods<_> {
    define<R extends (EJSONable | void)>
      (method: MeteorMethod2<TablespaceId, string, R>):
        (impl: (this: Meteor.MethodInvocation, layoutTemplate: LayoutTemplate<_, OrigB>) =>
          LayoutMethodResult<_, R>) => void;
    define<A3 extends EJSONable, R extends (EJSONable | void)>
      (method: MeteorMethod3<TablespaceId, string, A3, R>):
        (impl: (this: Meteor.MethodInvocation, layoutTemplate: LayoutTemplate<_, OrigB>, a3: A3) =>
          LayoutMethodResult<_, R>) => void;
    define<A3 extends {[A in AxisG<_>]: EJSONable}, R extends {[A in AxisG<_>]: EJSONable | void}>
      (method: MeteorAxisGenericLayoutMethod<_, A3, R>):
        (impl: <A extends Axis>(this: Meteor.MethodInvocation, layoutTemplate: LayoutTemplate<_, OrigB>, a3: A3[A]) =>
          LayoutMethodResult<_, R[A]>) => void;
  }
  Meteor.startup(() => {  // Load order for GenericIndexLayoutMethods
    GenericIndexLayoutMethods.prototype.define =
      function<_>(this: GenericIndexLayoutMethods<_>, method: MeteorMethodBase) {
      let gilm = this;
      return (impl: Function) => {
      // Untyped code; type-safe overload signatures.
      // tslint:disable-next-line:no-any
      defineModelMethodUntyped(method, function(this: Meteor.MethodInvocation, layoutName: string, ...args: any[]) {
        staticCheck(!$$.layoutsAreHardcoded(), () => "Layouts are hard-coded");
        const giLayouts = getLayoutsCollection(gilm.giWrapper);
        let spec = staticCheckNotNull(giLayouts.findOne(layoutName), () => "Nonexistent layout");
        let layoutTemplate = new LayoutTemplate(spec);
        // tslint:disable-next-line:no-any
        let result: LayoutMethodResult<_, any> = impl.call(this, layoutTemplate, ...args);
        let newSpec = result.newLayout;
        let newLayoutTemplate = new LayoutTemplate(newSpec);
        let newSpecCovered = coverLeaves(newLayoutTemplate);
        // Don't bother to validate again after covering the leaves: we trust that
        // step enough and want to save the time.
        giLayouts.update(layoutName, newSpecCovered);
        return result.response;
      });
      };
    };
  });

  Meteor.startup(() => giRun(<_>(w: GIWrapper<_>) => {
    const gilm = getGenericIndexLayoutMethods(w);

    function createInitialGrid(id: ColumnId) {
      const spec = emptyLayoutTemplateSpec<_, OrigB>(id);
      // Make an initial grid to give spreadsheet users somewhere to start.  In
      // the future, we may want to make this grid virtually infinite; that will
      // require implementation tricks.
      forAxes((a) => {
        spec.splits[a].set(randomTemplateSplitId<typeof a>(), {
          parentSpan: rootTemplateSpanId(DUMMY),
          mergedScope: mergedSpanOfSingleSpan(rootTemplateSpanId(DUMMY)),
          kind: TemplateSplitDefKind.FIXED,
          children: _.range(0, NUM_INITIAL_SPANS).map((i) => randomTemplateSpanId<typeof a>())
        });
      });
      let layoutTemplate = new LayoutTemplate(spec);
      return coverLeaves(layoutTemplate);
    }

    // We can't use defineLayoutMethod because this method is supposed to work
    // on an initially nonexistent layout.
    defineModelMethod(MeteorMethods.layoutInit)(() => {
      staticCheck(!$$.layoutsAreHardcoded(), () => "Layouts are hard-coded");
      const giLayouts = getLayoutsCollection(w);
      if (giLayouts.findOne(rootColumnId) != null) return;  // XXX Error?
      giLayouts.insert(createInitialGrid(rootColumnId));
    });

    defineModelMethod(MeteorMethods.layoutCreateViewObjectType)(() => {
      staticCheck(!$$.layoutsAreHardcoded(), () => "Layouts are hard-coded");
      const giLayouts = getLayoutsCollection(w);
      staticCheck(giLayouts.findOne(rootColumnId) != null, () => "Master data set layout does not exist yet");
      let viewObjectRoot = $$server.model.defineColumn({
        parentId: rootColumnId,
        index: -1,
        fieldName: null,
        specifiedType: SpecialType.TOKEN,
        isObject: true,
        objectName: null,
        formula: null,
        viewId: null,
        singular: false,
        isViewObjectRoot: true,
        userVisible: true
      });
      giLayouts.insert(createInitialGrid(viewObjectRoot));
      return viewObjectRoot;
    });

    defineModelMethod(MeteorMethods.layoutDeleteViewObjectType)((layoutName) => {
      staticCheck(!$$.layoutsAreHardcoded(), () => "Layouts are hard-coded");
      const giLayouts = getLayoutsCollection(w);
      let spec = staticCheckNotNull(giLayouts.findOne(layoutName), () => "Nonexistent layout");
      $$server.model.deleteViewObjectType(layoutName);
      giLayouts.remove(spec._id);
    });

    defineModelMethod(MeteorMethods.layoutChangeColumnFieldNameAndFormula)((columnId, fieldName, formula) => {
      $$server.model.changeColumnFieldName(columnId, fieldName);
      $$server.model.changeColumnFormula(columnId, formula, false);
    });

    defineModelMethod(MeteorMethods.layoutChangeMultipleColumnFormulas)((columnIdsAndFormulas) => {
      for (let cif of columnIdsAndFormulas) {
        $$server.model.changeColumnFormula(cif.columnId, cif.formula, false);
      }
    });

    gilm.define(gilm.layoutAddSplit)(
      (layoutTemplate, {splitAxis, parentSpan, mergedScope, isVariable}) => {
      type A = typeof splitAxis;

      let containerBinding = layoutTemplate.getExistingRectBinding(
        axisCons(splitAxis, parentSpan, mergedScope[Extreme.FIRST]));

      let prepareToClearResult = prepareToClearArea(layoutTemplate, axisCons(
        splitAxis, mergedSpanOfSingleSpan(parentSpan), mergedScope));

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);

      for (let id of prepareToClearResult.unusedAllocationIds) {
        deleteAllocation(newSpec, id);
      }

      let newSplitId = randomTemplateSplitId<A>();
      let newSplitDef: TemplateSplitDef<OrigB, A>;
      if (isVariable) {
        let newObjectType = $$server.model.defineColumn({
          parentId: fixmeAssertNotNull(containerBinding.trp).type,
          index: -1,
          fieldName: null,
          specifiedType: SpecialType.TOKEN,
          isObject: true,
          objectName: null,  // not named yet
          formula: null,
          viewId: null,
          singular: false,
          isViewObjectRoot: false,
          userVisible: true
        });
        newSplitDef = {
          parentSpan: parentSpan,
          mergedScope: mergedScope,
          kind: TemplateSplitDefKind.VARIABLE,
          child: randomTemplateSpanId<A>(),
          extraSpaceChild: randomTemplateSpanId<A>(),
          nestedObjectType: newObjectType
        };
      } else {
        newSplitDef = {
          parentSpan: parentSpan,
          mergedScope: mergedScope,
          kind: TemplateSplitDefKind.FIXED,
          // Initialize with two children by default.  The user probably
          // wouldn't insert a fixed split if they want only one child.
          children: [randomTemplateSpanId<A>(), randomTemplateSpanId<A>()]
        };
      }
      newSpec.splits[splitAxis].set(newSplitId, newSplitDef);

      return {
        response: {
          newSplitId
        },
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutDeleteSplit)(
      (layoutTemplate, {splitAxis, splitId}) => {
      let splitDef = staticCheckNotNull(layoutTemplate.spec.splits[splitAxis].get(splitId),
        () => "No split with the given ID");

      let descendants = findSplitDescendants(layoutTemplate, splitAxis, splitId);
      staticCheck(
        descendants.sameAxisSplitIds.length == 0 && descendants.otherAxisSplitIds.length == 0,
        () => "Please move or delete all nested collections and row/column groups first.");
      staticCheck(descendants.crossBindingRects.length == 0,
        () => "Deleting a collection with cross bindings is not yet supported.");

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      for (let child of archetypalChildrenOfTemplateSplit(splitDef)) {
        newSpec.spanStyles[splitAxis].delete(child);
      }
      for (let allocId of descendants.allocationIds) {
        let allocDef = assertNotNull(newSpec.allocations.get(allocId));
        if (allocDef.kind == TemplateAllocationDefKind.FIELD && splitDef.kind == TemplateSplitDefKind.VARIABLE &&
          allocDef.field == splitDef.nestedObjectType) {
          // Key field of the split we're deleting.  Delete the allocation but
          // not the schema column, which we delete below.
          newSpec.allocations.delete(allocId);
        } else if (allocationIsKeyField(allocDef)) {
          // We're trying to delete a fixed split that contains the key field of
          // the containing object.  No good.  This error will roll back any
          // deletions already performed.
          let fieldName = fieldNameForUI(getExistingColumn((<FieldAllocationDef<_, OrigB>>allocDef).field));
          throw new StaticError(`Please move the '${fieldName}' key field ` +
            `out of the ${uiTermForSplit(splitAxis, false)} first.`);
        } else {
          deleteAllocation(newSpec, allocId);
        }
      }

      if (splitDef.kind == TemplateSplitDefKind.VARIABLE) {
        $$server.model.deleteColumn(splitDef.nestedObjectType);
      }
      newSpec.splits[splitAxis].delete(splitId);
      return {
        response: undefined,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutMoveSplit)(
      (layoutTemplate, {splitAxis, splitId, newParentSpan, newMergedScope}) => {
      type A = typeof splitAxis;

      let splitDef = staticCheckNotNull(layoutTemplate.spec.splits[splitAxis].get(splitId),
        () => "No split with the given ID");
      let what = uiTermForSplit(splitAxis, splitDef.kind == TemplateSplitDefKind.VARIABLE);

      let scopeAxis = otherAxis(splitAxis);

      let descendants = findSplitDescendants(layoutTemplate, splitAxis, splitId);
      let previewCutSpec = previewCutSplit(layoutTemplate, splitAxis, splitId, descendants);
      let previewCutLayoutTemplate = new LayoutTemplate(previewCutSpec);

      try {
        previewCutLayoutTemplate.archetype.getParentSplitOfSpan(splitAxis, newParentSpan);
      } catch (e) {
        if (!(e instanceof StaticError)) throw e;
        // newParentSpan is not present in previewCutLayoutTemplate, so it must
        // have been a proper descendant of the split we're moving.
        //
        // It seems more helpful to the user to give this error priority over
        // "The new location does not belong to the same collection as the old
        // location".
        throw new StaticError(`Cannot move the ${what} into itself.`);
      }

      let oldContainer: RectangleId<_, TemplateS, OrigB> = axisCons(splitAxis, splitDef.parentSpan,
        layoutTemplate.archetype.containerOfMergedSpan(scopeAxis, splitDef.mergedScope));
      let newContainer: RectangleId<_, TemplateS, OrigB> = axisCons(splitAxis, newParentSpan,
        layoutTemplate.archetype.containerOfMergedSpan(scopeAxis, newMergedScope));
      checkMoveTarget(layoutTemplate, what, oldContainer, newContainer);

      // Checking this first makes the rest a little easier to understand.
      staticCheck(requireNoSplits(previewCutLayoutTemplate, splitAxis, newParentSpan, newMergedScope),
        () => "There are existing row/column groups or collections blocking the new location.  " +
          "Please move or delete them first.");
      let prepareClearResult = prepareToClearArea(previewCutLayoutTemplate,
        axisCons(splitAxis, mergedSpanOfSingleSpan(newParentSpan), newMergedScope));

      // Determine valid scope-axis spans at destination.
      let validOaSpansAtDestination = new Set<TemplateSpanId<OrigB, OtherAxis<A>>>();
      traverseMergedRect(previewCutLayoutTemplate, {
        preRect: (rect) => {
          validOaSpansAtDestination.add(rect[scopeAxis]);
        }
      }, axisCons(splitAxis, mergedSpanOfSingleSpan(newParentSpan), newMergedScope));
      // Any span defined by a split we're moving will remain available to
      // anything moving along with the split.
      let validOaSpansAfterMove = new Set<TemplateSpanId<OrigB, OtherAxis<A>>>();
      for (let oaSpan of validOaSpansAtDestination) {
        validOaSpansAfterMove.add(oaSpan);
      }
      for (let otherSplitId of descendants.otherAxisSplitIds) {
        for (let child of archetypalChildrenOfTemplateSplit(
          assertNotNull(layoutTemplate.spec.splits[scopeAxis].get(otherSplitId)))) {
          validOaSpansAfterMove.add(child);
        }
      }

      // Check that all descendants are valid at the destination: their scope-axis
      // spans are valid and they don't conflict with anything that already exists there.
      let invalidOaSpanMessage = (whatMoving: string) =>
        `A ${whatMoving} overlaps a ${axisSpanNames[scopeAxis]} ` +
        `that will no longer be inside the ${what}.  Please move or delete it first.`;
      for (let otherSplitId of descendants.sameAxisSplitIds) {
        let otherSplitDef = assertNotNull(layoutTemplate.spec.splits[splitAxis].get(otherSplitId));
        for (let e of extremes) {
          staticCheck(validOaSpansAfterMove.has(otherSplitDef.mergedScope[e]),
            () => invalidOaSpanMessage("nested collection or row/column group"));
        }
      }
      for (let otherSplitId of descendants.otherAxisSplitIds) {
        let otherSplitDef = assertNotNull(layoutTemplate.spec.splits[scopeAxis].get(otherSplitId));
        staticCheck(validOaSpansAfterMove.has(otherSplitDef.parentSpan),
          () => invalidOaSpanMessage("nested collection or row/column group"));
        if (validOaSpansAtDestination.has(otherSplitDef.parentSpan)) {
          let destRI = previewCutLayoutTemplate.archetype.getRectInfo(
            axisCons(splitAxis, newParentSpan, otherSplitDef.parentSpan));
          // XXX: This message is not the most specific.  It's a rare case.
          //
          // https://github.com/palantir/tslint/issues/3667
          // tslint:disable-next-line:strict-type-predicates
          staticCheck(destRI.coveringSplits[scopeAxis] == null,
            () => `A nested collection or row/column group that is moving along with the ${what} ` +
              `will overlap a collection or row/column group at the new location.`);
        }
      }
      let unusedAllocationsThatCannotMove = [];
      for (let allocId of descendants.allocationIds) {
        let allocDef = assertNotNull(layoutTemplate.spec.allocations.get(allocId));
        let container = layoutTemplate.containerOfTemplateAlloc(allocDef);
        let used = allocationIsNonLeaf(container, layoutTemplate) ||
          allocationContentIsUsed(allocDef);
        if (extremes.every((e) =>
          validOaSpansAfterMove.has(allocDef.mergedSpans[scopeAxis][e]))) {
          if (newParentSpan != splitDef.parentSpan) {
            // Check for overlap with used allocation at destination.
            // XXX: Repeated traversals may be inefficient.  Better ideas?
            let scopeAxisMergedSpanToCheck = allocDef.mergedSpans[scopeAxis];
            // If the allocation's scopeAxis span is defined by a descendant
            // split, then we need to check the nearest ancestor that exists at
            // the destination.  (If there is a used cell there, I believe we'll
            // get an "Allocation covers split end cap" error anyway, so we
            // could probably just skip the allocation overlap check in this
            // case if we wanted to.) ~ Matt 2018-03-27
            while (!validOaSpansAtDestination.has(scopeAxisMergedSpanToCheck[Extreme.FIRST])) {
              scopeAxisMergedSpanToCheck = mergedSpanOfSingleSpan(
                assertNotNull(layoutTemplate.archetype.getParentSplitOfSpan(
                  scopeAxis, scopeAxisMergedSpanToCheck[Extreme.FIRST])).def.parentSpan);
            }
            let overlapsUsedAtDest = false;
            traverseMergedRect(previewCutLayoutTemplate, {
              preRect: (rect) => {
                let ri = previewCutLayoutTemplate.archetype.getRectInfo(rect);
                if (ri.coveringAllocation != null &&
                  prepareClearResult.usedAllocationIds.has(
                    ri.coveringAllocation.def.templateAllocationId)) {
                  overlapsUsedAtDest = true;
                }
              }
            }, axisCons(splitAxis, mergedSpanOfSingleSpan(newParentSpan),
              scopeAxisMergedSpanToCheck));
            if (overlapsUsedAtDest) {
              staticCheck(!used,
                () => `A used cell that is moving along with the ${what} ` +
                `will overlap a used cell at the new location.`);
              unusedAllocationsThatCannotMove.push(allocId);
            }
          }
        } else {
          staticCheck(!used, () => invalidOaSpanMessage("used cell"));
          unusedAllocationsThatCannotMove.push(allocId);
        }
      }
      for (let crossBindingRect of descendants.crossBindingRects) {
        // XXX Revisit "cross binding" terminology once we support cross
        // bindings in structure editing.
        staticCheck(validOaSpansAfterMove.has(crossBindingRect[scopeAxis]),
          () => invalidOaSpanMessage("cross binding"));
      }

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      for (let allocId of unusedAllocationsThatCannotMove) {
        deleteAllocation(newSpec, allocId);
      }
      for (let allocId of prepareClearResult.unusedAllocationIds) {
        deleteAllocation(newSpec, allocId);
      }
      let newSplitDef = EJSON.clone(splitDef);
      newSplitDef.mergedScope = newMergedScope;
      newSplitDef.parentSpan = newParentSpan;
      newSpec.splits[splitAxis].set(splitId, newSplitDef);
      // And coverLeaves will do the rest!
      return {
        response: undefined,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutMakeAllocation)(
      (layoutTemplate, mergedRect) => {
      let prepareToClearResult = prepareToClearArea(layoutTemplate, mergedRect);
      staticCheck(prepareToClearResult.usedAllocationIds.size == 0,
        () => "The new cell overlaps cells that are in use.  Please move or delete them first.");

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      for (let id of prepareToClearResult.unusedAllocationIds) {
        deleteAllocation(newSpec, id);
      }
      addAllocation(layoutTemplate, newSpec, mergedRect);

      return {
        response: undefined,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutPurgeAllocation)(
      (layoutTemplate, allocId) => {
      staticCheck(layoutTemplate.spec.allocations.get(allocId) != null,
        () => "Nonexistent allocation");
      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      deleteAllocation(newSpec, allocId);
      return {
        response: undefined,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutMoveAllocation)(
      (layoutTemplate, {allocationId, newMergedSpans}) => {
      let allocationDef = staticCheckNotNull(layoutTemplate.spec.allocations.get(allocationId),
        () => "No allocation with the given ID");
      let oldContainer: RectangleId<_, TemplateS, OrigB> = perAxis((a) =>
        layoutTemplate.archetype.containerOfMergedSpan(a, allocationDef.mergedSpans[a]));
      let newContainer: RectangleId<_, TemplateS, OrigB> = perAxis((a) =>
        layoutTemplate.archetype.containerOfMergedSpan(a, newMergedSpans[a]));
      // Lucky us: if we wanted to say "allocation", then `checkMove` would need
      // a way to say "an" instead of "a". :/
      checkMoveTarget(layoutTemplate, "used cell", oldContainer, newContainer);

      let prepareToClearResult = prepareToClearArea(layoutTemplate, newMergedSpans);
      for (let otherAllocId of prepareToClearResult.usedAllocationIds) {
        if (otherAllocId != allocationId) {
          throw new StaticError(
            "The new location overlaps cells that are in use.  Please move or delete them first.");
        }
      }

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      for (let otherAllocId of prepareToClearResult.unusedAllocationIds) {
        deleteAllocation(newSpec, otherAllocId);
      }
      let newAllocationDef = EJSON.clone(allocationDef);
      newAllocationDef.mergedSpans = newMergedSpans;
      newSpec.allocations.set(allocationId, newAllocationDef);

      return {
        response: undefined,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutChangeAllocationKind)(
      (layoutTemplate, {allocationId, newKind}) => {
      let allocationDef = staticCheckNotNull(layoutTemplate.spec.allocations.get(allocationId),
        () => "No allocation with the given ID");
      if (allocationDef.kind == TemplateAllocationDefKind.LABEL)  // narrowing
        throw new StaticError("Cannot change allocation kind from LABEL");
      if (newKind == TemplateAllocationDefKind.LABEL)  // narrowing
        throw new StaticError("Cannot change allocation kind to LABEL");

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      let containerType = assertNotNull(layoutTemplate.getExistingRectBinding(perAxis((a) =>
        layoutTemplate.archetype.containerOfMergedSpan(a, allocationDef.mergedSpans[a]))).trp).type;
      let containerIsComputed = getExistingColumn(containerType).formula != null;

      if (newKind != allocationDef.kind) {

        // We preserve the formula of the field, hyperlink text, or button text,
        // though we delete and recreate the underlying fields to break
        // references and change user-visibility.
        let commonFormula: Formula;
        switch (allocationDef.kind) {
          case TemplateAllocationDefKind.FIELD:
            commonFormula = fallback(getExistingColumn(allocationDef.field).formula, DUMMY_FORMULA);
            $$server.model.deleteColumn(allocationDef.field);
            break;
          case TemplateAllocationDefKind.HYPERLINK:
            commonFormula = assertNotNull(getExistingColumn(allocationDef.textField).formula);
            $$server.model.deleteColumn(allocationDef.textField);
            $$server.model.deleteColumn(allocationDef.urlField);
            break;
          case TemplateAllocationDefKind.BUTTON:
            commonFormula = assertNotNull(getExistingColumn(allocationDef.textField).formula);
            $$server.model.deleteColumn(allocationDef.textField);
            break;
          default:
            return assertUnreachable(allocationDef);
        }
        let defCommon: AllocationDefCommon<_, TemplateS, OrigB> = {
          mergedSpans: allocationDef.mergedSpans,
          backgroundAllocationInfo: allocationDef.backgroundAllocationInfo,
          style: allocationDef.style,
        };
        let newAllocationDef: TemplateAllocationDef<_, OrigB>;
        switch (newKind) {
          case TemplateAllocationDefKind.FIELD: {
            let makeState = EJSON.equals(commonFormula, DUMMY_FORMULA) && !containerIsComputed;
            let field = $$server.model.defineColumn({
              parentId: containerType,
              index: -1,
              fieldName: null,
              specifiedType: makeState ? DEFAULT_STATE_FIELD_TYPE : null,
              isObject: false,
              objectName: null,
              formula: makeState ? null : commonFormula,
              viewId: null,
              singular: true,
              isViewObjectRoot: false,
              userVisible: true
            });
            newAllocationDef = {
              ...defCommon,
              kind: TemplateAllocationDefKind.FIELD,
              field
            };
            break;
          }
          case TemplateAllocationDefKind.HYPERLINK: {
            let textField = $$server.model.defineColumn({
              parentId: containerType,
              index: -1,
              fieldName: null,
              specifiedType: SpecialType.TEXT,
              isObject: false,
              objectName: null,
              formula: commonFormula,
              viewId: null,
              singular: true,
              isViewObjectRoot: false,
              userVisible: false
            });
            let urlField = $$server.model.defineColumn({
              parentId: containerType,
              index: -1,
              fieldName: null,
              specifiedType: SpecialType.TEXT,
              isObject: false,
              objectName: null,
              formula: DUMMY_FORMULA,
              viewId: null,
              singular: true,
              isViewObjectRoot: false,
              userVisible: false
            });
            newAllocationDef = {
              ...defCommon,
              kind: TemplateAllocationDefKind.HYPERLINK,
              textField,
              urlField
            };
            break;
          }
          case TemplateAllocationDefKind.BUTTON: {
            let textField = $$server.model.defineColumn({
              parentId: containerType,
              index: -1,
              fieldName: null,
              specifiedType: SpecialType.TEXT,
              isObject: false,
              objectName: null,
              formula: commonFormula,
              viewId: null,
              singular: true,
              isViewObjectRoot: false,
              userVisible: false
            });
            newAllocationDef = {
              ...defCommon,
              kind: TemplateAllocationDefKind.BUTTON,
              textField,
              procedureName: null
            };
            break;
          }
          default:
            return assertUnreachable(newKind);
        }

        newSpec.allocations.set(allocationId, newAllocationDef);
      }

      return {
        response: undefined,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutChangeAllocationButtonProcedure)(
      (layoutTemplate, {allocationId, newProcedureName}) => {
      let allocationDef = staticCheckNotNull(layoutTemplate.spec.allocations.get(allocationId),
        () => "No allocation with the given ID");
      if (allocationDef.kind != TemplateAllocationDefKind.BUTTON)  // narrowing
        throw new StaticError("Allocation kind must be BUTTON");

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      let newAllocationDef = EJSON.clone(allocationDef);
      newAllocationDef.procedureName = newProcedureName;
      newSpec.allocations.set(allocationId, newAllocationDef);

      return {
        response: undefined,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutInsertSpan)(
      (layoutTemplate, {splitAxis, neighbor, insertOnSide}) => {
      type A = typeof splitAxis;

      let splitId = staticCheckNotNull(layoutTemplate.archetype.getParentSplitOfSpan(splitAxis, neighbor),
        () => "Cannot insert a sibling of the root span.")
        .def.templateSplitId;
      let splitDef = assertNotNull(layoutTemplate.spec.splits[splitAxis].get(splitId));
      if (splitDef.kind != TemplateSplitDefKind.FIXED)  // narrowing
        throw new StaticError("Can only insert a child of a fixed split");
      let neighborIndex = splitDef.children.indexOf(neighbor);

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      let newSpanId = randomTemplateSpanId<A>();
      let newSplitDef = EJSON.clone(splitDef);
      newSplitDef.children.splice(
        neighborIndex + {[Extreme.FIRST]: 0, [Extreme.LAST]: 1}[insertOnSide], 0, newSpanId);
      newSpec.splits[splitAxis].set(splitId, newSplitDef);

      // Now extend the scope of any splits on the other axis (but not allocations).
      let scopeAxis = otherAxis(splitAxis);
      for (let [otherSplitId, otherSplitDef] of newSpec.splits[scopeAxis].entries()) {
        if (otherSplitDef.mergedScope[insertOnSide] ==
          wrapDoubleOtherAxis<_, RectangleId<_, TemplateS, OrigB>, A>(neighbor)) {
          let newOtherSplitDef = EJSON.clone(otherSplitDef);
          newOtherSplitDef.mergedScope[insertOnSide] =
            wrapDoubleOtherAxis<_, RectangleId<_, TemplateS, OrigB>, A>(newSpanId);
          newSpec.splits[scopeAxis].set(otherSplitId, newOtherSplitDef);
        }
      }

      return {
        response: newSpanId,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutDeleteSpan)(
      (layoutTemplate, params) => {
      let {splitAxis, spanToDelete} = params;
      type A = typeof splitAxis;
      let scopeAxis = otherAxis(splitAxis);
      let splitId = staticCheckNotNull(layoutTemplate.archetype.getParentSplitOfSpan(splitAxis, spanToDelete),
        () => "Cannot delete the root span.")
        .def.templateSplitId;
      let splitDef = assertNotNull(layoutTemplate.spec.splits[splitAxis].get(splitId));
      if (splitDef.kind != TemplateSplitDefKind.FIXED)  // narrowing
        throw new StaticError("Can only delete a child of a fixed split");
      let spanIndex = splitDef.children.indexOf(spanToDelete);
      staticCheck(!splitDef.children.every(
        (spanId2) => spanId2 == spanToDelete || isSpanStyleHidden(layoutTemplate.getSpanStyle(splitAxis, spanId2))),
        () => "Cannot delete the last unhidden child of a fixed split.");

      let prepareResult = prepareToDeleteSpan(layoutTemplate, params);

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      newSpec.spanStyles[splitAxis].delete(spanToDelete);
      let newSplitDef = EJSON.clone(splitDef);
      newSplitDef.children.splice(spanIndex, 1);
      newSpec.splits[splitAxis].set(splitId, newSplitDef);

      for (let allocId of prepareResult.allocationIdsToDelete) {
        deleteAllocation(newSpec, allocId);
      }
      for (let [allocId, newMergedSpans] of prepareResult.allocationsToResize.entries()) {
        let newAllocDef = EJSON.clone(assertNotNull(newSpec.allocations.get(allocId)));
        newAllocDef.mergedSpans = newMergedSpans;
        newSpec.allocations.set(allocId, newAllocDef);
      }
      for (let [otherSplitId, newMergedScope] of prepareResult.splitsToResize.entries()) {
        let newOtherSplitDef = EJSON.clone(assertNotNull(newSpec.splits[scopeAxis].get(otherSplitId)));
        newOtherSplitDef.mergedScope =
          wrapDoubleOtherAxis<_, MergedRectId<_, TemplateS, OrigB>, A>(newMergedScope);
        newSpec.splits[scopeAxis].set(otherSplitId, newOtherSplitDef);
      }

      return {
        response: undefined,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutPromote)((layoutTemplate, {allocationId, axis}) => {
      type A = typeof axis;
      let oa = otherAxis(axis);
      let allocDef = staticCheckNotNull(layoutTemplate.spec.allocations.get(allocationId),
        () => "No allocation with the given ID");
      if (allocDef.kind != TemplateAllocationDefKind.FIELD)  // narrowing
        throw new StaticError("Can only promote a field allocation");
      let columnId = allocDef.field;

      let parentSpan = staticCheckNotNull(singleOfPerExtreme(allocDef.mergedSpans[axis]),
        () => "Cannot promote an allocation that is merged on the promotion axis.");
      staticCheck(requireNoSplits(layoutTemplate, axis, parentSpan, allocDef.mergedSpans[oa]),
        () => "There are existing row/column groups or collections blocking the new collection.  " +
          "Please move or delete them first.");

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      // Leave the object type unnamed for consistency with the rest of the custom layout UI.
      $$server.model.changeColumnIsObject(columnId, true, /*objectName*/ null);
      let col = getExistingColumn(columnId);
      let childColumnId;
      if (getColumnType(col) == SpecialType.TOKEN) {
        childColumnId = col.children[0];
        $$server.model.changeColumnIsSingular(childColumnId, true);
      } else {
        childColumnId = columnId;
      }

      let newSplitId = randomTemplateSplitId<A>();
      let newSplitDef: VariableSplitDef<OrigB, A> = {
        parentSpan,
        mergedScope: allocDef.mergedSpans[oa],
        kind: TemplateSplitDefKind.VARIABLE,
        child: randomTemplateSpanId<A>(),
        extraSpaceChild: randomTemplateSpanId<A>(),
        nestedObjectType: columnId
      };
      newSpec.splits[axis].set(newSplitId, newSplitDef);

      let newAllocDef = EJSON.clone(allocDef);
      newAllocDef.mergedSpans[axis] = mergedSpanOfSingleSpan(newSplitDef.child);
      newAllocDef.field = childColumnId;
      newSpec.allocations.set(allocationId, newAllocDef);

      return {
        response: undefined,
        newLayout: newSpec
      };
    });

    gilm.define(gilm.layoutDemote)((layoutTemplate, {splitAxis, splitId}) => {
      let splitDef = staticCheckNotNull(layoutTemplate.spec.splits[splitAxis].get(splitId),
        () => "No split with the given ID");
      if (splitDef.kind != TemplateSplitDefKind.VARIABLE)  // narrowing
        throw new StaticError("Can only demote a variable split");
      let descendants = findSplitDescendants(layoutTemplate, splitAxis, splitId);
      let body: MergedRectId<_, TemplateS, OrigB> =
        axisCons(splitAxis, mergedSpanOfSingleSpan(splitDef.child), splitDef.mergedScope);
      let coveringAlloc = staticCheckNotNull(layoutTemplate.archetype.coveringAllocationOfMergedRect(body),
        () => "To demote a variable split, its body must be covered by a single allocation.");
      let columnId = splitDef.nestedObjectType;
      let col = getExistingColumn(columnId);
      let objectTypeName = objectNameForUI(col);

      // Note: descendants.allocationIds will also include allocations in
      // templateSplit.extraSpaceChild, which we don't care about.
      staticCheck(descendants.sameAxisSplitIds.length == 0 &&
        descendants.otherAxisSplitIds.length == 0 &&
        descendants.crossBindingRects.length == 0 &&
        EJSON.equals(coveringAlloc.def.mergedSpans, body),
        () => `Before the ${objectTypeName} collection can be reduced to a single cell, ` +
          `each ${objectTypeName} must consist of a single cell.`);

      // changeColumnIsSingular(columnId, true) below checks for this, but we
      // want to give a better error message.
      if (col.formula == null && col.viewObjectType == null) {
        let pluralFamilies = pluralFamiliesInStateColumn(columnId);
        if (pluralFamilies.length > 0) {
          let parentObjectTypeName = objectNameForUI(getExistingColumn(col.parent));
          let firstQFamilyId = qFamilyIdOfFamilyDoc(pluralFamilies[0]);
          let desc = valueToText(getReadableModel(), col.parent, firstQFamilyId.parentCellId);
          throw new StaticError(
            `Cannot reduce ${objectTypeName} collection to a cell because ` +
            `${pluralFamilies.length} ${parentObjectTypeName} object(s), including '${desc}', ` +
            `contain more than one ${objectTypeName} object.`);
        }
      }

      let allocId = coveringAlloc.def.templateAllocationId;

      let newSpec = shallowishCloneLayoutTemplateSpec(layoutTemplate.spec);
      $$server.model.changeColumnIsObject(columnId, false);
      $$server.model.changeColumnIsSingular(columnId, true);
      for (let child of archetypalChildrenOfTemplateSplit(splitDef)) {
        newSpec.spanStyles[splitAxis].delete(child);
      }
      newSpec.splits[splitAxis].delete(splitId);
      for (let otherAllocId of descendants.allocationIds) {
        if (otherAllocId != allocId) {
          assert(assertNotNull(newSpec.allocations.get(otherAllocId)).kind == TemplateAllocationDefKind.LABEL);
          newSpec.allocations.delete(otherAllocId);
        }
      }
      let newAllocDef = EJSON.clone(assertNotNull(layoutTemplate.spec.allocations.get(allocId)));
      if (newAllocDef.kind != TemplateAllocationDefKind.FIELD)  // narrowing
        throw new Error("Should not happen");
      newAllocDef.mergedSpans[splitAxis] = mergedSpanOfSingleSpan(splitDef.parentSpan);
      newAllocDef.field = columnId;  // relevant when demoting an unkeyed object type
      newSpec.allocations.set(allocId, newAllocDef);

      return {
        response: undefined,
        newLayout: newSpec
      };
    });

  }));

}
