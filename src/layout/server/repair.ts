namespace Objsheets.Layout {

  // Helper to generate EJSONable "case classes", i.e., simple classes that wrap
  // object literal types but have prototypes that can be tested with
  // instanceof.  The main use case is to define a sum type as a union of case
  // classes.  It's arguable whether our design is the cleanest solution to that
  // problem, but it works for now.  (The term "case class" is borrowed from
  // Scala, though these case classes don't have exactly the same features as
  // Scala ones.)

  // This is the only way to merge Data into the actual type.  `class
  // EJSONableCaseClassBaseUntyped<Data> extends Data` is not allowed.
  interface EJSONableCaseClassConstructor<Data extends EJSONable> {
    new(data: Data): Data & EJSONableCaseClassBaseUntyped;
    prototype: Data & EJSONableCaseClassBaseUntyped;
    registerEJSONType(typeName: string): void;
  }
  class EJSONableCaseClassBaseUntyped {
    private static typeName: undefined | string;
    static registerEJSONType(typeName: string) {
      assert(this.typeName == null);
      this.typeName = typeName;
      EJSON.addType(typeName, (val) => new this(EJSON.fromJSONValue(val)));
    }
    constructor(data: {}) {
      Object.assign(this, data);
    }
    typeName() {
      // XXX: Is there a fundamental reason why this cast is necessary or has
      // TypeScript just not implemented the required special logic?
      let subclass = <typeof EJSONableCaseClassBaseUntyped>this.constructor;
      return assertNotNull(subclass.typeName);
    }
    toJSONValue(): JSONable {
      return EJSON.toJSONValue(Object.assign({}, this));
    }
  }
  function getEJSONableCaseClassBase<Data extends EJSONable>() {
    return <EJSONableCaseClassConstructor<Data>><fixmeAny>EJSONableCaseClassBaseUntyped;
  }

  type UnknownAxis = Axis;

  class OldLabelAllocationDef extends getEJSONableCaseClassBase<AllocationDefCommon<never, TemplateS, OrigB> & {
    text: string;
  }>() {}
  OldLabelAllocationDef.registerEJSONType("Layout.LabelAllocationDef");

  class OldFieldAllocationDef extends getEJSONableCaseClassBase<AllocationDefCommon<never, TemplateS, OrigB> & {
    field: ColumnId /* or self for key */;
  }>() {}
  OldFieldAllocationDef.registerEJSONType("Layout.FieldAllocationDef");

  type OldTemplateAllocationDef = OldLabelAllocationDef | OldFieldAllocationDef;

  class OldFixedSplitDef extends getEJSONableCaseClassBase<SplitDefCommon<TemplateS, OrigB, UnknownAxis> & {
    children: TemplateSpanId<OrigB, UnknownAxis>[];
  }>() {}
  OldFixedSplitDef.registerEJSONType("Layout.FixedSplitDef");

  class OldVariableSplitDef extends getEJSONableCaseClassBase<SplitDefCommon<TemplateS, OrigB, UnknownAxis> & {
    child: TemplateSpanId<OrigB, UnknownAxis>;
    extraSpaceChild: TemplateSpanId<OrigB, UnknownAxis>;
    nestedObjectType: ColumnId;
  }>() {}
  OldVariableSplitDef.registerEJSONType("Layout.VariableSplitDef");

  type OldTemplateSplitDef = OldFixedSplitDef | OldVariableSplitDef;

  type MaybeOldTemplateSplitDef<A extends AxisL> = TemplateSplitDef<OrigB, A> | OldTemplateSplitDef;
  type MaybeOldTemplateAllocationDef<_> = TemplateAllocationDef<_, OrigB> | OldTemplateAllocationDef;

  export type MaybeOldLayoutTemplateSpec<_> = {
    _id: ColumnId;
    splits: {[A in AxisG<_>]:
      JSONKeyedMap<TemplateSplitId<OrigB, A>, MaybeOldTemplateSplitDef<A>>};
    allocations: JSONKeyedMap<TemplateAllocationId<OrigB>, MaybeOldTemplateAllocationDef<_>>;
    crossBindings: JSONKeyedMap<RectangleId<_, TemplateS, OrigB>, CrossBinding>;
    spanStyles: {[A in AxisG<_>]: JSONKeyedMap<TemplateSpanId<OrigB, A>, SpanStyle>};
  };

  function repairSplitDef<A extends AxisL>(oldDef: MaybeOldTemplateSplitDef<A>): TemplateSplitDef<OrigB, A> {
    if (oldDef instanceof OldFixedSplitDef) {
      return <FixedSplitDef<OrigB, A>>{...oldDef, kind: TemplateSplitDefKind.FIXED};
    } else if (oldDef instanceof OldVariableSplitDef) {
      return <VariableSplitDef<OrigB, A>>{...oldDef, kind: TemplateSplitDefKind.VARIABLE};
    } else {
      return oldDef;
    }
  }

  function repairAllocationDef<_>(oldDef: MaybeOldTemplateAllocationDef<_>) {
    if (oldDef instanceof OldLabelAllocationDef) {
      return <LabelAllocationDef<_, OrigB>>{...oldDef, kind: TemplateAllocationDefKind.LABEL};
    } else if (oldDef instanceof OldFieldAllocationDef) {
      return <FieldAllocationDef<_, OrigB>>{...oldDef, kind: TemplateAllocationDefKind.FIELD};
    } else {
      return oldDef;
    }
  }

  export function repairLayoutTemplateSpec<_>(oldSpec: MaybeOldLayoutTemplateSpec<_>): LayoutTemplateSpec<_, OrigB> {
    return {
      _id: oldSpec._id,
      splits: perAxis((a) => new JSONKeyedMap(oldSpec.splits[a].entries().map
        <[TemplateSplitId<OrigB, typeof a>, TemplateSplitDef<OrigB, typeof a>]>
        (([id, def]) => [id, repairSplitDef(def)]))),
      allocations: new JSONKeyedMap(oldSpec.allocations.entries().map
        <[TemplateAllocationId<OrigB>, TemplateAllocationDef<_, OrigB>]>
        (([id, def]) => [id, repairAllocationDef(def)])),
      crossBindings: oldSpec.crossBindings,
      spanStyles: oldSpec.spanStyles
    };
  }

}
