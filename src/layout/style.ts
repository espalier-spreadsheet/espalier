namespace Objsheets.Layout {

  export enum TextOrientation {
    SidewaysLeft = "sidewaysLeft",
    SidewaysRight = "sidewaysRight",
    Normal = "normal",
  }

  export enum TextAlignment {
    Left = "left",
    Right = "right",
    Center = "center",
  }

  export type BorderStyle = {
    color?: string;
    visibility?: boolean;
    isDouble?: boolean;
  };

  export type FieldNameLabelInfo = {
    expansionAxis: null | Axis;
    fieldName: string;
  };

  export enum ObjectStatus {
    NORMAL, NONEXISTENT, ERROR
  }

  // Information about special statuses of areas of the sheet used to draw
  // special default backgrounds.  We do not indicate a status if there is
  // another status already covering a superset of the area.
  export type AllocStatus = {
    spanObjectStatus: IndependentPerAxis<ObjectStatus>;
    areaIsUnbound: boolean;
    crossObjectStatus: ObjectStatus;
  };
  export function allocStatusIsNormal(allocStatus: AllocStatus) {
    return axesEvery((a) => allocStatus.spanObjectStatus[a] == ObjectStatus.NORMAL) &&
      !allocStatus.areaIsUnbound && allocStatus.crossObjectStatus == ObjectStatus.NORMAL;
  }

  export type AllocationStyle = {
    // For now, we just insert this string directly into a CSS
    // "background-color" rule.
    backgroundColor?: string;
    isTextItalic?: boolean;
    isTextBold?: boolean;
    isTextUnderlined?: boolean;
    textAlignment?: TextAlignment;
    textFont?: string;
    fontSize?: string;
    textColor?: string;
    textOrientation?: TextOrientation;
    // Code in rendering.tsx assumes that if the borderStyle property is set at all,
    // then it contains at least an empty BorderStyle object for each of the four borders.
    borderStyle?: IndependentPerAxis<PerExtreme<BorderStyle>>;

    // To be used by design view only.
    fieldNameLabelInfo?: FieldNameLabelInfo;

    // To be set by layout template instantiation only.  Will never be
    // undefined in a data layout.
    specialTextColor?: null | string;
    allocStatus?: AllocStatus;
    isImageEditable?: boolean; //defined only for image cells
  };

  export function textOrientationOfAllocationStyle(allocStyle: AllocationStyle) {
    return fallback(allocStyle && allocStyle.textOrientation, TextOrientation.Normal);
  }

  export type SpanStyle = {
    // If true, this is an "extra space" span that is exempt from the normal
    // minimum span size and can shrink to zero size.  Currently cannot be set
    // directly in a layout template.
    isExtraSpace?: boolean;
    // If true, this span is hidden when not in design view.  In the original
    // layout template, only children of fixed splits can be hidden, and it is
    // not allowed to hide all children of a fixed split.
    //
    // When design view is disabled, the original layout template runs through a
    // transformation that removes all hidden spans and their descendants.  In
    // design view, this property causes spans to be marked specially on screen
    // but does not actually hide them, and in support of the marking, the
    // design view transformation propagates the property down the hierarchy.
    //
    // FIXME: Hidden data is still sent to the client, which is misleading from
    // a security point of view.
    hide?: boolean;
    minSize?: number;
  };

  // XXX Be consistent about whether the parameter is nullable.
  export function isSpanStyleHidden(spanStyle: SpanStyle) {
    return fallback(spanStyle.hide, false);
  }

  export type BackgroundAllocationInfo = {
    zIndex: number;
  };

}
