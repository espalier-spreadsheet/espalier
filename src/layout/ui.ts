namespace Objsheets.Layout {

  // E.g., {designView: "1"}
  export type ViewObjectQueryParams = {[k: string]: string};

  // URL is relative to the "layouts" dir of the sheet.
  export function uiUrl(layoutName: ColumnId, dump: null | ViewObjectDump, queryParams: ViewObjectQueryParams) {
    let paramsWithDump: {[k: string]: string} = {
      ...(dump == null ? {} : {viewObjectData: JSON.stringify(dump)}),
      ...queryParams
    };
    let keys = Object.keys(paramsWithDump);
    return encodeURIComponent(layoutName) +
      (keys.length > 0 ? "?" + keys.map((k) =>
        encodeURIComponent(k) + "=" + encodeURIComponent(paramsWithDump[k])).join("&") : "");
  }

}
