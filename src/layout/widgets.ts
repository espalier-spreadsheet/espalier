namespace Objsheets.Layout {

  export type Hyperlink = {
    type: "hyperlink",
    url: string,
    text: string
  };
  export type Button = {
    type: "button",
    // This definitely isn't the final design we want: the procedure should
    // belong to the button.  We'll revisit this when we build the procedure
    // editor.
    procedureName: string,
    thisObj: QCellId,
    text: string
  };
  export type Image = {
    type: "image",
    dataurl: string | undefined;
  };
  export type AllocationContent = string | Hyperlink | Button | Image;

}
