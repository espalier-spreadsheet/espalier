namespace Objsheets {

  export class HtmlOption {
    // Help Blaze {{#each}} keep track of items as the menu changes.  Without
    // this, we saw a bug where the wrong option would be selected in the type
    // menu in the custom layout UI.
    public _id: string;
    constructor(public value: string, public label: string) {
      this._id = "option:" + value;
    }
  }

  export class HtmlOptgroup {
    public _id: string;
    constructor(public label: string, public members: HtmlOption[]) {
      this._id = "group:" + label;
    }
  }

  export type HtmlSelectItem = HtmlOption | HtmlOptgroup;

  export class HtmlSelectData {
    constructor(public items: HtmlSelectItem[], public currentValue: string) {}
  }

  // Since we aren't generating the <select> element itself, the event goes to the
  // parent template and we don't have to deal with the nonsense of making a
  // reference between the templates.  Yay!

  let htmlSelectContentTemplate: Blaze.Template = Template["html_select_content"];
  htmlSelectContentTemplate.helpers<HtmlSelectData>({
    currentValueFound: function() {
      for (let item of this.items) {
        if (item instanceof HtmlOptgroup) {
          for (let subitem of item.members) {
            if (this.currentValue === subitem.value) {
              return true;
            }
          }
        } else {
          if (this.currentValue === item.value) {
            return true;
          }
        }
      }
      return false;
    }
  });
  htmlSelectContentTemplate.helpers<HtmlSelectItem>({
    isOptgroup: function() {
      return this instanceof HtmlOptgroup;
    }
  });
  htmlSelectContentTemplate.helpers<HtmlOption>({
    isSelected: function() {
      let parent: HtmlSelectData | HtmlOptgroup = Template.parentData(1);
      let selectData = (parent instanceof HtmlSelectData) ? parent : <HtmlSelectData>Template.parentData(2);
      return this.value === selectData.currentValue;
    }
  });

  export function selectOptionWithValue(template: Blaze.TemplateInstance, selectSelector: string, value: string) {
    (<HTMLOptionElement>template.find(`${selectSelector} option[value=${value}]`)).selected = true;
  }

  export function getValueOfSelectedOption(template: Blaze.TemplateInstance, selectSelector: string): string {
    return template.$(selectSelector).val();
  }

}
