namespace Objsheets {

  export interface MeteorPublication0 {
    subscribe(callbacks?: Meteor.SubscriptionCallbacksArg): Meteor.SubscriptionHandle;
  }

  // Put NotCovariant in the client file: it's silly but harmless to treat
  // MeteorPublicationN as covariant in the server unibuild, though the
  // all-in-one project will flag an error anyway.
  export interface MeteorPublication1<A1 extends EJSONable> extends NotCovariant<A1> {
    subscribe(a1: A1, callbacks?: Meteor.SubscriptionCallbacksArg): Meteor.SubscriptionHandle;
  }

  export interface MeteorPublication2<A1 extends EJSONable, A2 extends EJSONable> extends NotCovariant<A1, A2> {
    subscribe(a1: A1, a2: A2, callbacks?: Meteor.SubscriptionCallbacksArg): Meteor.SubscriptionHandle;
  }

  export interface MeteorPublication3<A1 extends EJSONable, A2 extends EJSONable,
    A3 extends EJSONable> extends NotCovariant<A1, A2, A3> {
    subscribe(a1: A1, a2: A2, a3: A3, callbacks?: Meteor.SubscriptionCallbacksArg): Meteor.SubscriptionHandle;
  }

  // Minimal TypeScript wrapper
  export abstract class ReactMeteorDataPureComponent<P, D, S> extends React.PureComponent<P, S> {
    // XXX: `data` is an empty object during the first call to `getMeteorData`.
    data: undefined | D;
    abstract getMeteorData(): D;
    // strictNullChecks workaround.
    getData() {
      return assertNotNull(this.data);
    }
    // Promise TypeScript that this is defined for the call from
    // SelectiveReactMeteorDataPureComponent.  Overwritten by the Object.assign
    // below.
    componentWillUpdate(nextProps: P, nextState: S, nextContent: fixmeAny /* ?? */) {
      throw new Error("not reached");
    }
  }
  Object.assign(ReactMeteorDataPureComponent.prototype, ReactMeteorData);

  export abstract class SelectiveReactMeteorDataPureComponent<P, D, S> extends ReactMeteorDataPureComponent<P, D, S> {
    abstract shouldGetMeteorData(nextProps: P, nextState: S): boolean;
    abstract getMeteorDataReal(): D;
    private trackerInvalidated: undefined | boolean;
    getMeteorData() {
      this.trackerInvalidated = false;
      Tracker.onInvalidate(() => this.trackerInvalidated = true);
      return this.getMeteorDataReal();
    }
    componentWillUpdate(nextProps: P, nextState: S, nextContent: fixmeAny /* ?? */) {
      if (assertNotNull(this.trackerInvalidated) || this.shouldGetMeteorData(nextProps, nextState)) {
        super.componentWillUpdate(nextProps, nextState, nextContent);
      }
    }
  }

}
