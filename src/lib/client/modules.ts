// See explanation in `src/lib/server/modules.ts`.

declare module "dummy" {
  import * as SpinJs from "spin.js";
  global {
    namespace ExternalModules {
      export import Spinner = SpinJs.Spinner;
    }
  }
}

let withTracker: <P, D>(getMeteorData: (props: P) => D) =>
  (component: React.ComponentType<P & D>) => React.ComponentType<P>
  // Note: Npm.require does not seem to work, and `require` requires that the
  // app have a direct dependency on the `modules` Meteor package.  There's no
  // way to get `withTracker` without `require` since (by mistake?) the only
  // legacy export of the `react-meteor-data` package is `ReactMeteorData`.
  = require("meteor/react-meteor-data").withTracker;

let BlazeComponent = require("meteor/gadicc:blaze-react-component").default;
