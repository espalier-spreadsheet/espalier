namespace Objsheets {

  // Data context for the React wrapper Blaze template provided by the
  // react-template-helper Meteor package.
  export type ReactWrapperTemplateData<P> = P & {component: React.ComponentType<P>};

}
