namespace Objsheets {

  Template.registerHelper("not", (x: boolean) => !x);

  export function findAncestorTemplateInstanceOfView(view: Blaze.View, templateName: string) {
    while (view.name != "Template." + templateName) {
      view = view.parentView;
    }
    return view.templateInstance();
  }

  // Future: Make this better.
  export function standardServerCallback<R>(error: null | Error, result: R) {
    if (error != null) {
      alert("The operation failed on the server: " + error.message);
    }
  }

  export function standardServerCallbackThen<R>(callback: MeteorCallback<R>): MeteorCallback<R> {
    return (error, result) => {
      standardServerCallback(error, result);
      callback(error, result);
    };
  }

  export const MOUSE_BUTTON_LEFT = 0, MOUSE_BUTTON_RIGHT = 2;

  // modifiedKeyOfKeyEvent: Helper function that makes it easy to listen for a
  // keystroke with a unique set of modifiers.

  // Matt's interpretation. https://www.w3.org/TR/uievents-key/#keys-modifier
  let allTransientModifiers =
    ["Alt", "AltGraph", "Control", "Fn", "Hyper", "Meta", "Shift", "Super", "Symbol"];
  export function modifiedKeyOfKeyEvent(event: React.KeyboardEvent<Element>) {
    let parts = [];
    for (let m of allTransientModifiers) {
      if (m != event.key && event.getModifierState(m))
        parts.push(m);
    }
    parts.push(event.key);
    return parts.join("-");
  }

  let nonTypingTransientModifiers =
    allTransientModifiers.filter((m) => m != "Shift" && m != "AltGraph");
  export function charTypedOfKeyEvent(event: React.KeyboardEvent<Element>) {
    // XXX: Explicit way to distinguish character(s) typed from a special key?
    // The UI Events spec apparently doesn't have one, which is ridiculous.
    //
    // As per https://www.w3.org/TR/uievents/#keys-guidelines, event.key can
    // still be a printable character even if there were modifier keys that made
    // the keystroke nonprintable.  Guess that any modifier key other than Shift
    // or AltGraph makes a keystroke nonprintable.
    //
    // In Firefox in Matt's configuration, the Super modifier slips by this
    // check, but it also slips by Firefox's built-in check for whether to start
    // entering a term to find in the page. :/
    for (let m of nonTypingTransientModifiers)
      if (event.getModifierState(m))
        return null;
    return (event.key.length == 1) ? event.key : null;
  }

}
