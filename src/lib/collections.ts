namespace Objsheets {

  // Simplified collection interfaces implemented by our wrapper collections.

  type SelectorArg<T> = Mongo.Selector<T> | Mongo.ObjectID | string;
  type FindOneOptions = {
    sort?: Mongo.SortSpecifier;
    skip?: number;
    fields?: Mongo.FieldSpecifier;
    reactive?: boolean;
    transform?: Function;
  };
  type FindOptions = FindOneOptions & { limit?: number; };

  // Read operations copied from the Mongo.Collection<T> interface.
  export interface SimpleReadOnlyCollection<T extends Mongo.Doc> {
    find(selector?: SelectorArg<T>, options?: FindOptions): Mongo.Cursor<T>;
    findOne(selector?: SelectorArg<T>, options?: FindOneOptions): undefined | T;
    readonly _name: string;
  }

  // Read and write (but not administration) operations copied from the
  // Mongo.Collection<T> interface, except we don't allow the use of callbacks.
  export interface SimpleCollection<T extends Mongo.Doc> extends SimpleReadOnlyCollection<T>, NotCovariant<T> {
    insert(doc: Mongo.DocWithOptionalId<T>): string;
    remove(selector: SelectorArg<T>): number;
    update(selector: SelectorArg<T>, modifier: Mongo.Modifier<T>, options?: {
      multi?: boolean;
      upsert?: boolean;
    }): number;
    upsert(selector: SelectorArg<T>, modifier: Mongo.Modifier<T>, options?: {
      multi?: boolean;
    }): {
      numberAffected?: number; insertedId?: string;
    };
  }

  // After taking a type intersection, flatten it for accurate checking of EJSON-ability.
  // https://stackoverflow.com/a/51330053
  export type Flatten<T> = {[P in keyof T]: T[P]};

  // An in-memory cache of an underlying collection that writes through to the
  // underlying collection and notifies observers synchronously (unlike
  // Mongo.Collection).  Needed to maintain order of changes for transactional
  // publishing.
  export class SynchronousObserveCollection<T extends Mongo.DocWithStringId> implements SimpleCollection<T> {
    private back: SimpleCollection<T>;
    private cache: Mongo.Collection<T>;

    constructor(back: SimpleCollection<T>) {
      this.back = back;
      this.cache = newLocalCollection<T>(back._name);
      back.find().forEach((doc) => this.cache.insert(doc));
    }
    get _name() { return this.back._name; }
    // Used by ServerTablespace to compare the content of this collection
    // (serving as the committed collection) to the current collection.
    get _docs() { return this.cache._collection._docs; }
    find(selector?: SelectorArg<T>, options?: FindOptions) {
      if (arguments.length == 0) {
        // Note: this.cache.find(undefined) finds nothing!
        return this.cache.find();
      } else {
        return this.cache.find(selector, options);
      }
    }
    findOne(selector?: SelectorArg<T>, options?: FindOneOptions) {
      if (arguments.length == 0) {
        return this.cache.findOne();
      } else {
        return this.cache.findOne(selector, options);
      }
    }
    insert(doc: Mongo.DocWithOptionalId<T>) {
      // XXX If the second call throws an exception, the collections will get
      // out of sync.  Is there an easy way to ensure that won't happen?
      let id = this.back.insert(doc);
      if (doc._id == null) {
        doc = _.clone(doc);
        doc._id = id;
      }
      this.cache.insert(doc);
      return id;
    }
    update(selector: SelectorArg<T>,
      modifier: Mongo.Modifier<T>, options?: {multi?: boolean, upsert?: boolean}) {
      if (options && options.upsert)
        throw new Error("SynchronousObserveCollection.update with upsert = true is not implemented.");
      let num;
      if ((options && options.multi) || typeof selector == "string") {
        num = this.back.update(selector, modifier, options);
        this.cache.update(selector, modifier, options);
      } else {
        // Careful... an update that matches multiple documents without multi is
        // nondeterministic.  Don't let the collections get out of sync.
        // (Objsheets should never issue such an update anyway, but it's just as
        // easy to handle the scenario correctly as to raise an error.)
        let doc = this.cache.findOne(selector);
        if (doc != null) {
          num = this.back.update(doc._id, modifier, options);
          this.cache.update(doc._id, modifier, options);
        } else {
          num = 0;
        }
      }
      return num;
    }
    upsert(selector: SelectorArg<T>,
      modifier: Mongo.Modifier<T>, options?: { multi?: boolean; }) {
      // To support arbitrary selectors, we'd have to deal with the
      // nondeterminism of updating one of several matching documents as well as
      // generating an ID for an inserted document.  It wouldn't be terrible,
      // but we don't need it, so don't do it yet.
      if (typeof selector != "string")
        throw new Error("SynchronousObserveCollection.upsert is only implemented " +
          "when the selector is an ID in string form.");
      let ret = this.back.upsert(selector, modifier, options);
      this.cache.upsert(selector, modifier, options);
      return ret;
    }
    remove(selector: SelectorArg<T>) {
      let num = this.back.remove(selector);
      this.cache.remove(selector);
      return num;
    }
  }

  export class ReadOnlyCollectionWrapper<T extends Mongo.Doc> implements SimpleReadOnlyCollection<T> {
    private back: SimpleReadOnlyCollection<T>;

    constructor(back: SimpleReadOnlyCollection<T>) {
      this.back = back;
    }
    get _name() { return this.back._name; }
    find(selector?: SelectorArg<T>, options?: FindOptions) {
      if (arguments.length == 0) {
        // Note: this.back.find(undefined) finds nothing!
        return this.back.find();
      } else {
        return this.back.find(selector, options);
      }
    }
    findOne(selector?: SelectorArg<T>, options?: FindOneOptions) {
      if (arguments.length == 0) {
        return this.back.findOne();
      } else {
        return this.back.findOne(selector, options);
      }
    }
  }

  let IndependentLocalCollectionDriver = {
    open: (name: string, conn: fixmeAny) => new LocalCollection(name)
  };

  export function newLocalCollection<T extends Mongo.DocWithStringId>(name: string): Mongo.Collection<T> {
    // The default LocalCollectionDriver reuses the minimongo collection for
    // each name.  And using `new LocalCollection()` directly bypasses wrapper
    // code in Mongo.Collection that we need to handle
    // `collection.upsert("myId", ...)` and maybe other cases as well.
    return new Mongo.Collection<T>(name, {connection: null, _driver: IndependentLocalCollectionDriver});
  }

  // LocalCollection.observe* calls _SynchronousQueue.drain in an attempt to
  // ensure that the initial results are delivered before observe* returns while
  // allowing observe callbacks to make further changes to the same collection.
  // However, _SynchronousQueue.drain may not work if called concurrently by
  // multiple fibers (e.g., serving different clients subscribing to the same
  // collection).  This caused transactional publishing failures.  Since we
  // don't change the same collection from observe callbacks, just remap drain
  // to flush. (LocalCollection.observe* is currently the only thing that uses
  // it.)  This will also avoid indefinite postponement due to unrelated
  // concurrent activity on a collection.
  (<fixmeAny>Meteor)._SynchronousQueue.prototype.drain = (<fixmeAny>Meteor)._SynchronousQueue.prototype.flush;

  // Cache documents to provide a faster path for lookups by ID than minimongo,
  // especially for reactive lookups, which would otherwise do an
  // `observeChanges` and extra work during `pauseObservers` and
  // `resumeObservers`.
  //
  // If a `freezeFunc` is specified, then we freeze the documents in the cache,
  // otherwise we fall back to cloning them on every lookup like minimongo does.
  export class FastLookupCollectionCache<T extends Mongo.DocWithStringId> {
    documents = new Map<string, T>();
    dependencies = new Map<string, Tracker.Dependency>();

    constructor(public freezeFunc: undefined | ((doc: T) => void)) {}

    // FastLookupCollectionCache offers the following API so that callers can
    // feed it from an existing observer of the original collection, saving some
    // document cloning compared to if FastLookupCollectionCache always
    // registered its own observer.
    //
    // NOTE: The document passed to `add` or `change` will be held and may be
    // frozen!  Clone it first if you don't want this.
    add(document: T) {
      if (this.freezeFunc != null) {
        this.freezeFunc(document);
      }
      this.documents.set(document._id, document);
      this.invalidate(document._id);
    }
    change(document: T) {
      if (this.freezeFunc != null) {
        this.freezeFunc(document);
      }
      this.documents.set(document._id, document);
      this.invalidate(document._id);
    }
    remove(id: string) {
      this.documents.delete(id);
      this.invalidate(id);
    }

    private invalidate(id: string) {
      let dep = this.dependencies.get(id);
      if (dep != null) {
        this.dependencies.delete(id);
        dep.changed();
      }
    }

    get(id: string) {
      if (Tracker.currentComputation) {
        let dep = this.dependencies.get(id);
        if (dep == null) {
          dep = new Tracker.Dependency();
          this.dependencies.set(id, dep);
        }
        if (dep.depend()) {
          // Ensure that `dep` gets cleaned up if it is left with no dependent
          // computations because they all got invalidated for other reasons.
          const dep1 = dep;
          Tracker.currentComputation.onInvalidate(() => {
            if (!dep1.hasDependents()) {
              this.dependencies.delete(id);
            }
          });
        }
      }
      let doc = this.documents.get(id);
      if (this.freezeFunc == null) {
        doc = EJSON.clone(doc);
      }
      return doc;
    }
  }

}
