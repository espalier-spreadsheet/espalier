// Load order for `NewtypeDef`.  Using `Meteor.startup` leads to a long chain of
// other things needing to use `Meteor.startup`.
/// <reference path="type-funcs.ts" />

namespace Objsheets {

  // "Generic index" library that facilitates artifically adding generic-ness to
  // the index type of a mapped type M so that M[X] will always substitute
  // instead of falling back to lax assignability checking using the constraints
  // of M and X.  See Layout.Axis for the only current example.

  // Since TypeScript doesn't appear to have left any loophole to create an
  // "instantiable" type at the top level, every generic-index mapped type must
  // be generic in a type parameter named `_` by convention, and all code that
  // handles generic-index mapped types must carry this type parameter.  This is
  // admittedly ugly; a TypeScript suggestion to support substitution into
  // mapped types with non-generic index types (TODO file it) would avoid the
  // need for the whole generic-index mechanism.  `_` is ultimately always
  // instantiated with `never`, but only via `giRun`; the fact that `_` is
  // `never` is never directly visible.

  // note: FAKE_INDEX is JSONable.  Ugly, ugly.
  const FAKE_INDEX = "fake-index";
  // GenericIndex<_, K>: The index type to use for the mapped type if the index
  // type we originally wanted is K.
  export type GenericIndex<_, K> = K | (_ & typeof FAKE_INDEX);
  // Any type that is used inside a generic-index mapped type and takes a type
  // parameter (say `A`) for the index will need to loosen the constraint of `A`
  // to accept `GenericIndex<_, A>`.  If the type has no other need for a `_`
  // parameter, it can use `LooseIndex<K>` as a constraint that is looser than
  // `GenericIndex<_, K>` for any `_`.
  export type LooseIndex<K> = K | typeof FAKE_INDEX;

  // We have to be very careful not to manipulate data structures with `_`
  // instantiated as a concrete type, because then we fall back to the lax
  // assignability checking.  Of particular concern are functions that have a
  // `_` type parameter but take no parameter from which `_` can be inferred
  // (and likewise, classes that have a `_` type parameter and whose constructor
  // takes no parameter from which `_` can be inferred), so unless `_` gets
  // inferred from a known return type, it will default to `{}` and the returned
  // object will have lax checking.  To catch failure to infer or specify `_`,
  // we default it to `never` and intersect the type of the first parameter (of
  // the constructor in the case of a class) with `Check_<_, G>`, which
  // simplifies to `Failed_to_infer_generic_index_parameter` if `_` is
  // definitely `never` but not if `_` is a type variable of the caller.  (If
  // there is no first parameter, we add `DUMMY` as a parameter.)  To prevent
  // the `Check_` from simplifying in the signature itself, we add another type
  // parameter `G` that is never intended to be specified by callers and
  // defaults to `never`; by convention, `G` is constrained to
  // `Do_not_mess_with_this_type_parameter`. `Check_<_, G>` does not simplify in
  // the signature because `G` is unknown, but simplifes at the call site where
  // `G` defaults to `never`.

  export const ERROR_INTERFACE_DUMMY = Symbol();
  export const DUMMY = Symbol();
  export interface Do_not_mess_with_this_type_parameter {
    [ERROR_INTERFACE_DUMMY]: never;
  }
  // `IfDefinitelyNever<X, A, B, never>` -> `A` if `X` is definitely `never` (or
  // `{}` but that case should not arise), `B` otherwise.  If the `G` ("gate")
  // parameter is not `never`, then `IfDefinitelyNever` does not simplify.
  //
  // Tuple added around each side of the conditional type to defeat
  // https://github.com/Microsoft/TypeScript/pull/28851 .  Clearly there's some
  // risk of the TypeScript team changing things in a way that makes
  // IfDefinitelyNever impossible to implement any more, in which case we'll
  // probably give up the protection, but we'll keep it as long as we can.
  // ~ Matt 2019-05-04
  export type IfDefinitelyNever<X, A, B, G extends Do_not_mess_with_this_type_parameter> =
    [("good" | G)] extends [{[P in keyof X | "extra"]: "good"}[keyof X]] ? B : A;

  interface Failed_to_infer_generic_index_parameter {
    [ERROR_INTERFACE_DUMMY]: never;
  }
  export type Check_<_, G extends Do_not_mess_with_this_type_parameter> =
    IfDefinitelyNever<_, Failed_to_infer_generic_index_parameter, unknown, G>;

  // When a generic-index data structure has to be stored at the top level, we
  // wrap it in `GIWrapped` to avoid exposing a `never` instantiation that
  // someone could access unsafely.
  const GI_WRAPPED_MARKER = Symbol();
  export type GIWrapped<F> = Newtype<typeof GI_WRAPPED_MARKER, App<F, never>>;
  const giWrappedNewtypeDef = new NewtypeDef(GI_WRAPPED_MARKER);
  export interface GIWrapper<_> {
    wrap<F>(arg: App<F, _>): GIWrapped<F>;
    unwrap<F>(arg: GIWrapped<F>): App<F, _>;
  }
  class GIWrapperImpl<_> implements GIWrapper<_> {
    constructor(private eq: Eq<_, never>) {}
    wrap<F>(arg: App<F, _>): GIWrapped<F> {
      return giWrappedNewtypeDef.wrap(this.eq.cast(arg));
    }
    unwrap<F>(arg: GIWrapped<F>): App<F, _> {
      return this.eq.uncast(giWrappedNewtypeDef.unwrap(arg));
    }
  }
  export const neverGIWrapper = new GIWrapperImpl(Eq.reflexivity());

  // Introduce `_` and call a callback with a `GIWrapper` that can be used to
  // wrap and unwrap `GIWrapped` objects.  The `GIWrapper` will have to be
  // passed along to all code that needs to access generic-index data structures
  // stored at the top level, which fortunately is only a small part of the code
  // that touches generic-index data structures.
  export function giRun<R>(f: <_>(w: GIWrapper<_>) => R): R {
    return f(neverGIWrapper);
  }

}
