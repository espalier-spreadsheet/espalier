namespace Objsheets {

  export function assert(cond: boolean, msg?: () => string) {
    if (!cond)
      throw new Error(msg ? `assertion failed: ${msg()}` : "assertion failed");
  }

  // https://stackoverflow.com/questions/39419170/how-do-i-check-that-a-switch-block-is-exhaustive-in-typescript
  export function assertUnreachable(val: never, msg?: string): never {
    throw new Error(msg ? `assertion failed: ${msg}` : "assertion failed");
  }

  // Only the ! operator takes us from T to NonNullable<T>.  We would prefer to
  // use narrowing, but that only works if the parameter is
  // `null | undefined | T`, and then inference of `T` from a generic lookup
  // type that needs simplification doesn't work.
  export function assertNotNull<T>(value: T) {
    if (value == null)
      throw new Error("assertion failed");
    return value!;
  }

}
