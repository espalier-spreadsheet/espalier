namespace Objsheets {

  // MeteorCollectionSpec lets us specify the document type for a given
  // collection name centrally, even if the code to actually create the
  // collection can't be shared between client and server as it can in simple
  // Meteor apps.  This doesn't buy us anything right now for the sheet
  // collections because most of the code uses aliases based on
  // SheetCollectionTypes and code that iterates over the sheet collections is
  // problematic to type statically anyway. ~ Matt 2018-05-04

  export class MeteorCollectionSpec<T extends Mongo.DocWithStringId> {
    // See if protected flies or we have to do something else.
    constructor(protected name: string) {}

    makeLocalCollection() {
      return newLocalCollection<T>(this.name);
    }
    makeMongoCollection() {
      return new Mongo.Collection<T>(this.name);
    }
  }

  // Similar idea as https://github.com/dataflows/meteor-typescript-utils/ but
  // supports multiple arguments.

  let allMethods = new Map<string, MeteorMethodBase>();

  // This untyped base class should only be used in the implementations of
  // library facilities that support MeteorMethods with different numbers of
  // arguments and have appropriate type-safe interfaces.
  export class MeteorMethodBase {
    constructor(protected name: string) {
      // Catch some copy and paste errors.
      if (allMethods.get(name) != null)
        throw new Error(`A MeteorMethod was already created for ${name}.`);
      allMethods.set(name, this);
    }

    public define(func: Function) {
      Meteor.methods({[this.name]: func});
    }

    // Untyped; callers responsible for type safety.
    // tslint:disable-next-line:no-any
    public call(...argsAndCallback: any[]): void {
      Meteor.call(this.name, ...argsAndCallback);
    }
  }

  // As many as needed. :/
  export class MeteorMethod0<R extends (EJSONable | void)> extends MeteorMethodBase {
    define(func: (this: Meteor.MethodInvocation) => R) {
      super.define(func);
    }
    call(callback: MeteorCallback<R>) {
      return super.call(callback);
    }
  }
  export interface MeteorMethod0<R> extends NotCovariant<R> {}

  export class MeteorMethod1<A1 extends EJSONable, R extends (EJSONable | void)> extends MeteorMethodBase {
    define(func: (this: Meteor.MethodInvocation, a1: A1) => R) {
      super.define(func);
    }
    call(a1: A1, callback: MeteorCallback<R>) {
      return super.call(a1, callback);
    }
  }
  export interface MeteorMethod1<A1, R> extends NotCovariant<A1, R> {}

  export class MeteorMethod2<A1 extends EJSONable, A2 extends EJSONable,
    R extends (EJSONable | void)> extends MeteorMethodBase {
    define(func: (this: Meteor.MethodInvocation, a1: A1, a2: A2) => R) {
      super.define(func);
    }
    call(a1: A1, a2: A2, callback: MeteorCallback<R>) {
      return super.call(a1, a2, callback);
    }
  }
  export interface MeteorMethod2<A1, A2, R> extends NotCovariant<A1, A2, R> {}

  export class MeteorMethod3<A1 extends EJSONable, A2 extends EJSONable,
    A3 extends EJSONable, R extends (EJSONable | void)> extends MeteorMethodBase {
    define(func: (this: Meteor.MethodInvocation, a1: A1, a2: A2, a3: A3) => R) {
      super.define(func);
    }
    call(a1: A1, a2: A2, a3: A3, callback: MeteorCallback<R>) {
      return super.call(a1, a2, a3, callback);
    }
  }
  export interface MeteorMethod3<A1, A2, A3, R> extends NotCovariant<A1, A2, A3, R> {}

  export class MeteorMethod4<A1 extends EJSONable, A2 extends EJSONable,
    A3 extends EJSONable, A4 extends EJSONable, R extends (EJSONable | void)> extends MeteorMethodBase {
    define(func: (this: Meteor.MethodInvocation, a1: A1, a2: A2, a3: A3, a4: A4) => R) {
      super.define(func);
    }
    call(a1: A1, a2: A2, a3: A3, a4: A4, callback: MeteorCallback<R>) {
      return super.call(a1, a2, a3, a4, callback);
    }
  }
  export interface MeteorMethod4<A1, A2, A3, A4, R> extends NotCovariant<A1, A2, A3, A4, R> {}

  export class MeteorMethod5<A1 extends EJSONable, A2 extends EJSONable,
    A3 extends EJSONable, A4 extends EJSONable, A5 extends EJSONable,
    R extends (EJSONable | void)> extends MeteorMethodBase {
    define(func: (this: Meteor.MethodInvocation, a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) => R) {
      super.define(func);
    }
    call(a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, callback: MeteorCallback<R>) {
      return super.call(a1, a2, a3, a4, a5, callback);
    }
  }
  export interface MeteorMethod5<A1, A2, A3, A4, A5, R> extends NotCovariant<A1, A2, A3, A4, A5, R> {}

  let allPublications = new Map<string, MeteorPublicationBase>();

  // Ditto comments from MeteorMethodBase.
  export class MeteorPublicationBase {
    constructor(protected name: string) {
      // Catch some copy and paste errors.
      if (allPublications.get(name) != null)
        throw new Error(`A MeteorPublication was already created for ${name}.`);
      allPublications.set(name, this);
    }

    public publish(func: Function) {
      Meteor.publish(this.name, func);
    }

    // Untyped; callers responsible for type safety.
    // tslint:disable-next-line:no-any
    public subscribe(...argsAndCallbacks: any[]): Meteor.SubscriptionHandle {
      return Meteor.subscribe(this.name, ...argsAndCallbacks);
    }
  }

  // As many as needed. :/
  //
  // Public signatures for publish / subscribe are given in
  // src/lib/{client,server}/meteor-typescript.ts so they are only visible in
  // the applicable unibuild.
  export class MeteorPublication0 extends MeteorPublicationBase {}
  export class MeteorPublication1<A1 extends EJSONable> extends MeteorPublicationBase {}
  export class MeteorPublication2<A1 extends EJSONable, A2 extends EJSONable>
    extends MeteorPublicationBase {}
  export class MeteorPublication3<A1 extends EJSONable, A2 extends EJSONable,
    A3 extends EJSONable> extends MeteorPublicationBase {}

}
