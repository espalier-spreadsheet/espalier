namespace Objsheets {

  // We could consider https://www.npmjs.com/package/async-lock, but it will
  // probably be more work to adapt it to our purposes than to just write this.
  // :/ ~ Matt 2016-12-09

  export class Lock {
    private locked = false;
    private queue: ExternalModules.Future[] = [];

    public run<R>(op: () => R): R {
      if (this.locked) {
        let f = new ExternalModules.Future();
        this.queue.push(f);
        f.wait();
        // Now the lock is ours.
      } else {
        this.locked = true;
      }
      try {
        return op();
      } finally {
        let next = this.queue.shift();
        if (next != null) {
          next.return();
        } else {
          this.locked = false;
        }
      }
    }
  }
}
