namespace Objsheets {

  export interface MeteorPublication0 {
    publish(func: (this: Subscription) => PublishFunctionReturn): void;
  }

  export interface MeteorPublication1<A1 extends EJSONable> {
    publish(func: (this: Subscription, a1: A1) => PublishFunctionReturn): void;
  }

  export interface MeteorPublication2<A1 extends EJSONable, A2 extends EJSONable> {
    publish(func: (this: Subscription, a1: A1, a2: A2) => PublishFunctionReturn): void;
  }

  export interface MeteorPublication3<A1 extends EJSONable, A2 extends EJSONable,
    A3 extends EJSONable> {
    publish(func: (this: Subscription, a1: A1, a2: A2, a3: A3) => PublishFunctionReturn): void;
  }

}
