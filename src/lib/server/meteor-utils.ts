namespace Mongo {
  export interface Collection<T extends Doc> extends Objsheets.NotCovariant<T> {
    dropIfExists(): void;
  }
}

namespace Objsheets {
  Mongo.Collection.prototype.dropIfExists = function() {
    try {
      // Or rawCollection().drop(), but this triggers some Meteor-specific
      // handling that might be good.  If it breaks, big deal, we change our
      // code to call rawCollection().drop() instead.
      this._dropCollection();
    } catch (e) {
      if (e.name == "MongoError" && e.message == "ns not found") {
        // The collection didn't exist.
      } else {
        throw e;
      }
    }
  };

  export function httpRespondWithError(response: fixmeAny, statusCode: number, message: string) {
    response.statusCode = statusCode;
    response.setHeader("Content-Type", "text/plain");
    response.end(message + "\n");
  }
}
