/*
 * As mentioned in DEVELOPMENT.md, since our files are not external modules,
 * they cannot import external modules directly; any import statement would
 * cause the file to be treated as an external module, and our current build
 * system is not capable of emitting external modules at all.  `require` or
 * similar will get us access to a module at runtime, but not the type
 * information if it is associated with the external module name (e.g., `declare
 * module "foo"`).
 *
 * Fortunately, TypeScript lets us import the module's type information from
 * within an "ambient" (i.e., type declaration) context and then augment it onto
 * the global scope, without causing the enclosing file to be treated as an
 * external module.  The `import X = Y` statement copies the value, type, and
 * namespace meanings of Y all at once; it's the only way to copy any type
 * definitions nested in Y, but it requires that Y have namespace nature (a
 * seemingly arbitrary restriction).  If Y doesn't, then we can just use `let X:
 * typeof Y` and/or `type X = Y` to copy the value and type meanings as
 * applicable.  This doesn't emit anything; we still need the `require`.
 *
 * This file is for modules used only on the server.  We can use analogous
 * `src/lib/modules.ts` and `src/lib/client/modules.ts` as necessary.
 */

declare module "dummy" {
  import Future1 = require("fibers/future");  // value and type
  global {
    namespace ExternalModules {
      type Future = Future1;
      let Future: typeof Future1;
    }
  }
}

// The ExternalModules object is now created at runtime by the
// objsheets-npm-deps Meteor package.
ExternalModules.Future = Npm.require("fibers/future");
