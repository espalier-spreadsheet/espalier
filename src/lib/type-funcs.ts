namespace Objsheets {

  // Library for type functions.  Inspired by https://github.com/gcanti/fp-ts/
  // but provides meaningful type checking of code that is polymorphic in a type
  // function (see https://github.com/gcanti/fp-ts/issues/536) and lets a type
  // function close over a type variable.
  //
  // A type function of kind * -> * is encoded as a type of the form Fun<K, C>
  // and represents lambda X. TypeFuncs<C, X>[K].  (C is the closure
  // information; a list of types can be encoded as a tuple type and recovered
  // using a conditional type.)  App is used to apply a type function.  Once we
  // can encode a type function of kind * -> * in kind *, we can use this
  // mapping to encode a type function of any kind in kind *.  For this to work,
  // it's essential that App<F, X> does not constrain F to be a valid type
  // function, but instead just "gets stuck" if F is not.  If App<F, X>
  // constrained F, then higher-order type functions would need to constrain
  // their arguments, so this library would need to support type functions with
  // constrained arguments, and it would be a mess.  So in effect, we have an
  // untyped lambda calculus with non-function values at the type level.

  // Goal: Invariant<T> should bear no observable relationship to any other
  // type, including Invariant<T'> for T' != T.
  const INVARIANT_MARKER = Symbol();
  type Invariant<T> = {
    [INVARIANT_MARKER](t: T): T
  };

  // Fundamentals

  const F_Const = Symbol(), F_Identity = Symbol();
  export interface TypeFuncs<C, X> {
    [F_Const]: C;
    [F_Identity]: X;
  }

  const FUN_MARKER = Symbol();
  export type Fun<K extends keyof TypeFuncs<{}, {}>, C> = Invariant<[typeof FUN_MARKER, K, C]>;

  const BAD_APP_MARKER = Symbol();
  type BadApp<F, X> = Invariant<[typeof BAD_APP_MARKER, F, X]>;
  export type App<F, X> = [F] extends [Fun<infer K, infer C>] ? TypeFuncs<C, X>[K] : BadApp<F, X>;

  // Utilities

  const NEWTYPE_MARKER = Symbol();
  export type Newtype<M extends symbol, T> = Invariant<[typeof NEWTYPE_MARKER, M, T]> &
    // Hack: Preserve JSONability.
    ([T] extends [JSONable] ? JSONable : {});
  // XXX Consider converting to global functions for improved performance?
  export class NewtypeDef<M extends symbol> {
    constructor(m: M) {}
    wrap<T>(x: T) {
      return <Newtype<M, T>><{}>x;
    }
    unwrap<T>(x: Newtype<M, T>) {
      return <T><{}>x;
    }
  }

  // Token<X>: Can be used to construct Eq<X, Y> by a runtime check
  const TOKEN_MARKER = Symbol();
  export interface Token<X> extends Invariant<[typeof TOKEN_MARKER, X]> {}
  export class Token<X> {}

  // Eq<X, Y>: Witness of equality of X and Y
  const EQ_MARKER = Symbol();
  export interface Eq<X, Y> extends Invariant<[typeof EQ_MARKER, X, Y]> {}
  export class Eq<X, Y> {
    private constructor() {}
    cast<F>(input: App<F, X>) {
      return <App<F, Y>><{}>input;
    }
    uncast<F>(input: App<F, Y>) {
      return <App<F, X>><{}>input;
    }
    static tryMake<X, Y>(tokenX: Token<X>, tokenY: Token<Y>) {
      // They don't look comparable, do they?  That's the whole point.
      if (<{}>tokenX !== tokenY) {
        return null;
      } else {
        return new Eq<X, Y>();
      }
    }
    static reflexivity<X>() {
      return new Eq<X, X>();
    }
    // We don't need symmetry, etc. because you can use an Eq to cast another Eq.
  }

}
