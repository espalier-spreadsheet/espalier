// Utility definitions not specific to Object Spreadsheets.

namespace Objsheets {

  // Restrict this naive algorithm to JSONable values for now, since we know it
  // is safe to use on them.
  export function deepFreeze(o: JSONable) {
    if (o !== null && typeof o === "object") {
      Object.freeze(o);
      for (let k of Object.keys(o)) {
        deepFreeze((<fixmeAny>o)[k]);
      }
    }
  }

  export const equalByIdentityBrand = Symbol();
  export interface EqualByIdentityBranded {
    [equalByIdentityBrand]: undefined;
  }
  // Obviously this is unsound in that an application can have multiple intern
  // pools, but it seems to be the most reasonable thing we can do in
  // TypeScript.  We could probably do better in Scala.
  export type Interned = null | undefined | EqualByIdentityBranded;
  export type EqualByIdentity = null | undefined | number | string | EqualByIdentityBranded;

  // A JSONableInternPool can intern any JSONable object by its JSON
  // representation (key order sensitive).  Interning applies only to the entire
  // objects, not their sub-objects.
  export class JSONableInternPool {
    private set = new Set<JSONable>();
    private map = new Map<string, JSONable & Interned>();
    intern<T extends JSONable>(x: T): T & Interned {
      // XXX This cast might not be necessary with smarter narrowing.
      if (x == null) return <T & Interned>x;
      if (this.set.has(x)) {
        return <T & Interned>x;
      } else {
        let str = JSON.stringify(x);
        let xi = <T & Interned>this.map.get(str);
        // https://github.com/palantir/tslint/issues/3222
        // tslint:disable-next-line:strict-type-predicates
        if (xi == null) {
          xi = <T & Interned>x;
          // XXX: Why is this cast needed??
          this.map.set(str, <JSONable & Interned>xi);
        }
        return xi;
      }
    }
  }

  // FIXME: EJSONKeyedMap is effectively using "key order sensitive"
  // EJSON.equals comparison of keys, though we use regular EJSON.equals
  // throughout the codebase.  Apparently we've been consistent enough with key
  // order to never notice until now. ~ Matt 2017-12-07
  export class EJSONKeyedMap<K extends EJSONable, V> {
    protected obj: {[stringKey: string]: V};

    protected stringifyKey(k: K): string {
      return EJSON.stringify(k);
    }
    protected parseKey(k: string): K {
      return EJSON.parse(k);
    }

    constructor(ents: [K, V][] = []) {
      // XXX: Switch to ECMAScript 6 Map?  It makes toJSONValue/fromJSONValue a
      // little more complicated.
      this.obj = Object.create(null);
      for (let [k, v] of ents) {
        if (this.get(k) !== undefined)
          throw new Error(`Duplicate key during EJSONKeyedMap construction: ${this.stringifyKey(k)}`);
        this.set(k, v);
      }
    }

    public get(k: K): undefined | V {
      return this.obj[this.stringifyKey(k)];
    }

    public set(k: K, v: V): void {
      this.obj[this.stringifyKey(k)] = v;
    }

    public delete(k: K): void {
      delete this.obj[this.stringifyKey(k)];
    }

    public size(): number {
      return _.size(this.obj);
    }

    public keys(): K[] {
      let keys: K[] = [];
      for (let wk in this.obj) {
        keys.push(this.parseKey(wk));
      }
      return keys;
    }

    public values(): V[] {
      let values: V[] = [];
      for (let wk in this.obj) {
        values.push(this.obj[wk]);
      }
      return values;
    }

    public entries(): [K, V][] {
      let entries: [K, V][] = [];
      for (let wk in this.obj) {
        let v = this.obj[wk];
        entries.push([this.parseKey(wk), v]);
      }
      return entries;
    }

    public shallowClone(): EJSONKeyedMap<K, V> {
      return new EJSONKeyedMap(this.entries());
    }

    public typeName(): string {
      return "EJSONKeyedMap";
    }

    public toJSONValue<VE extends EJSONable>(this: EJSONKeyedMap<K, VE>): EJSONKeyedMapJSON {
      return EJSON.toJSONValue(this.obj);
    }

    public static fromJSONValue(json: EJSONKeyedMapJSON):
        EJSONKeyedMap<EJSON.any_ta, EJSON.any_ta> {
      let m = new EJSONKeyedMap<EJSON.any_ta, EJSON.any_ta>();
      m.obj = Object.assign(Object.create(null), EJSON.fromJSONValue(json));
      return m;
    }
  }
  export interface EJSONKeyedMap<K extends EJSONable, V> {
    [EJSON.symbols.ejsonableTypeParam2]?: V;
  }
  // JSONableDict would be more correct but produces an error in toJSONValue,
  // necessitating either a cast or more precise signatures for
  // EJSON.toJSONValue.  Don't bother for now.
  export type EJSONKeyedMapJSON = JSONable;
  EJSON.addType("EJSONKeyedMap", EJSONKeyedMap.fromJSONValue);

  // When the keys are JSONable, JSONKeyedMap can be used instead of
  // EJSONKeyedMap and avoids the significant overhead of scanning the keys for
  // items that need EJSON conversion (even if none are found).
  //
  // JSONKeyedMap assumes the keys do not contain JSONable data that has special
  // meaning to EJSON as a representation of an EJSON special type.
  //
  // FIXME: Keys are key order sensitive (see EJSONKeyedMap)
  export class JSONKeyedMap<K extends JSONable, V> extends EJSONKeyedMap<K, V> {
    protected stringifyKey(k: K): string {
      return JSON.stringify(k);
    }
    protected parseKey(k: string): K {
      return JSON.parse(k);
    }

    // Some boilerplate, but it seems unlikely that separating out the repeated
    // parts would actually make the code shorter.

    public shallowClone(): JSONKeyedMap<K, V> {
      return new JSONKeyedMap(this.entries());
    }

    public typeName(): string {
      return "JSONKeyedMap";
    }

    // Note for toJSONValue (inherited) and fromJSONValue: EJSON conversion of
    // keys is assumed to have no effect, but values may need conversion.

    public static fromJSONValue(json: JSONKeyedMapJSON):
        JSONKeyedMap<EJSON.any_ta, EJSON.any_ta> {
      let m = new JSONKeyedMap<EJSON.any_ta, EJSON.any_ta>();
      m.obj = EJSON.fromJSONValue(json);
      return m;
    }
  }
  export type JSONKeyedMapJSON = EJSONKeyedMapJSON;
  EJSON.addType("JSONKeyedMap", JSONKeyedMap.fromJSONValue);

  // FIXME: Keys are key order sensitive (see EJSONKeyedMap)
  export class EJSONSet<E extends EJSONable> {
    protected map: EJSONKeyedMap<E, boolean>;

    protected createMap() {
      return new EJSONKeyedMap<E, boolean>();
    }

    constructor(els: E[] = []) {
      this.map = this.createMap();
      for (let x of els) {
        this.add(x);
      }
    }

    public has(x: E): boolean {
      return !!this.map.get(x);
    }

    public hasAll(s: EJSONSet<E>): boolean {
      return forall(s.elements(), (x) => this.has(x));
    }

    public add(x: E): void {
      this.map.set(x, true);
    }

    public delete(x: E): void {
      this.map.delete(x);
    }

    public size(): number {
      return this.map.size();
    }

    public elements(): E[] {
      return this.map.keys();
    }

    public shallowClone(): EJSONSet<E> {
      return new EJSONSet(this.elements());
    }

    public typeName(): string {
      return "EJSONSet";
    }

    public toJSONValue(): EJSONSetJSON {
      return this.map.toJSONValue();
    }

    public static fromJSONValue(json: EJSONSetJSON): EJSONSet<EJSON.any_ta> {
      let s = new EJSONSet<EJSON.any_ta>();
      s.map = EJSONKeyedMap.fromJSONValue(json);
      return s;
    }
  }
  export type EJSONSetJSON = EJSONKeyedMapJSON;
  EJSON.addType("EJSONSet", EJSONSet.fromJSONValue);

  // See comments on JSONKeyedMap about purpose and assumptions.
  //
  // FIXME: Keys are key order sensitive (see EJSONKeyedMap)
  export class JSONSet<E extends JSONable> extends EJSONSet<E> {
    protected createMap() {
      return new JSONKeyedMap<E, boolean>();
    }

    public shallowClone(): JSONSet<E> {
      return new JSONSet(this.elements());
    }

    public typeName(): string {
      return "JSONSet";
    }

    public static fromJSONValue(json: JSONSetJSON): JSONSet<EJSON.any_ta> {
      let s = new JSONSet<EJSON.any_ta>();
      s.map = JSONKeyedMap.fromJSONValue(json);
      return s;
    }
  }
  export type JSONSetJSON = JSONKeyedMapJSON;
  EJSON.addType("JSONSet", JSONSet.fromJSONValue);

  export class Tree<T> {
    constructor(public root: T, public subtrees: Tree<T>[] = []) {}

    public size(): number {
      return this.subtrees.reduce((x, y) => x + y.size(), 1);
    }

    public forEach(op: (x: T) => void) {
      op(this.root);
      this.subtrees.forEach((s) => s.forEach(op));
    }

    /**
     * applies op to the root of each subtree
     */
    public map<S>(op: (x: T) => S): Tree<S> {
      return new Tree(op(this.root), this.subtrees.map((s) => s.map(op)));
    }

    public filter(pred: (x: T) => boolean): null | Tree<T> {
      return pred(this.root)
        ? new Tree(this.root, (this.subtrees.map((s) => s.filter(pred))).filter(isNotNull))
        : null;
    }

    public find(value: T): null | Tree<T> {
      return this.findT((n) => n.root === value);
    }

    public findT(pred: (t: Tree<T>) => boolean): null | Tree<T> {
      if (pred(this)) {
        return this;
      } else {
        for (let s of this.subtrees) {
          let n = s.findT(pred);
          if (n != null) {
            return n;
          }
        }
        return null;
      }
    }

    public typeName(): string {
      return "Tree";
    }

    public toJSONValue<TE extends EJSONable>(this: Tree<TE>): TreeJSON {
      return {
        root: EJSON.toJSONValue(this.root),
        subtrees: this.subtrees.map((s) => s.toJSONValue())
      };
    }

    public static fromJSONValue(json: TreeJSON): Tree<EJSON.any_ta> {
      return new Tree<EJSON.any_ta>(json.root, json.subtrees.map((s) => Tree.fromJSONValue(s)));
    }
  }
  export interface Tree<T> {
    [EJSON.symbols.ejsonableTypeParam1]?: T;
  }
  export type TreeJSON = {root: JSONable, subtrees: TreeJSON[]};
  EJSON.addType("Tree", Tree.fromJSONValue);

  export class Memo<K extends EJSONable, V> {
    // definitely assigned: constructor calls clear
    private values!: EJSONKeyedMap<K, V>;

    constructor(private recompute: (k: K) => V) {
      this.clear();
    }

    public clear(): void {
      this.values = new EJSONKeyedMap<K, V>();
    }

    public get(k: K): V {
      let v = this.values.get(k);
      if (v == null) {
        v = this.recompute(k);
        this.values.set(k, v);
      }
      return v;
    }
  }

  // helper functions
  export function forall<T>(list: T[], pred: (x: T) => boolean): boolean {
    for (let x of list) {
      if (!pred(x)) {
        return false;
      }
    }
    return true;
  }
  export function exists<T>(list: T[], pred: (x: T) => boolean): boolean {
    for (let x of list) {
      if (pred(x)) {
        return true;
      }
    }
    return false;
  }
  export function without<T>(list: T[], item: T): T[] {
    return list.filter((x) => x !== item);
  }

  // We only call this with two arguments, so let's use the simpler
  // implementation for now. ~ Matt 2016-03-14
  export function zip<A, B>(arr1: A[], arr2: B[]): [A, B][] {
    let length = Math.min(arr1.length, arr2.length);
    // TypeScript does not infer the type argument of "map" from the contextual
    // return type.
    // https://github.com/Microsoft/TypeScript/issues/11152
    return _.range(0, length).map<[A, B]>((i) => [arr1[i], arr2[i]]);
  }

  export function ejsonIndexOf<T extends EJSONable>(haystack: T[], needle: T) {
    return haystack.findIndex((it) => EJSON.equals(it, needle));
  }

  export function set<E extends JSONable>(x?: E[]): JSONSet<E> {
    return new JSONSet(x);
  }
  export function tree<T>(root: T, subtrees?: Tree<T>[]) {
    return new Tree(root, subtrees);
  }

}
