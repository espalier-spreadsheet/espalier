// Object Spreadsheets definitions needed by both client and server that
// don't fall into a more specific file.

namespace Objsheets {

  // TODO: Come up with a consistent plan for the use of these error classes vs.
  // Meteor.Error and under what conditions the server should return detailed
  // errors to the client.

  // Object Spreadsheets error detected at runtime: evaluation, transaction
  // execution, etc.  Includes a few errors that are static in nature but not
  // detected until runtime in the current implementation.
  export class RuntimeError extends Error {
    constructor(message: string) {
      super(message);
      // tslint:disable-next-line:max-line-length
      // https://github.com/Microsoft/TypeScript-wiki/blob/master/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
      // Since we are using a real Error object from the Error constructor, we
      // no longer need any of the workarounds to capture stack traces.
      Object.setPrototypeOf(this, RuntimeError.prototype);
    }
  }

  // Object Spreadsheets error detected statically: formula validation,
  // typechecking, etc.
  export class StaticError extends Error {
    constructor(message: string) {
      super(message);
      Object.setPrototypeOf(this, StaticError.prototype);
    }
  }

  // Many error messages include calls to expensive stringification functions.
  // To make sure we make those lazy, just require all error messages to be
  // lazy.

  export function staticCheck(cond: boolean, lazyMessage: () => string) {
    if (!cond) {
      throw new StaticError(lazyMessage());
    }
  }

  export function staticCheckNotNull<T>(value: T, lazyMessage: () => string) {
    if (value == null)
      throw new StaticError(lazyMessage());
    return value!;
  }

  export function runtimeCheck(cond: boolean, lazyMessage: () => string) {
    if (!cond) {
      throw new RuntimeError(lazyMessage());
    }
  }

  export function runtimeCheckNotNull<T>(value: T, lazyMessage: () => string) {
    if (value == null)
      throw new RuntimeError(lazyMessage());
    return value!;
  }

  export class SyntaxError extends Error {
    // details: Jison "hash"?
    // (https://github.com/zaach/jison/blob/5e13d8563c306c66cc00b9fe22ff6da74617e792/lib/jison.js#L1046)
    constructor(message: string, public details: fixmeAny) {
      super(message);
      Object.setPrototypeOf(this, SyntaxError.prototype);
    }
  }

  export class SemanticError extends Error {
    constructor(message: string) {
      super(message);
      Object.setPrototypeOf(this, SemanticError.prototype);
    }
  }
}

// Attach to the global object for debugging.
let theGlobal: fixmeAny = global || window;
theGlobal.Objsheets = Objsheets;

// Alias to save typing in the server shell and the browser console.
theGlobal.OS = Objsheets;
