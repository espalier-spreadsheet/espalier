namespace Objsheets {

  // Parameter structures shared by Meteor methods and the underlying Model methods.

  export interface DefineColumnParams extends EJSONableDict {
    // Used by ServerAppHelpers.loadProcedures to keep the IDs of editability
    // columns stable.  Discouraged to use for any other purpose.
    thisId?: NonRootColumnId;
    parentId: ColumnId;
    index: number;
    fieldName: null | string;
    specifiedType: null | OSType;
    isObject: boolean;
    objectName: null | string;
    formula: null | Formula;
    viewId: null | string;
    singular: boolean;
    isViewObjectRoot: boolean;
    userVisible: boolean;
  }

  export interface InsertUnkeyedStateObjectTypeWithFieldParams extends EJSONableDict {
    parentId: ColumnId;
    index: number;
    objectName: null | string;
    fieldName: null | string;
    specifiedType: OSType;
    viewId: null | string;
  }

  export interface AddCellRecursiveParams extends EJSONableDict {
    columnId: NonRootColumnId;
    ancestorQCellId: QCellId;
    // value == null means a "new" operation.
    value: null | OSValue;
    // newColumnType != null means create a new child column of 'columnId' and put the value there.
    newColumnType: null | OSType;
    // When newColumnType != null, the new column is added to this view.
    viewId: null | string;
    clientViewObjectId?: null | string;
    cleanup: boolean;
  }

  export interface AddCellRecursiveResponse extends EJSONableDict {
    qFamilyId: QFamilyId;
    // Important when calling "new".
    value: OSValue;
  }

  export type ProcedureArgsObj = {[name: string]: OSValue[]};

  export namespace MeteorMethods {
    export let executeCannedTransaction =
      new MeteorMethod4<TablespaceId, string, ProcedureArgsObj,
        /*clientViewObjectId*/ null | string, void>("executeCannedTransaction");
    export let executeCannedTransactionWithRedirect =
      new MeteorMethod4<TablespaceId, string, ProcedureArgsObj,
        /*clientViewObjectId*/ null | string, /*redirect url*/ null | string>("executeCannedTransactionWithRedirect");

    // Schema edits
    export let defineColumn =
      new MeteorMethod2<TablespaceId, DefineColumnParams, void>("defineColumn");
    export let insertUnkeyedStateObjectTypeWithField =
      new MeteorMethod2<TablespaceId, InsertUnkeyedStateObjectTypeWithFieldParams, void>
      ("insertUnkeyedStateObjectTypeWithField");
    export let changeColumnFieldName =
      new MeteorMethod3<TablespaceId, ColumnId, null | string, void>("changeColumnFieldName");
    export let changeColumnIsObject =
      new MeteorMethod3<TablespaceId, ColumnId, boolean, void>("changeColumnIsObject");
    export let changeColumnObjectName =
      new MeteorMethod3<TablespaceId, ColumnId, null | string, void>("changeColumnObjectName");
    export let changeColumnSpecifiedType =
      new MeteorMethod3<TablespaceId, ColumnId, null | OSType, void>("changeColumnSpecifiedType");
    export let changeColumnFormula =
      new MeteorMethod3<TablespaceId, ColumnId, null | Formula, void>("changeColumnFormula");
    export let changeColumnIsSingular =
      new MeteorMethod3<TablespaceId, ColumnId, boolean, void>("changeColumnIsSingular");
    export let changeColumnReferenceDisplayColumn =
      new MeteorMethod3<TablespaceId, ColumnId, null | ColumnId, void>("changeColumnReferenceDisplayColumn");
    export let reorderColumn =
      new MeteorMethod3<TablespaceId, ColumnId, number, void>("reorderColumn");
    export let deleteColumn =
      new MeteorMethod2<TablespaceId, ColumnId, void>("deleteColumn");

    // Data edits
    export let addCellRecursive =
      new MeteorMethod2<TablespaceId, AddCellRecursiveParams, AddCellRecursiveResponse>("addCellRecursive");
  }

}
