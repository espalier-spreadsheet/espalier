namespace Objsheets {

  // In the future, we can consider "branding" these like Path in the TypeScript
  // compiler.
  export type ColumnId = typeof rootColumnId | NonRootColumnId;
  export type OSType = ColumnId | SpecialType;

  // Use an enum to prevent automatic widening when rootColumnId is assigned to a variable.
  export enum RootColumnId { ROOT = "_root" }
  export const rootColumnId = RootColumnId.ROOT;

  const nonRootColumnIdBrand = Symbol();
  export type NonRootColumnId = string & {
    [nonRootColumnIdBrand]: undefined;
  };

  // (I tried using EJSON custom classes but it was too much of a pain to store in
  // the DB.  If I find a good solution, we could go back to using EJSON custom
  // classes. ~ Matt)
  export type Column = {
    _id: ColumnId;
    parent: null /*root*/ | ColumnId;
    children: NonRootColumnId[];
    // All readers should handle null or document why they don't.
    fieldName: null | string;
    // type specified by user (non-null for state columns)
    specifiedType: null | OSType;
    isObject: boolean;
    // All readers should handle null or document why they don't.
    objectName: null | string;
    // some JSON data structure, or null for state, root, or view object root
    formula: null | Formula;
    referenceDisplayColumn: null /*auto*/ | ColumnId;
    // Name of the sheet view this column belongs to, or null/undefined for the
    // main view.
    view: null | string;
    // If true, the column can have at most one item per parent object.  This
    // affects both mutations of state families and evaluation of computed
    // families but not trigger calls on computed families.  This is the
    // behavior we want for fields in custom layout.  Currently not supported
    // for object columns.
    //
    // Having this as a per-column option lets me continue to put off deciding
    // whether a sheet that has custom layouts should confirm entirely to the
    // expectations of custom layouts.  To the same end, `singular` is
    // initialized to false on sheets that predate its existence.  To set it to
    // true for all applicable columns on a sheet, you can use a command like
    // this in the Meteor shell:
    //
    //     OS.runIn(SHEET_NAME, () => OS.Columns.update({isObject: false}, {$set: {singular: true}}, {multi: true}))
    //
    // ~ Matt 2018-04-08
    singular: boolean;

    // Can only be true for children of the root.
    isViewObjectRoot: boolean;
    // Derived data but will reduce performance cost unless/until we develop a
    // clean way to cache things like this.
    viewObjectType: null | NonRootColumnId;

    // If false, the user can't navigate down to this column or select it as a
    // reference display column.  Used for hyperlink and button formulas and
    // editability columns.  All non-user-visible columns must be computed
    // fields.  Non-user-visible columns should normally be unnamed, but
    // editability columns currently have to be named so that Mavo templates can
    // use the editability information.
    userVisible: boolean;

    // The following fields are set during typechecking on the server.  If you
    // read them directly, you are introducing a tacit assumption that the
    // columns have been typechecked when your code runs.  Use
    // getColumn{Type,TypecheckError} instead, even in client-only code, as a
    // good habit.

    // checked type (may be SpecialType.ERROR)
    cachedType: null /* not computed */ | OSType;
    // formula type checking error message or null if none
    cachedTypecheckError: null | string;
    // true if the column is referenced in any formula on the sheet
    //
    // XXX: This does not take into account procedures.  As long as procedures
    // are hard-coded and getColumnIsReferenced is used only as part of a check
    // that also checks if the column is named, this shouldn't be a problem
    // because procedures can only refer to named columns.  We'll fix
    // getColumnIsReferenced to consider procedures when we make procedures
    // editable.
    cachedIsReferenced: boolean;

    // In the interim while default trigger generation occurs in
    // ServerAppHelpers.loadProcedures, plumb this info from there to the action
    // bar.  The design will be totally different once triggers are editable in
    // the sheet UI.
    // ~ Matt 2017-03-07
    areTriggersDefault?: boolean;
  };
  export type NonRootColumn = Column & {_id: NonRootColumnId, parent: ColumnId};

  // There are a few callers that do handle undefined returns. ~ Matt 2018-08-01
  export function getColumn(id: NonRootColumnId): undefined | NonRootColumn;
  export function getColumn(id: ColumnId): undefined | Column;
  export function getColumn(id: ColumnId): undefined | Column {
    return $$.getDocument(SheetCollections.Columns, id);
  }

  export function getExistingColumn(id: NonRootColumnId): NonRootColumn;
  export function getExistingColumn(id: ColumnId): Column;
  export function getExistingColumn(id: ColumnId): Column {
    return assertNotNull(getColumn(id));
  }

  // This function only uses the _id, but at the time it was introduced, taking
  // a Column saved us a bunch of code churn.  Change later if we like.
  // Alternatively, change model implementations to use the passed Column, which
  // would improve performance a little, although on the server, we'd be
  // trusting callers not to corrupt the model by passing a stale Column.
  //
  // XXX: Change to an accessor on the Column type?
  export function getColumnType(col: Column) {
    return getReadableModel().typecheckColumn(col._id);
  }

  export function getColumnTypecheckError(col: Column) {
    getReadableModel().typecheckColumn(col._id);
    // Careful!  `col` has old data.
    return getExistingColumn(col._id).cachedTypecheckError;
  }

  export function getColumnIsReferenced(col: Column) {
    getReadableModel().typecheckAll();
    return getExistingColumn(col._id).cachedIsReferenced;
  }

  export function getEditabilityColumn(col: NonRootColumn): undefined | NonRootColumn {
    assert(col.formula != null);
    return getColumn(<NonRootColumnId>(col._id + "_editable"));
  }

  export function columnIsPotentiallyEditable(col: NonRootColumn) {
    return col.formula == null ? !col.isViewObjectRoot : getEditabilityColumn(col) != null;
  }

  // Finds the lowest common ancestor of columnId1 and columnId2 and returns a
  // pair of arrays giving the sequences of ancestors from columnId1 and
  // columnId2 (respectively) to the common ancestor, inclusive.
  export function findCommonAncestorPaths(columnId1: ColumnId, columnId2: ColumnId):
      [ColumnId[], ColumnId[]] {
    let ancestors1: ColumnId[] = [];
    let cid = columnId1;
    while (true) {
      ancestors1.push(cid);
      if (cid === rootColumnId) {
        break;
      }
      cid = getExistingColumn(cid).parent;
    }
    let ancestors2: ColumnId[] = [];
    cid = columnId2;
    let idx: number;
    while (true) {
      ancestors2.push(cid);
      // We could make this not O(N^2) if we cared...
      if ((idx = ancestors1.indexOf(cid)) !== -1) {
        break;
      }
      cid = fixmeAssertNotNull(getExistingColumn(cid).parent);
    }
    ancestors1.splice(idx + 1, ancestors1.length - (idx + 1));
    return [ancestors1, ancestors2];
  }

  export enum SpecialType {
    // The empty type, subtype of all other types, used for literal empty sets, etc.
    // Not allowed for state columns.
    EMPTY = "empty",

    ERROR = "error",

    // Main primitive types
    TEXT = "text",
    NUMBER = "number",
    BOOL = "bool",
    DATE = "date",
    IMAGE = "image",

    TOKEN = "_token",  // special primitive, should never be user-visible
    UNIT = "_unit",  // primitive, deprecated but still used in ptc
  }

  export const MAIN_PRIMITIVE_TYPES = [SpecialType.TEXT, SpecialType.NUMBER, SpecialType.BOOL,
    SpecialType.DATE, SpecialType.IMAGE];
  export const DEFAULT_STATE_FIELD_TYPE = SpecialType.TEXT;

  let SPECIAL_TYPES: SpecialType[] = [];
  let t: keyof typeof SpecialType;
  for (t in SpecialType) {
    // XXX Why do we need this cast?
    SPECIAL_TYPES.push(<SpecialType>SpecialType[t]);
  }

  // It's messy to have some primitive types that begin with underscore and others
  // that don't.  We could have hidden the underscore only in the UI, though at
  // this stage of development, it might have been too costly in terms of us
  // forgetting that the underscore should be present internally.  Or we could have
  // chosen a different representation altogether.  But that's too much work at the
  // moment. :( ~ Matt 2015-11-13
  //
  // XXX TypeScript could automatically pass along the type-guard return type
  // without us annotating it again.
  export function typeIsSpecial(type: string /* for parseTypeStr */): type is SpecialType {
    return arrayContainsGuard(SPECIAL_TYPES, type);
  }
  export function typeIsReference(type: OSType): type is ColumnId {
    return !typeIsSpecial(type);
  }

  export function commonSupertype(t1: OSType, t2: OSType): OSType {
    return t1 !== SpecialType.EMPTY ? t2 !== SpecialType.EMPTY && t2 !== t1 ? SpecialType.ERROR : t1 : t2;
  }

  // Triggers!  For now, specially named procedures.  Long-term, should move to
  // a dedicated representation.
  export namespace TriggerType {
    export const NEW = "new", ADD = "add", REMOVE = "remove";
  }
  export const TRIGGER_CLEANUP = "cleanup";
  export function triggerName(col: Column, triggerType: string) {
    return stringifyColumnRef([col._id, !col.isObject])
      .replace(/[\[\]]/g, "").replace(/:/g, "_") + "_" + triggerType;
  }

  export declare let notifySomeSchemaCache: () => void;
}
