namespace Objsheets {

  // All view-update stuff is now piggybacked on the "procedures".  Misnomer,
  // but it's not worth the churn to move things around, since we plan to get
  // rid of all this when procedures and view-update specs are editable in the
  // sheet UI. ~ Matt 2017-02-23

  export namespace ServerAppHelpers {
    export interface PreProcedure {
      params: ([string, string] | [/*name*/ string, /*type str*/ string, /*singular*/ boolean])[];
      body: string;
    }
    export type ColumnViewUpdateSpec = {
      // These field names are cuter than Matt would normally like because we
      // are effectively making a DSL.

      // Must be set, otherwise you might as well not mention this column at
      // all.
      //
      // TODO: Make this richer.  The main use case will be having separate
      // conditions for creating a new object and deleting each existing object
      // in an object family.
      editableIf: string;

      newTrigger?: string;
      addTrigger?: string;
      removeTrigger?: string;
    };
    export interface AppViewUpdateSpec {
      [columnRefStr: string]: ColumnViewUpdateSpec;
    }
    // For now, do the simple thing and have the app files directly construct
    // LayoutSchemaSpecs.  However, the construction requires calling functions
    // such as `parseFieldRef` that have to run in the tablespace, so use a
    // thunk.  We can consider defining a PreLayoutSchemaSpec type that
    // undergoes some standard transformations; that will reduce the boilerplate
    // in the app files and avoid the need for the thunk.
    export type AppLayoutsSpec = <_>() => Layout.LayoutTemplateSpec<_, Layout.OrigB>[];
    // Hack to work around
    // https://github.com/Microsoft/TypeScript/issues/17277 .
    export type AppPreProcedures = {
      viewUpdateSpec?: AppViewUpdateSpec;
      layouts?: AppLayoutsSpec;
    } & {
      [procName: string]: PreProcedure | AppViewUpdateSpec | AppLayoutsSpec;
    };

    let _procedures = new Map<string, AppPreProcedures>();
    // Called from src/apps/*/server/*-procedures.ts.
    export function procedures(appName: string, defs: AppPreProcedures) {
      if (_procedures.get(appName) != null)
        throw new Error(`Procedures for ${appName} already defined.`);
      _procedures.set(appName, defs);
    }

    // Called by ServerTablespace.internalEnsureInitialized.
    export function loadProcedures(appName: null | string) {
      // Remove old view-update configuration.
      Columns.find({fieldName: /_editable$/}).forEach((col) => {
        $$server.model.deleteColumn(col._id);
      });
      Columns.update({}, {$unset: {areTriggersDefault: true}}, {multi: true});

      let appProcedures;
      if (appName == null || (appProcedures = _procedures.get(appName)) == null)
        return;
      for (let name in appProcedures) {
        if (name == "viewUpdateSpec" || name == "layouts") continue;
        let preProc = <PreProcedure>appProcedures[name];
        try {
          // NOTE: This is an interim implementation.  Once we have a basic
          // procedure editor, procedures will be loaded from dumps just like
          // column formulas.
          let params = preProc.params.map((p): ProcedureParamInfo => ({
              name: p[0],
              // Hack: we need to support buttons in the root object, and they
              // need to receive a `this` of the root object (for consistency
              // with formulas), so provide a way to specify the root object
              // type here, even though we haven't decided to allow it in
              // parseTypeStr in general.
              type: p[1] == "$" ? rootColumnId : parseTypeStr(p[1]),
              // Why does TypeScript want p[2] to be a string??
              singular: fallback(<undefined | boolean>p[2], true)
            }));
          let proc = parseProcedure({
            name: name,
            params: params,
            body: preProc.body
          });
          $$server.model.defineProcedure(proc);
        } catch (e) {
          // Incompatible schema change?
          console.log(`Failed to define app ${appName} sample procedure ${name} on sheet ${$$.id}:`, e.stack);
        }
      }
      let viewUpdateSpec = appProcedures.viewUpdateSpec;
      if (viewUpdateSpec) {
        let defineColumnOps: (() => void)[] = [];
        for (let columnRefStr in viewUpdateSpec) {
          try {
            // `spec` might be mutated by generateDefaultTriggers below.
            let spec = EJSON.clone(viewUpdateSpec[columnRefStr]);
            let col1 = getExistingColumn(parseColumnRef(columnRefStr)[0]);
            assert(col1.formula != null);
            let col = <NonRootColumn>col1;
            // Parse formula now, not inside the deferred operation when we have
            // no type information.
            let editableFormula = parseFormula(col.parent, spec.editableIf);

            // Actually defining the column invalidates the schema cache, and
            // we'd need to typecheck (if not the whole sheet, at least some
            // columns on demand) before parsing procedures.  So do all these
            // operations at the end.  That does mean triggers won't be able to
            // refer to _editable columns, but that doesn't seem like a very
            // useful thing to do anyway.  (And _editable columns won't be able
            // to refer to each other.  If we needed to make that work, we'd
            // have to define all the columns and then parse the formulas in a
            // second pass.)
            defineColumnOps.push(() => {
              try {
                let editableId = $$server.model.defineColumn({
                  // Keep IDs stable to avoid noise in dump diffs.  TBD how this
                  // works under the final design for editability formulas and
                  // triggers.
                  thisId: <NonRootColumnId>(col._id + "_editable"),
                  parentId: col.parent,
                  index: -1,
                  fieldName: assertNotNull(objectOrFieldName(col)) + "_editable",
                  specifiedType: SpecialType.BOOL,
                  isObject: false,
                  objectName: null,
                  formula: editableFormula,
                  viewId: null,
                  singular: true,  // May as well.
                  isViewObjectRoot: false,
                  userVisible: false
                });
                // Hack.  Don't trigger the code in Model.defineColumn that tries to
                // actually add to a view, even if currently it is lax enough that it
                // would work.
                Columns.update(editableId, {$set: {view: "hidden_editable"}});
              } catch (e) {
                console.log(`Failed to set up editable column ${columnRefStr} ` +
                  `for app ${appName} on sheet ${$$.id}:`, e.stack);
              }
            });

            if (spec.newTrigger == null &&
              spec.addTrigger == null &&
              spec.removeTrigger == null) {
              Columns.update(col._id, {$set: {areTriggersDefault: true}});
              generateDefaultTriggers(col, spec);
            }
            for (let triggerType of [TriggerType.NEW, TriggerType.ADD, TriggerType.REMOVE]) {
              let body = (<fixmeAny>spec)[triggerType + "Trigger"];
              if (body == null) continue;
              let params: ProcedureParamInfo[] = [{name: "this", type: col.parent, singular: true}];
              if (triggerType != TriggerType.NEW) {
                params.push({name: "value", type: getColumnType(col), singular: true});
              }
              try {
                $$server.model.defineProcedure(parseProcedure({
                  name: triggerName(col, triggerType),
                  params: params,
                  body: body
                }));
              } catch (e) {
                // We want to know which trigger it was.  The stack trace won't tell us.
                console.log(`Failed to define app ${appName} ${triggerType} trigger for ` +
                  `${stringifyColumnRef([col._id, !col.isObject])} on sheet ${$$.id}:`, e.stack);
              }
            }
          } catch (e) {
            // Catch anything else so we can at least see the sheet.
            //
            // FIXME: Risk of swallowing unexpected exceptions and leaving the
            // tablespace in an inconsistent state.  We could fix this by adding
            // nested transaction support to the server tablespace, which
            // wouldn't be terribly hard, but I'm not going to do it now.  If we
            // actually get an inconsistent state that confuses us, then maybe I
            // will. :/
            // ~ Matt 2017-05-10
            console.log(`Failed to set up editable column ${columnRefStr} ` +
              `for app ${appName} on sheet ${$$.id}:`, e.stack);
          }
        }

        for (let op of defineColumnOps)
          op();
      }
      const layoutsFn = appProcedures.layouts;
      if (layoutsFn) {
        giRun(<_>(w: GIWrapper<_>) => {
          const giLayouts = Layout.getLayoutsCollection(w);
          let layouts = layoutsFn<_>();
          for (let layout of layouts)
            giLayouts.insert(layout);
        });
      }
    }

    // For a given appName, we expect to use a layout dump file or hard-coded
    // layouts in src/apps, but not both.  Accordingly, `mkdump` will not dump
    // the `layouts` collection if there are hard-coded layouts.  To switch a
    // sheet from hard-coded layouts to a dump, simply initialize the sheet,
    // delete the hard-coded layouts, and make a dump.  (Actually, this doesn't
    // quite work: LabelAllocationDefs outside extra space spans need to be
    // replaced with FieldAllocationDefs.  It may be easier to just rebuild the
    // layouts.)
    export function hasLayouts(appName: null | string) {
      let appProcedures;
      return (appName != null && (appProcedures = _procedures.get(appName)) != null && appProcedures.layouts != null);
    }

    function recursiveWriteDiff(root: CellHandle, diff: ObjectDiff) {
      let resp: ObjectDiffResponse = {};
      for (let field in diff) {
        let fieldDiff = diff[field];
        let fieldResp: FamilyDiffResponse = [];

        // Let exceptions propagate.  diffObjectDumps shouldn't send
        // unrecognized fields, so we want to know if there's a problem.
        let childFamily = root.childFamilyByName(field);
        for (let diffItem of fieldDiff) {
          let itemResp: FamilyDiffItemResponse = {};
          fieldResp.push(itemResp);

          let cell = diffItem.key != null ? childFamily.cell(diffItem.key) : null;
          if (diffItem.remove) {
            fixmeAssertNotNull(cell).remove_Server();  // Idempotent
          } else {
            if (diffItem.add) {
              if (cell == null) {
                itemResp.key = childFamily.new_Server();
                cell = childFamily.cell(itemResp.key);
              } else {
                cell.add_Server();  // Idempotent
              }
            }
            if (diffItem.edit) {
              runtimeCheck(fixmeAssertNotNull(cell).existsPropagateErrors(),
                () => "Editing an object that does not exist.");
              let editResp = recursiveWriteDiff(fixmeAssertNotNull(cell), diffItem.edit);
              if (editResp != null) {
                itemResp.edit = editResp;
              }
            }
          }
        }

        if (!fieldResp.every(_.isEmpty)) {
          resp[field] = fieldResp;
        }
      }
      return _.isEmpty(resp) ? null : resp;
    }
    export function writeDiff(root: QCellId, diff: ObjectDiff) {
      let resp = recursiveWriteDiff(new CellHandle(root), diff);
      cleanup();
      return resp;
    }
    export function cleanup() {
      // Call cleanup trigger if any.  (We allow using other triggers without
      // defining a cleanup trigger.)
      if (Procedures.findOne({name: TRIGGER_CLEANUP}) != null) {
        $$server.model.executeCannedTransaction(
          TRIGGER_CLEANUP, {}, {}
        );
      }
    }
  }

  Meteor.startup(() => {  // load order
    defineModelMethod(MeteorMethods.writeDiff)((root, diff) => {
      return ServerAppHelpers.writeDiff(root, diff);
    });
  });
}
