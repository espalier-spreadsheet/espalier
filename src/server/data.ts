namespace Objsheets {

  getReadableModel = () => $$server.model;

  export interface CellHandle {
    add_Server(): void;
    setValue_Server(newValue: OSValue): void;
    remove_Server(): void;
  }

  export interface FamilyHandle {
    // For reading non-view-object state data directly from StateFamilies
    // without triggering evaluation.
    readState_Server(): undefined | FamilyDoc;
    stateValues_Server(): null | OSValue[];
    stateCells_Server(): null | CellHandle[];

    // Used only by low-level code (data writes and Model.repair).
    initState_Server(): void;

    // Throws EvaluationError if the family is not editable.  (Best to check
    // first that it exists.)  At this point, view object root families and
    // view object state families are not editable, so any family that passes
    // this check is guaranteed to be a master data set state family or a
    // computed family.
    checkEditable_Server(): void;
    new_Server(): OSValue;
    add_Server(value: OSValue): void;
    remove_Server(value: OSValue): void;
    setValues_Server(values: OSValue[]): void;
  }

  Meteor.startup(() => {  // load order for CellHandle, FamilyHandle

    FamilyHandle.prototype.readState_Server = function() {
      return $$.getDocument(SheetCollections.StateFamilies, this.docId());
    };

    FamilyHandle.prototype.read = function() {
      $$server.model.evaluateFamily(this.id());
      let doc = $$.getDocument(SheetCollections.AllFamilies, this.docId());
      return runtimeCheckNotNull(doc, () => "Read of family of nonexistent/erroneous object");
    };

    FamilyHandle.prototype.stateValues_Server = function() {
      let doc = this.readState_Server();
      return (doc != null) ? doc.values : null;
    };

    FamilyHandle.prototype.stateCells_Server = function() {
      let values = this.stateValues_Server();
      return (values != null) ? values.map((v) => this.cell(v)) : null;
    };

    // Precondition for {add,remove}One: family is editable.

    function addOne(col: NonRootColumn, fam: FamilyHandle, value: OSValue) {
      if (col.formula == null) {
        if (col.singular) {
          runtimeCheck(fixmeAssertNotNull(fam.values()).length == 0,
            () => "Attempted to add multiple items to a family in a singular column.");
        }
        $$server.model.invalidateDataCache();
        StateFamilies.update(fam.docId(), {
          $addToSet: {
            values: value
          }
        });
        for (let childFamily of fam.cell(value).childFamilies()) {
          // If we have virtual object columns in the future, we'll have to
          // check for them here.  But for now, you can't insert another root,
          // so we won't get view object roots here.
          if (getExistingColumn(childFamily.columnId).formula == null)
            childFamily.initState_Server();
        }
      } else {
        // Will throw if no trigger is defined.
        $$server.model.executeCannedTransaction(
          triggerName(col, TriggerType.ADD), {this: [fam.parentCellId], value: [value]},
          {this: col.parent, value: getColumnType(col)}
        );
      }
    }

    function deleteDescendants(ch: CellHandle) {
      for (let fh of ch.childFamilies()) {
        // There should never be any docs here for computed families; they
        // should already have been cleared by invalidateDataCache.
        let doc = fh.readState_Server();
        if (doc == null) continue;
        let children = fh.stateCells_Server();
        if (children != null) {
          for (let child of children) {
            deleteDescendants(child);
          }
        }
        StateFamilies.remove(fh.docId());
      }
    }

    function removeOne(col: NonRootColumn, fam: FamilyHandle, value: OSValue) {
      if (col.formula == null) {
        $$server.model.invalidateDataCache();
        let ch = fam.cell(value);
        deleteDescendants(ch);
        StateFamilies.update(fam.docId(), {
          $pull: {
            values: value
          }
        });
      } else {
        // Will throw if no trigger is defined.
        $$server.model.executeCannedTransaction(
          triggerName(col, TriggerType.REMOVE), {this: [fam.parentCellId], value: [value]},
          {this: col.parent, value: getColumnType(col)}
        );
      }
    }

    CellHandle.prototype.add_Server = function() {
      this.containingFamily().add_Server(this.value());
    };

    CellHandle.prototype.setValue_Server = function (newValue) {
      this.remove_Server();
      this.containingFamily().add_Server(newValue);
    };

    CellHandle.prototype.remove_Server = function() {
      this.containingFamily().remove_Server(this.value());
    };

    FamilyHandle.prototype.initState_Server = function() {
      StateFamilies.insert({
        _id: this.docId(),
        values: [],
        error: null
      });
    };

    // Keep consistent with isEditable.
    FamilyHandle.prototype.checkEditable_Server = function() {
      let col = getExistingColumn(this.columnId);
      runtimeCheck(!col.isViewObjectRoot, () => "Writing to a view object root family does not make sense.");
      runtimeCheck(this.values() != null, () => "Cannot edit an erroneous family");
      if (col.formula == null) {
        runtimeCheck(col.viewObjectType == null,
          () => "Writing to view objects from the server is not yet supported but will be supported really soon!");
      } else {
        let efh = this.editabilityFamily();
        if (!(efh != null && singleElement(readFamilyForFormula($$server.model, efh.id())) === true)) {
          throw new RuntimeError(`Column '${stringifyColumnRef([col._id, !col.isObject])}' of ` +
            `object '${valueToTextIgnoreErrors($$server.model, col.parent, this.parentCellId)}' is not editable.`);
        }
      }
    };

    FamilyHandle.prototype.new_Server = function() {
      this.parent().checkObjectExists();
      let col = getExistingColumn(this.columnId);
      this.checkEditable_Server();
      if (col.formula == null) {
        // Duplicates the static check on the "new" statement in procedures, but
        // this can also be reached directly via write APIs.
        runtimeCheck(getColumnType(col) == SpecialType.TOKEN,
          () => `new cannot be used on state column ${stringifyColumnRef([col._id, !col.isObject])} ` +
          `of type ${stringifyType(getColumnType(col))}.`);
        let token = Random.id();
        addOne(col, this, token);
        return token;
      } else {
        // Will throw if no trigger is defined.
        let result: TypedSet = fixmeAssertNotNull($$server.model.executeCannedTransaction(
          triggerName(col, TriggerType.NEW), {this: [this.parentCellId]},
          {this: col.parent}, getColumnType(col)
        ));
        return singleElement(result);
      }
    };

    FamilyHandle.prototype.add_Server = function(value) {
      // Includes the equivalent of this.parent().checkObjectExists().
      if (this.cell(value).existsPropagateErrors()) return;
      let col = getExistingColumn(this.columnId);
      this.checkEditable_Server();
      addOne(col, this, value);
    };

    FamilyHandle.prototype.remove_Server = function(value) {
      // Includes the equivalent of this.parent().checkObjectExists().
      if (!this.cell(value).existsPropagateErrors()) return;
      let col = getExistingColumn(this.columnId);
      this.checkEditable_Server();
      removeOne(col, this, value);
    };

    FamilyHandle.prototype.setValues_Server = function(values) {
      this.parent().checkObjectExists();
      let col = getExistingColumn(this.columnId);
      this.checkEditable_Server();
      if (col.formula == null && col.children.length == 0) {
        // Fast path: no triggers and no descendant families.
        if (col.singular) {
          runtimeCheck(values.length <= 1,
            () => "Attempted to add multiple items to a family in a singular column.");
        }
        $$server.model.invalidateDataCache();
        StateFamilies.update(this.docId(), {
          $set: {
            values: values
          }
        });
      } else {
        // Currently broken down to remove and add triggers.  We may add a "set"
        // trigger in the future.
        let toRemove = readFamilyForFormula($$server.model, this.id()).set.shallowClone();
        let toAdd = [];
        for (let newVal of values) {
          if (toRemove.has(newVal))
            toRemove.delete(newVal);
          else
            toAdd.push(newVal);
        }
        // Remove then add: less likely to break things??
        for (let val of toRemove.elements()) {
          removeOne(col, this, val);
        }
        for (let val of toAdd) {
          addOne(col, this, val);
        }
      }
    };

  });

  export function allCellIdsInStateColumn(columnId: ColumnId) {
    return allCellIdsInColumnIgnoreErrorsGeneric(columnId, StateFamilies);
  }

  export namespace MeteorMethods {
    Meteor.startup(() => {  // load order for defineModelMethod and MeteorMethods.*
      defineModelMethod(replaceCell)(function(qCellId, newValue, clientViewObjectId, cleanup) {
        withClientViewObjectId(this, clientViewObjectId, () => {
          new CellHandle(qCellId).setValue_Server(newValue);
          if (cleanup) ServerAppHelpers.cleanup();
        });
      });
      defineModelMethod(recursiveDeleteCell)(function(qCellId, clientViewObjectId, cleanup) {
        withClientViewObjectId(this, clientViewObjectId, () => {
          new CellHandle(qCellId).remove_Server();
          if (cleanup) ServerAppHelpers.cleanup();
        });
      });
    });
  }

}
