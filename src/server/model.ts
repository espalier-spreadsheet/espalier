namespace Objsheets {

  const modelSettings =  {
    profiling: 0
  };

  export class Model implements DataWritableModel {
    // True when code is a deferred evaluation block as per
    // startDeferringEvaluation.  When false, evaluatedAll is true.  When
    // true, evaluatedAll may still be true if no changes have been made.
    private deferringEvaluation = true;
    // When true, means that all computed data is complete and accurate.
    private evaluatedAll = false;
    // When false, means that no computed families are present in DB.
    private evaluatedSome = true;
    // When false, means that no cached information dependent on the schema
    // (i.e., what invalidateSchemaCache clears) is present in the DB.
    private someSchemaCache = true;

    // TODO: Indicate which methods are intended to be public!

    constructor() {
      // Special case: create root column if missing.
      if (getColumn(rootColumnId) == null) {
        // None of the other properties should be used.
        Columns.insert({
          _id: rootColumnId,
          parent: null,
          children: [],
          fieldName: null,
          specifiedType: SpecialType.TOKEN,  // Close enough to the truth?
          cachedType: null,
          cachedTypecheckError: null,
          cachedIsReferenced: false,
          isObject: true,  // Allow children.
          objectName: null,
          referenceDisplayColumn: null,
          formula: null,
          view: null,
          // Avoid confusing code that assumes that object types have singular = false.
          singular: false,
          isViewObjectRoot: false,
          viewObjectType: null,
          userVisible: true
        });
      }

      this.repair();
      // The Model is now returned in the deferred state and the
      // ServerTablespace is responsible for the first
      // performDeferredEvaluation.
    }

    private getAllColumns() {
      return Columns.find().fetch();
    }

    // The schema editing operations (defineColumn, changeColumn*,
    // reorderColumn, deleteColumn) generally have a precondition and
    // postcondition that the sheet is valid, though not necessarily fully
    // typechecked or evaluated.  Some operations have to support additional
    // scenarios, as commented.

    /**
     * Defines a new child column of parentId at given index.
     * If index == -1, the new column is created as the last child.
     *
     * Careful: this is called in the middle of changeColumnIsObject on an
     * inconsistent sheet and has to be safe in that scenario.
     */
    public defineColumn({thisId, parentId, index, fieldName, specifiedType,
      isObject, objectName, formula, viewId, singular, isViewObjectRoot, userVisible}: DefineColumnParams) {
      // Future: validate everything
      // Future: validate no fieldName for type _token.  For _unit, there could be borderline use cases.
      // XXX: Do not allow non-object columns to have type _token?  Currently it
      // won't hurt anything, and it doesn't make sense to tighten this until we
      // finalize the flow for specifying types of newly created columns.
      if (thisId != null) {
        if (getColumn(thisId) != null) {
          // debug
          console.error("id in use", getColumn(thisId));
          throw new Meteor.Error("defineColumn-id-in-use", "A column with the specified ID already exists.");
        }
      } else {
        thisId = <NonRootColumnId>Random.id();
      }
      let parentCol = getColumn(parentId);
      if (parentCol == null) {
        throw new Meteor.Error("defineColumn-no-parent", "The specified parent column does not exist.");
      }
      if (!(-1 <= index && index <= parentCol.children.length)) {
        throw new Meteor.Error("defineColumn-index-out-of-range", "Index out of range");
      }
      if (isViewObjectRoot) {
        // Starred fields are potentially useful.
        //  parentId: must be rootColumnId
        // *index: some UIs might use it
        //  fieldName: not allowed for unkeyed object column
        //  specifiedType: must be _token
        //  isObject: must be true
        // *objectName
        //  formula: must be null
        // *viewId: some UIs might use it
        //  singular: must be false for object column
        //
        // Currently, references to the root object are banned in various places
        // but references to view object roots are unrestricted.  This is
        // inconsistent but harmless, and I'm leaving it for now.
        // ~ Matt 2018-09-03
        if (!isObject) {
          throw new Meteor.Error("defineColumn-view-object-not-object",
            "A view object root must be an object column.");
        }
        if (parentId != rootColumnId) {
          throw new Meteor.Error("defineColumn-invalid-view-object-parent",
            "Only a child of the root can be a view object root.");
        }
        if (specifiedType != "_token") {
          throw new Meteor.Error("defineColumn-invalid-view-object-type",
            "A view object root must be unkeyed.");
        }
      }
      if (!userVisible) {
        if (isObject) {
          throw new Meteor.Error("non-userVisible-object",
            "A non-user-visible column must be a field.");
        }
        if (formula == null) {
          throw new Meteor.Error("non-userVisible-state",
            "A non-user-visible column must be computed.");
        }
      }
      if (singular && isObject) {
        throw new Meteor.Error("singular-object", "Object columns cannot be singular.");
      }
      let viewObjectType = isViewObjectRoot ? thisId : parentCol.viewObjectType;
      this.checkSpecifiedType(viewObjectType, specifiedType);
      if (specifiedType == "_token" && (fieldName != null)) {
        throw new Meteor.Error("defineColumn-fieldName-no-field",
          "An unkeyed object column cannot have a fieldName.");
      }
      if (!isObject && (objectName != null)) {
        throw new Meteor.Error("defineColumn-objectName-not-isObject",
          "A column with isObject = false cannot have an objectName.");
      }
      if (formula == null) {
        if (parentCol.formula != null) {
          throw new Meteor.Error("state-under-formula", "Cannot have a state column as child of a formula column.");
        }
        if (specifiedType == null) {
          // TODO perhaps a better flow would be to leave undefined, but check when
          //  user enters data
          throw new Meteor.Error("defineColumn-type-required", "Must specify type for a state column");
        }
      }
      if (formula != null) {
        if (specifiedType == "_token") {
          throw new Meteor.Error("unkeyed-column-formula", "An unkeyed object column cannot have a formula.");
        }
        validateFormula(formula);
      }
      if (!parentCol.isObject) {
        this.changeColumnIsObject(parentCol._id, true);
        parentCol = getExistingColumn(parentId);
        // If the parent column is state, we want to add the new column after
        // the child column implicitly created by the promotion, even if the
        // index was specified as 0.
        index = -1;
      }
      this.invalidateSchemaCache();
      let col: Column = {
        _id: thisId,
        parent: parentId,
        fieldName: fieldName,
        specifiedType: specifiedType,
        cachedType: null,
        cachedTypecheckError: null,
        cachedIsReferenced: false,
        isObject: isObject,
        objectName: objectName,
        referenceDisplayColumn: null,
        formula: formula,
        children: [],
        view: viewId,
        singular: singular,
        isViewObjectRoot: isViewObjectRoot,
        viewObjectType: viewObjectType,
        userVisible: userVisible
      };
      Columns.insert(col);
      if (viewId != null) {
        new View(viewId).addColumn(thisId, true);  // FIXME: honor index
      }
      let newChildren = parentCol.children.slice();
      if (index == -1) newChildren.push(thisId);
      else newChildren.splice(index, 0, thisId);
      // Meteor is nice for so many things, but not ORM...
      Columns.update(parentCol._id, {
        $set: {
          children: newChildren
        }
      });
      if (formula == null && col.viewObjectType == null) {
        // Insert empty state families.
        for (let parentCellId of allCellIdsInStateColumn(parentId)) {
          new FamilyHandle({columnId: thisId, parentCellId}).initState_Server();
        }
      }

      return thisId;
    }

    // Should be roughly equivalent to what you get by adding a field and then
    // "promoting" to an object type via the flow in changeColumnIsObject.  I'd
    // rather do this in one call to the server. ~ Matt 2015-11-12

    public insertUnkeyedStateObjectTypeWithField(
      {parentId, index, objectName, fieldName, specifiedType, viewId}: InsertUnkeyedStateObjectTypeWithFieldParams) {
      let objectColId = this.defineColumn({
        parentId,
        index,
        fieldName: null,
        specifiedType: SpecialType.TOKEN,
        isObject: true,
        objectName,
        formula: null,
        viewId,
        singular: false,
        isViewObjectRoot: false,
        userVisible: true
      });
      let fieldColId = this.defineColumn({
        parentId: objectColId,
        index: 0,
        fieldName,
        specifiedType,
        isObject: false,
        objectName: null,
        formula: null,
        viewId,
        // XXX Keep historical behavior; consider changing.
        singular: false,
        isViewObjectRoot: false,
        userVisible: true
      });
      return [objectColId, fieldColId];
    }

    public changeColumnFieldName(columnId: ColumnId, fieldName: null | string) {
      if (columnId === rootColumnId) {
        throw new Meteor.Error("modify-root-column", "Cannot modify the root column.");
      }
      let col = getExistingColumn(columnId);
      if (fieldName === col.fieldName) {
        return;
      }
      if (col.specifiedType == "_token" && (fieldName != null)) {
        throw new Meteor.Error("defineColumn-fieldName-no-field",
          "An unkeyed object column cannot have a fieldName.");
      }
      Columns.update(columnId, {
        $set: {
          fieldName: fieldName
        }
      });
    }

    public changeColumnObjectName(columnId: ColumnId, objectName: null | string) {
      if (columnId === rootColumnId) {
        throw new Meteor.Error("modify-root-column", "Cannot modify the root column.");
      }
      let col = getExistingColumn(columnId);
      if (objectName === col.objectName) {
        return;
      }
      if (!col.isObject && (objectName != null)) {
        throw new Meteor.Error("defineColumn-objectName-not-isObject",
          "A column with isObject = false cannot have an objectName.");
      }
      Columns.update(columnId, {
        $set: {
          objectName: objectName
        }
      });
    }

    public changeColumnIsObject(columnId: ColumnId, isObject: boolean, objectName?: null | string) {
      if (columnId === rootColumnId) {
        throw new Meteor.Error("modify-root-column", "Cannot modify the root column.");
      }
      let col = getExistingColumn(columnId);
      if (isObject === col.isObject) {
        return;
      }

      if (isObject) {
        if (!col.userVisible) {
          throw new Meteor.Error("non-userVisible-object",
            "A non-user-visible column must be a field.");
        }
        if (col.formula != null) {
          // Do not generate a new object name: [fieldName] is good enough.
          this.invalidateSchemaCache();
          Columns.update(columnId, {
            $set: {
              isObject: true,
              singular: false,
              objectName: objectName !== undefined ? objectName : null
            }
          });
        } else {
          // When making a state column into an object column:
          // column type becomes SpecialType.TOKEN, a new object name is generated,
          // and existing values are moved to a newly created child column
          this.invalidateSchemaCache();
          let oldFamilies;
          if (col.viewObjectType == null) {
            oldFamilies = StateFamilies.find(familySelectorForColumn(columnId)).fetch();
            // Temporarily remove all values so defineColumn does not insert
            // incorrect child state families.
            StateFamilies.update(familySelectorForColumn(columnId), {$set: {values: []}}, {multi: true});
          }
          Columns.update(columnId, {
            $set: {
              specifiedType: SpecialType.TOKEN,
              isObject: true,
              singular: false,
              objectName: objectName !== undefined ? objectName : nextAvailableColumnName("Object"),
              fieldName: null
            }
          });
          let childId = this.defineColumn({
            parentId: columnId,
            index: 0,
            fieldName: col.fieldName,
            specifiedType: col.specifiedType,
            isObject: false,
            objectName: null,
            formula: null,
            viewId: col.view,
            // XXX Keep historical behavior; consider changing.
            singular: false,
            isViewObjectRoot: false,
            userVisible: true
          });
          if (col.viewObjectType == null) {
            for (let family of assertNotNull(oldFamilies) /* CFA */) {
              let fh = new FamilyHandle(qFamilyIdOfFamilyDoc(family));
              // State column cannot be erroneous, must have values.
              let values = fixmeAssertNotNull(family.values);
              let tokens = values.map((value) => Random.id());
              // Child state families in the single child column get added below.
              StateFamilies.update(family._id, {
                $set: {
                  values: tokens
                }
              });
              for (let [token, value] of zip(tokens, values)) {
                let childFh = fh.cell(token).childFamily(childId);
                childFh.initState_Server();
                childFh.add_Server(value);
              }
            }
          }
        }
      } else {
        if (col.isViewObjectRoot) {
          throw new Meteor.Error("remove-object-view-object-root",
            "Cannot make a view object root not an object type.");
        }
        this.checkObjectTypeInUse(columnId);
        // When making a column into a value column:
        // - If column type is SpecialType.TOKEN (must be state), values are copied from
        //   the column's only child, and that child is removed.
        // - Otherwise, column must have no children.
        if (col.specifiedType === SpecialType.TOKEN) {
          assert(col.formula == null);
          if (col.children.length != 1) {
            throw new Meteor.Error("remove-object-has-children",
              "Object must have a single field before converting to values.");
          }
          let childId = col.children[0];
          let childCol = getExistingColumn(childId);
          if (childCol.isObject || childCol.children.length != 0) {
            throw new Meteor.Error("remove-object-complex-value",
              `Child '${fallback(fallback(childCol.objectName, childCol.fieldName), "(unnamed)")}' ` +
              `is not a simple value.`);
          }
          if (childCol.formula != null) {
            throw new Meteor.Error("remove-object-computed",
              `Child '${fallback(fallback(childCol.objectName, childCol.fieldName), "(unnamed)")}' ` +
              `is computed.  Change it to state first.`);
          }
          this.invalidateSchemaCache();
          if (col.viewObjectType == null) {
            StateFamilies.find(familySelectorForColumn(columnId)).forEach((family) => {
              let fh = new FamilyHandle(qFamilyIdOfFamilyDoc(family));
              let newValues: OSValue[] = [];
              // State column cannot be erroneous, must have values
              for (let value of fixmeAssertNotNull(family.values)) {
                let subValues = fh.cell(value).childFamily(childId).stateValues_Server();  // no evaluate: state
                if (subValues != null)
                  newValues.push(...subValues);
              }
              StateFamilies.update(family._id, {
                $set: {
                  values: newValues
                }
              });
            });
          }
          this.deleteColumn(childId);  // Does not populate schema cache
          Columns.update(columnId, {
            $set: {
              isObject: false,
              specifiedType: childCol.specifiedType,
              fieldName: childCol.fieldName,
              objectName: null
            }
          });
        } else {
          // Keyed object (normally computed, but state poses no problems)
          if (col.children.length) {
            throw new Meteor.Error("remove-object-has-children", "Please delete all child columns first.");
          }
          this.invalidateSchemaCache();
          Columns.update(columnId, {
            $set: {
              isObject: false,
              objectName: null
            }
          });
        }
      }
    }

    private checkSpecifiedType(referrerViewObjectType: null | ColumnId, specifiedType: null | OSType) {
      if (specifiedType != null && typeIsReference(specifiedType)) {
        let typeCol = getExistingColumn(specifiedType);
        if (typeCol.viewObjectType != null && typeCol.viewObjectType != referrerViewObjectType) {
          throw new Meteor.Error("other-view-object",
            "References to descendants of a view object can only be stored in the same view object.");
        }
      }
    }

    public changeColumnSpecifiedType(columnId: ColumnId, specifiedType: null | OSType) {
      if (columnId === rootColumnId) {
        throw new Meteor.Error("modify-root-column", "Cannot modify the root column.");
      }
      let col = getExistingColumn(columnId);
      if (specifiedType === col.specifiedType) {
        return;
      }
      if ((col.specifiedType === SpecialType.TOKEN) !== (specifiedType === SpecialType.TOKEN)) {
        throw new Meteor.Error("change-type-token", "Cannot change a column type to or from _token.");
      }
      this.checkSpecifiedType(col.viewObjectType, specifiedType);
      let newFamilies: undefined | FamilyDoc[];
      if (col.formula == null && col.viewObjectType == null) {
        if (col.isObject) {
          throw new Meteor.Error("change-type-state-keyed-object",
            "Oops... we haven't implemented changing the key type of a state object column " +
            "since we're deprecating state keyed objects.");
        }
        if (specifiedType == null) {
          throw new Meteor.Error("change-type-state-null", "A state column must have a specified type.");
        }
        // If we get here, there should be no descendant data to worry about.
        // Reparse existing data as the new type /before/ we invalidate computed
        // reference display columns.
        this.evaluateAll();
        newFamilies = StateFamilies.find(familySelectorForColumn(columnId)).map((family) => {
          let newValues: OSValue[] = [];
          for (let v of fixmeAssertNotNull(family.values)) {
            try {
              // XXX: It's O(m*n) to parse m references to an object column with n objects.  Add caching.
              newValues.push(parseValue(specifiedType,
                valueToText(getReadableModel(), fixmeAssertNotNull(col.specifiedType), v)));
            } catch (e) {
              // Ignore
            }
          }
          // Object newly allocated by fetch(), OK to mutate
          family.values = newValues;
          return family;
        });
      }

      this.invalidateSchemaCache();
      Columns.update(columnId, {
        $set: {
          specifiedType: specifiedType
        }
      });
      if (col.formula == null && col.viewObjectType == null) {
        // XXX If we crash here, the database will be corrupt, but there are
        // probably many other cases just as bad...
        for (let family of fixmeAssertNotNull(newFamilies)) {
          // We checked above there are no children.
          StateFamilies.update(family._id, {
            $set: {
              values: family.values
            }
          });
        }
      }
    }

    private _changeColumnType(columnId: ColumnId, type: OSType) {
      Columns.update(columnId, {
        $set: {
          cachedType: type
        }
      });
    }

    private _changeColumnTypecheckError(columnId: ColumnId, typecheckError: string) {
      Columns.update(columnId, {
        $set: {
          cachedTypecheckError: typecheckError
        }
      });
    }

    // Future: API to move and copy groups of columns.  This is an order of
    // magnitude more complicated.

    // doSnapshot controls whether the computed data is snapshotted or cleared
    // when changing a computed column to state.  It has no effect in all other
    // cases.
    public changeColumnFormula(columnId: ColumnId, formula: null | Formula, doSnapshot: boolean) {
      if (columnId === rootColumnId) {
        throw new Meteor.Error("modify-root-column", "Cannot modify the root column.");
      }
      let col = getExistingColumn(columnId);
      if (formula != null) {
        if (col.specifiedType == "_token") {
          throw new Meteor.Error("unkeyed-column-formula", "An unkeyed object column cannot have a formula.");
        }
        validateFormula(formula);
      }

      if ((col.formula == null) && (formula != null)) {
        // State to computed
        for (let childColumnId of col.children) {
          if (getExistingColumn(childColumnId).formula == null) {
            throw new Meteor.Error("state-under-formula", "Cannot have a state column as child of a formula column.");
          }
        }

        this.invalidateSchemaCache();
        // Hack: When a state column is converted to a formula column,
        // automatically remove the specified type.  This should be OK because having
        // to specify a type for a formula column is a rare case.  If at some point
        // we distinguish whether state column types were user-specified or inferred
        // from data, then we could consider keeping a user-specified type here.
        Columns.update(columnId, {
          $set: {
            formula: formula,
            specifiedType: null
          }
        });

        if (col.viewObjectType == null) {
          // Currently this is allowed to just blow away existing state cells.
          StateFamilies.remove(familySelectorForColumn(columnId));
        }
      } else if ((col.formula != null) && (formula == null)) {
        // Computed to state
        let parentCol = getExistingColumn(col.parent);
        if (parentCol.formula != null) {
          throw new Meteor.Error("state-under-formula", "Cannot have a state column as child of a formula column.");
        }
        let type = getColumnType(col), newType;
        if (type == SpecialType.TOKEN) {
          throw new Meteor.Error("unkeyed-computed", "Cannot make an unkeyed state object column computed.");
        }
        if (!col.userVisible) {
          throw new Meteor.Error("non-userVisible-state",
            "A non-user-visible column must be computed.");
        }
        if (doSnapshot && type !== SpecialType.EMPTY && type !== SpecialType.ERROR) {
          // We'd better set a specifiedType that matches the evaluated families.
          // If col.specifiedType is already set, col.type will be the same and
          // this will be a no-op.
          newType = type;
        } else {
          // In this case, there are no nonempty evaluated families.
          // The user can easily change the type if it isn't what they want.
          newType = DEFAULT_STATE_FIELD_TYPE;
        }
        let oldFamilies;
        if (col.viewObjectType == null) {
          // Take a snapshot of the computed data before invalidating it.
          this.evaluateAll();
          oldFamilies = AllFamilies.find(familySelectorForColumn(columnId)).fetch();
        }

        this.invalidateSchemaCache();
        Columns.update(columnId, {
          $set: {
            formula: null,
            specifiedType: newType
          }
        });

        if (col.viewObjectType == null) {
          // Insert the previous computed data as state data.
          for (let doc of assertNotNull(oldFamilies) /* CFA */) {
            // Convert erroneous families to empty. :/
            // If not snapshotting, clear all families.
            if (!doSnapshot || doc.error != null) {
              doc.error = null;
              doc.values = [];
            }
            StateFamilies.insert(doc);
          }
        }
      } else {
        // Change formula of a computed column
        this.invalidateSchemaCache();  // inferred type may change
        Columns.update(columnId, {
          $set: {
            formula: formula
          }
        });
      }
    }

    public changeColumnIsSingular(columnId: ColumnId, singular: boolean) {
      if (columnId === rootColumnId) {
        throw new Meteor.Error("modify-root-column", "Cannot modify the root column.");
      }
      let col = getExistingColumn(columnId);
      if (singular === col.singular) {
        return;
      }
      if (singular && col.isObject) {
        throw new Meteor.Error("singular-object", "Object columns cannot be singular.");
      }
      if (singular && col.viewObjectType == null && col.formula == null &&
        pluralFamiliesInStateColumn(columnId).length > 0) {
        throw new Meteor.Error("singular-data-is-plural",
          `Cannot make the ${fallback(col.fieldName, "(unnamed)")} column singular ` +
          `because it currently contains plural data.`);
      }

      this.invalidateSchemaCache();
      Columns.update(columnId, {$set: {singular}});
    }

    public changeColumnReferenceDisplayColumn(columnId: ColumnId, referenceDisplayColumn: null | ColumnId) {
      if (columnId === rootColumnId) {
        throw new Meteor.Error("modify-root-column", "Cannot modify the root column.");
      }
      // Don't bother with further validation here because we have to be prepared
      // anyway for the choice of reference display column to become invalid as a
      // result of modifications to the sheet.
      // We use a different convention than Match.Optional...
      check(referenceDisplayColumn, Match.OneOf(String, null));
      this.invalidateDataCache();  // Affects result of toText
      Columns.update(columnId, {
        $set: {
          referenceDisplayColumn: referenceDisplayColumn
        }
      });
    }

    public reorderColumn(columnId: ColumnId, newIndex: number) {
      if (columnId === rootColumnId) {
        throw new Meteor.Error("modify-root-column", "Cannot modify the root column.");
      }
      // May affect default reference display column, which affects result of toText
      this.invalidateDataCache();
      let col = getExistingColumn(columnId);
      let parentCol = getExistingColumn(col.parent);
      let children = parentCol.children.filter((x) => x !== columnId);
      children.splice(newIndex, 0, columnId);
      Columns.update(col.parent, {
        $set: {
          children: children
        }
      });
    }

    private checkObjectTypeInUse(columnId: ColumnId) {
      let dependents = Columns.find({specifiedType: columnId}).fetch();
      if (dependents.length > 0) {
        // Would it be more spreadsheet-like to automatically convert state
        // columns back to text and clear the specified type of formula columns?
        // Conversion will fail on the key column of a state keyed object.  For
        // now, just raise an error and let the user decide what to do.
        // ~ Matt 2017-02-20
        throw new Meteor.Error("object-type-in-use",
          // Message works for both delete and setting isObject = false...
          "Cannot remove the object type " + stringifyType(columnId) +
          " because it is the specified type of the following column(s): " +
          dependents.map((c) => stringifyColumnRef([c._id, true])).join(", ")
          );
      }
    }

    // Careful: this is called in the middle of changeColumnIsObject on an
    // inconsistent sheet and has to be safe in that scenario (and in
    // particular, not populate the schema cache).
    public deleteColumn(columnId: ColumnId) {
      if (columnId === rootColumnId) {
        throw new Meteor.Error("delete-root-column", "Cannot delete the root column.");
      }
      let col = getExistingColumn(columnId);
      if (col.children.length) {
        throw new Meteor.Error("delete-column-has-children", "Please delete all child columns first.");
      }
      if (col.isObject) {
        this.checkObjectTypeInUse(columnId);
      }
      let parentCol = getExistingColumn(col.parent);
      this.invalidateSchemaCache();
      if (col.viewObjectType == null) {
        StateFamilies.remove(familySelectorForColumn(columnId));
      }
      let newChildren = parentCol.children.slice();
      newChildren.splice(parentCol.children.indexOf(columnId), 1);
      Columns.update(parentCol._id, {
        $set: {
          children: newChildren
        }
      });
      Columns.remove(columnId);
      View.removeColumnFromAll(columnId);
    }

    public deleteViewObjectType(viewObjectRoot: NonRootColumnId) {
      let col = getExistingColumn(viewObjectRoot);
      staticCheck(col.isViewObjectRoot, () => "The specified column ID is not a view object root.");
      // We don't expect this to fail, but it's safer to go through all the
      // usual checks in case we add more invariants in the future.
      let recursiveDelete = (columnId2: NonRootColumnId) => {
        let col2 = getExistingColumn(columnId2);
        for (let columnId3 of col2.children)
          recursiveDelete(columnId3);
        this.deleteColumn(columnId2);
      };
      recursiveDelete(col._id);
    }

    private evaluateComputedFamily(qFamilyId: QFamilyId) {
      let col = getExistingColumn(qFamilyId.columnId);
      let s = JSON.stringify;
      if (modelSettings.profiling >= 2) {
        console.log(`[evaluateFamily1(qFamilyId=${s(qFamilyId)}) formula=${s(col.formula)}]`);
      }
      let typecheckError = getColumnTypecheckError(col);
      if (typecheckError != null) {
        throw new RuntimeError(`Formula failed type checking: ${typecheckError}`);
      }
      let result: TypedSet;
      let vars = new JSONKeyedMap([["this", new TypedSet(col.parent, new JSONSet([qFamilyId.parentCellId]))]]);
      result = evaluateFormulaDeterministic(this, vars, fixmeAssertNotNull(col.formula));
      if (modelSettings.profiling >= 2) {
        console.log("[/evaluateFamily1]");
      }
      if (col.singular) {
        runtimeCheck(result.set.size() <= 1,
          () => "Column is marked singular but formula returned multiple elements.");
      }
      return result;
    }

    // XXX This is a little inefficient; leave it until we implement the dep engine. ~ Matt 2018-09-05
    private getViewObjectStateFamilyValues(viewObjectType: NonRootColumnId, qFamilyId: QFamilyId) {
      let req = assertNotNull(this.activeViewObjects.get(getViewObjectContext(viewObjectType).id));
      let getFamilyData = (fam1: FamilyHandle): null | ViewObjectFamilyDump => {
        let col = getExistingColumn(fam1.columnId);
        // Only named state columns can have data.  This is a simple rule
        // that allows the custom-layout UI to have a sane condition for a
        // state column to be used.
        if (col.fieldName == null && col.objectName == null) return null;
        let ch = fam1.parent();
        let objData;
        if (ch.columnId == req.type) {
          objData = req.stateDump;
        } else {
          // XXX: Untested since the UI doesn't support generating such a request yet
          let parentFamData = getFamilyData(ch.containingFamily());
          if (parentFamData == null || parentFamData instanceof Array) return null;
          let sk = JSON.stringify(ch.value());
          if (!_.has(parentFamData, sk)) return null;
          objData = parentFamData[sk];
        }
        if (!_.has(objData, fam1.columnId)) return null;
        return objData[fam1.columnId];
      };
      let famData = getFamilyData(new FamilyHandle(qFamilyId));
      // XXX: Validate type and singular and no duplicates
      if (famData == null) {
        return [];
      } else if (famData instanceof Array) {
        return famData;
      } else {
        // XXX: Untested since the UI doesn't support generating such a request yet
        return Object.keys(famData).map((sk) => JSON.parse(sk));
      }
    }

    private pendingFamilies = new JSONSet<QFamilyId>();

    public evaluateFamily(qFamilyId: QFamilyId) {
      let fh = new FamilyHandle(qFamilyId);
      let docId = fh.docId();
      let ce = $$.getDocument(SheetCollections.AllFamilies, docId);
      if (ce == null) {
        if (this.pendingFamilies.has(qFamilyId)) {
          return null;
        }
        try {
          this.pendingFamilies.add(qFamilyId);

          // Check that the object exists, potentially evaluating ancestor
          // families.  If not, return null without adding an entry for the
          // nonexistent family to the DB.  (It's nice to have a canonical fully
          // evaluated state that doesn't include random nonexistent families
          // that may have been queried by clients.  Once we add dependency
          // tracking, this becomes more complicated because we do need to
          // represent nonexistent families that are transitive dependencies of
          // existing ones.)
          try {
            fh.parent().checkObjectExists();
          } catch (e) {
            if (e instanceof RuntimeError) {
              return null;
            } else {
              throw e;
            }
          }

          this.evaluatedSome = true;
          let col = getExistingColumn(qFamilyId.columnId);
          if (col.isViewObjectRoot) {
            ce = {
              _id: docId,
              values: null,
              error: "This is a view object root family.  View objects are ephemeral " +
                "and no list of all existing view objects is available."
            };
          } else if (col.formula == null) {
            if (col.viewObjectType != null) {
              ce = {
                _id: docId,
                values: this.getViewObjectStateFamilyValues(col.viewObjectType, qFamilyId),
                error: null
              };
            } else {
              ce = $$.getDocument(SheetCollections.StateFamilies, docId);
              if (ce == null)  // narrowing
                throw new Error("Missing state family");
            }
          } else {
            ce = {
              _id: docId,
              values: null,
              error: null
            };
            try {
              let content = this.evaluateComputedFamily(qFamilyId);
              ce.values = content.elements();
            } catch (e) {
              if (e instanceof RuntimeError) {
                ce.error = e.message;
              } else {
                throw e;
              }
            }
          }
          AllFamilies.insert(ce);
        } finally {
          // May as well make pendingFamilies robust to unexpected exceptions,
          // even though the model may be corrupt anyway if there's an
          // unexpected exception. ~ Matt 2017-04-15
          this.pendingFamilies.delete(qFamilyId);
        }
      }

      if (ce.values != null) {
        return new TypedSet(getColumnType(getExistingColumn(qFamilyId.columnId)), new JSONSet(ce.values));
      } else {
        return null;
      }
    }

    private inTypecheckFormula = false;

    // This method serves two purposes:
    // 1. Determine the type that the column should be assumed to have for the
    //    purposes of other formulas.
    // 2. Determine whether the formula passes type checking before we try to
    //    evaluate it.
    // These don't have to be done at the same time, but for now that's
    // convenient.  In fact, it would be better to separate the passes so that
    // cyclic dependencies that don't actually affect the inferred types don't
    // interfere with type inference.

    public typecheckColumn(columnId: ColumnId) {
      let col = getExistingColumn(columnId);
      // Cheap way of detecting which columns are referenced by a formula:
      // assume that typecheckColumn is called on them during typechecking of
      // the formula.
      if (this.inTypecheckFormula && !col.cachedIsReferenced) {
        Columns.update(col._id, {$set: {cachedIsReferenced: true}});
      }
      // The one place it's OK to read cachedType on the server.
      let type = col.cachedType;
      if (type == null) {
        this.someSchemaCache = true;
        // Formula columns of unspecified type are set to SpecialType.ERROR at the
        // beginning for cycle detection.
        type = fallback(col.specifiedType, SpecialType.ERROR);
        this._changeColumnType(columnId, type);
        if (col.formula != null) {
          let savedInTypecheckFormula = this.inTypecheckFormula;
          this.inTypecheckFormula = true;
          try {
            let vars = new JSONKeyedMap([["this", fixmeAssertNotNull(col.parent)]]);
            let actualType = typecheckFormula(this, vars, col.formula);
            if (col.specifiedType != null) {
              let t = type;
              staticCheck(commonSupertype(col.specifiedType, type) === col.specifiedType,
                () => `Column '${stringifyColumnRef([columnId, true])}' ` +
                `formula returns '${stringifyType(t)}', ` +
                `which is not convertible to the specified type '${col.specifiedType}'`);
            } else {
              type = actualType;
              this._changeColumnType(columnId, type);
            }
          } catch (e) {
            if (!(e instanceof StaticError)) {
              throw e;
            }
            // If type was unspecified, it is left as SpecialType.ERROR, i.e., unknown
            // for the purposes of other formulas.
            this._changeColumnTypecheckError(columnId, e.message);
          } finally {
            this.inTypecheckFormula = savedInTypecheckFormula;
          }
        }
      }

      if (typeIsReference(type))
        getExistingColumn(type);  // assert existence
      return type;
    }

    public typecheckAll() {
      if (modelSettings.profiling >= 1) {
        console.log("<typecheckAll>");
      }
      for (let col of this.getAllColumns()) {
        this.typecheckColumn(col._id);
      }
      if (modelSettings.profiling >= 1) {
        console.log("</typecheckAll>");
      }
    }

    public evaluateAll() {
      if (this.evaluatedAll)
        return;
      if (modelSettings.profiling >= 1) {
        console.log("<evaluateAll>");
      }
      this.typecheckAll();

      let evaluateSubtree = (qCellId: QCellId) => {
        let col = getExistingColumn(qCellId.columnId);
        for (let childColId of col.children) {
          let tset = this.evaluateFamily({
            columnId: childColId,
            parentCellId: qCellId.cellId
          });
          if (tset != null) {
            for (let value of tset.elements()) {
              let childQCellId = {
                columnId: childColId,
                cellId: cellIdChild(qCellId.cellId, value)
              };
              evaluateSubtree(childQCellId);
            }
          }
        }
      };

      // Future: Only evaluate what users are viewing.
      evaluateSubtree({
        columnId: rootColumnId,
        cellId: rootCellId
      });

      for (let [viewObjectId, viewObjectReq] of this.activeViewObjects) {
        let col = getColumn(viewObjectReq.type);
        if (col != null && col.isViewObjectRoot) {
          let viewObjectRootQCellId: QCellId = {columnId: viewObjectReq.type, cellId: [{viewObjectRoot: true}]};
          currentViewObject.withValue({type: viewObjectReq.type, id: viewObjectId,
            queryParams: viewObjectReq.queryParams},
            () => evaluateSubtree(viewObjectRootQCellId));
        } else {
          // The request is not valid at this time; ignore it.
        }
      }

      if (modelSettings.profiling >= 1) {
        console.log("</evaluateAll>");
      }
      this.evaluatedAll = true;
    }

    // These four are public only so they can be called by
    // ServerTablespace.run.
    public startDeferringEvaluation() {
      assert(!this.deferringEvaluation);
      this.deferringEvaluation = true;
    }
    public performDeferredEvaluation() {
      assert(this.deferringEvaluation);
      // If this throws an exception, we consider that we are still deferring
      // evaluation, so ServerTablespace.run can roll back and call
      // notifyUnevaluatedChangesRolledBack.  That seems reasonable.
      this.evaluateAll();
      this.deferringEvaluation = false;
    }
    public notifyUnevaluatedChangesRolledBack() {
      assert(this.deferringEvaluation);
      this.deferringEvaluation = false;
      this.notifyResetToEvaluatedState();
    }
    public notifyResetToEvaluatedState() {
      this.evaluatedAll = true;
      this.evaluatedSome = true;  // Don't know; be conservative
      this.someSchemaCache = true;
    }
    public notifyResetToUnknownState() {
      this.evaluatedSome = true;
      this.someSchemaCache = true;
      this.invalidateSchemaCache();
    }

    // For parts of the schema cache that live outside Model.
    public notifySomeSchemaCache() {
      this.someSchemaCache = true;
    }

    // Call this before modifying anything that affects the structure of the
    // sheet or the types of columns.
    public invalidateSchemaCache() {
      // Doing this regardless of typecheckedSome makes the correctness clearer.
      this.invalidateDataCache();
      if (!this.someSchemaCache)
        return;
      if (modelSettings.profiling >= 1) {
        console.log("--- invalidateSchemaCache ---");
      }
      $$.formulaEngine.invalidateSchemaCache();
      // A little faster than iterating over the columns here.
      Columns.update({}, {$set: {
        cachedType: null,
        cachedTypecheckError: null,
        cachedIsReferenced: false
      }}, {multi: true});
      this.someSchemaCache = false;
    }

    // Call this before modifying anything that affects computed data.
    public invalidateDataCache() {
      assert(this.deferringEvaluation,
        () => "invalidateDataCache must be called inside a deferred evaluation " +
        "block to define the point at which full evaluation should occur.");
      // We could have this.evaluatedAll && !this.evaluatedSome if there were no
      // computed families to evaluate, and the state change we're about to make
      // could be adding the first ones.
      this.evaluatedAll = false;
      // Make repeated calls fast so we don't have to avoid them.
      if (!this.evaluatedSome)
        return;
      if (modelSettings.profiling >= 1) {
        console.log("--- invalidateDataCache ---");
      }
      AllFamilies.remove({});
      this.evaluatedSome = false;
    }

    // Procedure object:
    // {
    //   name: string
    //   params: list of {name (str), type (str as usual), singular (bool)}
    //   body: statements list
    // }

    public defineProcedure(proc: Mongo.DocWithOptionalId<ProcedureDoc>) {
      proc._id = Random.id();
      validateProcedure(proc);
      Procedures.insert(proc);
      return proc._id;
    }

    // Overwrites (and optionally renames) the existing procedure with procId.

    public redefineProcedure(procId: string, proc: ProcedureDoc) {
      delete proc._id;  // ignored if passed
      validateProcedure(proc);
      // It's OK to pass a replacement document without an id:
      // https://docs.mongodb.org/manual/reference/method/db.collection.update/#update-parameter
      // No effect if no procedure with the ID exists.  OK?
      Procedures.update(procId, proc);
    }

    public deleteProcedure(procId: string) {
      // No effect if no procedure with the ID exists.  OK?
      Procedures.remove(procId);
    }

    // NOTICE: This may throw an exception and leave changes to the sheet data
    // that should be rolled back by ServerTablespace.run (though all
    // objsheets-level invariants should still hold when an exception is
    // thrown).
    //
    // The returned TypedSet will be null unless an expectedReturnType is
    // specified.
    public executeCannedTransaction(name: string, argsObj: ProcedureArgsObj,
      expectedParams?: {[name: string]: OSType},
      expectedReturnType: OSType = SpecialType.ERROR,
      returnRedirect = false): undefined | TypedSet {
      if (returnRedirect) {
        assert(expectedReturnType == "text" /*url*/);
      }
      const proc = Procedures.findOne({
        name: name
      });
      if (proc == null) {
        throw new Meteor.Error("no-such-procedure", `No such procedure '${name}'.`);
      }
      // Typecheck the procedure.  TODO: Cache this like for column formulas.
      try {
        let actualReturnType = typecheckProcedure(this, proc, returnRedirect);
        staticExpectType("procedure return type", actualReturnType, expectedReturnType);
        if (expectedParams) {
          let actualParams: {[name: string]: OSType} = {};
          for (let param of proc.params)
            actualParams[param.name] = param.type;
          staticCheck(_.isEqual(actualParams, expectedParams),
            () => "Procedure does not take the expected parameters.");
        }
      } catch (e) {
        if (!(e instanceof StaticError)) {
          throw e;
        }
        console.log(e.stack);
        throw new Meteor.Error("procedure-ill-typed",
          `Procedure '${name}' is ill-typed with respect to the current schema and cannot be executed.`);
      }
      // Future: Validate argument types!
      // Future: Add built-in parameters (clientUser, currentTime) here.
      let args = new JSONKeyedMap(proc.params.map<[string, TypedSet]>(
        (param) => {
          staticCheck(_.has(argsObj, param.name), () => `Missing argument ${param.name} to ${proc.name}`);
          return [param.name, new TypedSet(param.type, set(argsObj[param.name]))];
        }));
      try {
        let ret = executeProcedure(this, proc, args, returnRedirect);
        return (expectedReturnType != SpecialType.ERROR) ? ret : undefined;
      } catch (e) {
        // Our changes will be rolled back by ServerTablespace.run.
        if (e instanceof RuntimeError) {
          console.log("Transaction failed:", name, argsObj, e.stack);
          // Future: How much information to send to unprivileged clients?
          throw new Meteor.Error("transaction-failed", "Transaction failed.");
        } else {
          throw e;
        }
      }
    }

    activeViewObjects = new Map<ViewObjectId, ViewObjectRequest>();
    addViewObject(viewObjectId: ViewObjectId, req: ViewObjectRequest) {
      assert(this.activeViewObjects.get(viewObjectId) == null);
      this.invalidateDataCache();
      this.activeViewObjects.set(viewObjectId, req);
    }
    removeViewObject(viewObjectId: ViewObjectId) {
      assert(this.activeViewObjects.get(viewObjectId) != null);
      this.invalidateDataCache();
      this.activeViewObjects.delete(viewObjectId);
    }

    private repair() {
      // We can add repair steps for crashes and bugs in old versions of the
      // code here as well as automatic conversions of old data formats.
      //
      // NOTE: Given that our goal is to accept certain data sets that don't
      // satisfy all the normal invariants, we have to be extremely careful not
      // to call code that depends on invariants before we establish them.  In
      // particular, documents in sheet collections may not conform to the
      // declared types.  I considered using weaker type declarations during
      // repair, but I don't think that would have much value in catching bugs
      // relative to the hassle.
      // ~ Matt 2017-12-15

      // Clear typechecking info.
      Columns.update({}, {$set: {
        cachedType: null,
        cachedTypecheckError: null,
        cachedIsReferenced: false
      }}, {multi: true});

      // Remove the `type` and `typecheckError` fields.  They have been renamed
      // to `cachedType` and `cachedTypecheckError` and will be recomputed
      // later.
      Columns.update({}, {$unset: {type: true, typecheckError: true}}, {multi: true});

      // Initialize `singular` to false.
      Columns.update({singular: {$exists: false}}, {$set: {singular: false}}, {multi: true});

      // Initialize `isViewObjectRoot` to false and `viewObjectType` to null.
      Columns.update({isViewObjectRoot: {$exists: false}},
        {$set: {isViewObjectRoot: false, viewObjectType: null}}, {multi: true});

      // Initialize `userVisible` to true.
      Columns.update({userVisible: {$exists: false}}, {$set: {userVisible: true}}, {multi: true});

      // Copy state families from Cells to StateFamilies.
      $$server.oldCellsCollection.find().forEach((cellsDoc) => {
        let col = getColumn(cellsDoc.column);
        // If the column is missing, just drop the family like we would below.
        if (col && col.formula == null) {
          cellsDoc._id = new FamilyHandle(
            {columnId: cellsDoc.column, parentCellId: cellsDoc.key}).docId();
          delete cellsDoc.column;
          delete cellsDoc.key;
          StateFamilies.upsert(cellsDoc._id, cellsDoc);
        }
      });
      $$server.oldCellsCollection.dropIfExists();

      // Persistent placeholders are no longer supported.
      StateFamilies.update({}, {$unset: {numPlaceholders: true}}, {multi: true});

      // Remove orphaned columns and broken column child pointers.
      let liveColumnIds = new Set();
      let scanColumnSubtree = (col: Column) => {
        liveColumnIds.add(col._id);
        let validChildren = [];
        for (let childColId of col.children) {
          let childCol = getColumn(childColId);
          if (childCol != null) {
            validChildren.push(childColId);
            scanColumnSubtree(childCol);
          }
        }
        if (validChildren.length != col.children.length)
          Columns.update(col._id, {$set: {children: validChildren}});
      };
      // Note: the root column is inserted in the Model constructor if it
      // doesn't exist.
      scanColumnSubtree(getExistingColumn(rootColumnId));
      for (let col of Columns.find().fetch()) {
        if (!liveColumnIds.has(col._id)) {
          Columns.remove(col._id);
        }
      }

      // Remove all families except reachable state families.  (Computed
      // families will be reevaluated later.)
      let liveFamilies = new EJSONSet<OldQFamilyId>();
      // Traversal code adapted from evaluateAll.
      let scanCellSubtree = (ch: CellHandle) => {
        for (let fh of ch.childFamilies()) {
          let col = getExistingColumn(fh.columnId);
          if (col.formula == null && !col.isViewObjectRoot) {
            liveFamilies.add(fh.id());
            let children = fh.stateCells_Server();
            if (children == null) {
              // Add missing state families.  If there's a doc with values ==
              // null, guess the insert will throw an exception.
              fh.initState_Server();
              children = [];
            }
            for (let child of children) {
              scanCellSubtree(child);
            }
          }
        }
      };
      scanCellSubtree(new CellHandle(rootQCellId));
      for (let ce of StateFamilies.find().fetch()) {
        if (!liveFamilies.has(qFamilyIdOfFamilyDoc(ce))) {
          StateFamilies.remove(ce);
        } else {
          if (ce.values != null) {
            // Convert to packed dates.
            let modified = false;
            let convertArray = (arr: OldOSValue[]) => {
              for (let i = 0; i < arr.length; i++)
                arr[i] = convert(arr[i]);
            };
            let convert = (value: OldOSValue) => {
              if (value instanceof Date) {
                modified = true;
                return value.getTime();
              }
              if (value instanceof Array) {
                // Reference
                convertArray(value);
              }
              return value;
            };
            convertArray(ce.values);
            if (modified)
              StateFamilies.update(ce._id, {$set: {values: ce.values}});
          }
        }
      }

      for (let col of Columns.find().fetch()) {
        if (col.formula != null) {
          // A mitigation for the common problem of formula operations being
          // removed.  There are obviously many other ways a bad database can break
          // us.
          try {
            validateFormula(col.formula);
          } catch (e) {
            if (!(e instanceof Meteor.Error && e.error === "invalid-formula")) {
              throw e;
            }
            console.log(`Column '${stringifyColumnRef([col._id, true])}' contains invalid formula ` +
              `${JSON.stringify(col.formula)}: ${e.message}.  Resetting.`);
            col.formula = DUMMY_FORMULA;
            col.specifiedType = null;
            Columns.update(col._id, col);
          }
        }
      }

      giRun((w) => {
        const giLayouts = Layout.getLayoutsCollection(w);
        giLayouts.find().forEach((l) => {
          giLayouts.update(l._id, Layout.repairLayoutTemplateSpec(l));
        });
      });
    }

    private newObjectRecursive(columnId: ColumnId, ancestorQCellId: QCellId): CellId {
      if (columnId == ancestorQCellId.columnId) {
        return ancestorQCellId.cellId;
      } else {
        if (columnId === rootColumnId)  // narrowing
          throw new StaticError("newObjectRecursive reached root: incorrect ancestorQCellId? passed");
        let col = getExistingColumn(columnId);
        let parentCellId = this.newObjectRecursive(col.parent, ancestorQCellId);
        let key = new FamilyHandle({columnId: columnId, parentCellId: parentCellId}).new_Server();
        return cellIdChild(parentCellId, key);
      }
    }

    /**
     * Creates a new cell.
     * value == null means a "new" operation.
     * newColumnType != null means create a new child column of 'columnId' and put the value there.
     * Notice that if columnId does not already designate an object column, it will be promoted
     * to one by defineColumn.
     */
    public addCellRecursive({columnId, ancestorQCellId, value, newColumnType, viewId, cleanup}:
      AddCellRecursiveParams): AddCellRecursiveResponse {
      let col = getExistingColumn(columnId);
      if (newColumnType != null) {
        let promotingAncestorQCellId = !col.isObject && (ancestorQCellId.columnId == columnId);
        // Same error that FamilyHandle.prototype.checkEditable_Server would
        // eventually report if we got that far, but we need to detect it before
        // we try to call promoteCellId, which doesn't work on view objects.
        runtimeCheck(!promotingAncestorQCellId || col.viewObjectType == null,
          () => "Writing to view objects from the server is not yet supported but will be supported really soon!");
        // Will raise "state-under-formula" exception if col is computed.
        columnId = this.defineColumn({
          parentId: columnId,
          index: -1,
          fieldName: nextAvailableColumnName("value"),
          specifiedType: newColumnType,
          isObject: false,
          objectName: null,
          formula: null,
          viewId: viewId,
          // XXX Keep historical behavior; consider changing.
          singular: false,
          isViewObjectRoot: false,
          userVisible: true
        });
        col = getExistingColumn(columnId);
        if (promotingAncestorQCellId)
          ancestorQCellId = this.promoteCellId(ancestorQCellId);
      }
      let parentCellId = this.newObjectRecursive(col.parent, ancestorQCellId);
      let fam = new FamilyHandle({columnId: columnId, parentCellId: parentCellId});
      if (value == null) {
        value = fam.new_Server();
      } else {
        fam.add_Server(value);
      }
      return {qFamilyId: fam.id(), value: value};
    }

    /**
     * Auxiliary function to addCellRecursive.
     * Called when the column containing the given cell was promoted to an object column,
     * and adjusts the id to point to the object.
     *
     * Precondition: the cell does not belong to a view object.
     */
    private promoteCellId(oldValueCellId: QCellId) {
      let promotedColumnId = oldValueCellId.columnId;
      let newValueColumnId = getExistingColumn(promotedColumnId).children[0];
      let oldValueCellHandle = new CellHandle(oldValueCellId);
      let containingFamily = oldValueCellHandle.containingFamily();
      let value = oldValueCellHandle.value();
      // no evaluate: state (both columns)
      for (let promotedObjHandle of fixmeAssertNotNull(containingFamily.stateCells_Server())) {
        if (fixmeAssertNotNull(promotedObjHandle.childFamily(newValueColumnId).stateValues_Server())[0] == value)
          // This is the one we are looking for.
          return promotedObjHandle.id();
      }
      throw new Error("Could not find promoted object.");
    }

    // Implement DataWritableModel in src/language/procedures.ts.
    public FamilyHandle_new(fam: FamilyHandle) {
      return fam.new_Server();
    }
    public FamilyHandle_add(fam: FamilyHandle, value: OSValue) {
      fam.add_Server(value);
    }
    public FamilyHandle_remove(fam: FamilyHandle, value: OSValue) {
      fam.remove_Server(value);
    }
    public CellHandle_remove(ch: CellHandle) {
      ch.remove_Server();
    }
    public FamilyHandle_setValues(fam: FamilyHandle, values: OSValue[]) {
      fam.setValues_Server(values);
    }
  }

  notifySomeSchemaCache = () => $$server.model.notifySomeSchemaCache();

  export function withClientViewObjectId<R>(invocation: Meteor.MethodInvocation,
    clientViewObjectId: null | undefined | string, body: () => R) {
    let viewObjectContext: null | ViewObjectContext;
    if (clientViewObjectId == null) {
      viewObjectContext = null;
    } else {
      let serverViewObjectId = invocation.connection.id + ":" + clientViewObjectId;
      let req = runtimeCheckNotNull($$server.model.activeViewObjects.get(serverViewObjectId),
        () => "The specified view object does not exist.");
      viewObjectContext = {type: req.type, id: serverViewObjectId, queryParams: req.queryParams};
    }
    return currentViewObject.withValue(viewObjectContext, body);
  }

  Meteor.startup(() => {  // load order for defineModelMethod
    // The model methods do not automatically evaluate so that we can do bulk
    // changes from the server side, but for now we always evaluate after each
    // change from the client.  It would be a little harder for the client itself
    // to request this via another method (it would require a callback).
    // Future: validation!
    defineModelMethod(MeteorMethods.defineColumn)((params) => {
      $$server.model.defineColumn(params);
    });
    defineModelMethod(MeteorMethods.insertUnkeyedStateObjectTypeWithField)((params) => {
      $$server.model.insertUnkeyedStateObjectTypeWithField(params);
    });
    defineModelMethod(MeteorMethods.changeColumnFieldName)((columnId, fieldName) => {
      $$server.model.changeColumnFieldName(columnId, fieldName);
    });
    defineModelMethod(MeteorMethods.changeColumnIsObject)((columnId, isObject) => {
      $$server.model.changeColumnIsObject(columnId, isObject);
    });
    defineModelMethod(MeteorMethods.changeColumnObjectName)((columnId, objectName) => {
      $$server.model.changeColumnObjectName(columnId, objectName);
    });
    defineModelMethod(MeteorMethods.changeColumnSpecifiedType)((columnId, specifiedType) => {
      $$server.model.changeColumnSpecifiedType(columnId, specifiedType);
    });
    defineModelMethod(MeteorMethods.changeColumnFormula)((columnId, formula) => {
      $$server.model.changeColumnFormula(columnId, formula, true);
    });
    defineModelMethod(MeteorMethods.changeColumnIsSingular)((columnId, singular) => {
      $$server.model.changeColumnIsSingular(columnId, singular);
    });
    defineModelMethod(MeteorMethods.changeColumnReferenceDisplayColumn)((columnId, referenceDisplayColumn) => {
      $$server.model.changeColumnReferenceDisplayColumn(columnId, referenceDisplayColumn);
    });
    defineModelMethod(MeteorMethods.reorderColumn)((columnId, newIndex) => {
      $$server.model.reorderColumn(columnId, newIndex);
    });
    defineModelMethod(MeteorMethods.deleteColumn)((columnId) => {
      $$server.model.deleteColumn(columnId);
    });
    defineModelMethod(MeteorMethods.addCellRecursive)(function(params) {
      return withClientViewObjectId(this, params.clientViewObjectId, () => {
        let ret = $$server.model.addCellRecursive(params);
        if (params.cleanup) ServerAppHelpers.cleanup();
        return ret;
      });
    });
    function executeCannedTransactionCommon(invocation: Meteor.MethodInvocation,
      name: string, argsObj: ProcedureArgsObj, clientViewObjectId: null | string, returnRedirect: boolean) {
      let ret = withClientViewObjectId(invocation, clientViewObjectId,
        () => $$server.model.executeCannedTransaction(name, argsObj, undefined,
          returnRedirect ? SpecialType.TEXT : undefined, returnRedirect));
      if (returnRedirect) {
        if (ret == null)  // narrowing
          throw new Error("The procedure should have been typechecked to always return a value.");
        runtimeCheck(ret.set.size() <= 1, () => "Expected at most one redirect URL");
        return ret.set.size() == 0 ? null : /*typechecked*/ <string>ret.elements()[0];
      } else {
        return null;
      }
    }
    defineModelMethod(MeteorMethods.executeCannedTransaction)(function(name, argsObj, clientViewObjectId) {
      executeCannedTransactionCommon(this, name, argsObj, clientViewObjectId, false);
    });
    defineModelMethod(MeteorMethods.executeCannedTransactionWithRedirect)(function(name, argsObj, clientViewObjectId) {
      return executeCannedTransactionCommon(this, name, argsObj, clientViewObjectId, true);
    });
  });

}
