// Load order for "ServerTablespace extends Tablespace".  Meteor.startup won't
// work for this.
/// <reference path="../tablespace.ts" />

namespace Objsheets {

  // See the overview comment at the top of src/tablespace.ts.

  // Make the collections writable in the server build.
  export interface SheetCollection<T> extends SimpleCollection<T> {}

  // Unfortunately, self-repair is nontrivial to support because we'd need the
  // collections available in tx.collectionIndex.  We'd have to either
  // initialize the tablespaces on demand or put in the raw collections and
  // reuse them if the tablespaces are initialized later.  Plus, to avoid
  // concurrency problems, we'd need to wait for self-repair to finish before
  // initializing any tablespaces.  This is all doable, but let's not bother
  // unless/until we care about this feature. ~ Matt 2017-02-01
  tx.selfRepairMode = null;

  // We currently don't support undoing (or redoing) transactions from a
  // previous run of the server since we don't know if they're valid for this
  // run of the server.  We could just flag them as needing repair after
  // undo/redo, but it becomes complicated if the repair causes us to diverge
  // from the state expected by the saved transactions.  Given that we don't
  // support this or self-repair, we may as well use an in-memory transactions
  // collection for now and gain a little performance.
  tx.setTransactionsCollection(newLocalCollection<TransactionDoc>("transactions"));
  // Clean up from older versions.
  new Mongo.Collection<TransactionDoc>("transactions").dropIfExists();

  function changedFieldsToModifier(fields: fixmeAny) {
    // MongoDB does not like "$set: {}" or "$unset: {}", so add each
    // operator only when we see the first field.
    // NOTE: babrahams:transactions may not care but let's keep the code.
    let mod: fixmeAny = {};
    for (let k in fields) {
      if (fields[k] === undefined) {
        if (mod.$unset == null)
          mod.$unset = {};
        mod.$unset[k] = true;
      } else {
        if (mod.$set == null)
          mod.$set = {};
        mod.$set[k] = fields[k];
      }
    }
    return mod;
  }

  class ServerCollectionHandler<T extends Mongo.DocWithStringId> {
    currentCollection: Mongo.Collection<T>;
    cache: FastLookupCollectionCache<T>;
    private needRollback = false;

    constructor(public committedCollection: SimpleCollection<T>, private isPrimary: boolean,
      private committedDocs: LocalCollection._IdMap<T>, public freezeFunc: undefined | ((doc: T) => void)) {
      this.currentCollection = newLocalCollection<T>(this.committedCollection._name);
      this.committedCollection.find().forEach((doc) => { this.currentCollection.insert(doc); });
      this.cache = new FastLookupCollectionCache<T>(this.freezeFunc);
      this.currentCollection.find().observe({
        added: (document) => {
          this.cache.add(document);
        },
        changed: (newDocument, oldDocument) => {
          this.cache.change(newDocument);
        },
        removed: (oldDocument) => {
          this.cache.remove(oldDocument._id);
        }
      });
    }

    startTransaction() {
      this.needRollback = true;
      if (this.isPrimary) {
        // If this transaction is an undo or redo, we want tx to write the
        // undo/redo of primary data to the current collection so we can defer
        // publishing until we have successfully updated any derived
        // collections.
        tx.collectionIndex[this.committedCollection._name] = this.currentCollection;
      }
    }
    pushDownTransaction(useTx: boolean) {
      useTx = useTx && this.isPrimary;
      // Defer changes because mutating this.committedCollection while we diff
      // it might cause problems.
      let queue: (() => void)[] = [];
      DiffSequence.diffQueryUnorderedChanges(this.committedDocs, this.currentCollection._collection._docs, {
        added: (id: string, fields: fixmeAny) => {
          queue.push(() => {
            fields._id = id;
            if (useTx) {
              tx.insert(this.committedCollection, fields);
            } else {
              this.committedCollection.insert(fields);
            }
          });
        },
        changed: (id: string, fields: fixmeAny) => {
          queue.push(() => {
            let mod = changedFieldsToModifier(fields);
            if (useTx) {
              tx.update(this.committedCollection, id, mod);
            } else {
              this.committedCollection.update(id, mod);
            }
          });
        },
        removed: (id: string) => {
          queue.push(() => {
            if (useTx) {
              tx.remove(this.committedCollection, id);
            } else {
              this.committedCollection.remove(id);
            }
          });
        }
      });
      for (let op of queue) {
        op();
      }
      if (this.isPrimary) {
        // If useTx = true, now we're going to do the actual tx commit, and we
        // want tx to write to the synchronous collection and not the temporary
        // collection.
        tx.collectionIndex[this.committedCollection._name] =
          useTx ? this.committedCollection : null;
      }
      this.needRollback = false;
    }
    clearTransaction() {
      if (this.isPrimary) {
        // tx shouldn't be writing anything at all outside a tablespace
        // transaction.
        tx.collectionIndex[this.committedCollection._name] = null;
      }
      if (this.needRollback) {
        this.needRollback = false;
        let queue: (() => void)[] = [];
        DiffSequence.diffQueryUnorderedChanges(this.currentCollection._collection._docs, this.committedDocs, {
          added: (id: string, fields: fixmeAny) => {
            queue.push(() => {
              fields._id = id;
              this.currentCollection.insert(fields);
            });
          },
          changed: (id: string, fields: fixmeAny) => {
            queue.push(() => {
              let mod = changedFieldsToModifier(fields);
              this.currentCollection.update(id, mod);
            });
          },
          removed: (id: string) => {
            queue.push(() => {
              this.currentCollection.remove(id);
            });
          }
        });
        for (let op of queue) {
          op();
        }
      }
    }
  }

  export type OldCellsDoc = {
    _id: string;  // not meaningful
    // These constitute the QFamilyId.
    column: NonRootColumnId;
    key: CellId;
    values: OSValue[];
    error: string;
  };

  class ServerTablespaceWrapper {
    ts: undefined | ServerTablespace;
    initializationError = false;
    lock = new Lock();
  }

  export class ServerTablespace extends Tablespace {

    private static instances = new Map<string, ServerTablespaceWrapper>();
    private static internalRunIn<R>(id: string, op: () => R): R {
      // No yields
      let w1 = ServerTablespace.instances.get(id);
      if (w1 == null) {
        w1 = new ServerTablespaceWrapper();
        ServerTablespace.instances.set(id, w1);
      }
      // End no yields
      const w = w1;
      return w.lock.run(() => {
        if (w.ts == null) {
          if (w.initializationError) {
            // Without this check, we'd typically get an error about creating two
            // Mongo.Collections with the same name, which is less clear.
            throw new Error(`Initialization of tablespace [${id}] previously failed.  ` +
              `Restart the server to try again.`);
          }
          try {
            // May yield!  We've already stored `w` in `instances` so if another
            // fiber asks for the same tablespace while we yield, it will wait on
            // the lock.
            w.ts = new ServerTablespace(id);
          } catch (e) {
            w.initializationError = true;
            throw e;
          }
        }
        return ServerTablespace.currentEnvVar.withValue(w.ts, op);
      });
    }

    // If this gets out of hand, we can create a ServerTablespaceHandle class
    // that provides access to these operations but not getCollection.
    public static runIn<R>(id: string, op: () => R): R {
      return ServerTablespace.internalRunIn(id, () => $$server.runInTransaction(op, true));
    }
    public static undoIn(id: string): void {
      ServerTablespace.internalRunIn(id, () => $$server.undo());
    }
    public static redoIn(id: string): void {
      ServerTablespace.internalRunIn(id, () => $$server.redo());
    }

    // http://stackoverflow.com/a/25282455
    private static currentEnvVar = new Meteor.EnvironmentVariable<ServerTablespace>();
    public static getCurrent() { return assertNotNull(ServerTablespace.currentEnvVar.get()); }

    // Public for collection dump route only.
    primaryCollectionNames!: SheetCollectionName[];
    // XXX These types are a bit of a lie: members are briefly undefined.
    private origCollections = <{[N in SheetCollectionName]: Mongo.Collection<SheetCollectionTypes[N]>}>{};
    private collectionHandlers = <{[N in SheetCollectionName]: ServerCollectionHandler<SheetCollectionTypes[N]>}>{};
    public getCollection(name: SheetCollectionName): SheetCollection<fixmeAny> {
      // No need to assert(this.initialized): there should be no way for a
      // caller to get access to an uninitialized ServerTablespace.
      return this.collectionHandlers[name].currentCollection;
    }
    public getDocument<N extends SheetCollectionName>(collectionName: N, id: string):
      undefined | SheetCollectionTypes[N] {
      // TypeScript 3.5 changed the handling of indexed accesses to try to cut
      // down on some unsoundness, which is a laudable goal, but unfortunately
      // it just made a mess.  Cast to tell TypeScript what it should know by
      // simple following of property types, at least if we could declare N as a
      // singleton type. ~ 2019-08-27
      return <undefined | SheetCollectionTypes[N]>this.collectionHandlers[collectionName].cache.get(id);
    }

    // Only for loadDumpIfExists and Model.repair.
    public oldCellsCollection!: Mongo.Collection<OldCellsDoc>;
    private static oldCellsCollectionSpec = new PerTablespaceCollectionSpec<OldCellsDoc>("cells");

    private txPendingCollection =
      Tablespace.txPendingCollectionSpec.qualify(this.id).makeLocalCollection();

    // For strictNullChecks.  I don't see an obvious better way. ~ Matt 2018-08-02
    private model_: undefined | Model;
    public get model() { return fixmeAssertNotNull(this.model_); }

    getAppName() {
      let match = /(?:^|\.)([^.]+)$/.exec(this.id);
      return match ? match[1] : null;
    }
    private constructor(id: string) {
      super(id);
      ServerTablespace.currentEnvVar.withValue(this, () => {
        let appName = this.getAppName();

        // "Primary" collections are persistent, get undo history saved, and get
        // dumped and loaded from dumps.  "Derived" collections are in-memory,
        // do not get undo history saved, and do not get dumped or loaded from
        // dumps, on the assumption that they are just recomputed as needed.
        // Both kinds of collections are (transactionally) published to the
        // client, which doesn't care about the distinction.
        this.primaryCollectionNames = [
          SheetCollections.Columns, SheetCollections.StateFamilies, SheetCollections.Views];
        let derivedCollectionNames: SheetCollectionName[] = [
          SheetCollections.AllFamilies, SheetCollections.Procedures, SheetCollections.LayoutsHardcodedMeta];

        // Procedures was previously persistent even though it was cleared on
        // server startup.  Delete it to clean up.
        sheetCollectionSpecs[SheetCollections.Procedures].qualify(this.id).makeMongoCollection().dropIfExists();
        // Layouts is derived (and we clean up an existing persistent
        // collection) if there are hard-coded layouts, otherwise primary.
        if (ServerAppHelpers.hasLayouts(appName)) {
          sheetCollectionSpecs[SheetCollections.Layouts].qualify(this.id).makeMongoCollection().dropIfExists();
          derivedCollectionNames.push(SheetCollections.Layouts);
        } else {
          this.primaryCollectionNames.push(SheetCollections.Layouts);
        }

        // Create primary collections
        for (let cn of this.primaryCollectionNames) {
          // TypeScript 3.5 is reasonable to ding us here, because we haven't
          // declared the type of `cn` to be a singleton type variable bound on
          // each iteration of the loop.  So add a cast. ~ Matt 2019-08-27
          this.origCollections[cn] =
            <Mongo.Collection<fixmeAny>>sheetCollectionSpecs[cn].qualify(this.id).makeMongoCollection();
        }
        this.oldCellsCollection =
          ServerTablespace.oldCellsCollectionSpec.qualify(this.id).makeMongoCollection();
        this.createTxCursors();

        // Load a dump if appropriate.
        //
        // For compatibility with old mongoexport-based dumps, which may contain
        // serialized EJSON objects with keys prefixed with "EJSON" (a special
        // encoding that is recognized only by the Meteor Mongo driver), load
        // the dump into the real database before initializing the
        // SynchronousObserveCollections (which will read the data back,
        // decoding the special encoding).
        //
        // Careful, Columns is not defined yet.
        if (appName != null && this.origCollections[SheetCollections.Columns].findOne(rootColumnId) == null) {
          this.loadDumpIfExists(appName);
        }

        // Wrap primary collections in SynchronousObserveCollections.
        for (let cn of this.primaryCollectionNames) {
          let committedCollection = new SynchronousObserveCollection<fixmeAny>(this.origCollections[cn]);
          this.collectionHandlers[cn] = new ServerCollectionHandler<fixmeAny>(
            committedCollection, true, committedCollection._docs, sheetCollectionDeepFreezeFuncs[cn]);
        }
        // Set up derived collections.
        for (let cn of derivedCollectionNames) {
          let committedCollection = sheetCollectionSpecs[cn].qualify(this.id).makeLocalCollection();
          this.collectionHandlers[cn] = new ServerCollectionHandler<fixmeAny>(
            committedCollection, false, committedCollection._collection._docs, sheetCollectionDeepFreezeFuncs[cn]);
        }

        // Now we are ready to run normal code against Columns, StateFamilies, etc.

        console.log(`creating model of [${this.id}]`);

        // Various things are slow if we don't defer evaluation and commit to MongoDB.
        this.runInTransaction(() => {
          LayoutsHardcodedMeta.insert({
            _id: layoutsHardcodedMetaDocId,
            layoutsHardcoded: this.layoutsAreHardcoded()
          });

          this.formulaEngine_ = new FormulaEngine();
          this.model_ = new Model();

          // Reload procedures defined with ServerAppHelpers.procedures on every
          // server startup to provide a reasonable development cycle: after
          // saving, the server automatically restarts and the procedure is there.
          // For consistency, we won't keep any procedures other than the ones
          // defined with ServerAppHelpers.procedures unless/until a use case
          // arises.
          ServerAppHelpers.loadProcedures(appName);
        }, false);
        console.log(`Tablespace [${this.id}] successfully initialized.`);
      });
    }

    private loadDumpIfExists(appName: string) {
      // TO MAKE A DUMP:
      // tests/dev-tools/mkdump APPNAME
      try {
        // Since we currently use RelsheetsServer.compile for procedures, we don't
        // support dumps for procedures.
        //
        // We assume that each dump file contains data in the correct format.
        // There's no benefit to specifying the collection types here.
        // tslint:disable-next-line:no-any
        let collsAndNames: [SheetCollection<any>, string][] =
          [[this.oldCellsCollection, ServerTablespace.oldCellsCollectionSpec.name]];
        for (let name of this.primaryCollectionNames) {
          collsAndNames.push([this.origCollections[name], name]);
        }
        for (let [coll, collName] of collsAndNames) {
          let dump: string;
          try {
            dump = Assets.getText(`dump/${appName}_${collName}.json`);
            console.log(`reading dump/${appName}_${collName}.json`);
          } catch (e) {
            // Assume there's no dump to load for this appName.  XXX: Be stricter?
            continue;
          }
          // Not JSON.parse: if we're loading a new-style dump, we'd try to
          // insert {$type: ...} into the database, which would fail because the
          // driver only encodes actual EJSON objects.
          let docs = EJSON.parse(dump);
          for (let doc of docs) {
            coll.insert(doc);
          }
        }
      } catch (e) {
        console.log(`Failed to load dump for ${appName} into sheet '${$$.id}':`, e.stack);
      }
    }

    // If useTx = false, then don't use babrahams:transactions (in particular,
    // don't discard redo history), but still defer evaluation and publishing
    // and push down using a diff.
    private runInTransaction<R>(op: () => R, useTx: boolean): R {
      for (let cn of sheetCollectionNames)
        this.collectionHandlers[cn].startTransaction();
      if (this.model_)
        this.model_.startDeferringEvaluation();
      let result: R;
      try {
        result = op();
        // The first transaction is initialization, so the model should exist by now.
        this.model.performDeferredEvaluation();
      } catch (e) {
        for (let cn of sheetCollectionNames)
          this.collectionHandlers[cn].clearTransaction();
        if (this.model_)
          this.model_.notifyUnevaluatedChangesRolledBack();
        throw e;
      }
      // XXX: Once we have real security, this may leak that a transaction
      // occurred that affected documents the client can't see.  We'll want to
      // have a separate "pending" publisher for each client.
      this.txPendingCollection.insert({_id: txPendingDocId});
      if (useTx) {
        tx.purgeRedo({context: this.transactionContext()});
        tx.start(/*description*/ undefined, {context: this.transactionContext()});
      }
      for (let cn of sheetCollectionNames)
        this.collectionHandlers[cn].pushDownTransaction(useTx);
      if (useTx) {
        if (_.isEmpty(tx._instance()._items)) {
          /* Don't record empty transactions.  The user may think something is
          * broken if they press undo or redo and nothing happens, unless we add
          * support for displaying a meaningful transaction description so they
          * understand what is going on (arguably a good feature anyway but not
          * important now).
          *
          * This introduces an inconsistency in behavior: an operation that
          * coincidentally produces no net diff will generate no history entry,
          * so an undo command will undo the /previous/ operation.  I don't
          * think this is a big problem because we currently intend the
          * undo/redo feature for interactive use where the user will realize
          * what happened and just redo, not as an API where people would write
          * code that relies on the behavior being consistent.
          *
          * ~ Matt 2017-01-27
          *
          * View object changes currently rely on empty transactions not being
          * recorded, so if we change that, we may want a special flag for view
          * object changes.  It gets murky when the user edits a computed field
          * of a view object that may or may not be designed to ever propagate
          * to the master data set. ~ Matt 2018-07-14
          */
          tx.rollback();
        } else {
          tx.commit();
        }
      }
      for (let cn of sheetCollectionNames)
        this.collectionHandlers[cn].clearTransaction();
      this.txPendingCollection.remove(txPendingDocId);
      return result;
    }

    // Currently, these do nothing if there is no transaction to undo or redo.  OK?
    private undo() {
      this.runInTransaction(() => {
        tx.undo({context: this.transactionContext()});
        this.model.notifyResetToUnknownState();
      }, false);
    }
    private redo() {
      this.runInTransaction(() => {
        tx.redo({context: this.transactionContext()});
        this.model.notifyResetToUnknownState();
      }, false);
    }

    viewObjectRequests = new Map<ViewObjectId, ViewObjectRequestQueue>();

    private pumpViewObject(serverViewObjectId: ViewObjectId, rq: ViewObjectRequestQueue, removeOld: boolean) {
      if (rq.current == null) {
        for (let nextCtx of rq.all) {
          rq.current = nextCtx;
          try {
            $$server.runInTransaction(() => {
              if (removeOld) {
                // If this throws an exception, we report it against the new
                // request: weird but I'm not sure what else to do.
                // ~ Matt 2018-09-05
                $$server.model.removeViewObject(serverViewObjectId);
              }
              this.model.addViewObject(serverViewObjectId, nextCtx.req);
            }, false);
            nextCtx.addedViewObject = true;
            nextCtx.publisher.ready();
          } catch (e) {
            // error calls onStop, which will try to take the lock.  If we do it
            // inline, we get deadlock.  Deferring it seems to be a simple, safe
            // solution.
            Meteor.defer(() => nextCtx.publisher.error(e));
          }
          return;
        }
        // There must have been no more requests.
        if (removeOld) {
          $$server.runInTransaction(() => {
            $$server.model.removeViewObject(serverViewObjectId);
          }, false);
        }
        this.viewObjectRequests.delete(serverViewObjectId);
      }
    }

    public static definePublications() {
      Tablespace.tablespacePub.publish((id) => ServerTablespace.internalRunIn(id, () => {
        let cursors: Mongo.Cursor<{}>[] = sheetCollectionNames.map((cn) => {
          // Yuck...
          let c: {find(): Mongo.Cursor<{}>} = $$server.collectionHandlers[cn].committedCollection;
          return c.find();
        });
        cursors.push($$server.txPendingCollection.find());
        return cursors;
      }));
      // Meteor doesn't support a single subscription containing multiple
      // cursors from the Transactions collection, so for symmetry, define a
      // separate publication for each cursor.
      Tablespace.txUndoablePub.publish((id) =>
        ServerTablespace.internalRunIn(id, () => $$server.txUndoableCursor));
      Tablespace.txRedoablePub.publish((id) =>
        ServerTablespace.internalRunIn(id, () => $$server.txRedoableCursor));

      Tablespace.viewObjectPub.publish(function(tsId, clientViewObjectId, req) {
        ServerTablespace.internalRunIn(tsId, () => {
          staticCheck(/^[A-Za-z0-9]*$/.test(clientViewObjectId),
            () => "Invalid characters in client view object ID");
          let serverViewObjectId = this.connection.id + ":" + clientViewObjectId;

          let rq_ = $$server.viewObjectRequests.get(serverViewObjectId);
          if (rq_ == null) {
            rq_ = new ViewObjectRequestQueue();
            $$server.viewObjectRequests.set(serverViewObjectId, rq_);
          }
          const rq = rq_;
          let ctx: ViewObjectRequestContext = {req, publisher: this, addedViewObject: false};
          rq.all.add(ctx);
          $$server.pumpViewObject(serverViewObjectId, rq, false);
          // Defer onStop to avoid deadlock if the subscription is already
          // stopped and onStop runs the callback synchronously and it tries to
          // take the lock.
          Meteor.defer(() => this.onStop(() => ServerTablespace.internalRunIn(tsId, () => {
            rq.all.delete(ctx);
            if (rq.current == ctx) {
              rq.current = null;
              $$server.pumpViewObject(serverViewObjectId, rq, ctx.addedViewObject);
            }
          })));
        });
      });
    }

    public layoutsAreHardcoded() {
      return ServerAppHelpers.hasLayouts(this.getAppName());
    }
  }

  class ViewObjectRequestQueue {
    current: null | ViewObjectRequestContext = null;
    all = new Set<ViewObjectRequestContext>();
  }
  type ViewObjectRequestContext = {
    req: ViewObjectRequest;
    publisher: Subscription;
    addedViewObject: boolean;
  };

  ServerTablespace.definePublications();

  Tablespace.getCurrent = ServerTablespace.getCurrent;

  // Shortcut variable available to server-only files that refers to the same
  // object as $$ but provides access to server-only APIs.
  export declare var $$server: ServerTablespace;
  Object.defineProperty(Objsheets, "$$server", {
    get: () => ServerTablespace.getCurrent()
  });

  /**
   * Define a Meteor method using "impl", wrapped to run in the tablespace given
   * as the first parameter and defer evaluation.  (The tablespace is still
   * passed to "impl".)
   *
   * As of TypeScript 2.8.1, this seems to be the best interface we can provide.
   * If we try to use a wrapModelMethod function like this:
   *
   *   MeteorMethods.foo.define(wrapModelMethod((ts, foo) => ...))
   *
   * and we write overload signatures for wrapModelMethod with different numbers
   * of arguments, then TypeScript isn't able to choose the correct one to
   * contextually type the lambda.  I looked into filing a bug but it's probably
   * not worth it.
   *
   * If we let defineModelMethod take the `impl` parameter directly, then while
   * a developer is writing the body of the lambda, certain kinds of incomplete
   * syntax cause the type inference to fail and the lambda parameters to lose
   * their types so that code completion does not work.  By taking the `method`
   * parameter and returning a function that takes the lambda, we finalize the
   * type inference regardless of the validity of the lambda.
   *
   * Hopefully https://github.com/Microsoft/TypeScript/issues/5453 will
   * eventually let us use the wrapModelMethod design.
   *
   * ~ Matt 2018-04-02
   */
  export function defineModelMethod
    <R extends (EJSONable | void)>
    (method: MeteorMethod1<TablespaceId, R>): (impl: (this: Meteor.MethodInvocation) => R) => void;
  export function defineModelMethod
    <A2 extends EJSONable, R extends (EJSONable | void)>
    (method: MeteorMethod2<TablespaceId, A2, R>): (impl: (this: Meteor.MethodInvocation, a2: A2) => R) => void;
  export function defineModelMethod
    <A2 extends EJSONable, A3 extends EJSONable, R extends (EJSONable | void)>
    (method: MeteorMethod3<TablespaceId, A2, A3, R>):
    (impl: (this: Meteor.MethodInvocation, a2: A2, a3: A3) => R) => void;
  export function defineModelMethod
    <A2 extends EJSONable, A3 extends EJSONable, A4 extends EJSONable, R extends (EJSONable | void)>
    (method: MeteorMethod4<TablespaceId, A2, A3, A4, R>):
    (impl: (this: Meteor.MethodInvocation, a2: A2, a3: A3, a4: A4) => R) => void;
  export function defineModelMethod
    <A2 extends EJSONable, A3 extends EJSONable, A4 extends EJSONable, A5 extends EJSONable,
    R extends (EJSONable | void)>
    (method: MeteorMethod5<TablespaceId, A2, A3, A4, A5, R>):
    (impl: (this: Meteor.MethodInvocation, a2: A2, a3: A3, a4: A4, a5: A5) => R) => void;
  export function defineModelMethod(method: MeteorMethodBase) {
    return (impl: Function) => defineModelMethodUntyped(method, impl);
  }
  export function defineModelMethodUntyped(method: MeteorMethodBase, impl: Function) {
    // Untyped code; type-safe overload signatures.
    // tslint:disable-next-line:no-any
    method.define(function(this: Meteor.MethodInvocation, tsId: TablespaceId, ...args: any[]) {
      try {
        return ServerTablespace.runIn(tsId, () => impl.call(this, ...args));
      } catch (e) {
        if (e instanceof StaticError || e instanceof RuntimeError) {
          // XXX: Once we have real security, don't do this for unprivileged
          // users because it may leak private information.
          throw new Meteor.Error(e.constructor.name, e.message);
        } else {
          throw e;
        }
      }
    });
  }

  export namespace MeteorMethods {
    Meteor.startup(() => {  // load order for defineModelMethod and MeteorMethods.*
      undo.define((tsid) => ServerTablespace.undoIn(tsid));
      redo.define((tsid) => ServerTablespace.redoIn(tsid));
    });
  }

  Router.route("/:sheet/dump/:collection", function(this: fixmeAny) {
    let res = this.response;
    ServerTablespace.runIn(this.params.sheet, () => {
      let collectionName = this.params.collection;
      if ($$server.primaryCollectionNames.indexOf(collectionName) < 0) {
        httpRespondWithError(res, 404, "Not a primary collection.  Not producing a dump file.");
        return;
      }
      let docs = $$.getCollection(this.params.collection).find({}, {sort: {_id: 1}}).fetch();
      // For neatness and to avoid a diff when adding the first hard-coded
      // layout to a sheet, don't produce dump files for empty collections.
      if (docs.length == 0) {
        httpRespondWithError(res, 404, "Collection is empty.  Not producing a dump file.");
      } else {
        // Add a trailing newline to avoid triggering edge cases in editors, version
        // control tools, etc.
        let dump = EJSON.stringify(docs) + "\n";
        res.setHeader("Content-Type", "application/json");
        res.end(dump);
      }
    });
  }, <fixmeAny>{where: "server"});

  // Alias to save typing in the server shell.
  export let runIn = ServerTablespace.runIn;
}
