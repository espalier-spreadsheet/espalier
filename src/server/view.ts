// Thanks TypeScript handbook
// (https://www.typescriptlang.org/docs/handbook/declaration-merging.html#merging-namespaces-with-classes)
// for this clue!  We need "declare" to avoid the "different file" error.  And
// if we put this inside the main "namespace Objsheets" block, the compiler
// seems to mistranslate the references to "View" in the rest of that block.
// TODO: file a bug about that (not likely to get to it). ~ Matt 2017-01-24
declare namespace Objsheets {
  namespace View {
    let removeColumnFromAll: (columnId: ColumnId) => void;
  }
}

namespace Objsheets {

  export interface View {
    addColumn(columnId: ColumnId, own: boolean): void;
    removeColumn(columnId: ColumnId): void;
    reorderColumn(columnId: ColumnId, newIndex: number): void;
    renameTo(newId: string): void;
  }

  Meteor.startup(() => {  // load order for View

    View.prototype.addColumn = function(columnId, own = false) {
      let def = this.def();
      let col = getColumn(columnId);
      let parentId = col != null ? col.parent : null;
      if (parentId != null) {
        let layoutTree = def.layout;
        let layoutSubtree = layoutTree.find(parentId);
        if (layoutSubtree != null) {
          layoutSubtree.subtrees.push(new Tree(columnId));
          Views.upsert(fixmeAssertNotNull(this.id), def);
        }
        if (own) {
          Columns.update(columnId, {
            $set: {
              view: this.id
            }
          });
        }
      }
    };

    View.prototype.removeColumn = function(columnId) {
      let def = this.def();
      def.layout = fixmeAssertNotNull(def.layout.filter((x) => x !== columnId));
      Views.update(fixmeAssertNotNull(this.id), def);
    };

    View.prototype.reorderColumn = function(columnId, newIndex) {
      let def = this.def();
      let col = getColumn(columnId);
      let parentId = col != null ? col.parent : null;
      if (parentId != null) {
        let layoutTree = def.layout;
        let layoutSubtreeParent = layoutTree.find(parentId);
        let layoutSubtreeChild = layoutTree.find(columnId);
        if ((layoutSubtreeParent != null) && (layoutSubtreeChild != null)) {
          layoutSubtreeParent.subtrees = layoutSubtreeParent.subtrees.filter((x) => x.root !== columnId);
          layoutSubtreeParent.subtrees.splice(newIndex, 0, layoutSubtreeChild);
          Views.update(fixmeAssertNotNull(this.id), {
            $set: {
              layout: layoutTree
            }
          });
        }
      }
    };

    View.removeColumnFromAll = function(columnId) {
      Views.find().forEach((view) => {
        if (view.layout.find(columnId) != null) {
          new View(fixmeAssertNotNull(view._id)).removeColumn(columnId);
        }
      });
    };

    // I typed this sequence of commands in the shell once.  Putting it here so
    // we can call it from the shell if we have a need. ~ Matt 2017-02-16
    View.prototype.renameTo = function(newId) {
      let def: ViewDef & {_id?: string} = this.def();
      def._id = newId;
      Views.insert(def);
      Views.remove(fixmeAssertNotNull(this.id));
      Columns.update({view: this.id}, {$set: {view: newId}}, {multi: true});
    };
  });

  export namespace MeteorMethods {
    Meteor.startup(() => {  // load order for defineModelMethod and MeteorMethods.*
      defineModelMethod(viewReorderColumn)((viewId, columnId, newIndex) =>
        new View(viewId).reorderColumn(columnId, newIndex));
    });
  }

}
