namespace Objsheets {

  export type ViewDef = {
    layout: Tree<ColumnId>;
  };
  export type ViewDoc = Flatten<ViewDef & {_id: string}>;

  // Server write API is in server/view.ts.  (The only write called directly
  // from the client is reorderColumn, and it's a direct Meteor method call; I
  // don't see value in providing a wrapper in this case. ~ Matt 2017-01-24)
  export class View {
    // `id = null` refers to the default view.
    constructor(public id: null | string) {}

    public def(): ViewDef {
      return this.id != null ? Views.findOne(this.id) || {
        layout: new Tree<ColumnId>(rootColumnId)
      } : {
        layout: View.rootLayout()
      };
    }

    public static rootLayout() {
      return fixmeAssertNotNull(this.drillDown(rootColumnId).filter((x) => this.ownerOf(x) == null));
    }

    public static drillDown(startingColumnId: ColumnId): Tree<ColumnId> {
      let startingCol = Columns.findOne(startingColumnId);
      let children = (startingCol != null ? startingCol.children : null) || [];
      return new Tree(startingColumnId, children.map((child) => this.drillDown(child)));
    }

    public static ownerOf(columnId: ColumnId) {
      let col = Columns.findOne(columnId);
      return col != null ? col.view : null;
    }
  }

  export namespace MeteorMethods {
    export let viewReorderColumn = new MeteorMethod4<TablespaceId, string, ColumnId, number, void>("viewReorderColumn");
  }

}
