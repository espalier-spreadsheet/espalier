# Info for HTML authors

To use:

- Include `mavo.css` and `mavo.js` in your page. If you need to support older browsers, include `mavo.es5.js` instead.
- Some (pretty crappy atm, but better than nothing) docs are here: http://mavo.io/docs
- In lieu of docs, the demos could help too http://mavo.io/docs
- The repo for the http://mavo.io website is https://github.com/leaverou/mavo-website
- The contents of this repo can be accessed over HTTP at http://dev.mavo.io

For contributing, please look at [CONTRIBUTING.md](https://github.com/LeaVerou/mavo/blob/master/CONTRIBUTING.md).
