// I think this is substantial enough to go in its own file.
//
// TODO: Consider submitting to one of the major typing registries?  Is it
// better than nothing even if far from complete?
// ~ Matt 2017-03-24

declare class Mavo {
  // ...
  id: string;
  render(data: fixmeAny): void;
  static init(): void;
}

declare namespace Mavo {

  class Backend {
    url: string;
    mavo: Mavo;
    permissions: fixmeAny;

    constructor(url: string, mavo: Mavo);

    getFile(): fixmeAny;

    static register(cls: SpecificBackendClass): void;
  }

  // "Specific", unlike Backend itself, which doesn't have a `test` method.
  interface SpecificBackendClass {
    // Looks like this is all that is actually used, and there isn't a stronger
    // way to say "subclass of Backend".
    // http://stackoverflow.com/questions/34800184/how-write-a-type-whose-members-are-subclasses-of-a-given-class-in-typescript
    test(url: string): boolean;
    new(url: string, mavo: Mavo): Backend;
  }

}
